var FPDBulkVariations = function (a) {
	"use strict";
	var b = this,
		c = $(a.mainOptions.bulkVariationsPlacement).addClass("fpd-bulk-variations fpd-container"),
		d = a.mainOptions.bulkVariations,
		e = "";
	this.getOrderVariations = function () {
		var a = [];
		return c.find(".fpd-row").each(function (b, c) {
			var d = $(c),
				e = {};
			d.children(".fpd-select-col").each(function (b, c) {
				var d = $(c).children("select");
				null == d.val() && (a = !1, d.addClass("fpd-error")), e[d.attr("name")] = d.val()
			}), a !== !1 && a.push({
				variation: e,
				quantity: parseInt(d.find(".fpd-quantity").val())
			})
		}), a
	}, this.setup = function (a) {
		"object" == typeof a && (c.children(".fpd-variations-list").empty(), a.forEach(function (a) {
			c.children(".fpd-variations-list").append(e);
			var b = c.children(".fpd-variations-list").children(".fpd-row:last");
			Object.keys(a.variation).forEach(function (c) {
				b.find('select[name="' + c + '"]').val(a.variation[c])
			}), b.find(".fpd-quantity").val(a.quantity)
		})), f()
	};
	var f = function () {
			var b = 0;
			c.find(".fpd-quantity").each(function () {
				b += Number(this.value)
			}), a.setOrderQuantity(Number(b))
		},
		g = function () {
			if (a.$container.on("getOrder", function () {
					a._order.bulkVariations = b.getOrderVariations()
				}), "object" == typeof d) {
				var g = Object.keys(d);
				e += '<div class="fpd-row">';
				for (var h = 0; h < g.length; ++h) {
					var i = g[h],
						j = d[i];
					e += '<div class="fpd-select-col"><select name="' + i + '"><option value="" disabled selected>' + i + "</option>";
					for (var k = 0; k < j.length; ++k) e += '<option value="' + j[k] + '">' + j[k] + "</option>";
					e += "</select></div>"
				}
				e += '<div><input type="number" class="fpd-quantity" step="1" min="1" value="1" /></div>', e += '<div class="fpd-remove-row"><span class="fpd-icon-close"></span></div>', e += "</div>"
			}
			c.append('<div class="fpd-variations-list">' + e + "</div>").prepend('<div class="fpd-clearfix"><span class="fpd-title fpd-left">' + a.getTranslation("plus", "bulk_add_variations_title") + '</span><span class="fpd-add-row fpd-btn fpd-right">' + a.getTranslation("plus", "bulk_add_variations_add") + "</span></div>"), c.on("click", ".fpd-add-row", function () {
				c.children(".fpd-variations-list").append(e), f()
			}), c.on("click", ".fpd-remove-row", function () {
				var a = $(this).parents(".fpd-row:first");
				a.siblings(".fpd-row").length > 0 && (a.remove(), f())
			}), c.on("change", "select", function () {
				var a = $(this);
				a.removeClass("fpd-error")
			}), c.on("change", ".fpd-quantity", function () {
				var a = $(this);
				this.value < Number(a.attr("min")) && (this.value = Number(a.attr("min"))), "" == this.value && (this.value = 1), f()
			}), f()
		};
	g()
},
FPDColorSelection = function (a) {
	"use strict";
	var b = null,
		c = !1,
		d = a.mainOptions.colorSelectionPlacement,
		e = function (e) {
			if (FPDUtil.elementHasColorSelection(e)) {
				var f = $('<div class="fpd-cs-item" data-id="' + e.id + '"><div class="fpd-title">' + e.title + '</div><div class="fpd-colors"></div></div>');
				b.append(f);
				var g = FPDUtil.elementAvailableColors(e, a);
				if (e.type == FPDPathGroupName && e.getObjects().length > 1) {
					for (var h = 0; h < g.length; ++h) f.children(".fpd-colors").append('<input type="text" value="' + g[h] + '" />');
					f.find(".fpd-colors input").spectrum({
						showPaletteOnly: $.isArray(e.colors),
						preferredFormat: "hex",
						showInput: !0,
						showInitial: !0,
						showButtons: !1,
						showPalette: a.mainOptions.colorPickerPalette && a.mainOptions.colorPickerPalette.length > 0,
						palette: $.isArray(e.colors) ? e.colors : a.mainOptions.colorPickerPalette,
						show: function (b) {
							var c = $(this).parent(".fpd-colors"),
								d = FPDUtil.changePathColor(e, c.children("input").index(this), b);
							FPDUtil.spectrumColorNames($(this).spectrum("container"), a), e._tempFill = d
						},
						move: function (b) {
							var c = $(this).parent(".fpd-colors"),
								d = FPDUtil.changePathColor(e, c.children("input").index(this), b);
							a.currentViewInstance.changeColor(e, d)
						},
						change: function (b) {
							var c = $(this).parent(".fpd-colors"),
								d = FPDUtil.changePathColor(e, c.find("input").index(this), b);
							$(document).unbind("click.spectrum"), a.currentViewInstance.setElementParameters({
								fill: d
							}, e)
						}
					})
				} else if (1 != g && (g.length > 1 || e.type == FPDPathGroupName && 1 === e.getObjects().length)) {
					var i = !1;
					"dropdown" == a.mainOptions.colorSelectionDisplayType && -1 == d.indexOf("inside-") && (f.addClass("fpd-dropdown").children(".fpd-title").append('<span class="fpd-icon-arrow-dropdown"></span>'), i = !0);
					for (var h = 0; h < g.length; ++h) {
						var j = g[h],
							k = a.mainOptions.hexNames[j.replace("#", "").toLowerCase()];
						k = k ? k : j, "string" == typeof j && 4 == j.length && (j += j.substr(1, j.length)), f.find(".fpd-colors").append('<div data-color="' + j + '" style="background-color: ' + j + '" class="fpd-item fpd-tooltip" title="' + k + '"><div class="fpd-label">' + (i ? k : j) + "</div></div>"), f.find(".fpd-item:last").click(function () {
							var b = tinycolor($(this).css("backgroundColor"));
							a.deselectElement(), a.currentViewInstance.currentUploadZone = null;
							var c = b.toHexString();
							e.type == FPDPathGroupName && (c = FPDUtil.changePathColor(e, 0, b)), a.viewInstances[0].setElementParameters({
								fill: c
							}, e)
						})
					}
					FPDUtil.updateTooltip(f)
				} else f.children(".fpd-colors").append('<input type="text" value="' + e.colors[0] + '" />'), f.find("input").spectrum({
					showButtons: !1,
					preferredFormat: "hex",
					showInput: !0,
					showInitial: !0,
					showPalette: a.mainOptions.colorPickerPalette && a.mainOptions.colorPickerPalette.length > 0,
					palette: a.mainOptions.colorPickerPalette,
					show: function (b) {
						FPDUtil.spectrumColorNames($(this).spectrum("container"), a), e._tempFill = b.toHexString()
					},
					move: function (b) {
						(c === !1 || "png" !== FPDUtil.elementIsColorizable(e)) && a.viewInstances[0].changeColor(e, b.toHexString())
					},
					change: function (b) {
						$(document).unbind("click.spectrum"), a.viewInstances[0].setElementParameters({
							fill: b.toHexString()
						}, e)
					}
				}).on("dragstart.spectrum", function () {
					c = !0
				}).on("dragstop.spectrum", function (b, d) {
					c = !1, a.viewInstances[0].changeColor(e, d.toHexString())
				})
			}
		},
		f = function () {
			a.$container.on("productCreate", function () {
				var c = a.getElements(0).filter(function (a) {
					return a.showInColorSelection
				});
				c.length > 0 ? (null == b && (b = -1 !== d.indexOf("inside-") ? a.$mainWrapper.append('<div class="fpd-color-selection fpd-inside-main fpd-clearfix fpd-' + d + '"></div>').children(".fpd-color-selection") : $(d).addClass("fpd-color-selection fpd-clearfix fpd-custom-pos")), b.find("input").spectrum("destroy"), b.empty(), -1 !== d.indexOf("inside-") && (c = [c[0]]), c.forEach(function (a) {
					e(a)
				}), b.off().on("click", ".fpd-dropdown > .fpd-title", function () {
					$(this).next(".fpd-colors").toggleClass("fpd-active").parent(".fpd-dropdown").siblings(".fpd-dropdown").children(".fpd-colors").removeClass("fpd-active")
				}), b.show()) : b && b.hide()
			}).on("elementRemove", function (a, c) {
				c.showInColorSelection && b.children('[data-id="' + c.id + '"]').find("input").spectrum("destroy").remove()
			}).on("elementColorChange", function (a, c, d) {
				b && "string" == typeof d && b.children('[data-id="' + c.id + '"]').find('.fpd-colors > [data-color="' + d + '"]').addClass("fpd-active").siblings().removeClass("fpd-active")
			})
		};
	f()
},
FPDDrawingModule = function (a, b) {
	"use strict";
	this.drawCanvas = null;
	var c = this,
		d = "#000000",
		e = function (a, b) {
			if ("vLine" === b) {
				var c = new fabric.PatternBrush(a);
				return c.getPatternSrc = function () {
					var a = fabric.document.createElement("canvas");
					a.width = a.height = 10;
					var b = a.getContext("2d");
					return b.strokeStyle = this.color, b.lineWidth = 5, b.beginPath(), b.moveTo(0, 5), b.lineTo(10, 5), b.closePath(), b.stroke(), a
				}, c
			}
			if ("hLine" === b) {
				var d = new fabric.PatternBrush(a);
				return d.getPatternSrc = function () {
					var a = fabric.document.createElement("canvas");
					a.width = a.height = 10;
					var b = a.getContext("2d");
					return b.strokeStyle = this.color, b.lineWidth = 5, b.beginPath(), b.moveTo(5, 0), b.lineTo(5, 10), b.closePath(), b.stroke(), a
				}, d
			}
		},
		f = function () {
			var f = b.parents(".fpd-draggable-dialog:first").length > 0 ? b.parents(".fpd-draggable-dialog:first").width() : b.parent(".fpd-content").width();
			f -= 2 * parseInt(b.css("paddingLeft")), c.drawCanvas = new fabric.Canvas(b.find(".fpd-drawing-canvas").get(0), {
				containerClass: "fpd-drawing-container",
				isDrawingMode: !0,
				width: f,
				height: 150
			}), b.find(".fpd-drawing-brush-type .fpd-item").click(function (a) {
				a.stopPropagation();
				var f = $(this),
					g = f.parent().prevAll(".fpd-dropdown-current:first"),
					h = f.data("value");
				g.html(f.clone()).data("value", h), c.drawCanvas.freeDrawingBrush = "hline" === h ? e(c.drawCanvas, "hLine") : "vline" === h ? e(c.drawCanvas, "vLine") : new fabric[h + "Brush"](c.drawCanvas), c.drawCanvas.freeDrawingBrush && (c.drawCanvas.freeDrawingBrush.color = d, c.drawCanvas.freeDrawingBrush.width = b.find(".fpd-drawing-line-width").val()), f.parents(".fpd-dropdown:first").removeClass("fpd-active")
			}), b.find(".fpd-drawing-line-color").spectrum({
				color: d,
				showButtons: !1,
				preferredFormat: "hex",
				showInput: !0,
				showInitial: !0,
				showPalette: a.mainOptions.colorPickerPalette && a.mainOptions.colorPickerPalette.length > 0,
				palette: a.mainOptions.colorPickerPalette,
				move: function (a) {
					d = a.toHexString(), c.drawCanvas && (c.drawCanvas.freeDrawingBrush.color = d)
				},
				change: function (a) {
					d = a.toHexString(), c.drawCanvas && (c.drawCanvas.freeDrawingBrush.color = d)
				}
			}), b.find(".fpd-slider-range").rangeslider({
				polyfill: !1,
				rangeClass: "fpd-range-slider",
				disabledClass: "fpd-range-slider--disabled",
				horizontalClass: "fpd-range-slider--horizontal",
				verticalClass: "fpd-range-slider--vertical",
				fillClass: "fpd-range-slider__fill",
				handleClass: "fpd-range-slider__handle",
				onSlide: function (a, b) {
					this.$element.parent().prev(".fpd-slider-number").val(b).change()
				}
			}), b.find(".fpd-slider-number").change(function () {
				var a = $(this);
				this.value > Number(a.attr("max")) && (this.value = Number(a.attr("max"))), this.value < Number(a.attr("min")) && (this.value = Number(a.attr("min"))), a.next(".fpd-range-wrapper").children("input").val(this.value).rangeslider("update", !0, !1), a.hasClass("fpd-drawing-line-width") && c.drawCanvas && (c.drawCanvas.freeDrawingBrush.width = this.value)
			}), b.find(".fpd-clear-drawing").click(function () {
				c.drawCanvas.clear()
			}), b.find(".fpd-add-drawing").click(function () {
				var b = {
						autoCenter: !0,
						draggable: !0,
						resizable: !0,
						removable: !0,
						rotatable: !0,
						autoSelect: !0,
						colors: !1,
						patterns: !1,
						isCustom: !0
					},
					d = c.drawCanvas.toSVG({
						suppressPreamble: !0
					}).replace(/(?:\r\n|\r|\n)/g, "");
				a.addElement("image", d, (new Date).getTime(), $.extend({}, a.currentViewInstance.options.customImageParameters, b)), c.drawCanvas.clear()
			})
		};
	f()
},
FPDNamesNumbersModule = {
	setup: function (a, b) {
		var c = null;
		b.off().find(".fpd-list").empty();
		var d = function (b, c) {
				a.currentViewInstance.numberPlaceholder && "string" == typeof b && a.currentViewInstance.setElementParameters({
					text: b
				}, a.currentViewInstance.numberPlaceholder), a.currentViewInstance.textPlaceholder && "string" == typeof c && (a.mainOptions.disableTextEmojis && (c = c.replace(FPDEmojisRegex, ""), c = c.replace(String.fromCharCode(65039), "")), a.currentViewInstance.setElementParameters({
					text: c
				}, a.currentViewInstance.textPlaceholder)), a.currentViewInstance.stage.renderAll()
			},
			e = function (c, d, e) {
				c = "undefined" == typeof c ? "" : c, d = "undefined" == typeof d ? "" : d;
				var f = '<div class="fpd-row">';
				if (a.currentViewInstance.numberPlaceholder) {
					var g = "";
					Array.isArray(a.currentViewInstance.numberPlaceholder.numberPlaceholder) && (g = 'min="' + a.currentViewInstance.numberPlaceholder.numberPlaceholder[0] + '" max="' + a.currentViewInstance.numberPlaceholder.numberPlaceholder[1] + '" '), f += '<div class="fpd-number-col"><input type="number" placeholder="' + a.currentViewInstance.numberPlaceholder.originParams.text + '" class="fpd-number" value="' + c + '" ' + g + " /></div>"
				}
				if (a.currentViewInstance.textPlaceholder && (f += '<div class="fpd-name-col"><div><input type="text" placeholder="' + a.currentViewInstance.textPlaceholder.originParams.text + '" value="' + d + '" /></div></div>'), a.mainOptions.namesNumbersDropdown && a.mainOptions.namesNumbersDropdown.length > 0 || e) {
					for (var h = [e], i = a.mainOptions.namesNumbersDropdown.length > 0 ? a.mainOptions.namesNumbersDropdown : h, j = "", k = 0; k < i.length; ++k) selected = e === i[k] ? 'selected="selected"' : "", j += '<option value="' + i[k] + '" ' + selected + ">" + i[k] + "</option>";
					f += '<div class="fpd-select-col"><label><select>' + j + "</select></label></div>"
				}
				return f += '<div class="fpd-remove-col"><span><span class="fpd-icon-remove"></span></span></div></div></div>', b.find(".fpd-list").append(f), FPDUtil.createScrollbar(b.find(".fpd-scroll-area")), b.find(".fpd-list .fpd-row:last")
			};
		if (a.currentViewInstance.textPlaceholder || a.currentViewInstance.numberPlaceholder)
			if (b.children(".fpd-names-numbers-panel").toggleClass("fpd-disabled", !1), a.currentViewInstance.names_numbers && Array.isArray(a.currentViewInstance.names_numbers))
				for (var f = 0; f < a.currentViewInstance.names_numbers.length; ++f) {
					var g = a.currentViewInstance.names_numbers[f];
					e(g.number, g.name, g.select)
				} else e();
			else b.children(".fpd-names-numbers-panel").toggleClass("fpd-disabled", !0);
		b.on("click", ".fpd-remove-col", function () {
			var d = $(this).parents(".fpd-row:first");
			d.siblings(".fpd-row").length > 0 && (d.remove(), c && c.get(0) === d.get(0) && b.find(".fpd-row:first input:first").mouseup(), a.currentViewInstance.names_numbers = FPDNamesNumbersModule.getViewNamesNumbers(b), a.currentViewInstance.changePrice(a.currentViewInstance.options.namesNumbersEntryPrice, "-"))
		}), b.on("mouseup keyup", ".fpd-row input", function () {
			var b = $(this);
			if (c && c.get(0) !== b.parents(".fpd-row:first").get(0)) {
				var e = b.parents(".fpd-row:first");
				d(e.find(".fpd-number").val(), e.find(".fpd-name-col input").val())
			} else {
				var f = b.hasClass("fpd-number") ? a.currentViewInstance.numberPlaceholder.maxLength : a.currentViewInstance.textPlaceholder.maxLength;
				0 != f && this.value.length > f && (this.value = this.value.substr(0, f)), b.hasClass("fpd-number") ? (void 0 !== b.attr("min") && "" !== this.value && (this.value > Number(b.attr("max")) && (this.value = Number(b.attr("max"))), this.value < Number(b.attr("min")) && (this.value = Number(b.attr("min")))), d(this.value)) : d(!1, this.value)
			}
			c = b.parents(".fpd-row:first")
		}), b.on("click", ".fpd-btn", function () {
			var f = e();
			b.find(".fpd-list .fpd-row:last input:first").focus(), d(f.find(".fpd-number").attr("placeholder"), f.find(".fpd-name-col input").attr("placeholder")), a.currentViewInstance.names_numbers = FPDNamesNumbersModule.getViewNamesNumbers(b), a.currentViewInstance.changePrice(a.currentViewInstance.options.namesNumbersEntryPrice, "+"), c = f
		}), b.on("change", "input, select", function () {
			a.currentViewInstance.names_numbers = FPDNamesNumbersModule.getViewNamesNumbers(b)
		})
	},
	getViewNamesNumbers: function (a) {
		var b = [];
		return a.find(".fpd-list .fpd-row").each(function (a, c) {
			var d = $(c),
				e = {};
			d.children(".fpd-number-col").length > 0 && (e.number = d.find(".fpd-number").val()), d.children(".fpd-name-col").length > 0 && (e.name = d.find(".fpd-name-col input").val()), d.children(".fpd-select-col").length > 0 && (e.select = d.find(".fpd-select-col select").val()), b.push(e)
		}), b
	}
},
FPDDynamicViews = function (a, b) {
	"use strict";

	function c(a, b, c) {
		var d = a[b];
		a.splice(b, 1), a.splice(c, 0, d)
	}
	var d = null,
		e = a.mainOptions.dynamicViewsOptions.unit,
		f = a.mainOptions.dynamicViewsOptions.formats,
		g = [],
		h = null,
		i = function () {
			b.find(".fpd-list").sortable({
				placeholder: "fpd-item fpd-sortable-placeholder",
				scroll: !1,
				axis: "y",
				start: function (a, b) {
					h = b.item.index()
				},
				update: function (b, d) {
					var e = d.item.index(),
						f = a.$productStage.children(".fpd-view-stage"),
						g = a.$viewSelectionWrapper.children(".fpd-item");
					0 == e ? (f.eq(h).insertBefore(f.eq(0)), g.eq(h).insertBefore(g.eq(0))) : h > e ? (f.eq(h).insertBefore(f.eq(e)), g.eq(h).insertBefore(g.eq(e))) : (f.eq(h).insertAfter(f.eq(e)), g.eq(h).insertAfter(g.eq(e))), c(a.viewInstances, h, e), h == a.currentViewIndex && a.selectView(e)
				}
			}), a.$container.on("productSelect", function () {
				b.find(".fpd-list").empty()
			}).on("productCreate", function () {
				a.currentViewInstance.options.layouts && a.currentViewInstance.options.layouts.length > 0 ? b.find(".fpd-add-from-layouts").removeClass("fpd-hidden") : b.find(".fpd-add-from-layouts").addClass("fpd-hidden")
			}).on("uiSet", function () {
				var b = a.mainBar.$content.find(".fpd-dynamic-views-blank");
				if ($.isArray(f) && f.length > 0) {
					var c = b.find(".fpd-blank-formats").removeClass("fpd-hidden");
					f.forEach(function (a, b) {
						$("<span/>", {
							"class": "fpd-item",
							"data-value": b,
							html: a[0] + " x " + a[1]
						}).appendTo(c.find(".fpd-dropdown-list"))
					})
				}
				b.find(".fpd-dynamic-views-unit").text(e), a.mainBar.$content.on("click", ".fpd-blank-formats .fpd-item", function () {
					var b = $(this).data("value");
					a.mainBar.$content.find(".fpd-blank-custom-size .fpd-width").val(f[b][0]).nextAll("input:first").val(f[b][1])
				}).on("click", ".fpd-dynamic-views-blank .fpd-btn", function () {
					var c = !0,
						f = {};
					b.find(".fpd-blank-custom-size input").each(function () {
						var a = $(this);
						0 == this.value.length ? (c = !1, a.addClass("fpd-error")) : f[a.hasClass("fpd-width") ? "stageWidth" : "stageHeight"] = FPDUtil.unitToPixel(Number(this.value), e)
					}), c && (f.output = {
						width: FPDUtil.pixelToUnit(f.stageWidth, "mm"),
						height: FPDUtil.pixelToUnit(f.stageHeight, "mm")
					}, d = a.viewInstances.length, a.addView({
						title: Date.now(),
						thumbnail: "",
						elements: [],
						options: f
					}), b.find(".fpd-blank-custom-size input").val("").removeClass("fpd-error"))
				}).on("click", ".fpd-dynamic-views-layouts .fpd-item", function () {
					var b = $(this),
						c = b.parent().children(".fpd-item").index(b);
					d = a.viewInstances.length, a.addView(g[c])
				})
			}).on("viewCreate", function (c, f) {
				null !== d && (a.selectView(d), a.mainBar.callModule("dynamic-views"), d = null), f.toDataURL(function (a) {
					{
						var c = FPDUtil.pixelToUnit(f.options.stageWidth, e),
							d = FPDUtil.pixelToUnit(f.options.stageHeight, e);
						$("<div/>", {
							"class": "fpd-item",
							html: '<div class="fpd-view-thumbnail"><picture style="background-image: url(' + a + ')"></picture></div><div class="fpd-actions"><div class="fpd-copy-view"><span class="fpd-icon-copy"></span></div><div class="fpd-dimensions"><input type="number" class="fpd-width" value="' + c + '" />x<input type="number" value="' + d + '" />' + e + '</div><div class="fpd-remove-view"><span class="fpd-icon-remove"></span></div></div>'
						}).appendTo(b.find(".fpd-list"))
					}
					FPDUtil.createScrollbar(b.find(".fpd-scroll-area"))
				})
			}).on("viewRemove", function (a, c) {
				b.find(".fpd-list .fpd-item").eq(c).remove()
			}).on("viewCanvasUpdate", function (c, d) {
				if (a.productCreated) {
					var e = a.$productStage.children(".fpd-view-stage").index($(d.stage.wrapperEl)),
						f = FPDUtil.getScalingByDimesions(d.options.stageWidth, d.options.stageHeight, 250, 200);
					d.toDataURL(function (c) {
						b.find(".fpd-item").eq(e).find("picture").css("background-image", "url(" + c + ")"), "" === d.thumbnail && a.$viewSelectionWrapper.find(".fpd-item").eq(e).find("picture").css("background-image", "url(" + c + ")")
					}, "transparent", {
						multiplier: f
					}, !1, !1)
				}
			}).on("secondaryModuleCalled", function (b, c, d) {
				if ("fpd-dynamic-views-layouts" == c) {
					d.find(".fpd-scroll-area .fpd-grid").empty();
					var e = a.currentViewInstance.options.layouts;
					$.isArray(e) && (g = e, e.forEach(function (a) {
						$("<div/>", {
							"class": "fpd-item fpd-tooltip",
							title: a.title,
							html: '<picture style="background-image: url(' + a.thumbnail + '");"></picture>'
						}).appendTo(d.find(".fpd-scroll-area .fpd-grid"))
					}), FPDUtil.updateTooltip(d.children(".fpd-scroll-area")), FPDUtil.createScrollbar(d.children(".fpd-scroll-area")))
				}
			}), b.on("click", ".fpd-btn", function () {
				a.mainBar.callSecondary($(this).hasClass("fpd-add-blank-view") ? "fpd-dynamic-views-blank" : "fpd-dynamic-views-layouts")
			}).on("click", ".fpd-list .fpd-item", function () {
				var c = b.find(".fpd-item").index($(this));
				a.selectView(c)
			}).on("change keyup", ".fpd-dimensions input", function () {
				var b, c = $(this);
				c.hasClass("fpd-width") ? (a.currentViewInstance.options.stageWidth = FPDUtil.unitToPixel(this.value, e), b = {
					width: this.value
				}) : (a.currentViewInstance.options.stageHeight = FPDUtil.unitToPixel(this.value, e), b = {
					height: this.value
				}), a.currentViewInstance.resetCanvasSize(), a.$container.trigger("viewSizeChange", [a.currentViewInstance, b])
			}).on("click", ".fpd-copy-view", function (c) {
				c.stopPropagation();
				for (var d = b.find(".fpd-item").index($(this).parents(".fpd-item:first")), e = a.viewInstances[d], f = e.stage.getObjects(), g = [], h = 0; h < f.length; ++h) {
					var i = f[h];
					if (void 0 !== i.title && void 0 !== i.source) {
						var j = {
							title: i.title,
							source: i.source,
							parameters: e.getElementJSON(i),
							type: FPDUtil.getType(i.type)
						};
						g.push(j)
					}
				}
				a.addView({
					title: e.title,
					thumbnail: e.thumbnail,
					elements: g,
					options: e.options
				})
			}).on("click", ".fpd-remove-view", function (c) {
				c.stopPropagation();
				var d = b.find(".fpd-item").index($(this).parents(".fpd-item:first"));
				a.removeView(d)
			})
		};
	i()
},
FancyProductDesignerPlus = {
	version: "1.0.7",
	setup: function (a, b) {
		b.mainOptions.colorSelectionPlacement && "" !== b.mainOptions.colorSelectionPlacement && new FPDColorSelection(b), a.on("langJSONLoad", function () {
			if (b.mainOptions.bulkVariationsPlacement && b.mainOptions.bulkVariations) {
				var a = new FPDBulkVariations(b);
				b.bulkVariations = a
			}
		}), -1 != b.mainOptions.mainBarModules.indexOf("names-numbers") && a.on("viewCreate", function (a, b) {
			b.names_numbers && b.names_numbers.length > 1 && b.changePrice((b.names_numbers.length - 1) * b.options.namesNumbersEntryPrice, "+")
		})
	}
};

FancyProductDesignerPlus.availableModules = ["names-numbers", "drawing", "dynamic-views"];