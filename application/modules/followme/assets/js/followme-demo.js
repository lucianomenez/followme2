// ################### NO ALTERAR NI SIQUIERA EL ORDEN DEL ARCHIVO DISENOS.JSON #####################

var allFonts = "";
var allFlags = "";
var allShields = "";
var base_aux = "";

$(() => {

    $("#m_base").change(function(){
        base_aux = $("#m_base").val();
    });
  
    $.getJSON(`fuentes/fonts`).done(function(data){
        data.forEach(function(font){
            font.name = font.fontName;
            font.relSize = font.relsize;
            font.url = base_url + font.font;
            delete font.fontName;
            delete font.relsize;
            delete font.font;
            //console.log(font);
        });
        allFonts = data;
        resto();
    })
});

function resto() {

  function readJSON(file) {
    var request = new XMLHttpRequest();
    request.open('GET', file, false);
    request.send(null);
    if (request.status == 200)
      return request.responseText;
  };

  var color_capucha_interior = "";
  var color_bolsillo = "";
  var color_bolsillo_vivo = "";

  //console.log(allFonts);

  function search(nameKey, myArray) {
    for (var i = 0; i < myArray.length; i++) {
      if (myArray[i].name === nameKey) {
        return myArray[i];
      }
    }
  }
  //
  // var colors = "#FFFFFF ,#000000 ,#990505 ,#FF796A ,#C81702 ,#AD1F09 ,#170704 ,#E83D00 ,#FFBFA8 ,#FF915D ,#D9A991 ,#FF9A46 ,#A67E50 ,#7A725E ,#E8CD7A ,#EBB405 ,#C8C4B6 ,#e8dcaa ,#D6BC43 ,#5B5A47 ,#6B6B67 ,#C4D46E ,#E8FF82 ,#41423F ,#4F504E ,#838A80 ,#489434 ,#939A92 ,#55BF4C ,#00AD68 ,#007D55 ,#1D9671 ,#031711 ,#666A69 ,#C9CDCC ,#009B76 ,#315755 ,#8AADAC ,#3ACFCE ,#009DA2 ,#0D3437 ,#4D9DA6 ,#00879B ,#007B8E ,#0096B9 ,#6F9FB0 ,#BED7E0 ,#0087C3 ,#003E74 ,#2B4B70 ,#003793 ,#37548B ,#0C1321 ,#332561 ,#5B4885 ,#9C88A9 ,#5C1D4E ,#841B49 ,#B31957 ,#D8C3CA ,#EA3369 ,#B51947 ,#D75379 ,#E59FB2 ,#4C3337 ,#D74259 ,#790F1D ,#E23043 ,#FF7783 ,#C9262D ,#F25358 ,#BE1619 ,#9F5B5C";

  //var colors = JSON.parse($.getJSON(base_url + 'followme/colores/colors'));
  //var colorsName = JSON.parse($.getJSON(base_url + 'followme/colores/nombres'));
  var colors = JSON.parse(readJSON(base_url + 'followme/colores/colors/Frisa'));
  var colorsName = JSON.parse(readJSON(base_url + 'followme/colores/nombres'));
  var config_campera = JSON.parse(readJSON(base_url + 'followme/assets/img/arquitectura/me/config/campera/config.json'));
  var config_buzo = JSON.parse(readJSON(base_url + 'followme/assets/img/arquitectura/me/config/buzo/config.json'));
  var config_chomba = JSON.parse(readJSON(base_url + 'followme/assets/img/arquitectura/me/config/chomba/config.json'));
  var config_remera = JSON.parse(readJSON(base_url + 'followme/assets/img/arquitectura/me/config/remera/config.json'));

  const randomNumber = Math.floor(Math.random() * 1000); 

  //ACA
  var $fpd = $('#fpd'),
    pluginOptions = {
      productsJSON: base_url + 'followme/productos/demo',
      designsJSON: base_url + '/followme/assets/json/examples/disenos.json?rnd=' + randomNumber, //followme/assets/json/examples/disenos.json',
      stageWidth: 1200,
      stageHeight: 900,
      hexNames: colorsName,
      initialActiveModule: "",
      fonts: allFonts,
      mainBarModules: ["products", "molderia", "manage-layers", "designs", "text","disenos-molderia","images"],
      actions: {
        "top": [],
        "right": [],
        "bottom": ["save", "load"],
        "left": ["undo", "redo", "snap", "magnify-glass"]
      },
      elementParameters: {
        "z": 500
      },
      selectedColor: "#f5f5f5",
      boundingBoxColor: "#005ede",
      outOfBoundaryColor: "#990000",
      cornerIconColor: "#000000",
      customTextParameters: {
        colors : colors, strokeColors : colors, "left": 0, "top": 0, "z": -1, "replaceInAllViews": false, "autoCenter": true, "draggable": true, "rotatable": true, "resizable": true, "autoSelect": true, "topped": false, "uniScalingUnlockable": false, "curvable": true, "curveSpacing": 1, "curveRadius": 1, "curveReverse": true, "boundingBoxMode": "clipping", "fontSize": 25, "minFontSize": 1, "maxFontSize": 200, "widthFontSize": 0, "maxLength": 0, "maxLines": 0, "textAlign": "center", "removable": true, "fill": "#000000", "fontFamily": "A2"
      },
      customImageParameters: { colors : colors, "left": 0, "top": 0, "minScaleLimit": 0.01, "replaceInAllViews": false, "autoCenter": true, "draggable": true, "rotatable": true, "resizable": true, "zChangeable": true, "autoSelect": false, "topped": false, "uniScalingUnlockable": false, "boundingBoxMode": "clipping", "scaleMode": "cover", "removable": true, "z": 500 },
      langJSON: base_url + 'followme/assets/lang/spanish.json',
      templatesDirectory: base_url + 'followme/assets/html/'
    };

  var fpd = new FancyProductDesigner($fpd, pluginOptions);

  $fpd.on('ready', function () {

    $(".fpd-tool-curved-text").removeClass("fpd-hidden");
    $("#curved_text").removeClass("fpd-hidden");

    $fpd.on('elementAdd', function (e, ele) {
      if (ele.text) {
        //console.log("SKU: " + ele.sku);
        //console.log("Shadow: " + ele.shadowColor);
        $(".fpd-tool-curved-text").removeClass("fpd-hidden");
        $("#curved_text").removeClass("fpd-hidden");

        var sku_copy = ele.sku;
        var shadowColor_copy = ele.shadowColor;

        const aplique = $('#tipo_aplique');
        if (fpd.viewInstances[0] != undefined){
          fpd.setElementParameters({ shadowColor: aplique.val()},ele)
        }
        aplique.val('');

        const curva = $('#tipo_curva');

        if (fpd.viewInstances[0] != undefined){
          fpd.setElementParameters({ sku: curva.val() }, ele)
        }
        curva.val('');

        if (ele.sku == "Seleccione"){
          fpd.setElementParameters({ sku: sku_copy }, ele)
          fpd.setElementParameters({ shadowColor: shadowColor_copy }, ele)
          //console.log("SKU: " + sku_copy);
          //console.log("Shadow: " + shadowColor_copy);
        }

        //console.log("SKU: " + sku_copy);
        //console.log("Shadow: " + shadowColor_copy);
      }

      if (ele.title.includes("Escudo") || ele.title.includes("Bandera")){

        ele.sku = Math.round((ele.aCoords.br.y - ele.aCoords.tr.y) * 0.115 );
        console.log(ele);
      }






    })

    $("#generar_molderia").on('click', function(e) {
      mangas = $("#m_mangas").val();
      base = $("#m_base").val();
      producto = $("#m_producto").val();
      espalda = $("#m_espalda").val();

      if (espalda == "lisa"){
        espalda_lisa = true;
        espalda = base
      }else{
        espalda_lisa = false;
        espalda = false
      }


      if (mangas == "lisa"){
        mangas_lisa = true;
        mangas = base;
      }else{
        mangas_lisa = false
      }

      data = {
        id : producto,
        mangas : mangas,
        base : base,
        espalda : base,
        mangas_lisa : mangas_lisa,
        espalda_lisa : espalda_lisa

      }

      $.ajax({
          url : base_url + '/followme/productos/GetMolderias/'+producto,
          method: 'POST',
          data: data,
          success: function(data){
            fpd.setupProducts(data);
          },
          error: function(data){console.log(data)}
      });


    });

    $fpd.on('elementModify', function (e, ele) {

      //          console.log(ele);
      var font = {};
      if (ele.text) {
        allFonts.forEach((_font) => {
          if (_font.name == ele.fontFamily) {
            font = _font;
          }
        });

        let printer = ((ele.fontSize * font.relSize) / 10).toFixed(1);
        //console.log(printer);
        const textSize = (ele.fontSize * font.relSize) * 0.04;
        $("#printer").text("Texto actual: " + printer + "cm");
      }

      if (ele.title.includes("Escudo") || ele.title.includes("Bandera") || ele.title.includes("Diseño")){
        let size = Math.round((ele.aCoords.br.y - ele.aCoords.tr.y) *0.115 );
        ele.sku = size;
        $("#tamanioEscudo").text("Tamaño del escudo: " + ele.sku + "cm");
      }



    })

    $fpd.on('elementColorChange', function (e, ele, hex) {
      if (ele.title === 'Color general') {
        if (fpd.currentViewInstance !== null) {
          let elements = fpd.getElements(0, 'image');
          elements.forEach((e) => {
            if (e.title !== 'Color general' && e.isEditable) {
              fpd.currentViewInstance.changeColor(e, hex, true)
            }
          })
        }
      }
    })

    $fpd.on('productSelect', function (e, i, cat, prod) {
      if (prod[0].id) {
        const id = prod[0].id;
        if (id) {
          //console.log(id);
          // get ME for product and setup ME products
          $.getJSON(`productos/molderiaespecialdemo/${id}`)
            .done(function (data) {
              fpd.setupMe(data);
            })

            // $.getJSON(`productos/GetMolderiasRotas/${id}`)
            //   .done(function (data) {
            //     fpd.setupProducts(data);
            //   })

            $.getJSON(`escudos/shields`).done(function(data){
                data.forEach(function(shield){
                    let indexOfAssets = shield.shield.indexOf("assets");
                    // Verificar si "/assets" se encontró en la cadena
                    if (indexOfAssets !== -1) {
                        // Obtener la subcadena que está después de "/assets"
                        shield.source = shield.shield.substring(indexOfAssets);
                    }

                    //shield.source = shield.shield; //base_url + shield.shield;
                    shield.thumbnail = shield.source;
                    shield.title = shield.shieldName;
                    shield.parameters = {
                        "zChangeable": true,
                        "left": 400,
                        "top": 390,
                        "removable": true,
                        "draggable": true,
                        "rotatable": true,
                        "resizable": true,
                        "uniScalingUnlockable": true,
                        "scale": 0.1,
                        "autoCenter": true,
                        "colors" : "frisa"
                    }
                    shield.show = true;

                    delete shield.shieldName;
                    delete shield.shield;
                    //console.log(shield);
                });
                allShields = data;
            })

            $.getJSON(`banderas/flags`).done(function(data){
                data.forEach(function(flag){
                    let indexOfAssets = flag.flag.indexOf("assets");
                    // Verificar si "/assets" se encontró en la cadena
                    if (indexOfAssets !== -1) {
                        // Obtener la subcadena que está después de "/assets"
                        flag.source = flag.flag.substring(indexOfAssets);
                    }

                    //flag.source = flag.flag; //base_url + flag.flag;
                    flag.thumbnail = flag.source;
                    flag.title = flag.flagName;
                    flag.parameters = {
                        "zChangeable": true,
                        "left": 400,
                        "top": 390,
                        "removable": true,
                        "draggable": true,
                        "rotatable": true,
                        "resizable": true,
                        "uniScalingUnlockable": true,
                        "scale": 0.1,
                        "autoCenter": true,
                        "colors" : "frisa"
                    }
                    flag.show = true;

                    delete flag.flagName;
                    delete flag.flag;
                    //console.log(flag);
                });
                allFlags = data;
            })

            $.getJSON(`disenos/GetDisenos/${id}`).done(function(data){
              data.forEach(function(objeto, index){
                // Verifica si el título del objeto es "Escudos"
                //console.log(objeto);
                //console.log(index);
                if(objeto.title === "Escudos"){
                  data[index].category[0].designs = allShields; // Agrega los datos de "allShields" a los datos de "Escudos"
                  data[index].category[1].designs = allFlags; // Agrega los datos de "allFlags" a los datos de "Escudos"
                }
              });
              fpd.setupDesigns(data);
                //console.log(data);
            })

        }
      }
      // $(".fpd-top-tools").addClass("oculto");
    })

    $(".fpd-actions-container .fpd-pos-left").prepend(`
        <div class="fpd-action-btn fpd-tooltip fpd-primary-text-color" title="Descargar Diseño" id="generate_dos">
        <span class="fpd-icon-print"></span>
        </div>`);

    $("#generate").on('click', function (e) {
      if (($("#nro_de_orden").val() != "") && ($("#fecha_de_entrega").val() != "") && ($("#responsable_email").val() != "") && ($("#colegio_form").val() != "")){
        e.preventDefault();
        // if ($("#m_producto").val() != ""){
        //   fpd.getViewsDataURL(generatePdf, "transparent", {onlyExportable : true});
        // }else{
           fpd.getViewsDataURL(generatePdf);
        // }
      }else{
        e.preventDefault();
        alert ("Por favor, antes de generar una orden, completá todos los datos.")
      }
    });

    $("#generate_dos").on('click', function (e) {
        fpd.getViewsDataURL(generatePdf2, "transparent", {onlyExportable : true});
    });

  });

  //get category index by category name
  var _getCategoryIndexInMe = function (catName) {

    var catIndex = $.map(fpd.products, function (obj, index) {

      if (obj.category == catName) {
        return index;
      }
    }).shift();

    return isNaN(catIndex) ? false : catIndex;

  };

  var _addGridMe = function (views) {

    var fpdInstance = fpd,
      lazyClass = 'fpd-hidden',
      currentCategoryIndex = 0,
      $gridWrapper = $('[data-module="molderia"] .fpd-grid');

    var thumbnail = views[0].productThumbnail ? views[0].productThumbnail : views[0].thumbnail,
      productTitle = views[0].productTitle ? views[0].productTitle : views[0].title;

    var $lastItem = $('<div/>', {
      'class': 'fpd-item fpd-tooltip ' + lazyClass,
      'data-title': productTitle,
      'data-source': thumbnail,
      'html': '<picture data-img="' + thumbnail + '"></picture>'
    }).appendTo($gridWrapper);

    $lastItem.click(function (evt) {

      evt.preventDefault();

      var $this = $(this),
        index = $gridWrapper.children('.fpd-item').index($this);

      if (fpdInstance.mainOptions.swapProductConfirmation) {

        var $confirm = FPDUtil.showModal(fpdInstance.getTranslation('modules', 'products_confirm_replacement'), false, 'confirm', fpdInstance.$modalContainer);

        $confirm.find('.fpd-confirm').text(fpdInstance.getTranslation('modules', 'products_confirm_button'))
          .click(function () {

            fpdInstance.selectMe(index, currentCategoryIndex);
            $confirm.find('.fpd-modal-close').click();

          })

      }
      else {
        fpdInstance.selectMe(index, currentCategoryIndex);
      }


    }).data('views', views);

  };

  fpd.addMe = function (views, category) {

    var catIndex = _getCategoryIndexInMe(category);

    if (category === undefined) {
      fpd.mespecial.push(views);
    }
    else {

      if (catIndex === false) {

        catIndex = fpd.mespecial.length;
        fpd.mespecial[catIndex] = { category: category, products: [] };

      }

      fpd.mespecial[catIndex].products.push(views);

    }

    _addGridMe(views);

  };

  fpd.selectMe = function (index, categoryIndex) {

    var instance = fpd;

    instance.currentCategoryIndex = categoryIndex === undefined ? instance.currentCategoryIndex : categoryIndex;

    var productsObj;
    if (instance.mespecial && instance.mespecial.length && instance.mespecial[0].category) { //categories enabled
      var category = instance.mespecial[instance.currentCategoryIndex];
      productsObj = category.products;
    }
    else { //no categories enabled
      productsObj = instance.mespecial;
    }

    instance.currentProductIndex = index;
    if (index < 0) { currentProductIndex = 0; }
    else if (index > productsObj.length - 1) { instance.currentProductIndex = productsObj.length - 1; }

    var product = productsObj[instance.currentProductIndex];

    // /**
    //  * Gets fired when a product is selected.
    //  *
    //  * @event FancyProductDesigner#productSelect
    //  * @param {Event} event
    //  * @param {Number} index - The index of the product in the category.
    //  * @param {Number} categoryIndex - The index of the category.
    //  * @param {Object} product - An object containing the product (views).
    //  */
    // $elem.trigger('productSelect', [index, categoryIndex, product]);

    instance.loadProduct(product, instance.mainOptions.replaceInitialElements);

  };

  fpd.setupMe = function (models) {

    $gridWrapper = $('[data-module="molderia"] .fpd-grid');
    $gridWrapper.empty();

    var mespecial = models === undefined ? [] : models;

    fpd.mespecial = [];

    mespecial.forEach(function (productItem) {

      if (productItem.hasOwnProperty('category')) { //check if products JSON contains categories

        productItem.products.forEach(function (singleProduct) {
          fpd.addMe(singleProduct);
        });

      }
      else {
        fpd.addMe(productItem);
      }

    });

    FPDUtil.createScrollbar($('[data-module="molderia"] .fpd-scroll-area'));
  };

    const generatePdf = (images) => {

        const product = fpd.getProduct();
        const orderN = $("#nro_de_orden").val();
        const fechaE = $("#fecha_de_entrega").val();
        var prototipo = $('#prototipo').val();
        const colegio = $("#colegio_form").val();
        const responsable = $("#responsable").val();
        const responsable_email = $("#email_responsable").val();
        const curso = $("#curso").val();
        let observaciones = $("#observaciones").val();

        if(prototipo == ""){
            prototipo = "No tiene";
        }

        if (elementos_custom > 0) {
            for (i = 0; i < elementos_custom; i++){
                let texto = ($(".custom_element_texto")[i].value);
                let fuente =($(".custom_element_fuente")[i].value);
                let cm = ($(".custom_element_cm")[i].value);
                let estilo = ($(".custom_element_estilo")[i].value);
                let color = ($(".custom_element_color")[i]);
                color = color.options[color.selectedIndex].text;
                let curva = ($(".custom_element_curva")[i].value);

                if (($(".custom_element_aclaracion")[i].value) != ""){
                    texto = texto +'('+String(i+1)+')';
                    observaciones = observaciones +'('+String(i+1)+')'+($(".custom_element_aclaracion")[i].value);
                }

                elemento_custom = {
                    parameters : { fill : "#FFFFFF"},
                    title : "custom",
                    type : "custom",
                    texto : texto,
                    fuente : fuente,
                    cm : cm,
                    estilo : estilo,
                    color : color,
                    curva : curva
                }

                product[0].elements.push(elemento_custom);
            }
        }

        const pngBack = document.getElementById('pdf-background');
        const pngBack2 = document.getElementById('pdf-background2');
        const pngBack3 = document.getElementById('pdf-background3');

        const dataURL = fpd.getProductDataURL();

        fpd.getProductDataURL(function(dataURL){
            $.ajax({
                url: base_url + 'followme/api/save_image',
                type: 'POST',
                data: {
                    colegio : colegio,
                    curso : curso,
                    nro_orden : orderN,
                    fecha_entrega : fechaE,
                    prototipo : prototipo,
                    responsable : responsable,
                    responsable_email : responsable_email,
                    base64_image: dataURL
                },
                success: function (data) {
                    console.log("imagen guardada");
                }
            });
        });

        const title = product[0].title.toLowerCase();
        const ruta = product[0].thumbnail.toLowerCase();
        const modelME = product[0].title.match(/ME[0-9]{1,2}/) || "";

        const isCampera = title.includes("campera") ? true : false;
        const isBuzo = title.includes("buzo") ? true : false;
        const isChomba = title.includes("chomba") ? true : false;
        const isRemera = title.includes("remera") ? true : false;
        const isCardigan = ruta.includes("cardigans") ? true : false;
        const isME = title.includes("- me") ? true : false;
        const isRaglan = title.includes("rach") ? true : false;
        const isCustom = title.includes("custom") ? true : false;

        var title_split = title.split(" ");
        var me_split = title_split[title_split.length - 1].split("e");
        var numero_molderia = me_split[me_split.length - 1].toUpperCase();

        var config_molderia;

        if(isChomba){
            config_molderia = config_chomba[numero_molderia];
        }
        else if(isRemera){
            config_molderia = config_remera[numero_molderia];
        }
        else{
            config_molderia = config_campera[numero_molderia];
        }
        if(isME){
            var isMangaEspecial = config_molderia[2].mangaEspecial;
        }

        var color_cuello_chomba;
        var color_punio_chomba;

        console.log(title);

        var doc = new jsPDF({ unit: "mm", format: "legal" });
        doc.setFontSize(10);

        if(isChomba){
            doc.addImage(pngBack2, "PNG", 0, 0, 216, 356 ); // Plantilla de generacion de PDF de Chomba

            doc.text(orderN, 29, 16); // Coloca numero de orden en la plantilla
            doc.text(fechaE, 29, 23); // Coloca fecha de entrega en la plantilla
            doc.text(prototipo, 193.5, 16); // Coloca prototipo en la plantilla

            if(!isME){
                doc.circle(59, 135.5, 1.8, "F"); // Circulo "Tipo de Producto Regular"
            }
            if(title.includes("jersey")){ // TIPO DE TELA
                doc.circle(32, 153, 1.8, "F"); // Circulo "Jersey"
            }else{
                doc.circle(58.8, 153.1, 1.8, "F"); // Circulo "Pique"
            }
            if(isRaglan){ // MANGAS
                doc.circle(59, 162.5, 1.8, "F"); // Circulo "Raglan"
            }else{
                doc.circle(32, 162.5, 1.8, "F"); // Circulo "Manga Regular"
            }
            if(title.includes("con")){ // PUÑOS
                doc.circle(32, 181.2, 1.8, "F"); // Circulo "Puños Si"
            }else{
                doc.circle(58.7, 181.2, 1.8, "F"); // Circulo "Puños No"
            }

            product[0].elements.map((element) => {
                if(element.parameters.fill){
                    const color = colorsName[element.parameters.fill.toUpperCase().replace('#','')] || "";
                    switch(element.title){
                        case "Base":
                            doc.text(color, 140, 155, {align: "center"});
                            break;
                        case "Mangas":
                            doc.text(color, 140, 155 + 8.7*1, {align: "center"});
                            break;
                        case "Cuello":
                            //doc.text(color, 140, 155 + 8.7*2, {align: "center"});
                            color_cuello_chomba = color;
                            break;
                        case "Puños":
                            //doc.text(color, 140, 155 + 8.7*3, {align: "center"});
                            color_punio_chomba = color;
                            break;
                        case "Cartera":
                            doc.text(color, 140, 155 + 8.7*4, {align: "center"});
                            break;
                        default:
                            break;
                    }
                }
            });
        }
        else if(isRemera){
            doc.addImage(pngBack3, "PNG", 0, 0, 216, 356 ); // Plantilla de generacion de PDF de Remera

            doc.text(orderN, 29, 16); // Coloca numero de orden en la plantilla
            doc.text(fechaE, 29, 23); // Coloca fecha de entrega en la plantilla
            doc.text(prototipo, 193.5, 16); // Coloca prototipo en la plantilla

            if(!isME){
                doc.circle(59.1, 135.2, 1.8, "F"); // Circulo "Tipo de Producto Regular"
            }
            doc.circle(32, 153, 1.8, "F"); // Circulo "Jersey"
            doc.circle(32.2, 162.8, 1.8, "F"); // Circulo "Manga Regular"
            doc.circle(32.5, 171, 1.8, "F"); // Circulo "Cuello Redondo"
            if(title.includes("con")){ // PUÑOS
                doc.circle(32, 181, 1.8, "F"); // Circulo "Puños Si"
            }
            else{
                doc.circle(58.8, 181, 1.8, "F"); // Circulo "Puños No"
            }

            product[0].elements.map((element) => {
                if(element.parameters.fill){
                    const color = colorsName[element.parameters.fill.toUpperCase().replace('#','')] || "";
                    switch(element.title){
                        case "Base":
                            doc.text(color, 140, 155, {align: "center"});
                            break;
                        case "Mangas":
                            doc.text(color, 140, 155 + 8.7*1, {align: "center"});
                            break;
                        case "Cuello":
                            doc.text(color, 140, 155 + 8.7*2, {align: "center"});
                            break;
                        case "Puños":
                            doc.text(color, 140, 155 + 8.7*3, {align: "center"});
                            break;
                        default:
                            break;
                    }
                }
            });
        }
        else{
            doc.addImage(pngBack, "PNG", 0, 0, 216, 356, "", 'FAST'); // Plantilla de generacion de PDF de Campera
            
            doc.text(orderN, 29, 9); // Coloca numero de orden en la plantilla
            doc.text(fechaE, 29, 16); // Coloca fecha de entrega en la plantilla
            doc.text(prototipo, 193.5, 9); // Coloca prototipo en la plantilla
            
            if(!isME){
                doc.circle(59.22, 121.25, 1.8, "F"); // Circulo "Tipo de Producto Regular"
            }

            if(isBuzo){
                doc.circle(32.1, 138.7, 1.8, "F"); // Circulo "Cuerpo Buzo"
                doc.circle(42.4, 182, 1.8, "F"); // Circulo "Cierre No"
                // doc.circle(42.4, 200, 1.8, "F"); // Circulo "Cordones No"
            }
        
            if(isCampera){
                doc.circle(58.9, 138.7, 1.8, "F"); // Circulo "Cuerpo Campera"
                doc.circle(32, 182, 1.8, "F"); // Circulo "Cierre Si"
                doc.circle(84.7, 182.1, 1.8, "F");  // Circulo "Cierre Oculto"
                // doc.circle(32, 200, 1.8, "F"); // Circulo "Cordones Si"
            }

            doc.circle(32.1, 147.3, 1.8, "F"); // Circulo "Forreria Capucha Si"
            //doc.circle(42.4, 147.4, 1.8, "F"); // Circulo "Forreria Capucha No"
            doc.circle(58.65, 145.65, 1.8, "F"); // Circulo "Forreria Capucha Liso"
            // doc.circle(58.6, 149.6, 1.8, "F"); // Circulo "Forreria Capucha Sublimado"

            if(title.includes("bolsillo")){ // BOLSILLOS
                doc.circle(32, 191, 1.8, "F"); // Circulo "Bolsillos Si"
                if(title.includes("canguro")){
                    doc.circle(58.70, 188.8, 1.8, "F"); // Circulo "Bolsillos Canguro"
                }
                if(title.includes("oculto")){
                  doc.circle(58.70, 192.5, 1.8, "F"); // Circulo "Bolsillos Canguro"
              }
                if(title.includes("ojal")){
                    doc.circle(84.6, 188.8, 1.8, "F"); // Circulo "Bolsillos Ojal"
                }
            }
            else{
                doc.circle(42.4, 191, 1.8, "F"); // Circulo "Bolsillos No"
            }

            if(title.includes("cordones")){
                doc.circle(32, 200, 1.8, "F"); // Circulo "Cordones Si"
            }
            else{
                doc.circle(42.4, 200, 1.8, "F"); // Circulo "Cordones No"
            }

            product[0].elements.map((element) => {
                doc.setFontSize(10);
                if(element.parameters.fill){
                    const color = colorsName[element.parameters.fill.toUpperCase().replace('#','')] || "";
                    switch(element.title){
                        case "Base":
                            doc.text(color, 140, 140, {align: "center"});
                            break;
                        case "Capucha":
                            doc.text("Afuera: "+color, 140, 140 + 8.7*0.75, {align: "center"});
                            break;
                        case "Capucha Interior":
                            color_capucha_interior = color;
                            if(color.split(" ")[0].length === 2){ // Para corregir cuando se elige un color general no se ponga frisa
                              doc.text("Adentro: "+ "1"+color, 140, 140 + 8.7*1.2, {align: "center"});
                            }
                            else{
                              doc.text("Adentro: "+ color, 140, 140 + 8.7*1.2, {align: "center"});
                            }
                            break;
                        case "Mangas":
                            doc.text(color, 140, 140 + 8.7*2, {align: "center"});
                            break;
                        case "Puños":
                            doc.text(color, 140, 140 + 8.7*3, {align: "center"});
                            break;
                        case "Cintura":
                            doc.text(color, 140, 140 + 8.7*4, {align: "center"});
                            break;
                        case "Cierre":
                            doc.text(color, 140, 140 + 8.7*5, {align: "center"});
                            break;
                        case "Bolsillo":
                            doc.text(color, 140, 140 + 8.7*5.75, {align: "center"});
                            color_bolsillo = color;
                            break;
                        case "Bolsillos":
                            doc.text(color, 140, 140 + 8.7*5.75, {align: "center"});
                            color_bolsillo = color;
                            break;
                        case "Bolsillo Vivos":
                            doc.text("Vivos: "+color, 140, 140 + 8.7*6.25, {align: "center"});
                            color_bolsillo_vivo = color;
                            break;
                        //case "Cordones":
                            //doc.text(color, 140, 140 + 8.7*7, {align: "center"});
                            //break;
                        case "Botones":
                            doc.text(color, 140, 140 + 8.7*7, {align: "center"});
                            break;
                        case "Espalda":
                            break;
                        default:
                            break;
                    }
                }
            });

            if(title.includes("canguro")){
                if(color_bolsillo != color_bolsillo_vivo){
                    doc.circle(84.6, 192.5, 1.8, "F"); // Circulo "Bolsillos Vivos"
                }
                else{
                    doc.setTextColor(255, 255, 255);
                    doc.text("Vivos: "+color_bolsillo_vivo, 140, 140 + 8.7*6.25, {align: "center"});
                    doc.setFontSize(12);
                    doc.text("Vivos: "+color_bolsillo_vivo, 140, 140 + 8.7*6.25, {align: "center"});
                    doc.setTextColor(0, 0, 0);
                }
            }

        }

        doc.addImage(images[0], "PNG", -10, 20, 140, 105, '', 'FAST');
        doc.text("Frente", 30, 112, {align: "center"});

        doc.addImage(images[1], "PNG", 92, 20, 140, 105, '', 'FAST');
        doc.text("Derecha", 80 , 112, {align: "center"});

        doc.addImage(images[2], "PNG", 40, 20, 140, 105,'', 'FAST');
        doc.text("Espalda", 140, 112, {align: "center"});

        doc.addImage(images[3], "PNG", 145, 20, 140, 105, '', 'FAST');
        doc.text("Izquierda", 180, 112, {align: "center"});

    // set "tipo de producto"
    if((isME) && (!isCustom)){
        if(isChomba){
            doc.circle(137.6, 135.6, 1.8, "F"); // for "Moldería especial"
            doc.circle(141.5, 153, 1.8); // "tela" reference for ME
            //doc.circle(140, 155 + 8.7 * 2, 1.8); // "cuello" reference for ME
        }
        else if(isRemera){
            doc.circle(137.7, 135, 1.8, "F"); // for "Moldería especial"
            doc.circle(141.5, 153, 1.8);
        }
        else{
            doc.circle(137.5, 121.3, 1.8, "F"); // for "Moldería especial"
            doc.circle(140, 138.7, 1.8); // "cuerpo" reference for ME
            doc.circle(140, 138.7 + 8.7 * 2, 1.8); // "mangas" reference for ME
        }
    }
    else if(isCustom){
        doc.circle(137.5, 121.5, 1.8, "F"); // for "Moldería especial"
        doc.circle(140, 138.7, 1.8); // "cuerpo" reference for ME
        doc.circle(50, 305, 1.8);
    }
    // doc.circle(83.6, 121.5, 1.8, "F"); // for "Full print"
    // doc.circle(111, 121.5, 1.8, "F"); // for "Degrade"
    // doc.circle(179.6, 121.5, 1.8, "F"); // for "Reversible"

    // set "colores"
    doc.setFontSize(12);
    var colorsME = "";
    var coloresUbicacion = [];
    console.log(product[0])
    product[0].elements.map((element) => {

      if (element.parameters.fill) {
        const color = colorsName[element.parameters.fill.toUpperCase().replace('#', '')] || '';
        const ubicacion = element.parameters.ubicacion;
        //console.log(element);
        switch (element.title) {
          case "Color 0":
          case "Color 1":
          case "Color 2":
          case "Color 3":
          case "Color 4":
          case "Color 5":
          case "Color 6":
            colorsME += element.title + ": " + color + " | ";
            coloresUbicacion.push([element.title, color, ubicacion]);
            break;
          default:
            break;
        }
      }

    });


    var commentME = "";
    var commentME1 = "";
    var commentME2 = "";
    var commentME3 = "";

    var coloresFrente = "";
    var coloresManga = "";

    var colores_me;

    if (modelME[0] == undefined){
      nro = $("#m_base").val();
    }else{
      var nro = modelME[0].substr(2,2);
    }

    console.log(nro, base_aux);

    console.log(coloresUbicacion);

    coloresUbicacion.forEach(function(color) {
      if(color[2] == "F" || color[2] == "FM"){
        coloresFrente += color[0] + ": " + color[1] + " | ";
      }
      if(color[2] == "M" || color[2] == "FM"){
        coloresManga += color[0] + ": " + color[1] + " | ";
      }
    });

    if ((isME) && (isCustom != true)) {
      commentME1 = "F"+nro+"-P"+nro+"-E"+nro+" //// " +"F"+nro+": Colores --> "+ coloresFrente + "\n" ;
      if ((nro == "21") || (nro == "24") || (nro == "25")){
        colores= colorsME.split(" | ");
        colores_me = colorsME.split(" | ");
        commentME2 = "P" + nro + ": Colores -->  " + colores[0] + "\n" ;
      }else if ((nro == "23")){
        colores= colorsME.split(" | ");
        commentME2 = "Perfil Derecho :"+colores[2] + "Perfil Izquierdo :"+ colores[1] + "\n" ;
      }
      else{
        commentME2 = "////P" +nro+": Colores --> "+ coloresManga + "\n" ;
      }
      commentME3 = "////"+"E"+nro+": Colores --> "+ coloresFrente + "\n" ;
      console.log(colorsME);
    }
    colores_me = colorsME.split(" | ");

    doc.setFontSize(10);
    
    // COLORES DE MANGAS

    if((isME) && (!isCustom)){
        if(isMangaEspecial){
            if(isChomba){
                doc.circle(141.5, 163, 1.8);
            }
            else if(isRemera){
                doc.circle(141.5, 162, 1.8);
            }
        }
        else{
            console.log("NO es molderia con manga especial");
            if(isChomba){
                let color_puro = colores_me[0].split(": ")
                doc.text(String(color_puro[1]), 140, 155 + 8.7*1, {align: "center"});
            }
            else if(isRemera){
                let color_puro = colores_me[0].split(": ")
                doc.text(String(color_puro[1]), 140, 155 + 8.7*1, {align: "center"});
            }
        }
    }

    if (isCustom){

        var espalda_color = "";
        var mangas_color = "";

         espalda_lisa = $("#m_espalda").val();

          product[1].elements.forEach(function (arrayItem) {
            var x = arrayItem
            if ((x.title).toString() == "Mangas"){
              console.log("adentro");
              mangas_color = colorsName[x.parameters.fill.toUpperCase().replace('#','')] || "";
            }
            if ((x.title).toString() == "Espalda"){
              console.log(x.parameters.fill);
              espalda_color = colorsName[x.parameters.fill.toUpperCase().replace('#','')] || "";
              console.log(espalda_color);
            }

          });

         if (espalda_lisa == "base"){
           var nro_espalda = " (Lisa)"
           commentME3 = "////"+" E"+nro_espalda+": Colores --> "+colorsME + "\n" ;
         }else{
           var nro_espalda = " (Lisa)"
           commentME3 = "//// "+"E "+nro_espalda+": Colores --> "+espalda_color + "\n" ;
         }
         commentME1 = "F ME"+$("#m_base").val()+"-P (Lisa)"+"-E"+nro_espalda+" //// " +"F"+nro+": Colores --> "+colorsME + "\n" ;

         commentME2 = "//// P (Lisa): Color --> "+mangas_color + "\n" ;
      }

    // set text
    var textCounter = 0;
    var textTejidoCounter = 0;
    var flag_tejido = false;
    var cordones = false;

    var colores_tejidos = new Map();
    var tiras_buzo_campera = [];

    var count_tejido_cuello_3 = 0;
    var count_tejido_cuello_4 = 0;
    var count_tejido_cuello_5 = 0;
    var count_tejido_punios_3 = 0;
    var count_tejido_punios_4 = 0;
    var count_tejido_punios_5 = 0;
    
    product.map((view) => {
      view.elements.map((element) => {

        if (element.type == "text") {

          console.log("Parametros:");
          console.log(element.parameters);

          if ((element.parameters.fill == null) || (element.parameters.fill == "#000000")){
            element.parameters.fill = "0E0E0E";
          }

          if (element.parameters.stroke == null){
            element.parameters.stroke = "";
          }

          someText = element.parameters.text.replace(/(\r\n|\n|\r)/gm, "");

          // console.log(element.parameters);
          const linePos = 225.5 + 8.7 * textCounter;
          // text
          doc.text(someText, 5, linePos);
          // font
          doc.text(element.parameters.fontFamily, 90, linePos, { align: "center" });
          // size
          //          const font = allFonts.find(element.fontFamily);
          const font = search(element.parameters.fontFamily, allFonts);
          let textSize = ((element.parameters.fontSize * font.relSize) / 10).toFixed(2);
          textSize = Math.round(textSize);

          doc.text(String(textSize), 109, linePos, { align: "center" });
          // style
          doc.text(element.parameters.shadowColor.toString(), 132, linePos, { align: "center" });
          // color

          const color_texto = colorsName[element.parameters.fill.toUpperCase().replace('#','')] || "";
          const color_contorno = colorsName[element.parameters.stroke.toUpperCase().replace('#','')] || "";

          if ((element.parameters.shadowColor.toString() == "AS") || (element.parameters.shadowColor.toString() == "B") || (element.parameters.shadowColor.toString() == "E") || (element.parameters.shadowColor.toString() == "ADR")){
            doc.text(color_texto, 170, linePos, { align: "center" });
          }else if (element.parameters.shadowColor.toString() == "C"){
            doc.text('Contorno: '+color_contorno, 170, linePos+0.9, { align: "center" });
          }else if (element.parameters.shadowColor.toString() == "AF"){
            doc.setFontSize(9);
            doc.text('Aplique: '+color_texto, 170, linePos-1.6, { align: "center" });
            doc.text('Festón: '+color_contorno, 170, linePos+0.9, { align: "center" });
            doc.setFontSize(10);
          }else{
            doc.setFontSize(9);
            doc.text('Arriba: '+color_texto, 170, linePos-1.6, { align: "center" });
            doc.text('Abajo: '+color_contorno, 170, linePos+0.9, { align: "center" });
            doc.setFontSize(10);
          }
          // curve
          doc.text(element.parameters.sku, 203, linePos, { align: "center" });
          textCounter++
        }

        if (element.type === "custom") {
           console.log(element.parameters);
          const linePos = 225.5 + 8.7 * textCounter;
          // text
          doc.text(element.texto, 5, linePos);
          // font
          doc.text(element.fuente, 90, linePos, { align: "center" });
          // size
          doc.text(element.cm, 109, linePos, { align: "center" });
          // style
          doc.text(element.estilo.toString(), 132, linePos, { align: "center" });
          // color
          doc.text(element.color, 170, linePos, { align: "center" });
          doc.text(element.curva, 203, linePos, { align: "center" });
          textCounter++
        }

        if (element.title.includes("1CM") || element.title.includes("2CM")) {

          if (element.source.includes("frente")){

            const color_aplique = colorsName[element.parameters.fill.toUpperCase().replace('#','')] || "";
            let tamanioTira, nombreTira
            nameSplit = element.title.split("-");
            tamanioTira = nameSplit[0];
            nombreTira = nameSplit[1];
            tira = nombreTira.split(" ");
            ladoTira = tira[1] + " " + tira[2];
            tipoTira = tira[3];

            if(isRemera || isChomba){
              const linePos = 225.5 + 8.7 * textCounter;

              nameInPDFview = "Tiras - " + nombreTira;
              // Texto
              doc.text(nameInPDFview, 5, linePos);
              // Fuente
              doc.text("-", 90, linePos, { align: "center" });
              // CM
              doc.text(element.title.charAt(0), 109, linePos, { align: "center" });
              // Estilo
              doc.text("AS", 132, linePos, { align: "center" });
              // Color
              doc.text(color_aplique, 170, linePos, {align: "center"});
              // Curva
              doc.text("C1", 203, linePos, { align: "center" });

              textCounter++;
            }
            else{ // Si es buzo o campera
              tiras_buzo_campera.push([color_aplique, tamanioTira, ladoTira, tipoTira]);
              
              //console.log("Tira " + nombreTira + " de color: " + color_aplique);
              //console.log("Tamaño de la tira: " + tamanioTira);
              //console.log("Lado de la tira: " + ladoTira);
              //console.log("Tipo de tira: " + tipoTira);
            }
          }
        }

        if (element.title.includes("Tejido")) { // TEJIDO ACA
          // color
          const color_aplique= colorsName[element.parameters.fill.toUpperCase().replace('#','')] || "";
          //doc.text(color_aplique, 170, linePos, {align: "center"});
          if(element.title.includes("Cuello")){
            if(element.title.includes("1") || element.title.includes("2")){
              colores_tejidos.set(element.title,color_aplique);
            }
            if(element.title.includes("3")){
              count_tejido_cuello_3++;
              colores_tejidos.set(element.title,color_aplique);
              colores_tejidos.set("count_tejido_cuello_3",count_tejido_cuello_3);
            }
            if(element.title.includes("4")){
              count_tejido_cuello_4++;
              colores_tejidos.set(element.title,color_aplique);
              colores_tejidos.set("count_tejido_cuello_4",count_tejido_cuello_4);
            }
            if(element.title.includes("5")){
              count_tejido_cuello_5++;
              colores_tejidos.set(element.title,color_aplique);
              colores_tejidos.set("count_tejido_cuello_5",count_tejido_cuello_5);
            }
          }
          if(element.title.includes("Puños")){
            if(element.title.includes("1") || element.title.includes("2")){
              colores_tejidos.set(element.title,color_aplique);
            }
            if(element.title.includes("3")){
              count_tejido_punios_3++;
              colores_tejidos.set(element.title,color_aplique);
              colores_tejidos.set("count_tejido_punios_3",count_tejido_punios_3);
            }
            if(element.title.includes("4")){
              count_tejido_punios_4++;
              colores_tejidos.set(element.title,color_aplique);
              colores_tejidos.set("count_tejido_punios_4",count_tejido_punios_4);
            }
            if(element.title.includes("5")){
              count_tejido_punios_5++;
              colores_tejidos.set(element.title,color_aplique);
              colores_tejidos.set("count_tejido_punios_5",count_tejido_punios_5);
            }
          }
        }

        if (element.title.includes("Escudo") || element.title.includes("Bandera")) {
          var color_escudo;
          var sin_color = false;
          if (element.parameters.fill == false){
            element.parameters.fill = "#0E0E0E";
            sin_color = true;
          }
          const linePos = 225.5 + 8.7 * textCounter;
          // text
          doc.text(element.title, 5, linePos);
          doc.text("-", 90, linePos, { align: "center" });
          doc.text(String(element.parameters.sku), 109, linePos, { align: "center" });
          // font
          doc.text("B", 132, linePos, { align: "center" });
          // color
          if(element.title.includes("Bandera") && sin_color){
            color_escudo = "Acorde a catálogo";
          }
          else{
            color_escudo = colorsName[element.parameters.fill.toUpperCase().replace('#','')] || "";
          }
          doc.text(color_escudo, 170, linePos, {align: "center"});
          doc.text("C1", 203, linePos, { align: "center" });
          // curve
          textCounter++
        }

        if (element.title.includes("Cordones")) {
          if (!element.source.includes("sombra") && cordones == false){
            doc.setFontSize(10);
            const color = colorsName[element.parameters.fill.toUpperCase().replace('#','')] || "";
            doc.text(color, 140, 140 + 8.7*7, {align: "center"});
            doc.circle(32, 200, 1.8, "F"); // for "Si"

            doc.setFillColor(255, 255, 255);
            doc.circle(42.4, 200, 1.8, "F"); // for "No"
            console.log(element);
            console.log("Cordones");
            cordones = true;
          }
        }

        if (element.title.includes("Corderito")){
          doc.setFillColor(255, 255, 255);
          doc.circle(58.6, 145.7, 1.8, "F"); // for
          doc.setFillColor(0, 0, 0);

          doc.circle(84.6, 145.7, 1.8, "F"); // for "Corderito"
          const linePos = 225.5 + 8.7 * textCounter;
          doc.setFontSize(10);

          doc.setTextColor(255,255,255);
          doc.text("Adentro: "+color_capucha_interior, 140, 140 + 8.7*1.2, {align: "center"});
          doc.setTextColor(0,0,0);
          doc.text("Adentro: "+element.title, 140, 140 + 8.7*1.2, {align: "center"});
          textCounter++
        }

        if (element.title.includes("Peluche")){
          doc.setFillColor(255, 255, 255);
          doc.circle(58.6, 145.7, 1.8, "F"); // for
          doc.setFillColor(0, 0, 0);
          doc.setFontSize(10);
          doc.circle(84.6, 149.6, 1.8, "F");
          const linePos = 225.5 + 8.7 * textCounter;
          doc.setTextColor(255,255,255);
          doc.text("Adentro: "+color_capucha_interior, 140, 140 + 8.7*1.2, {align: "center"});
          doc.setTextColor(0,0,0);
          doc.text("Adentro: "+element.title, 140, 140 + 8.7*1.2, {align: "center"});
          textCounter++
        }

      });
    });

    let tejidos_cuello_12 = [];
    let tejidos_cuello_3 = [];
    let tejidos_cuello_4 = [];
    let tejidos_cuello_5 = [];
    let tejidos_punios_12 = [];
    let tejidos_punios_3 = [];
    let tejidos_punios_4 = [];
    let tejidos_punios_5 = [];
    let contador_posicion_texto = 0;
    let posicion_texto;
    let posicion_cuadrado;

    if(!isChomba && !isRemera && tiras_buzo_campera.length !== 0){
      //console.log(tiras_buzo_campera);

      const grupos_tiras = {};

      tiras_buzo_campera.forEach((tira) => {
        const color = tira[0];
        const tamanio = tira[1];
        const tiraSinColor = tira.slice(1);
        const tiraSinTamanio = tiraSinColor.slice(1);
        if(!grupos_tiras[color]){
          grupos_tiras[color] = {};
        }
        if(!grupos_tiras[color][tamanio]){
          grupos_tiras[color][tamanio] = [];
        }
        grupos_tiras[color][tamanio].push(tiraSinTamanio);
      });
      
      //console.log(grupos_tiras);

      const grupos_tiras_array = Object.entries(grupos_tiras);
      //console.log(grupos_tiras_array);

      const gruposConMasDeUnElemento = grupos_tiras_array.filter((grupo) => {
        const tamanios = grupo[1];
        return Object.values(tamanios).some((tamanio) => tamanio.length > 1);
      });
      
      const gruposConUnElemento = grupos_tiras_array.filter((grupo) => {
        const tamanios = grupo[1];
        return Object.values(tamanios).every((tamanio) => tamanio.length === 1);
      });

      //console.log(gruposConMasDeUnElemento);
      //console.log(gruposConUnElemento);

      gruposConUnElemento.forEach(function(elemento_tira){
        let color = elemento_tira[0];
        let tamanio = elemento_tira[1];
        Object.entries(tamanio).forEach(function([tamanio, valores]){
          valores.forEach(function(valoresTira){
            const linePos = 225.5 + 8.7 * textCounter;
            nameInPDFview = "Tira - " + valoresTira[0] + " " + valoresTira[1];
            // Texto
            doc.text(nameInPDFview, 5, linePos);
            // Fuente
            doc.text("-", 90, linePos, { align: "center" });
            // CM
            doc.text(tamanio.charAt(0), 109, linePos, { align: "center" });
            // Estilo
            doc.text("AS", 132, linePos, { align: "center" });
            // Color
            doc.text(color, 170, linePos, {align: "center"});
            // Curva
            doc.text("C1", 203, linePos, { align: "center" });

            textCounter++;
          });
        });
      });

      const parejasDeTiras = {};
      if(gruposConMasDeUnElemento.length > 0){
        gruposConMasDeUnElemento.forEach(function(elemento_tira){
          let color = elemento_tira[0];
          let tamanio = elemento_tira[1];
          Object.entries(tamanio).forEach(function([tamanio, valores]){
            valores.forEach(function(valoresTira){
              
              if(!parejasDeTiras[valoresTira[0]]){
                // Si el nombre no ha sido visto antes, agregamos un nuevo registro a la estructura auxiliar
                parejasDeTiras[valoresTira[0]] = {
                  cantTiras: 1,
                  tamanio: tamanio,
                  tiras: [valoresTira],
                  color: color,
                };
              }
              else{
                // Si el nombre ya ha sido visto antes, incrementamos la cantidad de veces visto y agregamos la tira a la lista de tiras asociadas
                parejasDeTiras[valoresTira[0]].cantTiras++;
                parejasDeTiras[valoresTira[0]].tiras.push(valoresTira);
              }
              
            });
          });
        });
      }
      for(const nombreTira in parejasDeTiras){
        if(parejasDeTiras[nombreTira].cantTiras > 1){
          const linePos = 225.5 + 8.7 * textCounter;
          nameInPDFview = "2 Tiras - " + nombreTira;
          // Texto
          doc.text(nameInPDFview, 5, linePos);
          // CM
          doc.text(parejasDeTiras[nombreTira].tamanio.charAt(0), 109, linePos, { align: "center" });      
          // Fuente
          doc.text("-", 90, linePos, { align: "center" });
          // Estilo
          doc.text("AS", 132, linePos, { align: "center" });
          // Color
          doc.text(parejasDeTiras[nombreTira].color, 170, linePos, {align: "center"});
          // Curva
          doc.text("C1", 203, linePos, { align: "center" });

          textCounter++;
        }
        else{
          const linePos = 225.5 + 8.7 * textCounter;
          nameInPDFview = "Tira - " + parejasDeTiras[nombreTira].tiras[0][0] + " " + parejasDeTiras[nombreTira].tiras[0][1];
          // Texto
          doc.text(nameInPDFview, 5, linePos);
          // CM
          doc.text(parejasDeTiras[nombreTira].tamanio.charAt(0), 109, linePos, { align: "center" });      
          // Fuente
          doc.text("-", 90, linePos, { align: "center" });
          // Estilo
          doc.text("AS", 132, linePos, { align: "center" });
          // Color
          doc.text(parejasDeTiras[nombreTira].color, 170, linePos, {align: "center"});
          // Curva
          doc.text("C1", 203, linePos, { align: "center" });

          textCounter++;
        }
      }
    }

    if(isChomba){

      colores_tejidos.forEach((valor,clave)=> {

        if((clave.includes("1") || clave.includes("2")) && clave.includes("Cuello")){
          tejidos_cuello_12.push(clave, valor);
        }
        if(clave.includes("3") && clave.includes("Cuello")){
          tejidos_cuello_3.push(clave, valor);
        }
        if(clave.includes("4") && clave.includes("Cuello")){
          tejidos_cuello_4.push(clave, valor);
        }
        if(clave.includes("5") && clave.includes("Cuello")){
          tejidos_cuello_5.push(clave, valor);
        }
        if((clave.includes("1") || clave.includes("2")) && clave.includes("Puños")){
          tejidos_punios_12.push(clave, valor);
        }
        if(clave.includes("3") && clave.includes("Puños")){
          tejidos_punios_3.push(clave, valor);
        }
        if(clave.includes("4") && clave.includes("Puños")){
          tejidos_punios_4.push(clave, valor);
        }
        if(clave.includes("5") && clave.includes("Puños")){
          tejidos_punios_5.push(clave, valor);
        }

      })

      doc.setFontSize(10);

      if(tejidos_cuello_12.length !== 0|| tejidos_cuello_3.length !== 0 || tejidos_cuello_4.length !== 0 || tejidos_cuello_5.length !== 0){
        doc.rect(140, 153 + 8.7*2, 3, 3);
      } else{
        doc.text(color_cuello_chomba, 140, 155 + 8.7*2, {align: "center"});
      }

      if(tejidos_punios_12.length !== 0 || tejidos_punios_3.length !== 0 || tejidos_punios_4.length !== 0 || tejidos_punios_5.length !== 0){
        doc.rect(140, 154 + 8.7*3, 3, 3, 'F');
      } else{
        if(color_punio_chomba !== undefined){
          doc.text(color_punio_chomba, 140, 155 + 8.7*3, {align: "center"});
        }
      }

      doc.setFontSize(10);

      if (tejidos_cuello_12.length !== 0){
        let tejido = tejidos_cuello_12[0].split(" ");
        tejido = tejido[0] + " " + tejido[1];
        let texto = tejido + " COLOR " + color_cuello_chomba + " CON 1 LÍNEA " + tejidos_cuello_12[1];
        posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
        posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
        doc.rect(3.5, posicion_cuadrado, 3, 3);
        if(contador_posicion_texto === 0){
          doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
        }else{
          doc.text(texto, 9, posicion_texto);
        }
        contador_posicion_texto++;
      }
      if (tejidos_cuello_3.length !== 0){
        let texto;
        let tejido1 = tejidos_cuello_3[0].split(" ");
        let tejido = tejido1[0] + " " + tejido1[1];
        let color_tejido_A;
        let color_tejido_B;
        if(tejido1.includes('A')){
          color_tejido_A = tejidos_cuello_3[1];
          color_tejido_B = tejidos_cuello_3[3];
        }
        else{
          color_tejido_A = tejidos_cuello_3[3];
          color_tejido_B = tejidos_cuello_3[1];
        }
        if(tejidos_cuello_3[1] === tejidos_cuello_3[3]){ //Mismos colores
          texto = tejido + " COLOR " + color_cuello_chomba + " CON 2 LÍNEAS " + color_tejido_A;
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3);
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto, { maxWidth: 170});
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
        else{
          if(tejidos_cuello_3[2]){
            texto = tejido + " COLOR " + color_cuello_chomba + " CON 2 LÍNEAS ARRIBA " + color_tejido_A + "/ ABAJO " + color_tejido_B;
          }
          else{
            texto = tejido + " COLOR " + color_cuello_chomba + " CON 1 LÍNEA " + tejidos_cuello_3[1];
          }
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3);
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
      }
      if (tejidos_cuello_4.length !== 0){
        let texto;
        let tejido1 = tejidos_cuello_4[0].split(" ");
        let tejido = tejido1[0] + " " + tejido1[1];
        let color_tejido_A;
        let color_tejido_B;
        if(tejido1.includes('A')){
          color_tejido_A = tejidos_cuello_4[1];
          color_tejido_B = tejidos_cuello_4[3];
        }
        else{
          color_tejido_A = tejidos_cuello_4[3];
          color_tejido_B = tejidos_cuello_4[1];
        }
        if(tejidos_cuello_4[1] === tejidos_cuello_4[3]){ //Mismos colores
          texto = tejido + " COLOR " + color_cuello_chomba + " CON 2 LÍNEAS " + color_tejido_A;
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3);
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
        else{
          if(tejidos_cuello_4[2]){
            texto = tejido + " COLOR " + color_cuello_chomba + " CON 2 LÍNEAS ARRIBA " + color_tejido_A + "/ ABAJO " + color_tejido_B;
          }
          else{
            texto = tejido + " COLOR " + color_cuello_chomba + " CON 1 LÍNEA " + tejidos_cuello_4[1];
          }
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3);
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
      }
      if (tejidos_cuello_5.length !== 0){
        let texto;
        let tejido1 = tejidos_cuello_5[0].split(" ");
        let tejido = tejido1[0] + " " + tejido1[1];
        let color_tejido_A;
        let color_tejido_B;
        if(tejido1.includes('A')){
          color_tejido_A = tejidos_cuello_5[1];
          color_tejido_B = tejidos_cuello_5[3];
        }
        else{
          color_tejido_A = tejidos_cuello_5[3];
          color_tejido_B = tejidos_cuello_5[1];
        }
        if(tejidos_cuello_5[1] === tejidos_cuello_5[3]){ //Mismos colores
          texto = tejido + " COLOR " + color_cuello_chomba + " CON 2 LÍNEAS " + color_tejido_A;
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3);
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
        else{
          if(tejidos_cuello_5[2]){
            texto = tejido + " COLOR " + color_cuello_chomba + " CON 2 LÍNEAS ARRIBA " + color_tejido_A + "/ ABAJO " + color_tejido_B;
          }
          else{
            texto = tejido + " COLOR " + color_cuello_chomba + " CON 1 LÍNEA " + tejidos_cuello_5[1];
          }
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3);
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
      }
      if (tejidos_punios_12.length !== 0){
        let tejido = tejidos_punios_12[0].split(" ");
        tejido = tejido[0] + " " + tejido[1];
        let texto = tejido + " COLOR " + color_punio_chomba + " CON 1 LÍNEA " + tejidos_punios_12[1];
        posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
        posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
        doc.rect(3.5, posicion_cuadrado, 3, 3, 'F');
        if(contador_posicion_texto === 0){
          doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
        }else{
          doc.text(texto, 9, posicion_texto);
        }
        contador_posicion_texto++;
      }
      if (tejidos_punios_3.length !== 0){
        let texto;
        let tejido1 = tejidos_punios_3[0].split(" ");
        let tejido = tejido1[0] + " " + tejido1[1];
        let color_tejido_A;
        let color_tejido_B;
        if(tejido1.includes('A')){
          color_tejido_A = tejidos_punios_3[1];
          color_tejido_B = tejidos_punios_3[3];
        }
        else{
          color_tejido_A = tejidos_punios_3[3];
          color_tejido_B = tejidos_punios_3[1];
        }
        if(tejidos_punios_3[1] === tejidos_punios_3[3]){ //Mismos colores
          texto = tejido + " COLOR " + color_punio_chomba + " CON 2 LÍNEAS " + color_tejido_A;
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3, 'F');
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
        else{
          if(tejidos_punios_3[2]){
            texto = tejido + " COLOR " + color_punio_chomba + " CON 2 LÍNEAS ARRIBA " + color_tejido_A + "/ ABAJO " + color_tejido_B;
          }
          else{
            texto = tejido + " COLOR " + color_punio_chomba + " CON 1 LÍNEA " + tejidos_punios_3[1];
          }
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3, 'F');
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
      }
      if (tejidos_punios_4.length !== 0){
        let texto;
        let tejido1 = tejidos_punios_4[0].split(" ");
        let tejido = tejido1[0] + " " + tejido1[1];
        let color_tejido_A;
        let color_tejido_B;
        if(tejido1.includes('A')){
          color_tejido_A = tejidos_punios_4[1];
          color_tejido_B = tejidos_punios_4[3];
        }
        else{
          color_tejido_A = tejidos_punios_4[3];
          color_tejido_B = tejidos_punios_4[1];
        }
        if(tejidos_punios_4[1] === tejidos_punios_4[3]){ //Mismos colores
          texto = tejido + " COLOR " + color_punio_chomba + " CON 2 LÍNEAS " + color_tejido_A;
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3, 'F');
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
        else{
          if(tejidos_punios_4[2]){
            texto = tejido + " COLOR " + color_punio_chomba + " CON 2 LÍNEAS ARRIBA " + color_tejido_A + "/ ABAJO " + color_tejido_B;
          }
          else{
            texto = tejido + " COLOR " + color_punio_chomba + " CON 1 LÍNEA " + tejidos_punios_4[1];
          }
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3, 'F');
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
      }
      if (tejidos_punios_5.length !== 0){
        let texto;
        let tejido1 = tejidos_punios_5[0].split(" ");
        let tejido = tejido1[0] + " " + tejido1[1];
        let color_tejido_A;
        let color_tejido_B;
        if(tejido1.includes('A')){
          color_tejido_A = tejidos_punios_5[1];
          color_tejido_B = tejidos_punios_5[3];
        }
        else{
          color_tejido_A = tejidos_punios_5[3];
          color_tejido_B = tejidos_punios_5[1];
        }
        if(tejidos_punios_5[1] === tejidos_punios_5[3]){ //Mismos colores
          texto = tejido + " COLOR " + color_punio_chomba + " CON 2 LÍNEAS " + color_tejido_A;
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3, 'F');
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
        else{
          if(tejidos_punios_5[2]){
            texto = tejido + " COLOR " + color_punio_chomba + " CON 2 LÍNEAS ARRIBA " + color_tejido_A + "/ ABAJO " + color_tejido_B;
          }
          else{
            texto = tejido + " COLOR " + color_punio_chomba + " CON 1 LÍNEA " + tejidos_punios_5[1];
          }
          posicion_texto = 315 + 7.5 * contador_posicion_texto;
        //posicion_texto = 308 + 7.5 * contador_posicion_texto;
          posicion_cuadrado = 312 + 7.5 * contador_posicion_texto
          doc.rect(3.5, posicion_cuadrado, 3, 3, 'F');
          if(contador_posicion_texto === 0){
            doc.text(texto, 9, posicion_texto);//doc.text(texto, 43, posicion_texto);
          }else{
            doc.text(texto, 9, posicion_texto);
          }
          contador_posicion_texto++;
        }
      }
    }

    // Observaciones
    posicion_texto = 315 + 7.5 * contador_posicion_texto;
      //posicion_texto = 308 + 7.5 * contador_posicion_texto;
    if(contador_posicion_texto === 0){
      doc.text(commentME1+commentME2+commentME3+ observaciones, 9, posicion_texto);//doc.text(commentME1+commentME2+commentME3+ observaciones, 43, posicion_texto);
    }else{
      doc.text(commentME1+commentME2+commentME3+ observaciones, 9, posicion_texto);
    }
    if(commentME1){
      doc.circle(5, posicion_texto - 1, 1.8); // reference for ME in comments
    }

    // (Optional) beautiful indented output.

    var pdf = doc.output('blob');
    var formData = new FormData();
    formData.append('pdf', pdf);
    // $.ajax( base_url + '/followme/api/upload_pdf/'+orderN,
    // {
    //     method: 'POST',
    //     data: formData,
    //     processData: false,
    //     contentType: false,
    //     success: function(data){console.log(data)},
    //     error: function(data){console.log(data)}
    // });

   doc.save(`ficha-${orderN}.pdf`)

  }

  const generatePdf2 = (images) => {
     var doc = new jsPDF({ orientation: 'landscape', unit: "mm", format: [610, 360] });
     mda = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABdMAAAJwCAYAAABiR/jyAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDoAABSCAABFVgAADqXAAAXb9daH5AAADQTSURBVHja7N1rchs5lgZQwcElTO9/gTN7wPzodpcsS0w+Esj7OCeiIma6XBIJXFwAn1L0mHN+AAAAAAAAP/tlCAAAAAAA4D5hOgAAAAAAHBCmAwAAAADAAWE6AAAAAAAcEKYDAAAAAMABYToAAAAAABy4vfsF/u9//9cocqaZ/PUPUwgAAAAA8fzPv/711n9/M4RcbDZ6P4J2AAAAAEhKmM5u03v/g4AdAAAAABIQprPDNARPjY2AHQAAAACCEaazigD93LETsAMAAADAhYTpnE2IvmdchesAAAAAsJEwnbMI0a8db+E6AAAAACwkTOcdAvSYcyFYBwAAAICTCdN5hRA91/wI1wEAAADgTcJ0niFEzz9vgnUAAAAAeIEwnUcI0WvOpWAdAAAAAB4kTOceIXqf+RWsAwAAAMAdwnR+IkjvO9+CdQAAAAD4QpjOV0J0BOsAAAAA8IUwnd+E6BzVhWAdAAAAgLZ+GQI+BOk8XidqBQAAAICWPJnem2CUd+vG0+oAAAAAtODJ9L4E6ZxVR2oJAAAAgPI8md6P4JPVdeVpdQAAAADK8WR6L4J0dtWZWgMAAACgFE+m9yHc5Mqa87Q6AAAAAKkJ0+sTohOpDoXqAAAAAKTkY15qE6QTsSbVJQAAAADpCNPrElgSvT7VKAAAAABp+JiXmoSUZKxVHwEDAAAAQFieTK9HkE7m2lW/AAAAAIQkTK9FEEmVOlbLAAAAAIQiTK9D+EjFmlbXAAAAAIQgTK9B4Ej1+lbjAAAAAFxKmJ6fkJFOta7eAQAAALiEMD03wSJd617tAwAAALCVMD0vYSLWgHUAAAAAwCY3Q5CSABG+Xw/DcAAAAACwgifT8xGkw/31YY0AAAAAcDphei5CQrBWAAAAALiAMD0P4SA8v2asGwAAAABOIUzPQSAI760fawgAAACAtwjT4xMCgrUEAAAAwMWE6UAnnlIHAAAA4CXC9NiEfrBubVlfAAAAADxMmB6XoA+sMwAAAACCEKYD3XlKHQAAAIBDwvSYBHtwzbqz9gAAAAD41s0QhCPMe90wT5w0t8MwAAAAAPCZMJ2sRtDvL2SvYQapMwAAAACCEKbHIoj92Uj8Os1r7jUpUAcAAABAmB6IwPVvo/D7MN/51qZQHQAAAKAxYTrRjKbvU7gen1AdAAAAoDFhegyCVAHlUA+p1qtAHQAAAKAZYTpXE0oej4lwPR5PqQMAAAA0I0y/XtegVAj52lgJ1uOtX7UMAAAA0IAw/Vodg1HB43njJ1iPtY7VNgAAAEBhwnR2ETSuHVPB+vU8pQ4AAABQmDD9Ol3CT+Hi/nEWrF+/rtU9AAAAQDG/DAELCRSvG/dh/C/lBxoAAAAAxQjTr9EhaBPkxpkHwbp1DgAAAMCbfMwLZxPaxp8bIe8+PvYFAAAAoAhPpu9XOcgUGOaZJ0+rW/cAAAAAPEGYzlkEs3nnzdztIVAHAAAASMzHvOxVNUwTxtaaQ6Hv+h5gzQAAAAAk48l03iUUrDmn5nUtP7AAAAAASEaYvk/F8EzgWptQXU8AAAAA4D+E6cARofo6AnUAAACAJITpe3gqnSpzLljXHwAAAABaEqbzCmEqQvVzCdQBAAAAghOm8ywBKl/rQU2cQ6AOAAAAEJgwfT0BGR0I1fULAAAAgNKE6TxDWMojNaJO3iNQBwAAAAhImL5WpVBMQMqz9aJmAAAAAChDmM4jhKKonb08nQ4AAAAQjDB9HWEY/Jun1PUQAAAAgPSE6RwRgnJmLaknAAAAAFISpgO7CdUBAAAASOdmCJao8vEMAk921JePM7nfS6zDx+vEWJ2/TxlTAAAA+A9hOnC18SFQ53vzzT8vCH5vPL/+N8YTAACA1oTp/ERowhX1JlTnzDqY+pnxBAAAgLMI089XIQwUlHB17QnV9c8VX3cYT+MJAAAAr/IXkAIRCel6mR97foAym41plXkDAACAEITp5/JUOpxbi+pR31zx/Wbx8ZzF5xAAAAAuIUwHohOq1zWbfm/jCQAAAAkJ0/lMYEn0+lSjdUyvwXgCAABAJsL08wgRYA+Bun6pdxtPAAAA2E6Yzm8CSrLVq5rNaXpNxhMAAAAyEqYDmQnUAQAAANhCmH6O7E/hCSTJXr+j8HvTK7024wkAAAABCNOBKnz0CwAAAADLCNMRPqKm2WF6jcYTAAAAMhOmv09YAPF4Sh0AAACAUwnTgcqG1x6CHzoaTwAAAEhPmN6bJ3fpUudqnUcJqo0nAAAAfEuY/h4hAeQxvFYAAAAAXiVMBzrxlDoAAAAALxGm9yVQRP17bTv4DR7jCQAAACUI04GuIj6l7odcVOQHAAAAAJQgTH9d5nBAYAfx1oN1qb8DAAAAgd0MAcB/g+x58fcHAAAgjlfviO54UJQwHeDPA8+84HsCAABwvbno67j3QRHC9Gub6xU0cHhsjUzrEQAAoLy58Xu4A0JywnSA760M1R2gAAAArjUv/p7uhZCQMB3gvjNDdYclAACAa81Ar8MdEZIRpveiScM562daewAAAKnMwK/JnRGSEKbXaL7AXg46AAAAOcwkr9E9ExIQpgMAAABQzUz6eoXqENgvQwAAAABAIdNrB1YQpvfhJ5sAAABAZfOjRhgtUIeghOmaGQAAAEB20/sBVhOmAwAAAJDZ9L6AHYTpPfiIFwA9EwAAKpreH7CLMB0AAACAbKp8Pvqj7xUIQJiucQEAAABkMr1n4ArCdAAAAACymN47cBVhen0++xcAAACgBoE6XEiYDgAAAEAGgmTjAJcSpmtSAAAAANHJZowHXE6YDgCs5OPGAAB4l+AYCEGYXpsAAwAAAMhMkG5sIAxhOgAAAADkJFCHjYTpAAAAAEQkKDZOEIowXUMCAAAAiEYeA4QjTK/L56UDAAAA9OCHD7CBMB0AAACASATDxg1CEqYDAAAAAMABYToAAAAAUXi62vhBWML0mg3I56UDAAAA2QiCgdCE6QAAAABQhx9KwCLCdAAAAACuJgAGwhOmAwAAAEAtfjgBCwjTAQAAALiS4BdIQZher5H7y0cBAAAAAE4mTAcAAADgKp5KN7aQhjAdAAAAAAAOCNMBAAAAAOCAMB0AAACAK/gYEmMMqQjTazUaf/koAAAAAMACwnQAAAAAdvPENJCOMB0AAAAA6vKDCziJMB0AAACAnYS7QErCdAAAAAAAOCBMBwAAAIDHDUMAPQnT/5b1V400cgAAACA6H/Fi3CEtYToAAAAAPMbDjNCYMB0AAACAHbI/HS1Ih+aE6QAAAABw3zj4/4EGhOkAAAAAUJ/PTYc3CdMBAAAAWC1zkOspdODj40OYXqWxa+oAAADwnvnhyV2eI4+BZm6GAAAAAGhkvvHvhaf9mHPgv4TpAAAAQAdzwdcQtO4be86bC3ULLxKmAwAAAJXNDV9bOFmTeQX+4DPTAch4aQEAgEfOo7Pg9yIWgTs04sl0AAAAoJIZ4HsLWGPMxzvMIfAXT6YDgEsIAIAz0/mvw/kNoBhherwNFwAAAKhxr5c15DSsFeA7wnQNHgAAALKbXpv3fiHZDDQhTAcAAAAym0leo6eBAZITpgMAAABZTa+Xk3nKHPiRMB0AAADIaHrd3ifATsJ0AAAAIJvp9bPAsIaAe4TpAAAAQCZCQCLy8TDQgDAdAByAAQCymN6L9wZwFWG6Jg8AAADu7t5Tdx6sAQ4J0wEAFxIAIDpPcQNwOWE6AAAAwLUE6tfyEAjwEGE6AAAAEJmg2XxlIZSH4oTpmjQAAABENb1XUHMQhTAdAAAAiGh6z2zgQUXgYcJ0AAAAAAA4IEwHAAAAopneu9cOEI0wHQAAACAWofQePuIFeIowHQAAAIhEkGwcMhPQQ2HCdAAAACAKAbK5AwhLmA4AAAAQk4B6HU+QA08TpgMAAAARCI7BuoPQhOkAAAAAcQk7AYIQpgMAAABXExibv518xAvwEmE6AAAAQGx+2AAQgDAdAAAAAM7jyXco6mYI/HQXAIAlZ0YXaQD38rPHyd6ipoELCdPzsoECACsvc8O4nf41nN8AsGdez34MvEyYDgDgUnv0NYaxO/1ru8gDrO+7VcfLHhLfUNtQkzAdzj/UOdgAkGnfevZrD2N36vd0bgAAgCSE6XQlcADAnvb69xvG7vTX4MwA2MMAIDhhOg5lLsgA2O867m8z4OtxXgAg257hByJAK8J0XHxdkAGwH777OoaxazueAN3uduRmj/15PRobeIAwHQcpF2QA7JldLmAz0Rw7LwBgrwAIRpiOC6tDD1j/YG112N9m0rl2ZgAAgCCE6VS9gGYYT5djAKrtxVH3t5l83p0ZAHsaAAQgTM/plQuVw4rLMeDCiRrouL/NIvPvzABAtP0h2x5rLwXeFilMFxQYK4cfwOEaauz7Ufa3WawO9CnAvgYAF/oVYBOdNlMcJgGg3B4xjZ33BAAAlVwRpgvQAaA+T9Cec2byHvqOXcf3BoB9ASC0nWG6AB0cgADoty9MY+c9AuhfNOThEihoV5huwwTrBAD7gbEDAHvuFQTbwCl2hOkuNQAALtzOnN4vAPYE1BGk9mvxIrQQweYFgH3AHmdMAfQsANJbFabbJAEA2MXZEwAAWG5FmO4yAwDgDOWcaEwB9KqaY2w+gbbODtM1VAAAdnL+NAYAZ/IXVZpTgB/9MgTgQgyA3u+9AgAA3HdmmO5iBADATs6fxgLQn840jDUA93gyHQAAAOAfPhYEgG+dEabPDz/9BAB45yzlPRs3tQRwLQG6vUZdAYfOCNM1BgAAAOCrzMHrMOYlyKyAU531ZDoAAAAAAJTlM9MBAMjGwxzGBuBMnl4G4CHCdAAAAOBsFX64J2QH4A/CdAAAruAJagCwx6J2IBVhOgAAANCVp88fN80t0J0wHWz4AOAib4wA9CH3MwAOCNMBAAAAAOCAMB0AAADoqPJT535LCWCBmyEAhzmAZh69XOrJALBunwWAdITpAIBL/eP/nYD9PMYSgEx7VvcfEvjLRwE+hOlgswdw4Xv16+rbAAA8ctf3GxtQhDAd4myuALxnXvj99HEAyBUYjibz4YwCcCJ/ASkA4PJ+7fd3yQWA+OzXAAjTwaEMIL0Z6HX4FV57HwAAUJQwHa4lTAB4XdTwWqAOAFQ7c7lnA3ycE6ZrUmDtAPDepdOeYKwAuu199hIA0jnryXQbEVgzAC7s+V6jPRAAau9hziMAJ/IxL7D/8CVEAOhzKXzmtdofOPO8AQAAnOzMMN2hHawRgNU8XWUvBMD+DACXOPvJdBck+H5dWBsAfXk6fc3eCgD2EAC28jEvsO6AJkQHONds8tqr7h32RADsZ/nOUNP8APzjtqhx+RUvHKQAAPulsQYAgDJWPZnuEE/Vy+nXfwDYYzZ7D5X2mJHs6wLQb48GgIfcFn5tT6jj0g8Avc9S9lTjDQAAZdwWf32BOi6bgJ7Au2bz2pzWlbMmAM58AFzvtuF7DBdhHJQA4L/noWf3p4yBsT3YmANU77nZ9uZXziByHIAvbhu/lyeHcGkE0J95b0yn+XfGBAhE/8UZFGjltvn7eUodmyAAvLc/zuCvD+MOAAAl3S76vp8P/IJ1lz0A4Pm9dAZ8TVd+/9m8HgCA2Pu1/AsKuAV4DfcuABqNixyQj96NeupzKRvGAwAA6OIW/PWtvqC5cOW5IANAFa/8BWBHe/XuM03UM0K3QN1ZDbh6P9MzwVkVWrk1f/+dLlwaIQD02Ofnpu/jfOdsB9B978223whJAd50MwQuTgBA2bPCPPnrZRqD2WSOAbjG9P4A+hGmAwCR+Rzs98fvkQvyUDvp5xQAAFjslyFIR6AAALmNIK/h8z/G2vsBQE82HwAHhOkAEJsfooLLdfUfegDOJwCQgjAdAIDqhtcOAAC8S5gOAEAHGZ/uFqQD6KcABCJMBwBcgFFP8V6jugdghXnSnwFo6WYIAAC2EZDGmoepPgCeJmgFoC1hOgAAXUUJ1YXoAACQgDAdAMhgfOR/Ek5gmmNupnp427QWAOcTACoSpgMAwD9WBuudA/Sf/qxgHQCANITpAEAWmZ/+EhjWmjdPXj82Ds98DWsEsAdjjoHwhOkAQLbLkl+nxqX9enPB1zOu0G/tA0AqwnQAgLUEhFQyN3xtawagZp8HSO+XIQAAkhleK1xiFvs+AADwFGE6AJDR8Bphq1n8+wGAsyFwSJgOALiQuCxBRAJ1sCadRwAIRZgOALjAulTDPbPp9wbQ11EjwB/8BaQAQHYjyKFfiA4AMc8IAHAKT6YDAC7MLuvwk+k1AADAvwnTAYBKxsfeYHv394OuBOoAeu1PZzGAbXzMCwBQ0eqPfnFxAwAAaEaYDgBU9jn0nid+LWC/aR3CpesPANoTpgMAXdwL4eaDfw4A4Lszhh84ADQgTAcAEKBDFp5OB+ztAFxGmA6ccal1aAXQ6wGAXHu7/RzgScJ04OuB6oqv4RAHoNfDM/WnniDXvgEAJQjTwaE48mtxUQbQ6wGAXmeGRzlDANsJ08HBKNvrdmAC0OvB0+kAAGwnTIfal8wO78tFGtDr9XoAAIDlhOlQx/S+hS2AXq/Xg/5gnQAArCFMh1oXJ2PhEgno9Xo9ZxqB66/zR73MJ/+9tUK3/UfNA7CEMB0cajuMkcM0oNfr9dC5L1grsF7kHz4CcBJhOvS4QBk3F0hAr9frQV/4/bWsEUA/AHiBMB36XaCM5b85NAJ6vV7PYzxtWa83WCMAAC8QpkO/yxN/j62LJKDX6/XknePRpIaNH+AsYl8HLvbLEGjAhD3MCFeMN6D3YLydmXvXbKXvAwCQnifToeelifvjLzAA9Hq9HvQJ1CFwNh+dBsl5Mh3iHFRtqOYD0FswH9EDgKhziTEEAFhOmA4uL9yfG/MD6PV6PXToF9YBAMABH/MC/S5KvD5XPhIA0Ov1+u78ejqQoU8BwBKeTIdrLusuoXnnDkCv1+uhav2pfwCAO4TpDtmYO56fQ/MI6PV6PdYdwFcZn4qfxhngccJ0cEjh9fk0p7gsoNfr9foS+gfqEQCaEKaDizguGoBej14P6h0A+wEcEKaDzYdz5tlcg16PXg/6CABAYcJ0cCnCnJ/Nr/9j3WPOMTcAAJQjTAeXOc6fe/MPej16fXV+cKq/AQDOou0I00FDwyUU0OvR61FHAEAOHmq40M0QgIsQS2vCJgd6PXo9gL0OgLP78dd/70y6gTAdHDjZUxs2NdDr0eurGQHXhR9uANTfe8Ad4/mvZe2cRJgOsRoftevE5gV6PXo9ZOkt6hkA6uz9nmI/iTAd6l6AcCkF9Hr0eoBO9F/AfeK116R/PshfQAq1miHqBrBmiVE3ase6BXoSSAGfz4PT661FmA5w3SYFxF+roIbuExqpDwDgz7v+LPI++IYwHVx+UEeA9YlaAtCzAF7vndP76kGYDg6YqCcA9HpQuwDw/D45vc9ehOngQoG6AjVtTaKuACA6Hy1GpHPd9L57EqaDSzDqC7AWUV/vGMZaLQBAk7OcPbz5OAjTwYUHdYaxRx2gzgCAPTxh7wxXaUzajYswHcAGDVh7qDcAAL4/tzm7Odv+lzAdNAXUHAB6/bs8ZQf6EoC+2HusWoyXMN2Cxjij9sB6A7VnfL0udQrn8QNGyH+GsP85N3xLmG6jA2xCYJ2BGgQAwNnsrDEsO47CdNBAUYcA6PUA2XlIDnj3POZM5ox7SJgOYAMCawvUIwCAcxjG9oAwHTRS1CQAer0xNccAkfnNA+zNxjiEm/kEABzoIFRtZg0MhrVFwF4ugLMvAuh7zrin8WQ6aKioTQD0e8hSk/Pjuc+1ffbPA2BfY+0enpowHTRU1ChYRwBkuHzPk74OAO4CmIOXCNMBbDwA6PXQaS0I1QHsKZiLl/jMdNBUPz7Wfm6VzWrduPoMUNCT0OsjnimmcdR/koyZ8xSA/QRntKcI06GncfH3spG5AIJDNAARerbzlP0RgGv3oFT7sI95gV4HyBGkSY1v/gEA+p5R4Mq6t85YdecBnKUoNkfCdOhzkBtJXqNDp4MBAHo9fetQvQNgTzFXYQnTobas4bRg3QEBrBMA7BM8c38A0NdZTphu86Vugx2F3odgHQDnFWcQ5z11rk6gJ3dB9HPzFoYwHWoeNEbx9+Yw5bDgAoH1AehDoDYB9DXzt5UwHWoZzd6rgFEtArhcgPoGAHuJedxCmI4FWkfX8FKo7uAAADhreI8AwHLCdKhBmCxUB74nFEFNO5sAAM5ImM9TCNMhP5fVv8ej85g4QACAPRIA7PGwgDAdzZaqPKkOgDMMgF4C6GWY29MI0xUVuQmLjZHegDq0FgD90BiBu4gxBJwRNhCmg0NFl7EyXrjIA3oWqD0A93l7ErxMmA50O7B0ObQ4VAAQYd8FAHDnpQxhOpquy6nLPQA4zwAAgDPvgZv5+MNIdikRCEKf9Q4kPnABoXqDMzTU2ietadDDOhnG5FrCdJcBHBaNZd1NR48AAJwxAKi2B1U3Fvw3s8C8hzh7CNMBPKUOgIsFAES8p4F6X/P1ZSAv8pnpdL10aq50GVsbJAD2V5xVALBnxDwnjYvOS1d93/Q8mQ7w52biQgcAuPQDACuMwK8n+rklxG9kejIdNFyMM1QjvELNY7yMo7MggH0n2v4yErxGDgjTAepvIEIDAACcDd0tQN+6pgcMr7cOYTqAQy8AAABwnuyhtEzkB8J00MQw7gDkF/mJLfspAPYOKp5xfqrdYR3WrQdhOpowOMShFgHgirO1szlA7T7vzuY9lSNMVySAQ5B+CgAAgHvs63df998mhOmQoyljDoBaB25Ar8CZT+0D2DtIRpgOYIMEoAYhGeoOZ3TA/qIHsZAwXXMBAACXTed0AADnmQPCdIA+QYBNEDUHAIB7F+4RPDIH5uEbwnRwiMCc4NAGAPZccIcAsEcfEKYDAADQ/dLtLx8F0Kv0VXvFIWE6gMsWALisAwD0PU/4WJcH3QwBAACUutD5oS8AwN7z12cj6evmAZ5M/54LCAAAOGPjQg6AveSV9/H5n6ivjxd4Mh3g9UDA5gMA8NolHuCnexZU3/fGxd9fj3iDMB2Hdui3DhxQAQDnjEAXc3chgFZ96ui9DmMYlzAdAADofHEfi74uANzjt51xjkjIZ6ZbZN02KlBTAAAAADxNmA4AefgBDqCPEZ0HfdBP9D+wd1C2jwrTAXBpAAB2iRaYOKuA9QjwMGE6ALCSp0wA9H7MBwCUIEwHABdOAAAA9y+iCvObK8L0BJMEAABQgMAEAHhWqIxWmA4AALS9EH0IeDuPqweoAOwV8BRhOgAAwHkEA8YCADhHuB98C9MBAACgJj/QIAu/KQKkIEx3SMK8AgDA6vOscy0IjOG7/QFS9UxhOgAAAC7oAIB9+oAwHcCGBGocgJU8fQgAlLjTCdMBaL0RGk8AvQzQQzCfgH7wCGE6xOdJHnODuTWeAPodqEdAf6KyFD9YE6YXmETMKQCAsw5YJwBgb15LmA4AAAAAwBVS/ZBbmA45+BUocwIA2NfLXkwBgJbSnVeE6QCAAxkAgHOK8WAHD6aRev0L0zUkzC995sJBFQD7kvOBunNeBQD78YuE6ViwAADg3AsA4HxyQJgOuXjCxBwAQMmLCQAA5c+q6c+rwnQAyHkIycYPogB96pp9wg9ZjAUA2INPIkx34MJFD2Ovl5pvNQcA2OMxl7hjsWKdl1rrwnRs2tBj01fzAADOrTibA+zqcyV7nTAdHJAx3gDUuLB43c4K1cfZGAB6E8Sv6dJ1LUwHlzaMMwBg3wcAeFX5EP03YboLAABUPtAB6H16ov0ArEtQj+vGrtX43cw5zRd89h8+TE1/6dg6GAAAzr8AUHdf/O7/tlc+Pm7tCNMhP4H6mjEF9CtwoQHrA1bWpnsHxNwfxjf3GOPFx8eHj3lRMFShsRtL+tWDvck4ATF6f7azg74IgDvy83tnt48zGU3f9yFPpqMh1tkYPPHpkBD9wuzpGwDsD3Q9BznPAtTcW2bR98UPhOlQ7/CsAbp0AH8fDK1zulzoUA/6Imoc8wiP5QBjUT37DefChOlgQzBeDqkAgPOUcxAArN+D5sXfnzcJ06HmkzkugI+NEQCAMzDOt1iH0LdvjgvWPYn5C0gdoqhdB2rBuAAOrahrnLutDQCAEwjTof6B2yXvn3GYahr0JMC+pV+VPi84A4E1Cvb72vP1+Z/tfMzLcw3fAiP75jAav3d4p4Yc+u3lELGe4Yya0BudeQEg6/42d5+RPZkOvS6lnZ7O7vxxLsNrM9cAYF8EgIf4gWS8+Xg101meBQnTQWPyvgAgF4FhjMv1VFPeQ4I61fu8L+8V9NKO87BsPoXpQPbwWYDugArWCFif9B5/tQOgX59BrnDt2M8M8+kz0+Hvxt61eW7/nCkbGwCUu4SiLnA+BsjeV+2rdfay0+dTmG7xwyvNbFz4vXGRhjPWij4DvHOurtpDMvZH5x8Aduz9rBvrVPMpTHf5xjxHbXbgMGU8gb/PKOAsbE1g3dnDwJ2lwvimnE+fmQ7gcArWDGBtxrrwmQd9XF0C6LPGNSBhOjicg/VlPI0dqFfyXgCH9YA5B6znNvt+9jPUzP79hengwIZaBQBcprOcL5x7etYj+glc3XP1XfvWx8eHMB3AwRSsIVCn3i855kJtAHotVxKqGzNheoOCxaYGYI8CZxH0ruw1aC3YQ601UO/6sHG6nDDdBgfoRzgwWUsA+lfUPjn0adCnIOj90JPq98emJGE62PxRl4A1hbr03nlkToY6QD8ArO2/CNUbjcPNPANw0cHJYcN4GkfoccFEr1T/57FPWV9Anh49mr3fFoTp4FBDvloE9HuwHhlBvvcs8H4Adxfs9StUDNfbn82E6fUX+7SBafIAQLozB2Su16nOl93t1BdQcZ136W8Zw3U52BfCdNDkcZnguYOEeag/nno9er31iDoG9AHYcR+KtHacuR4gTAeXOhxGAb0evR4AZxBQ99ebi8+KxvlNwnSAHAcLcAA1puj11iIA9jTs930ZnwB+GQKLDYcD1Jr3BuoUAHc6ezCgB8F9wnQbO2oANQaAfo+aAdCfUD9wQJgOGj1qi+d4KqPfeFqPqC/jBKDngfoHYTpo9KgpwLpEXaFuAPQn1BJq64gwHTR61BJgfaKejBlgvQP6ANW9/ZvRwnTFgkaPGvKejSfGmL01pI7QgwD9CSDhOVyYbrNATaB2AOsVtWMMIS4PRul3YB1AEMJ00OxRM4B1i5oxlt43gP6E+kIdHRCmg2aPWoGrTesXvR4APdN8gTojev0I03vx64GaPWoE/RTrGDViXL1fwNrHXBsjeIEw3WJGfaA2UFsYc9QGANjnUHOomQPCdNDs2V8PaiL/GjGHWNuoB73Y+wT13nlMzBXOWbTsn8J00Oxx4ARrw3tBDRhv7w/QC/QmrAvUxwFhej8+51ezx9wD1jvmHgDsdahHKtbE0roQplu8qBeSN3JAr0evN/7eF6h542OOsDZQC+sJ08GlG5u6sTKe6PVYb+bB+wH0Bn0J5270xwPCdHBRwkYO6PXo9egZgB6hL2F94Ex+QJjek89N1+wxn5hj4+l9Yj7Ni/cAat94mRsqrg91qxcuI0wHzR5z6BJlPNEnMId6H3AlD3zF25PseWRfp2pY/1tCmO7gjoaAOUOvN576BuZMb/G6Qf3bo8wH+gvO5AeE6X35yb9mT4EmrvYxnuYHvZ4S60udAWftWeOi/x6c6WhxVrqZCwjfKPzgQwPH3BtPvR71yWvzONUbatkasI8Bzt762VmE6aDZ40Dqwlh7/o2nXo/atKeoOQDA2duZ/AQ+5qX3YVjTyFdfLlzGXT/FeOo5GHfy90A1BwD2YxLOhyfTIe+F0A9D+l28O87BVAvGU69HPVJsTak7sB6AuL3I+ds+cJcwHTR7HOajz8lUE8ZTX9Lr1SH6IGrXPgDWOc7fzuRXE6YzHepLNSGN3sXWoVJdGE+9HjVIvLWk/sC6AJwZjGcBwvQ1hWGREaExqUOHd71VbUTZq6w3vV6vp2svVINgbQD1epYzeON+L0yHHg1Lo3dorzJ/Qo/z3+s0nnq9Xo96OX0NqUOynoUAcAZ3BrpDmM7vhe/A36uZzabvmzpzOtVK6Iu5tafX6/V0vBirRbBWAD1tGoPahOnrishPpsjW6GbR90X9Gp5qxnii14M6w10TAGdwZ6TVhOlAxobvgstP9TDVj/FErwcg1T4E0Ln3OYsnI0zn8+K1UHi1efqICGz4xhO9Xn0B7OvbM+nrBuD53ugcHogwHdCIAdDrAfL15Jns9QL5eRBTD23vlyFQ6AAAALhzAgD3CdP5zF9kAwAAkMfwGgFgH2E6AAAA5DW8NgDYQ5ju4PCVp9MBAADcPTvehwHgLmE6AAAA5De8FrCOgbVuhgAAAABK+B3EzYu/PwCU5Ml0h4nv+KgXAACA3PfQccH3BIDSPJkOAAAANX0OuOfirw8A5QnT+cl0MAIAACjjrGDdPRGAtoTp+w4tPjoFAACAKHfU78wn/iwAtCNM5x5PpwMAAPTh/gcAd/gLSAEAAACAR/jkBVoTpqNJAgAAAAAcEKbv49flAAAAAACSEqYDAAAAAMABYTqP8FEvAAAAAEBrwvS9fNQLAAAAAEBCwnQe5el0AAAAgLU8iAmBCdMBAAAAAOCAMH2/zD9h9HQ6AAAAANCSMB0AAAAAAA4I03mWp9MBAAAAgHaE6dfwl0kAAAAAkJEHLWlLmI6mCQAAAABwQJgOAAAAAAAHhOnXyf5RL55OBwAAAADaEKYDAAAAAMABYfq1PJ0OAAAAAJCAMB0AAAAA4hiGAGISpvMuT6cDAAAAAOUJ069X4aeNAnUAAAAAoDRhOgAAAADwDA9W0pIwHU0UAAAAAOCAMD0Gf7EEAAAAAEBgwnTO5Ol0AAAAAKAkYXocVZ5OF6gDAAAAAOUI0wEAAAAA4IAwnRU8nQ4AAADwOn+/HgQkTNcoAQAAAOBZHqakHWE6GioAAAAAwAFhejyVnk4XqAMAAAAAJQjTAQAAAADggDA9Jk+nAwAAAAAEIkwHAAAAAIADwnR28HQ6AAAAAJCaMD2uYQgAAAAA2sqQDXmAklaE6WiuAAAAAAAHhOmxeTodAAAAACAAYToAAAAAABy4GYLwxoePSAEAWO2R85bfGgQAgMaE6QAAdDZf+LNCdQAAaMjHvORQ5cLmCXsAoMLZZDrXAAC8faaCdITpAAC49AEAxOQ34iAQYbrmCQDAawTyAADQiM9Mz8VfRgrAT+advQMAAAB4kzAdAHKbT/x7wTo8tm5QH/fopQAATfmYl3wc3gH4bS7+8wD66vd/Rj8FAGhImA4AOc3N/x1A9Z7qB5QAsP9+AqkI03MaXjeAg6qDLkD6fgwAQCLCdADIRXADoK8C0IuHEyEIYbpGCgAAAADAAWE6APTkSUx4n4cb9EIAABoRprvAea0AAAAAvMsPqSlPmJ6fkBoAAAAAYLGbIWADgT/3THXz0JhYRwAAAAAXEqbXMD7i/iqNAJDvzCf+zDAuf/076woAAABgMx/zUodwjSzm4j+fdUzmwj8PAABAbnIfCECYjkbPTnPzf1d5TKqPCwAAAL3uuBCeML2W4bVgQzWuABBwr/r8DwAApOQz0+uJ8PnpgnS+u0SzblymdQdAoj3O3wMCAEBKnkyvaTT93vS9lAMAOfdsT6sDAJCGML2u8bE32N79/QAAiMffjwIAQFnC9PpGke8BAEBsAnEAwJmA0oTpPax6atzT6AAAuHgDwB4yGLiYv4C0b9OdJ3wNuOKSPQq9FwAAAACSEKb39TWQnA/+OQAAAACAdoTp/CY0B+sXoAO/GWRcAQDgJT4zHdhB2AsAOOMAQB9+0E5JwnTAhuw9AAAAAHBAmA4AAAAAAAeE6cAufg3auAKAPQ0A7JeQljAdyMjHpAAAzhMAYK+FrYTpAACAizIAABwQpgPsJXAAAAAASEiYDuzks92MJ4AeDADQhwfKKOVmCIDEG7IgAwDIdn75zFkGNed9ApCIMB2AVZeYD5caFtSRWlIf6oNK9TqL17A1G2/cq4TO84l/r8Z61b35BpYSpgO7jY++v+Y1m7+X6oEB+9aEWlIfz/x3aiPP+cBe//fXGA3XrHW7dx4y/rZnh/eYeezHxa+nS0/Jdq+2DilDmA6Q9/CUMSCoEhgQq47UkvpQG1TubaPp+7Zu981HppCrw3vMPu67xnq+8GfVAPA2YTqQ/VDnQJTzkuzirJZ2fK8OT3SOE77+aFgbGNNM7z9iT9PTrcWM7/HKu0PmfX4GHGs/VAEuI0wHIOKFzMVZLXWoo7nhPc7A4yTYhufXTKcffjkb9KsvZ6D7X2Mk3OdH0HFFn4GX/TIEwAVsoPUP/g7LzEDzNwPW9dzwNWfgcbK286/taR5b7YvRejpErp9V6+WKfX6qAYA/eTIdqHAYGklep9dYe45RS1Eu8CPwOLnAWs/Pfh/9//peNgPXpfqgW788o/adrXG3gzcI0wHy6fjrki7NLpGZD/DTPKSpD+LNmb9P47pepqejH8V9bVMd8J/+Z0xgMx/zAvD3Aa3Tr69ne58Oi+Ym22tVs8YBdWSsvVasFeMDUIQn04GrRPsp+jz43ys+9TQTv25PoamlDHXkAmkczJs9wNyrC7Xz5/nfWgH0bFLzZDpgI3/sIBzlsDxOfN8uL3Sfi7nw66pRaxWsWa/debrmHNnnARoTpgOdL34z6PfBuJqDvO9BXRoL0L/0nU7jN5vNkRrLP/bVnoge1hLsJUwHbPwumtC9jqYxUR9gzXovbBtrc9Trvma+gVKE6YDDv/ft/YBaMpawzvjyT7T1Zs1ir7BWwJqCB/kLSAFe2/SveKo+6pP844Xxw8H33VqaTcYkw5qO9vr0GKLX6WhQq1efDfzlds7U9nkAlvBkOpDtsnXGAddT6efN3dj435nP2nU0Nv135KqPV3qH2oi1Pxu72GNc9WxA7n6PellVb1X7zGhcG7CdJ9MBXt/0x4N/zmHt/tdxgOrrzFqaG9f1znU+XDjeqpPhskbSvuZscF1PR61W3+dfeYBhBpg3+zgQgifTAXo44/C56qkxF91edbSqlq56qnHlR96MO//7KFwfq/qDXlNjzIbxOm3tRX7S03rtM2az8Xxl2+fVCeYXPoTpgI2aGBe0YV7VUpKvf9XluuJ7veL9CSfW1uRH4/U9mtbkaDrf3c/K03jY5xOe1/UU4BTCdMDlOMeBcFz4Ooc6oNEcz2TjY92wsz6//lPp+3Vz1m+sRd83/LBdvWY/B/lhJuYIgvGZ6UC1g7ZDBLjIGpP8Y6GXCwC8fu8Z7PP/rDdnDtzlIQhPpgMOxOu+ZoVD7yj+/ag5t6PZ+Fg3gP0DZ3R1XHWu/SABCEWYDjhgOhgD+ojxgD6mNQr2NXPgfRbcn2ALYTpg44z/uq/6vHQHf86q82FMAPQk1Bqpz1LuBgAfPjMdiHVAXHk4d/CvP2Y+Z08tuRwbD8hYs9k+D1lP73uezn6OG+rcmR3gXZ5MByoezqD6RRki1Ze+C+A8TY9z21CLmHO6E6YDNmQbPgAAQJX72WfdHkTx4A0sJkwHiH1AdBgyVgAAlc630b8nAPxImA5EkjkMddAHAPs2gDsb5sgeTWHCdKD6BdlGDDjgA9CFh1PMRZX5nuYAiEiYDgAAAHwm2AaAbwjTAVw6AAAqEqA65xJ7bakr/U9tkI4wHXDoj/t9XQCNFWoJAJzfASAIYToQjZ+gAwDQ/UwJXdbYXHSn0gOMASwhTAequjLUdmgx3qglgE5nH/1cbalpzDXQgjAdwGEw80XZZRm1BIB+Hnc+Ipyl1US8OQZI62YIAHA4Ri3x5vyeFXzMxfXih51x5/7VedNf9HTWrc1pvo3fBePlI15y7tXQhifTAZc0Y3DvQAvoUXDlPjRP+DMA5DgX6edAeMJ0AIdbWF1HLkbA6r6hz8SdG1Bn4E4IZfiYFwCOLjLjwu/tEIha0newZtXOuTL/yv/84f0Qbz2OQPXeeR70FYCTeTIdoPbBO+tlYT7xv+MSqJYAavf0e31bT499lh3WCmrOuEAlwnQAB41Il4D54J9xMVFLZ9WSS3bMvjmNNfDk2rXGwR0KYDlhOuAwFu81jIBfb8cF1SW4x5qcG+poqrsSZtCvBXp63LOBtU7E87i6xB0bChGmAxDhIjCDvR5y1tLc+N9FumRXDp1nwffkYm6OqvXzCj3d+qz5Onxeer8xFxbrX7CcMB3g/qZtE197aX7367kwq6UqtVChlkfAsdEjIF8P83Fu9gNrJPfcmH/rE0oTpgM4NF1x0XVRVkfRainr56d3WUfzorow9ujpe9eddYW9bO2asMZYdaaHNm6GAAi+yc/N3y/C68h8KRgO7zSqpRW9YT54ydn1sUsjwJg887r0mTo9AP2cnOfp6Od6+/x7+3zkuhMQ1+pbEJYwHaDPwWHHodal2KVcLdV6/49e+rv2munCt7Uv4GxA77OsfT7GPm9v0P+hNR/zAuBiANaDNZ7l0g+APW7n+3U+AP0K/iBMB+i1aQ/vB9Tgi6ZxeXtcQB9DrdFpLxtqy5qEaoTpgA3emII6MhbG5T0C9RjzbS0aO3Vw7bgMY4fxQ61QnTAd4PFN2+busEXNOVBLAHohNepMHesvmBtYSpgOEOewMYq8D6/fmuj4+tWkceHa+VZrqAPjY+yMIXnnRn2QhjAdcHDw3qFrLQ3joD6AMutVr7l2nEay16DG6tSfeck/juaQVITpgI2992Y/vGbrqum8DPW5fbyNjd5iX8jxHjKuV/3FGcBfdGm9Yg+HLYTpgMOCS0iWS7MwLv6hcxg/l4JEY2OeaoztMP96uqlSX167MSTd3LjbkZYwHei6qY+Ar8mh1oF715is/ux/tRRnDM6cj1GkRj5/f5+Zn3cNROg10c4mo+g4Z91vMqzRq8/DmXvwsA7eMu2/LfqDeaOkmyEAEl7M5wlf48zXFOn1nHmwmcFeD+/X30i2XrPX0dVjMILOx5WvaTTo45l71O4xf7UGR8AxGwXXqzUXe52OALUV5QdqFfZ564rVd0M1RhnCdMCh/5qDdJaPVbnqguCwde4hdwR/fdXraPdaGon6567xGQ/8+5m4xrrsJSv38Rlozp8Zs5F4Pq23vOt0XLwOo9VD9n1+91h6Kj3/neOReTQ/lCVMByps5jPIJn50wBjFx9qBON4hVy31ev/jxT8X5YcwK8ZnPPlnq/1QNPMlfPc4jwRjpp+za16v2heO6qr7Oefsfd4aQ3+GFwjTARu5A8a778mvk3JlLXX+IVXlz/t+pz7O+PxewYM9EmcDc+o1nPn6I3yUDgAnEKYD4ICOWvK+jZM5AWsIrInVpiEAsvtlCAAAAABIwg8ngMsI0wEAAAAA4IAwHQAAAICVfMQLUIIwHQAAAIAMfMQLcClhOgAAAAAAHBCmAwAAALCKj3gByhCmAwAAABCdj3gBLidMBwAAAACAA8J0AAAAAFbwES9AKcJ0AAAAACLzES9ACMJ0AAAAAAA4IEwHAAAA4Gw+4gUoR5gOAAAAQFQ+4gUIQ5gOAAAAQFSecAfCEKYDAAAAcKYzA3BPpgNhCNMBAAAAAOCAMB0AAAAAAA4I0wEAAAA4i494AcoSpgMAAAAAwAFhOgAAAAAAHBCmAwAAABCNj3gBwhGmAwAAAADAAWE6AAAAAAAcEKYDAAAAcJYR5GsAnE6YDgAAAEAUgnQgLGE6AAAAAGcam/87gC2E6QAAAACcTTAO1Gtsc06jAAAAAAAAd3gyHQAAAAAADgjTAQAAAADggDAdAAAAAAAOCNMBAAAAAOCAMB0AAAAAAA4I0wEAAAAA4MD/DwCLngHE1vzEJwAAAABJRU5ErkJggg=="

//imprimo la marca de agua
   var alto = 4;

   for(var j=0; j < 6; ++j) {
     var ancho = 4;

     for(var i=0; i < 8; ++i) {
       doc.addImage(mda, "PNG", alto, ancho, 100/3, 40/3, '', '', 0); // marca de agua// marca de agua
              ancho = ancho + 15;
     }
     alto = alto + 35;
    }

     doc.addImage(images[0], "PNG", -10, 20, 140, 105, "", 'FAST');
     doc.addImage(images[1], "PNG", 92, 20, 140, 105, "", 'FAST');
     // side images
     if (images[2]) {
       doc.addImage(images[2], "PNG", 40, 20, 140, 105, "", 'FAST');
     }
     if (images[3]) {
       doc.addImage(images[3], "PNG", 145, 20, 140, 105, "", 'FAST');
     }
     doc.setFontSize(12);
     doc.setTextColor("red");
     doc.text("Las medidas y colores de la imagen son aproximados.", 107.5, 120, { align: "center" });

     doc.save(`FollowMe-DiseñosPersonalizados.pdf`);
  }

}

