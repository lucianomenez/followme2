const colorsName = {
    "FFFFFF": "WHITE",
    "E8DCAA": "ICECREAM",
    "000000": "BLACK",
    "41423F": "DARK SMOKE",
    "C9CDCC": "LIGHT SMOKE",
    "838A80": "SILVER",
    "4F504E": "STEEL",
    "666A69": "ALUMINIUM",
    "939A92": "PLATINUM",
    "BED7E0": "CRYSTAL",
    "6F9FB0": "QUARTZ",
    "4D9DA6": "SAPPHIRE",
    "8AADAC": "AQUAMARINE",
    "3ACFCE": "EMERALD",
    "1D9671": "DIAMOND",
    "009DA2": "CARIBEAN SEA",
    "0096B9": "SWIMMING POOL",
    "0087C3": "JACUZZI",
    "007B8E": "WATERFALL",
    "315755": "LAKE",
    "00879B": "LAGOON",
    "003E74": "RIVER",
    "003793": "OCEAN",
    "0C1321": "SEA",
    "031711": "DARK SEA",
    "007D55": "MINT",
    "009B76": "KIWI",
    "00AD68": "LIME",
    "55BF4C": "MELON",
    "8DA440": "GREEN APPLE",
    "489434": "PINEAPPLE",
    "5B5A47": "OLIVES",
    "6B6B67": "ALMONDS",
    "7A725E": "CONCRETE",
    "A67E50": "SAND",
    "C8C4B6": "STONE",
    "E8CD7A": "BANANA",
    "D6BC43": "PEAR",
    "EBB405": "LEMON",
    "E83D00": "PUMPKIN",
    "C81702": "ORANGE",
    "AD1F09": "APRICOT",
    "C9262D": "RED APPLE",
    "990505": "MANGO",
    "790F1D": "CHERRY",
    "4C3337": "WINE",
    "170704": "CACAO",
    "D9A9912": "LAVENDER",
    "D8C3CA": "ROSE",
    "F25358": "WATERMELON ",
    "E59FB2": "LOTUS",
    "D75379": "LILIUM",
    "E23043": "FREESIA",
    "B51947": "ORCHID",
    "B31957": "RASPBERRY",
    "9C88A9": "BLUEBERRY",
    "5B4885": "GRAPE",
    "332561": "CRANBERRY",
    "5C1D4E": "PLUM",
    "FFBFA8": "PEACH SMOKE",
    "FF915D": "ORANGE SMOKE",
    "FF796A": "WATERMELON SMOKE",
    "BE1619": "APPLE SMOKE",
    "D74259": "LILIUM SMOKE",
    "EA3369": "FREESIA SMOKE",
    "9F5B5C": "RASPBERRY SMOKE",
    "841B49": "PLUM SMOKE",
    "0D3437": "OIL SMOKE",
    "37548B": "RIVER SMOKE",
    "2B4B70": "OCEAN SMOKE",
    "FF9A46": "BRIGHT ORANGE",
    "FF7783": "BRIGHT PINK",
    "E8FF82": "BRIGHT YELLOW"
};

$(document).ready(function(){

    var base_url = $("#base_url").val();

    var colors = "#FFFFFF ,#000000 ,#990505 ,#FF796A ,#C81702 ,#AD1F09 ,#170704 ,#E83D00 ,#FFBFA8 ,#FF915D ,#D9A991 ,#FF9A46 ,#A67E50 ,#7A725E ,#E8CD7A ,#EBB405 ,#C8C4B6 ,#e8dcaa ,#D6BC43 ,#5B5A47 ,#6B6B67 ,#C4D46E ,#E8FF82 ,#41423F ,#4F504E ,#838A80 ,#489434 ,#939A92 ,#55BF4C ,#00AD68 ,#007D55 ,#1D9671 ,#031711 ,#666A69 ,#C9CDCC ,#009B76 ,#315755 ,#8AADAC ,#3ACFCE ,#009DA2 ,#0D3437 ,#4D9DA6 ,#00879B ,#007B8E ,#0096B9 ,#6F9FB0 ,#BED7E0 ,#0087C3 ,#003E74 ,#2B4B70 ,#003793 ,#37548B ,#0C1321 ,#332561 ,#5B4885 ,#9C88A9 ,#5C1D4E ,#841B49 ,#B31957 ,#D8C3CA ,#EA3369 ,#B51947 ,#D75379 ,#E59FB2 ,#4C3337 ,#D74259 ,#790F1D ,#E23043 ,#FF7783 ,#C9262D ,#F25358 ,#BE1619 ,#9F5B5C";

    var layouts = [{
        "title": "Buzo cuello oculto",
        "thumbnail": "img/assets/previews/preview-buzo-cuello-oculto.png",
        "elements": [
            {
                "type": "image",
                "source": "img/assets/front/element-base--lh.png",
                "title": "Shadow",
                "parameters": {
                  "left": 322,
                  "top": 390,
                  "locked": true,
                  "showInColorSelection": false,
                  "removable": false,
                  "draggable": false,
                  "resizable": false,
                  "rotatable": false,
                  "advancedEditing": false,
                  "editable": false,
                  "uploadZone": false,
                  "colors": false,
                  "topped": true,
                  "replace": "base"
                }
            },
            {
                "type": "image",
                "source": "img/assets/front/element-capucha--lh.png",
                "title": "Shadow",
                "parameters": {
                  "left": 322,
                  "top": 390,
                  "locked": true,
                  "showInColorSelection": false,
                  "removable": false,
                  "draggable": false,
                  "resizable": false,
                  "rotatable": false,
                  "advancedEditing": false,
                  "editable": false,
                  "uploadZone": false,
                  "colors": false,
                  "topped": true,
                  "replace": "capucha"
                }
            },
            {
                "type": "image",
                "source": "img/assets/me/ME1-base.png",
                "title": "Base",
                "parameters": {
                  "left": 322,
                  "top": 390,
                  "fill": "#fff",
                  "showInColorSelection": true
                }
            },
            {
                "type": "image",
                "source": "img/assets/me/ME1-torso.png",
                "title": "Torso",
                "parameters": {
                  "left": 322,
                  "top": 390,
                  "fill": "#fff",
                  "showInColorSelection": true
                }
            }
        ]
    }];

    var $fpd = $('#fpd'),
        pluginOptions = {
            productsJSON: 'productos',
            //layouts: layouts,
            // designsJSON: 'http://localhost:8090/followme/json/buzo-d.json',
            stageWidth: 1200,
            stageHeight: 800,
            hexNames: colorsName,
            initialActiveModule: "",
            mainBarModules: ["products","molderia","manage-layers","designs","text","images"],
            actions:{
                "top":[],
                "right":["save","load","download","manage-layers"],
                "bottom":["undo","redo"],
                "left":[]
            },
            elementParameters: {
                "colors": colors,
                "z": 500
            },
            selectedColor: "#f5f5f5",
            boundingBoxColor: "#005ede",
            outOfBoundaryColor: "#990000",
            cornerIconColor: "#000000",
            // colorSelectionPlacement: "#fpd-color-selection-placement",
            customTextParameters: {"left":0,"top":0,"z":-1,"colors":colors,"replaceInAllViews":false,"autoCenter":true,"draggable":true,"rotatable":true,"resizable":true,"autoSelect":true,"topped":false,"uniScalingUnlockable":false,"curvable":true,"curveSpacing":10,"curveRadius":80,"curveReverse":false,"boundingBoxMode":"inside","fontSize":30,"minFontSize":1,"maxFontSize":1000,"widthFontSize":0,"maxLength":0,"maxLines":0,"textAlign":"left","removable":true},
            customImageParameters: {"left":0,"top":0,"minScaleLimit":0.01,"replaceInAllViews":false,"autoCenter":true,"draggable":true,"rotatable":true,"resizable":true,"zChangeable":true,"autoSelect":false,"topped":false,"uniScalingUnlockable":false,"boundingBoxMode":"inside","scaleMode":"cover","removable":true,"z":500},
            // colorSelectionDisplayType: "dropdown",
            langJSON: base_url+'followme/assets/lang/spanish.json',
            templatesDirectory: base_url+'followme/assets/html/'
        };

    var fpd = new FancyProductDesigner($fpd, pluginOptions);

    $fpd.on('ready', function() {

        $fpd.on('elementColorChange', function(e, ele, hex) {
            // console.log(ele);
            // console.log(hex);
            // var p = fpd.getProduct();
        })

        $fpd.on('productSelect', function(e, i, cat, prod) {
            // console.log(cat);
            const id = prod[0]._id.$id;
            // get ME for product and setup ME products
            $.getJSON(`followme/productos/molderiaespecial/${id}`)
            .done(function(data) {
                fpd.setupMe(data);
            })

        })
    })

	//get category index by category name
	var _getCategoryIndexInProducts = function(catName) {

        var catIndex =  $.map(fpd.products, function(obj, index) {

            if(obj.category == catName) {
                return index;
            }
        }).shift();

        return isNaN(catIndex) ? false : catIndex;

    };

    var _addGridProduct = function(views) {

        var fpdInstance = fpd,
            lazyClass = 'fpd-hidden',
            currentCategoryIndex = 0,
            $gridWrapper = $('[data-module="molderia"] .fpd-grid');

        var thumbnail = views[0].productThumbnail ? views[0].productThumbnail : views[0].thumbnail,
            productTitle = views[0].productTitle ? views[0].productTitle : views[0].title;

        var $lastItem = $('<div/>', {
                            'class': 'fpd-item fpd-tooltip '+lazyClass,
                            'data-title': productTitle,
                            'data-source': thumbnail,
                            'html': '<picture data-img="'+thumbnail+'"></picture>'
                        }).appendTo($gridWrapper);

        $lastItem.click(function(evt) {

            evt.preventDefault();

            var $this = $(this),
                index = $gridWrapper.children('.fpd-item').index($this);

            if(fpdInstance.mainOptions.swapProductConfirmation) {

                var $confirm = FPDUtil.showModal(fpdInstance.getTranslation('modules', 'products_confirm_replacement'), false, 'confirm', fpdInstance.$modalContainer);

                $confirm.find('.fpd-confirm').text(fpdInstance.getTranslation('modules', 'products_confirm_button'))
                .click(function() {

                    fpdInstance.selectMe(index, currentCategoryIndex);
                    $confirm.find('.fpd-modal-close').click();

                })

            }
            else {
                fpdInstance.selectMe(index, currentCategoryIndex);
            }


        }).data('views', views);

    };

    fpd.addMe = function(views, category) {

        var catIndex = _getCategoryIndexInProducts(category);

        if(category === undefined) {
            fpd.mespecial.push(views);
        }
        else {

            if(catIndex === false) {

                catIndex = fpd.mespecial.length;
                fpd.mespecial[catIndex] = {category: category, products: []};

            }

            fpd.mespecial[catIndex].products.push(views);

        }

        _addGridProduct(views);

    };

    fpd.selectMe = function(index, categoryIndex) {

        var instance = fpd;

        instance.currentCategoryIndex = categoryIndex === undefined ? instance.currentCategoryIndex : categoryIndex;

        var productsObj;
        if(instance.mespecial && instance.mespecial.length && instance.mespecial[0].category) { //categories enabled
            var category = instance.mespecial[instance.currentCategoryIndex];
            productsObj = category.products;
        }
        else { //no categories enabled
            productsObj = instance.mespecial;
        }

        instance.currentProductIndex = index;
        if(index < 0) { currentProductIndex = 0; }
        else if(index > productsObj.length-1) { instance.currentProductIndex = productsObj.length-1; }

        var product = productsObj[instance.currentProductIndex];

        // /**
        //  * Gets fired when a product is selected.
        //  *
        //  * @event FancyProductDesigner#productSelect
        //  * @param {Event} event
        //  * @param {Number} index - The index of the product in the category.
        //  * @param {Number} categoryIndex - The index of the category.
        //  * @param {Object} product - An object containing the product (views).
        //  */
        // $elem.trigger('productSelect', [index, categoryIndex, product]);

        instance.loadProduct(product, instance.mainOptions.replaceInitialElements);

    };

    fpd.setupMe = function(models) {

        $gridWrapper = $('[data-module="molderia"] .fpd-grid');
        $gridWrapper.empty();

        var mespecial = models === undefined ? [] : models;

        fpd.mespecial = [];

        mespecial.forEach(function(productItem) {

            if(productItem.hasOwnProperty('category')) { //check if products JSON contains categories

                productItem.products.forEach(function(singleProduct) {
                    fpd.addMe(singleProduct);
                });

            }
            else {
                fpd.addMe(productItem);
            }

        });

    };


$("#generate").on('click', function() {

    fpd.getViewsDataURL(generatePdf);

});

const generatePdf = (images) => {

    // get the order number from the backend
    const orderN = "H0010";
    const pngBack = document.getElementById('pdf-background');
    const product = fpd.getProduct();
    console.log(product);

    var doc = new jsPDF({ unit: "mm", format: "legal" });

    doc.addImage(pngBack, "PNG", 0, 0, 216, 356 );
    doc.addImage(images[0], "PNG", 25, 10, 150, 100);
    doc.addImage(images[0], "PNG", 105, 10, 150, 100);

    // set "tipo de producto"
    doc.circle(59, 121.5, 1.8, "F"); // for "Regular"
    // doc.circle(83.6, 121.5, 1.8, "F"); // for "Full print"
    // doc.circle(111, 121.5, 1.8, "F"); // for "Degrade"
    // doc.circle(137.5, 121.5, 1.8, "F"); // for "Moldería especia"
    // doc.circle(179.6, 121.5, 1.8, "F"); // for "Reversible"



    if (product[0].title.includes("Buzo")) {
        // set "cuerpo"
        doc.circle(32, 138.7, 1.5, "F");

        // set "cierre"
        doc.circle(42.4, 182, 1.8, "F"); // for "No"

        // set "cordones"
        doc.circle(42.4, 200, 1.8, "F"); // for "No"
    }
    if (product[0].title.includes("Campera")) {
        // set "cuerpo"
        doc.circle(59, 138.7, 1.5, "F");

        // set "cierre"
        doc.circle(32, 182, 1.8, "F"); // for "Si"
        // doc.circle(58.6, 182, 1.8, "F"); // for "visto"
        // doc.circle(84.6, 182, 1.8, "F"); //for "oculto"

        // set "cordones"
        doc.circle(32, 200, 1.8, "F"); // for "Si"
    }

    // set "forrería capuchas"
    // doc.circle(32, 147.4, 1.8, "F"); // for "Si"
    doc.circle(42.4, 147.4, 1.8, "F"); // for "No"
    // doc.circle(58.6, 145.7, 1.8, "F"); // for "Liso"
    // doc.circle(58.6, 149.6, 1.8, "F"); // for "Sublimado"
    // doc.circle(84.6, 145.7, 1.8, "F"); // for "Corderito"
    // doc.circle(84.6, 149.6, 1.8, "F"); // for "Peluche"

    // set "bolsillos"
    if (product[0].title.includes("bolsillo")) {
        doc.circle(32, 191, 1.8, "F"); // for "Si"
        if (product[0].title.includes("visto")) {
            doc.circle(58.6, 191, 1.8, "F"); // for "canguro"
        }
        if (product[0].title.includes("oculto")) {
            doc.circle(84.6, 191, 1.8, "F"); //for "ojal"
        }
        if (product[0].title.includes("vivo")) {
            doc.circle(103, 191, 1.8, "F"); //for "vivos"
        }
    } else {
        doc.circle(42.4, 191, 1.8, "F"); // for "No"
    }

    // set "colores"
    doc.setFontSize(12);
    product[0].elements.map((element) => {

        if (element.parameters.fill) {
            const color = colorsName[element.parameters.fill.toUpperCase().replace('#','')];
            switch (element.title) {
                case "Base":
                    doc.text(color, 140, 140, {align: "center"});
                    break;
                case "Forrería capucha":
                    doc.text(color, 140, 140 + 8.7*1, {align: "center"});
                    break;
                case "Mangas":
                    doc.text(color, 140, 140 + 8.7*2, {align: "center"});
                    break;
                case "Puños":
                    doc.text(color, 140, 140 + 8.7*3, {align: "center"});
                    break;
                case "Cintura":
                    doc.text(color, 140, 140 + 8.7*4, {align: "center"});
                    break;
                case "Cierre":
                    doc.text(color, 140, 140 + 8.7*5, {align: "center"});
                    break;
                case "Bolsillos":
                    doc.text(color, 140, 140 + 8.7*6, {align: "center"});
                    break;
                case "Cordones":
                    doc.text(color, 140, 140 + 8.7*7, {align: "center"});
                    break;
                default:
                    break;
            }
        }
    })

    doc.text(orderN, 29, 16 );

    doc.save(`ficha-${orderN}.pdf`)
}

});
