<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * dna2
 *
 * Description of the class dna2
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Followme extends MX_Controller {


    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');

         $this->load->model('cms/Model_node');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
      //  $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->usuario = $this->user->get_user($this->idu);
    }

    function Index() {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['fm_js'] = $this->module_url."assets/js/followme.js";

        $this->parser->parse("index", $data);
    }

    function Demo() {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['fm_js'] = $this->module_url."assets/js/followme-demo.js";
        $data['login'] = $this->isloggedin();
        $page['rand']=rand();

        // $args2['post_status'] = 'published';
        // $args2['post_type'] = 'options';
        // $args2['type'] = 'categorias_paleta';


        // $data['categorias_paleta'] = $this->Model_node->get($args2, 1)[0]['items'];

        // if (! $data['categorias_paleta'] ) {
        //     $data['categorias_paleta']  =array();
        // }

        // $categoria= $data['categorias_paleta'][0]['value'];

        // $post_type='paletas';
        // $args['post_type'] = $post_type;
        // $args['post_status'] = 'published';
        // $args2 =  $data['categorias_paleta'][0]['value'];
        // $paletas['data']= $this->Model_node->get_productos($args,$args2, 999, 0);
        // $paletas['categoria_name']=$data['categorias_paleta'][0]['text'];
        // $data['paletas']= $this->parser->parse('website/data_paletas_html', $paletas,true,true);


        // Último
        // $data['categorias_paleta']  =array();
        // $data['categorias_paleta'][0]=array(
        //     'text'=>'Camperas, buzos y pantalones',
        //     'value'=>'camperas_buzos',
        //     'items'=>array('Frisa','Jersey')
        // );

        // $data['categorias_paleta'][1]=array(
        //     'text'=>'Remeras y chombas',
        //     'value'=>'remeras_chombas',
        //     'items'=>array('Pique')
        // );

        // $data['categorias_paleta'][2]=array(
        //     'text'=>'Pelito y peluche',
        //     'value'=>'pelito_peluche'
        // );

        // $paletas['data']=array();
        // $array_paletas_exists=array();
        // $array_paletas=array();

        // if(is_array( $data['categorias_paleta'][0]['items'])){
        //     foreach ( $data['categorias_paleta'][0]['items'] as $key_item => $value_item) {
        //         $args=array();
        //         $args['category']=$value_item;
        //         $result= $this->Model_node->get_paleta($args, 999, 0);
        //         if(is_array($result)){
        //             foreach ($result as $key_result => $value_result) {
        //                 $name = explode(" - ", $value_result['colorName']);
        //                 $value_result['colorName']=$name[1];
        //                 if(!in_array($value_result['colorName'],$array_paletas_exists)){
        //                     $array_paletas_exists[]=$value_result['colorName'];
        //                     $value_result['categoria_paleta_name']= $data['categorias_paleta'][0]['text'];
        //                     $array_paletas[]= $value_result;

        //                 }
        //             }
        //         }
        //     }

        // }

        // $paletas['data'] = $array_paletas;
   
        // $data['paletas']= $this->parser->parse('website/data_paletas_html', $paletas,true,true);
        // Último
        $data["footer"] = $this->footer();

        $params = array('slug' => 'design-room');
          if ($page = $this->Model_node->get_one($params)) {
            $data=array_merge($data,$page);
          }
  //        $this->parser->parse("website/layout/menu-loggedin", $data);
        echo $this->parser->parse("index", $data, true, true);
  //        $this->parser->parse("website/layout/footer", $data);

    }


    function footer(){
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      return  $this->parser->parse("website/layout/footer", $data, true);
    }


    private function isloggedin()
    {
        if (!$this->session->userdata('loggedin')) {
            $this->session->userdata('loggedin',false);
            return false;
        } else {
            return true;
        }
    }

}

/* End of file dna2 */
/* Location: ./system/application/controllers/welcome.php */
