<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * dna2
 *
 * Description of the class dna2
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Api extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('admin/Model_productos');
        $this->load->model('msg');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->usuario = $this->user->get_user($this->idu);

    }

    function save_model($data) {
    //    $data = $this->input->post();
        $data["created_by"] = $this->usuario->name." ".$this->usuario->lastname;
        $data["date"] = date ("d/m/Y");
        $this->Model_productos->insert_producto($data);
        return;
    }

    function upload_pdf($nro_orden){
      move_uploaded_file($_FILES['pdf']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/followme/application/modules/admin/assets/fichas/".$nro_orden.".pdf");
    }


    function save_image(){
      $base64_str = substr($_POST['base64_image'], strpos($_POST['base64_image'], ",")+1);
      //decode base64 string
      $decoded = base64_decode($base64_str);
      $png_url = APPPATH.'modules/followme/assets/modelos_guardados/product-'.strtotime('now').".png";
      //create png from decoded base 64 string and save the image in the parent folder
    //  $result = file_put_contents($png_url, $decoded);

  //    $data["decoded"] = $decoded;
      $data['colegio'] = $_POST['colegio'];
      $data['curso'] = $_POST['curso'];
      $data['nro_orden'] = $_POST['nro_orden'];
      $data['responsable'] = $_POST['responsable'];
      $data['responsable_email'] = $_POST['responsable_email'];
      $data["png_url"] = $png_url;
      $data["producto"] = $_POST['doc'];
      $this->save_model($data);

      //send result - the url of the png or 0
      header('Content-Type: application/json');
      if($result) {

      	echo json_encode($png_url);
      }
      else {
      	echo json_encode(0);
      }
    }

    //returns the current folder URL
    function get_folder_url() {
    	$url = $_SERVER['REQUEST_URI']; //returns the current URL
    	$parts = explode('/',$url);
    	$dir = $_SERVER['SERVER_NAME'];
    	for ($i = 0; $i < count($parts) - 1; $i++) {
    		$dir .= $parts[$i] . "/";
    	}
    	return 'http://'.$dir;
    }



}

/* End of file dna2 */
/* Location: ./system/application/controllers/welcome.php */
