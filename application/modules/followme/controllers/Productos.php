<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Productos
 *
 * Endpoints públicos para listar los json con los productos disponibles
 *
 */
class Productos extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->config('dashboard/config');

        $this->load->model('Product', 'modelProduct');
        $this->load->model('Element', 'modelElement');
        $this->load->model('Colors', 'modelColors');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
    }

    public function Demo()
    {

        $frisaColors = join(",",$this->modelColors->get_all(["category" => "Frisa"], true));
        $piqueColors = join(",",$this->modelColors->get_all(["category" => "Pique"], true));
        $jerseyColors = join(",",$this->modelColors->get_all(["category" => "Jersey"], true));

      //Cardigans con Bolsillo Oculto

        $json_cardigan010 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/cardigans/bolsillos_ocultos/sin_capucha/010SCBO.json');
        $json_cardigan010 = str_replace('jerseyColors', $jerseyColors, $json_cardigan010);
        $json_cardigan010 = str_replace('frisaColors', $frisaColors, $json_cardigan010);


        $json_cardigan021 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/cardigans/bolsillos_ocultos/capucha/sin_cordones/021CCBO.json');
        $json_cardigan021 = str_replace('jerseyColors', $jerseyColors, $json_cardigan021);
        $json_cardigan021 = str_replace('frisaColors', $frisaColors, $json_cardigan021);


        $json_cardigan031 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/cardigans/bolsillos_ocultos/capucha/cordones/031CCBOC.json');
        $json_cardigan031 = str_replace('jerseyColors', $jerseyColors, $json_cardigan031);
        $json_cardigan031 = str_replace('frisaColors', $frisaColors, $json_cardigan031);


        $modelo1 = array(
          'category' => 'Cardigans con Bolsillo Oculto',
          'products' => array(
                json_decode($json_cardigan010, true),
                json_decode($json_cardigan021, true),
                json_decode($json_cardigan031, true)

            )
        );

        //Cardigans sin Bolsillo

        $json_cardigan011 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/cardigans/sin_bolsillos/sin_capucha/011SCSB.json');
        $json_cardigan011 = str_replace('frisaColors', $frisaColors, $json_cardigan011);
        $json_cardigan011 = str_replace('jerseyColors', $jerseyColors, $json_cardigan011);


        $json_cardigan020 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/cardigans/sin_bolsillos/capucha/sin_cordones/020CCSB.json');
        $json_cardigan020 = str_replace('frisaColors', $frisaColors, $json_cardigan020);
        $json_cardigan020 = str_replace('jerseyColors', $jerseyColors, $json_cardigan020);


        $json_cardigan030 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/cardigans/sin_bolsillos/capucha/cordones/030CCSBC.json');
        $json_cardigan030 = str_replace('frisaColors', $frisaColors, $json_cardigan030);
        $json_cardigan030 = str_replace('jerseyColors', $jerseyColors, $json_cardigan030);


        $modelo2 = array(
          'category' => 'Cardigans sin Bolsillo',
          'products' => array(
                json_decode($json_cardigan011, true),
                json_decode($json_cardigan020, true),
                json_decode($json_cardigan030, true)
            )
        );

        //Cardigans con Bolsillo Ojal

        $json_cardigan012 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/cardigans/bolsillos_ojal/sin_capucha/012SCBD.json');
        $json_cardigan012 = str_replace('jerseyColors', $jerseyColors, $json_cardigan012);
        $json_cardigan012 = str_replace('frisaColors', $frisaColors, $json_cardigan012);


        $json_cardigan022 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/cardigans/bolsillos_ojal/capucha/sin_cordones/022CCBD.json');
        $json_cardigan022 = str_replace('jerseyColors', $jerseyColors, $json_cardigan022);
        $json_cardigan022 = str_replace('frisaColors', $frisaColors, $json_cardigan022);


        $json_cardigan032 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/cardigans/bolsillos_ojal/capucha/cordones/032CCBDC.json');
        $json_cardigan032 = str_replace('jerseyColors', $jerseyColors, $json_cardigan032);
        $json_cardigan032 = str_replace('frisaColors', $frisaColors, $json_cardigan032);


        $modelo3 = array(
          'category' => 'Cardigans con Bolsillo Ojal',
          'products' => array(
                json_decode($json_cardigan012, true),
                json_decode($json_cardigan022, true),
                json_decode($json_cardigan032, true)


            )
        );

        //Buzos con Bolsillo Oculto

        $json_buszos111 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/buzos/bolsillos_ocultos/sin_capucha/111SCBO.json');
        $json_buszos111 = str_replace('jerseyColors', $jerseyColors, $json_buszos111);
        $json_buszos111 = str_replace('frisaColors', $frisaColors, $json_buszos111);


        $json_buszos121 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/buzos/bolsillos_ocultos/capucha/sin_cordones/121CCBO.json');
        $json_buszos121 = str_replace('jerseyColors', $jerseyColors, $json_buszos121);
        $json_buszos121 = str_replace('frisaColors', $frisaColors, $json_buszos121);


        $modelo4 = array(
          'category' => 'Buzos con Bolsillo Oculto',
          'products' => array(
                json_decode($json_buszos111, true),
                json_decode($json_buszos121, true)

            )
        );

        //Buzos sin Bolsillo

        $json_buszos110 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/buzos/sin_bolsillos/sin_capucha/110SCSB.json');
        $json_buszos110 = str_replace('jerseyColors', $jerseyColors, $json_buszos110);
        $json_buszos110 = str_replace('frisaColors', $frisaColors, $json_buszos110);


        $json_buszos120 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/buzos/sin_bolsillos/capucha/sin_cordones/120CCSB.json');
        $json_buszos120 = str_replace('jerseyColors', $jerseyColors, $json_buszos120);
        $json_buszos120 = str_replace('frisaColors', $frisaColors, $json_buszos120);


        $modelo5 = array(
          'category' => 'Buzos sin Bolsillo',
          'products' => array(
                json_decode($json_buszos110, true),
                json_decode($json_buszos120, true)
            )
        );

        //Buzos con Bolsillo Ojal

        $json_buszos112 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/buzos/bolsillos_ojal/sin_capucha/112SCBD.json');
        $json_buszos112 = str_replace('jerseyColors', $jerseyColors, $json_buszos112);
        $json_buszos112 = str_replace('frisaColors', $frisaColors, $json_buszos112);


        $json_buszos122 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/buzos/bolsillos_ojal/capucha/sin_cordones/122CCBD.json');
        $json_buszos122 = str_replace('jerseyColors', $jerseyColors, $json_buszos122);
        $json_buszos122 = str_replace('frisaColors', $frisaColors, $json_buszos122);


        $modelo6 = array(
          'category' => 'Buzos con Bolsillo Ojal',
          'products' => array(
                json_decode($json_buszos112, true),
                json_decode($json_buszos122, true)

            )
        );

        //Buzos con Bolsillo Canguro Vivos

        $json_buszos114 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/buzos/bolsillos_canguro_vivos/sin_capucha/114SCBCV.json');
        $json_buszos114 = str_replace('jerseyColors', $jerseyColors, $json_buszos114);
        $json_buszos114 = str_replace('frisaColors', $frisaColors, $json_buszos114);


        $json_buszos124 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/buzos/bolsillos_canguro_vivos/capucha/sin_cordones/124CCBCV.json');
        $json_buszos124 = str_replace('jerseyColors', $jerseyColors, $json_buszos124);
        $json_buszos124 = str_replace('frisaColors', $frisaColors, $json_buszos124);


        $modelo7 = array(
          'category' => 'Buzos con Bolsillo Canguro',
          'products' => array(
                json_decode($json_buszos114, true),
                json_decode($json_buszos124, true)

            )
        );

        //Campera con Bolsillo Oculto

        $json_campera216 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/camperas/bolsillos_ocultos/capucha/sin_cordones/216CCBO.json');
        $json_campera216 = str_replace('jerseyColors', $jerseyColors, $json_campera216);
        $json_campera216 = str_replace('frisaColors', $frisaColors, $json_campera216);


        $json_campera226 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/camperas/bolsillos_ocultos/capucha/cordones/226CCBOC.json');
        $json_campera226 = str_replace('frisaColors', $frisaColors, $json_campera226);
        $json_campera226 = str_replace('jerseyColors', $jerseyColors, $json_campera226);


        $modelo8 = array(
          'category' => 'Campera con Bolsillo Oculto',
          'products' => array(
                json_decode($json_campera216, true),
                json_decode($json_campera226, true)

            )
        );

        //Campera sin Bolsillo

        $json_campera215 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/camperas/sin_bolsillos/capucha/sin_cordones/215CCSB.json');
        $json_campera215 = str_replace('jerseyColors', $jerseyColors, $json_campera215);
        $json_campera215 = str_replace('frisaColors', $frisaColors, $json_campera215);


        $json_campera225 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/camperas/sin_bolsillos/capucha/cordones/225CCSBC.json');
        $json_campera225 = str_replace('jerseyColors', $jerseyColors, $json_campera225);
        $json_campera225 = str_replace('frisaColors', $frisaColors, $json_campera225);


        $modelo9 = array(
          'category' => 'Campera sin Bolsillo',
          'products' => array(
                json_decode($json_campera215, true),
                json_decode($json_campera225, true)
            )
        );

        //Campera con Bolsillo Ojal

        $json_campera217 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/camperas/bolsillos_ojal/capucha/sin_cordones/217CCBD.json');
        $json_campera217 = str_replace('jerseyColors', $jerseyColors, $json_campera217);
        $json_campera217 = str_replace('frisaColors', $frisaColors, $json_campera217);


        $json_campera227 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/camperas/bolsillos_ojal/capucha/cordones/227CCBDC.json');
        $json_campera227 = str_replace('jerseyColors', $jerseyColors, $json_campera227);
        $json_campera227 = str_replace('frisaColors', $frisaColors, $json_campera227);



        $modelo10 = array(
          'category' => 'Campera con Bolsillo Ojal',
          'products' => array(
                json_decode($json_campera217, true),
                json_decode($json_campera227, true)

            )
        );

        //Campera con Bolsillo Canguro Vivos

        $json_campera219 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/camperas/bolsillos_canguro_vivos/capucha/sin_cordones/219CCBCV.json');
        $json_campera219 = str_replace('jerseyColors', $jerseyColors, $json_campera219);
        $json_campera219 = str_replace('frisaColors', $frisaColors, $json_campera219);


        $json_campera229 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/camperas/bolsillos_canguro_vivos/capucha/cordones/229CCBCVC.json');
        $json_campera229 = str_replace('jerseyColors', $jerseyColors, $json_campera229);
        $json_campera229 = str_replace('frisaColors', $frisaColors, $json_campera229);



        $modelo11 = array(
          'category' => 'Campera con Bolsillo Canguro',
          'products' => array(
                json_decode($json_campera219, true),
                json_decode($json_campera229, true)

            )
        );

        //Chomba

        $json_chomba= file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/chombas/chomba_jersey/punos/001CHOM.json');
        $json_chomba = str_replace('jerseyColors', $jerseyColors, $json_chomba);

        $json_chomba2 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/chombas/chomba_jersey/sin_punos/002CHOM.json');
        $json_chomba2 = str_replace('jerseyColors', $jerseyColors, $json_chomba2);

        $json_chomba3= file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/chombas/chomba_pique/punos/003CHOM.json');
        $json_chomba3 = str_replace('piqueColors', $piqueColors, $json_chomba3);

        $json_chomba4= file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/chombas/chomba_pique/sin_punos/004CHOM.json');
        $json_chomba4 = str_replace('piqueColors', $piqueColors, $json_chomba4);

        $modelo12 = array(
          'category' => 'Chomba',
          'products' => array(
                json_decode($json_chomba, true),
                json_decode($json_chomba2, true),
                json_decode($json_chomba3, true),
                json_decode($json_chomba4, true)


            )
        );

        //Remera

        $json_remera = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/remeras/punos/001REME.json');
        $json_remera= str_replace('jerseyColors', $jerseyColors, $json_remera);

        $json_remera2 = file_get_contents(APPPATH.'modules/followme/assets/img/arquitectura/remeras/sin_punos/002REME.json');
        $json_remera2= str_replace('jerseyColors', $jerseyColors, $json_remera2);

        $modelo13 = array(
          'category' => 'Remeras',
          'products' => array(
                json_decode($json_remera, true),
                json_decode($json_remera2, true)

            )
        );

        //Camperas
        $data[] = $modelo11;
        $data[] = $modelo8;
        $data[] = $modelo10;
        $data[] = $modelo9;

        //Cardigans
        $data[] = $modelo1;
        $data[] = $modelo3;
        $data[] = $modelo2;


        //Chomba
        $data[] = $modelo12;

        //Buzos
        $data[] = $modelo7;
        $data[] = $modelo4;
        $data[] = $modelo6;
        $data[] = $modelo5;

        //Remera
       $data[] = $modelo13;

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }


    public function MolderiaEspecialDemo($id = null)
    {
      // prevent E_WARNING in not existent folders
      error_reporting(E_ERROR | E_PARSE);

      // functions for the images parameters
      require_once "Helper.php";

      $frisaColors = join(",",$this->modelColors->get_all(["category" => "Frisa"], true));
      $jerseyColors = join(",",$this->modelColors->get_all(["category" => "Jersey"], true));
      $piqueColors = join(",",$this->modelColors->get_all(["category" => "Pique"], true));

      $paths = [
        // BUZOS
        "114SCBCV" => "assets/img/arquitectura/buzos/bolsillos_canguro_vivos/sin_capucha/",
        "124CCBCV" => "assets/img/arquitectura/buzos/bolsillos_canguro_vivos/capucha/sin_cordones/",
        "121CCBO" => "assets/img/arquitectura/buzos/bolsillos_ocultos/capucha/sin_cordones/",
        "111SCBO" => "assets/img/arquitectura/buzos/bolsillos_ocultos/sin_capucha/",
        "122CCBD" => "assets/img/arquitectura/buzos/bolsillos_ojal/capucha/sin_cordones/",
        "112SCBD" => "assets/img/arquitectura/buzos/bolsillos_ojal/sin_capucha/",
        "120CCSB" => "assets/img/arquitectura/buzos/sin_bolsillos/capucha/sin_cordones/",
        "110SCSB" => "assets/img/arquitectura/buzos/sin_bolsillos/sin_capucha/",
        // CAMPERAS
        "229CCBCVC" => "assets/img/arquitectura/camperas/bolsillos_canguro_vivos/capucha/cordones/",
        "219CCBCV" => "assets/img/arquitectura/camperas/bolsillos_canguro_vivos/capucha/sin_cordones/",
        "226CCBOC" => "assets/img/arquitectura/camperas/bolsillos_ocultos/capucha/cordones/",
        "216CCBO" => "assets/img/arquitectura/camperas/bolsillos_ocultos/capucha/sin_cordones/",
        "227CCBDC" => "assets/img/arquitectura/camperas/bolsillos_ojal/capucha/cordones/",
        "217CCBD" => "assets/img/arquitectura/camperas/bolsillos_ojal/capucha/sin_cordones/",
        "225CCSBC" => "assets/img/arquitectura/camperas/sin_bolsillos/capucha/cordones/",
        "215CCSB" => "assets/img/arquitectura/camperas/sin_bolsillos/capucha/sin_cordones/",
        // CHOMBAS
        "001CHOM" => "assets/img/arquitectura/chombas/chomba_jersey/punos/",
        "002CHOM" => "assets/img/arquitectura/chombas/chomba_jersey/sin_punos/",
        "003CHOM" => "assets/img/arquitectura/chombas/chomba_pique/punos/",
        "004CHOM" => "assets/img/arquitectura/chombas/chomba_pique/sin_punos/",
        // REMERAS
        "001REME" => "assets/img/arquitectura/remeras/punos/",
        "002REME" => "assets/img/arquitectura/remeras/sin_punos/"
      ];

      if ($id) {
        $productUrl = $paths[$id];
      }

      // init the response
      $response = [];

      // check if file exists
      if (file_exists(APPPATH."modules/followme/$productUrl/me.json")) {
          $dataMe = json_decode(file_get_contents(APPPATH."modules/followme/$productUrl/me.json"), true);

          // naranja: #FF9A46, azul: #0096B9, verde: #55BF4C, rosa: #FF7783, negro: #41423F
          //$ordersColors = json_decode(file_get_contents(APPPATH."modules/followme/assets/img/arquitectura/me/config.json"), true);

          $ordersColors_buzo = json_decode(file_get_contents(APPPATH."modules/followme/assets/img/arquitectura/me/config/buzo/config.json"), true);

          $ordersColors_campera = json_decode(file_get_contents(APPPATH."modules/followme/assets/img/arquitectura/me/config/campera/config.json"), true);

          $ordersColors_chomba = json_decode(file_get_contents(APPPATH."modules/followme/assets/img/arquitectura/me/config/chomba/config.json"), true);

          $ordersColors_remera = json_decode(file_get_contents(APPPATH."modules/followme/assets/img/arquitectura/me/config/remera/config.json"), true);

          if (strpos($dataMe["title"], "Buzo") !== false) {
            $ordersColors = $ordersColors_buzo;
            $productColors = $frisaColors;
          } elseif (strpos($dataMe["title"], "Campera") !== false){
              $ordersColors = $ordersColors_campera;
              $productColors = $frisaColors;
          } elseif (strpos($dataMe["title"], "Chomba") !== false){
              $ordersColors = $ordersColors_chomba;
              if(strpos($dataMe["title"], "Jersey") !== false){
                $productColors = $jerseyColors;
              }
              elseif(strpos($dataMe["title"], "Piqué") !== false){
                $productColors = $piqueColors;
              }
          } elseif (strpos($dataMe["title"], "Remera") !== false){
            $ordersColors = $ordersColors_remera;
            $productColors = $jerseyColors;
          }

          // iterate over every product of ME (order array in me.json)
          foreach ($dataMe["ordenes"] as $nroOrden) {
            $orden = [];
            foreach ($dataMe["shadows"] as $vistaKey => $shadow) {
              $vista = [];
              $vista["title"] = "$dataMe[title] - ME$nroOrden";
              $vista["molderia_especial"] = $nroOrden;
              $vista["id"] = $dataMe["id"];
              $vista["options"] = generalOptions($shadow, $vistaKey, $dataMe["title"]);

              // set path and url for the me images
              $path = APPPATH."modules/followme/assets/img/arquitectura/me/orden$nroOrden/$dataMe[tipo]/$vistaKey";
              $url = "assets/img/arquitectura/me/orden$nroOrden/$dataMe[tipo]/$vistaKey";

              // preview
              $vista["thumbnail"] = $productUrl."preview/me/me$nroOrden-$vistaKey.png";

              // pixel element for general color

              // add shadow images elements
              foreach ($shadow as $shadowImage) {
                $vista["elements"][] = shadowElement("$productUrl/$shadowImage");
              }

              // get all images in the folder
              $images = scandir($path);
              if ($images) {
                $images = array_values(array_diff($images, ['..', '.', '.DS_Store', '_DS_Store']));
                foreach ($images as $i => $image) {
                  // add me element to view
                  $vista["elements"][] = imageElement(
                    "$url/$image",
                    "Color $i",
                    "color$i",
                    $ordersColors[$nroOrden][0][$i][0],
                    $productColors,
                    $ordersColors[$nroOrden][0][$i][1] //Paso ubicacion del color
                  );
                }
              }

              // set custom images to elements
              if ($dataMe["images"]) {
                foreach ($dataMe["images"] as $customImage) {
                  //$colors = $frisaColors;
                  // set color for product element by type
                  switch ($customImage["title"]) {
                    case 'Puños':
                      if($dataMe['tipo'] == 'campera'){
                        $colors = $frisaColors;
                        $color = $ordersColors[$nroOrden][1]["punios"]; //Mooooooodifique ACA
                      }
                      elseif($dataMe['tipo'] == 'chomba'){
                        if(strpos($dataMe["title"], "Jersey") !== false){
                          $colors = $jerseyColors;
                        }
                        elseif(strpos($dataMe["title"], "Piqué") !== false){
                          $colors = $piqueColors;
                        }
                        $color = $ordersColors[$nroOrden][1]["punios"]; //Mooooooodifique ACA
                      }
                      elseif($dataMe['tipo'] == 'buzo'){
                        $colors = $frisaColors;
                        $color = $ordersColors[$nroOrden][1]["punios"]; //Mooooooodifique ACA
                      }
                      elseif($dataMe['tipo'] == 'remera'){
                        $colors = $jerseyColors;
                        $color = $ordersColors[$nroOrden][1]["punios"]; //Mooooooodifique ACA
                      }
                    break;
                    case 'Mangas':
                    $colors = $frisaColors;
                    $color = "#FFF";
                    break;
                    case 'Capucha Interior':
                    $colors = $jerseyColors;
                    break;
                    case 'Cintura':
                    $colors = $frisaColors;
                      $color = $color = $ordersColors[$nroOrden][1]["cintura"]; //Mooooooodifique ACA
                      break;
                    case 'Bolsillo':
                    $colors = $frisaColors;
                    case 'Bolsillo Vivos':
                      $colors = $frisaColors;
                      $color = end($ordersColors[$nroOrden][0]);
                      break;
                    case 'Capucha':
                      $colors = $frisaColors;
                      $color = $ordersColors[$nroOrden][0][0];
                      break;
                    case 'Cuello':
                      if($dataMe['tipo'] == 'chomba'){
                        if(strpos($dataMe["title"], "Jersey") !== false){
                          $colors = $jerseyColors;
                        }
                        elseif(strpos($dataMe["title"], "Piqué") !== false){
                          $colors = $piqueColors;
                        }
                        $color = $ordersColors[$nroOrden][1]["cuello"]; //Mooooooodifique ACA
                      }
                      elseif($dataMe['tipo'] == 'remera'){
                        $colors = $jerseyColors;
                        $color = $ordersColors[$nroOrden][1]["cuello"]; //Mooooooodifique ACA
                      }
                      break;
                    case 'Cartera':
                      if(strpos($dataMe["title"], "Jersey") !== false){
                        $colors = $jerseyColors;
                      }
                      elseif(strpos($dataMe["title"], "Piqué") !== false){
                        $colors = $piqueColors;
                      }
                      $color = $ordersColors[$nroOrden][1]["cuello"];
                      break;
                    default:
                      $color = $customImage["color"];
                      break;
                  }

                  // -------- Hack para que no tire error cartera.png en vistas que no hay
                  if (($customImage["title"] == 'Cartera' AND $vistaKey != 'frente') OR ($customImage["title"] == 'Puños' AND strpos($dataMe["title"], "sin Puños") !== false)){
                    //No carga la cartera
                  } else {
                  //---------------

                    // add product element to view
                    $vista["elements"][] = imageElement(
                      "$productUrl"."$vistaKey/$customImage[source]",
                      $customImage["title"],
                      $customImage["title"],
                      $color,
                      $colors
                    );
                  } //Corchete del hack
                }

              }

        //    $vista["elements"][] = SisaFrontal();

              // asign view to product
              $orden[] = $vista;
            }
            // asign product to array of products
            $response[] = $orden;
          }
      }

      // return json
      return $this->output
      ->set_content_type('application/json')
      ->set_status_header(200)
      ->set_output(json_encode($response));
    }



    public function GetMolderias($id = null)
    {
      // prevent E_WARNING in not existent folders
      error_reporting(E_ERROR | E_PARSE);
      $post = $this->input->post();



      // functions for the images parameters
      require_once "Helper.php";
      $frisaColors = join(",",$this->modelColors->get_all(["category" => "Frisa"], true));
      $jerseyColors = join(",",$this->modelColors->get_all(["category" => "Jersey"], true));

      $paths = [
        // BUZOS
        "114SCBCV" => "assets/img/arquitectura/buzos/bolsillos_canguro_vivos/sin_capucha/",
        "124CCBCV" => "assets/img/arquitectura/buzos/bolsillos_canguro_vivos/capucha/sin_cordones/",
        "121CCBO" => "assets/img/arquitectura/buzos/bolsillos_ocultos/capucha/sin_cordones/",
        "111SCBO" => "assets/img/arquitectura/buzos/bolsillos_ocultos/sin_capucha/",
        "122CCBD" => "assets/img/arquitectura/buzos/bolsillos_ojal/capucha/sin_cordones/",
        "112SCBD" => "assets/img/arquitectura/buzos/bolsillos_ojal/sin_capucha/",
        "120CCSB" => "assets/img/arquitectura/buzos/sin_bolsillos/capucha/sin_cordones/",
        "110SCSB" => "assets/img/arquitectura/buzos/sin_bolsillos/sin_capucha/",
        // CAMPERAS
        "229CCBCVC" => "assets/img/arquitectura/camperas/bolsillos_canguro_vivos/capucha/cordones/",
        "219CCBCV" => "assets/img/arquitectura/camperas/bolsillos_canguro_vivos/capucha/sin_cordones/",
        "226CCBOC" => "assets/img/arquitectura/camperas/bolsillos_ocultos/capucha/cordones/",
        "216CCBO" => "assets/img/arquitectura/camperas/bolsillos_ocultos/capucha/sin_cordones/",
        "227CCBDC" => "assets/img/arquitectura/camperas/bolsillos_ojal/capucha/cordones/",
        "217CCBD" => "assets/img/arquitectura/camperas/bolsillos_ojal/capucha/sin_cordones/",
        "225CCSBC" => "assets/img/arquitectura/camperas/sin_bolsillos/capucha/cordones/",
        "215CCSB" => "assets/img/arquitectura/camperas/sin_bolsillos/capucha/sin_cordones/",
        // CHOMBAS
        "001CHOM" => "assets/img/arquitectura/chombas/chomba_jersey/punos/",
        "002CHOM" => "assets/img/arquitectura/chombas/chomba_jersey/sin_punos/",
        "003CHOM" => "assets/img/arquitectura/chombas/chomba_pique/punos/",
        "004CHOM" => "assets/img/arquitectura/chombas/chomba_pique/sin_punos/",
        // REMERAS
        "001REME" => "assets/img/arquitectura/remeras/punos/",
        "002REME" => "assets/img/arquitectura/remeras/sin_punos/"
      ];

      if ($id) {
        $productUrl = $paths[$id];
      }
      // init the response
      $response = [];

      //check if file exists
      if (file_exists(APPPATH."modules/followme/".$productUrl."/custom.json")) {
          $dataMe = json_decode(file_get_contents(APPPATH."modules/followme/$productUrl/custom.json"), true);
          // naranja: #FF9A46, azul: #0096B9, verde: #55BF4C, rosa: #FF7783, negro: #41423F
          $ordersColors = json_decode(file_get_contents(APPPATH."modules/followme/assets/img/arquitectura/me/config.json"), true);

          // iterate over every product of ME (order array in me.json)

          $dataMe["ordenes"][0] = $dataMe["ordenes"];
          foreach ($dataMe["ordenes"] as $nroOrden) {
            $orden = [];

            foreach ($dataMe["shadows"] as $vistaKey => &$shadow) {
              $vista = [];
              $vista["title"] = "$dataMe[title] - ME-Custom";
              $vista["molderia_especial"] = "Custom";
              $vista["id"] = $dataMe["id"];
              $vista["options"] = generalOptions($shadow, $vistaKey);

              switch ($vistaKey){
                  case "frente":
                  $path = APPPATH."modules/followme/assets/img/arquitectura/me/orden".$post['base']."/".$dataMe['tipo']."/".$vistaKey;
                  $url = "assets/img/arquitectura/me/orden".$post['base']."/".$dataMe['tipo']."/".$vistaKey;
                  if ($post['base'] == "lisa"){
                    $vista["thumbnail"] = $productUrl."preview/frente/preview.png";
                  }else{
                    $vista["thumbnail"] = $productUrl."preview/me/me".$post['base']."-".$vistaKey.".png";
                  }
                  break;
                  case "espalda":
                  $path = APPPATH."modules/followme/assets/img/arquitectura/me/orden".$post['espalda']."/".$dataMe['tipo']."/".$vistaKey;
                  $url = "assets/img/arquitectura/me/orden".$post['espalda']."/".$dataMe['tipo']."/".$vistaKey;
                  if ($post['espalda'] == "lisa"){
                    $vista["thumbnail"] = $productUrl."preview/espalda/preview.png";
                  }else{
                    $vista["thumbnail"] = $productUrl."preview/me/me".$post['espalda']."-".$vistaKey.".png";
                  }
                  break;
                  case "lateral_derecho":
                  $path = APPPATH."modules/followme/assets/img/arquitectura/me/orden".$post['mangas']."/".$dataMe['tipo']."/".$vistaKey;
                  $url = "assets/img/arquitectura/me/orden".$post['mangas']."/".$dataMe['tipo']."/".$vistaKey;
                  $vista["thumbnail"] = $productUrl."preview/lateral_derecho/preview.png";
                  break;
                  case "lateral_izquierdo":
                  $path = APPPATH."modules/followme/assets/img/arquitectura/me/orden".$post['mangas']."/".$dataMe['tipo']."/".$vistaKey;
                  $url = "assets/img/arquitectura/me/orden".$post['mangas']."/".$dataMe['tipo']."/".$vistaKey;
                  $vista["thumbnail"] = $productUrl."preview/lateral_izquierdo/preview.png";
                  break;
              }
              // set path and url for the me images
              // $path = APPPATH."modules/followme/assets/img/arquitectura/me/orden$nroOrden/$dataMe[tipo]/$vistaKey";
              // $url = "assets/img/arquitectura/me/orden$nroOrden/$dataMe[tipo]/$vistaKey";

              // preview

              // pixel element for general color
      //        $vista["elements"][] = pixelElement();
              // add shadow images elements
              foreach ($shadow as $shadowImage) {
                $vista["elements"][] = shadowElement("$productUrl/$shadowImage");
              }

              // get all images in the folder
              $images = scandir($path);
              if ($images) {

                $images = array_values(array_diff($images, ['..', '.', '.DS_Store', '_DS_Store']));
                foreach ($images as $i => $image) {
                  // add me element to view
                  $vista["elements"][] = imageElement(
                    "$url/$image",
                    "Color $i",
                    "color$i",
                    $ordersColors[$nroOrden][$i],
                    $frisaColors
                  );
                }
              }


            if (($post['mangas_lisa'] == true) and ($vistaKey == "frente")){
              $dataMe["images"][] = array (
                "source" => "mangas.png",
                "title" => "Mangas"
              );
            }

            if (($post['espalda_lisa'] == "true") and ($vistaKey === "espalda")){
              $aux = array (
                "source" => "base.png",
                "title" => "Espalda"
              );
              array_unshift (  $dataMe["images"],$aux);
            }

            if (($post['base'] == 'lisa')and ($vistaKey == "frente")){
                 $aux = array (
                "source" => "base.png",
                "title" => "Base"
              );
              array_unshift (  $dataMe["images"],$aux);
            }
            //
            // if ($post['espalda'] == 'lisa'){
            //   $dataMe["images"][] = array (
            //     "source" => "base.png",
            //     "title" => "Espalda"
            //   );
            // }

              // set custom images to elements
              if ($dataMe["images"]) {
                foreach ($dataMe["images"] as $customImage) {
                  //$colors = $frisaColors;
                  // set color for product element by type
                  switch ($customImage["title"]) {
                    case 'Puños':
                    $colors = $frisaColors;
                    break;
                    case 'Mangas':
                    $colors = $frisaColors;
                    $color = "#FFF";
                    break;
                    case 'Cordones':
                    $colors = $frisaColors;
                    $color = "#FFF";
                    break;
                    case 'Base':
                    $colors = $frisaColors;
                    $color = "#FFF";
                    case 'Espalda':
                    $colors = $frisaColors;
                    $color = "#FFF";
                    break;
                    case 'Capucha Interior':
                    $colors = $jerseyColors;
                    break;
                    case 'Cintura':
                    $colors = $frisaColors;
                      $color = end($ordersColors[$nroOrden]);
                      break;
                    case 'Bolsillo':
                    $colors = $frisaColors;
                    case 'Bolsillo Vivos':
                      $colors = $frisaColors;
                      $color = end($ordersColors[$nroOrden]);
                      break;
                    case 'Capucha':
                      $colors = $frisaColors;
                      $color = $ordersColors[$nroOrden][0];
                      break;
                    default:
                      $color = $customImage["color"];
                      break;
                  }

                    // add product element to view



                  $vista["elements"][] = imageElement(
                    "$productUrl"."$vistaKey/$customImage[source]",
                    $customImage["title"],
                    $customImage["title"],
                    $color,
                    $colors
                  );
                }

              }

              foreach ($vista["elements"] as $key => &$value) {
                // if ($vistaKey == "espalda"){
                //   if (($value['title'] == "Base") or ($value['title'] == "Color 0") or ($value['title'] == "Color 1") or ($value['title'] == "Color 2")){
                //      $value['parameters'][] = array ("excludeFromExport" => true);
                //   }
                //
                // }


                if (($value["title"] == "Espalda") and ($vistaKey != "espalda")){
                  unset($vista["elements"][$key]);
                  $vista["elements"] = array_values($vista["elements"]);
                }
                // code...
              }
              // asign view to product
              $orden[] = $vista;

            }


            // asign product to array of products
            $response[] = $orden;
          }
      }

      return $this->output
      ->set_content_type('application/json')
      ->set_status_header(200)
      ->set_output(json_encode($response));
    }



}
