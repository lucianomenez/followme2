<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

function shadowElement($source = null)
{
  return [
    "type" => "image",
    "source" => $source,
    "title" => "Shadow",
    "parameters" => [
      "left" => 400,
      "top" => 390,
      "locked" => true,
      "showInColorSelection" => false,
      "removable" => false,
      "draggable" => false,
      "resizable" => false,
      "rotatable" => false,
      "advancedEditing" => false,
      "editable" => false,
      "uploadZone" => false,
      "colors" => false,
      "topped" => true,
    ]
  ];
}

function imageElement($source, $name, $colorGroup = false, $fill = "#fff", $colors = "", $ubicacion = "")
{
  return [
    "type" => "image",
    "source" => $source,
    "title" => $name,
    "parameters" => [
      "left" => 400,
      "top" => 390,
      "colorLinkGroup" => $colorGroup,
      "fill" => $fill,
      "colors" => $colors,
      "showInColorSelection" => true,
      "boundingBoxMode" => "none",
      "ubicacion" => $ubicacion
    ]
  ];
}


function SisaFrontal()
{
  return [
    "type" => "image",
    "source" => "assets/img/arquitectura/lineas_sisa_mediasisa.png",
    "title" => "Lineas",
    "parameters" => [
      "left"=> 400,
      "top" => 390,
      "boundingBoxMode" => "none",
      "fill" => "#000",
      "showInColorSelection"=> true
    ]
  ];
}

function SisaLateral()
{
  return [
    "type" => "image",
    "source" => "assets/img/arquitectura/sisa_costado.png",
    "title" => "Lineas",
    "parameters" => [
      "left"=> 400,
      "top" => 390,
      "boundingBoxMode" => "none",
      "fill" => "#000",
      "showInColorSelection"=> true
    ]
  ];
}



function pixelElement()
{
  return [
    "type" => "image",
    "source" => "assets/img/modelo/pixel.png",
    "title" => "Color general",
    "parameters" => [
      "left" => 400,
      "top" => 329,
      "colorLinkGroup" => "general",
      "showInColorSelection" => true,
      "z" => -1
    ]
  ];
}

function generalOptions($view = "", $viewName = "", $product = "")
{
  switch ($viewName) {
    case 'frente':
      $left = 280.0;
      $width = 235.0;
      $height = 355.0;
      $top = 325.0;
      break;
    case 'espalda':
      $left = 280.0;
      $width = 245.0;
      $height = 400.0;
      $top = 290.0;
      break;
    case 'lateral_derecho':
      if (strpos($product, "Chomba") !== false) {
        $left = 315.0;
        $width = 85.0;
        $height = 125.0;
        $top = 300.0;
      }
      elseif (strpos($product, "Remera") !== false) {
        $left = 345.0;
        $width = 70.0;
        $height = 110.0;
        $top = 270.0;
      }
      else{
        $left = 365.0;
        $width = 70.0;
        $height = 400.0;
        $top = 325.0;
      }
      break;
    case 'lateral_izquierdo':
      if (strpos($product, "Chomba") !== false) { // Delimitar area de escudos y textos
        $left = 405.0;
        $width = 85.0;
        $height = 125.0;
        $top = 300.0;
      }
      elseif (strpos($product, "Remera") !== false) {
        $left = 385.0;
        $width = 70.0;
        $height = 110.0;
        $top = 270.0;
      }
      else{
        $left = 365.0;
        $width = 70.0;
        $height = 400.0;
        $top = 325.0;
      }
      break;
    default:
      $left = 280.0;
      $width = 235.0;
      $height = 400.0;
      $top = 325.0;
      break;
  }

  return [
    "usePrintingBoxAsBounding" => true,
    "boundingBoxMode" => "none",
    "printingBox" => [
      "left" => $left,
      "top" => $top,
      "width" => $width,
      "height" => $height,
      "visibility" => false
    ]
  ];
}
