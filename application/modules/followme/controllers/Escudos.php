<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Colores
 *
 * Endpoints públicos para listar los json con los escudos disponibles
 *
 */ // SW-Ingenieria
class Escudos extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->model('Shields', 'modelShields');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
    }

    public function Shields() {
        $data = $this->modelShields->get_all();
        foreach ($data as &$shield){
            unset($shield['_id']);
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }
}