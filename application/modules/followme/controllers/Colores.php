<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Colores
 *
 * Endpoints públicos para listar los json con los colores disponibles
 *
 */
class Colores extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->model('Colors', 'modelColors');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
    }

    public function Colors($category = "") {
        $query = $category ? ["category" => $category] : [];
        $data = $this->modelColors->get_all($query);
        $arr = array();
        foreach ($data as $color){
          $arr[] = $color['colorHex'];
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($arr));
    }

    public function Nombres($category = "") {
        $query = $category ? ["category" => $category] : [];
        $data = $this->modelColors->get_names($query);
        $arr = array();
        foreach ($data as $key => $color){
          $arr[substr($color['colorHex'],1)] = $color['colorName']." ";
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($arr));
    }






}