<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Banderas
 *
 * Endpoints públicos para listar los json con las banderas disponibles
 *
 */ // SW-Ingenieria
class Banderas extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->model('Flags', 'modelFlags');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
    }

    public function Flags() {
        $data = $this->modelFlags->get_all();
        foreach ($data as &$flag){
            unset($flag['_id']);
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }
}