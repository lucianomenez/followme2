<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Colores
 *
 * Endpoints públicos para listar los json con los colores disponibles
 *
 */ // SW-Ingenieria
class Fuentes extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->model('Fonts', 'modelFonts');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
    }

    public function Fonts() {
        $data = $this->modelFonts->get_all();
        foreach ($data as &$font){
            unset($font['_id']);
        }

        return $this->output
            ->set_content_type('application/json')
            ->set_status_header(200)
            ->set_output(json_encode($data));
    }
}