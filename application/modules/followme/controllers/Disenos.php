<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Productos
 *
 * Endpoints públicos para listar los json con los productos disponibles
 *
 */
class Disenos extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->config('dashboard/config');

        $this->load->model('Product', 'modelProduct');
        $this->load->model('Element', 'modelElement');
        $this->load->model('Colors', 'modelColors');

        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
    }

    public function GetDisenos($id = null)
    {
      // prevent E_WARNING in not existent folders
      error_reporting(E_ERROR | E_PARSE);

      // functions for the images parameters
      $frisaColors = join(",",$this->modelColors->get_all(["category" => "Frisa"], true));

      $paths = [
        // BUZOS
        "114SCBCV" => "assets/img/arquitectura/buzos/bolsillos_canguro_vivos/sin_capucha/",
        "124CCBCV" => "assets/img/arquitectura/buzos/bolsillos_canguro_vivos/capucha/sin_cordones/",
        "121CCBO" => "assets/img/arquitectura/buzos/bolsillos_ocultos/capucha/sin_cordones/",
        "111SCBO" => "assets/img/arquitectura/buzos/bolsillos_ocultos/sin_capucha/",
        "122CCBD" => "assets/img/arquitectura/buzos/bolsillos_ojal/capucha/sin_cordones/",
        "112SCBD" => "assets/img/arquitectura/buzos/bolsillos_ojal/sin_capucha/",
        "120CCSB" => "assets/img/arquitectura/buzos/sin_bolsillos/capucha/sin_cordones/",
        "110SCSB" => "assets/img/arquitectura/buzos/sin_bolsillos/sin_capucha/",
        // CAMPERAS
        "229CCBCVC" => "assets/img/arquitectura/camperas/bolsillos_canguro_vivos/capucha/cordones/",
        "219CCBCV" => "assets/img/arquitectura/camperas/bolsillos_canguro_vivos/capucha/sin_cordones/",
        "226CCBOC" => "assets/img/arquitectura/camperas/bolsillos_ocultos/capucha/cordones/",
        "216CCBO" => "assets/img/arquitectura/camperas/bolsillos_ocultos/capucha/sin_cordones/",
        "227CCBDC" => "assets/img/arquitectura/camperas/bolsillos_ojal/capucha/cordones/",
        "217CCBD" => "assets/img/arquitectura/camperas/bolsillos_ojal/capucha/sin_cordones/",
        "225CCSBC" => "assets/img/arquitectura/camperas/sin_bolsillos/capucha/cordones/",
        "215CCSB" => "assets/img/arquitectura/camperas/sin_bolsillos/capucha/sin_cordones/",
        // CHOMBAS
        "001CHOM" => "assets/img/arquitectura/chombas/chomba_jersey/punos/",
        "002CHOM" => "assets/img/arquitectura/chombas/chomba_jersey/sin_punos/",
        "003CHOM" => "assets/img/arquitectura/chombas/chomba_pique/punos/",
        "004CHOM" => "assets/img/arquitectura/chombas/chomba_pique/sin_punos/",
        // REMERAS
        "001REME" => "assets/img/arquitectura/remeras/punos/",
        "002REME" => "assets/img/arquitectura/remeras/sin_punos/"
      ];

      if ($id) {
        $productUrl = $paths[$id];
      }

      // init the response
      $default = json_decode(file_get_contents(APPPATH."modules/followme/assets/json/examples/disenos.json"), true);
      $response = [];

      // check if file exists
      if (file_exists(APPPATH."modules/followme/$productUrl/disenos.json")) {
          $response = json_decode(file_get_contents(APPPATH."modules/followme/$productUrl/disenos.json"), true);
          $response = array_merge($response, $default);
      }

      // return json
      return $this->output
      ->set_content_type('application/json')
      ->set_status_header(200)
      ->set_output(json_encode($response));
    }
}
