<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Element extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->config->load('user/config');
    }

    public function add($data) {

        $element = $this->save($data);

        return $element;
    }

    public function get($e) {

        $element = $this->db->get_where('elements', ['id' => $e['id']])->result(false)[0];

        $element['source'] = base_url().$element['source'];

        if(is_array($element['parameters'])) {

            switch ($element['layer_type']) {
                case 'lighting':
                    $element['parameters'] = array_merge($this->parameters__lh(), $element['parameters']);
                    break;
                
                default:
                    $element['parameters'] = array_merge($this->parameters__img($e['color_group']), $element['parameters']);
                    break;
            }
        }

        return $element;
    }

    // parametros para las capas de luz y sombra
    private function parameters__lh(){
        return [
            "left" => 322,
            "top" => 390,
            "locked" => true,
            "showInColorSelection" => false,
            "removable" => false,
            "draggable" => false,
            "resizable" => false,
            "rotatable" => false,
            "advancedEditing" => false,
            "editable" => false,
            "uploadZone" => false,
            "colors" => false,
            "topped" => true,
        ];
    }

    private function parameters__img($colorGroup = false){
        return [
            "left" => 322,
            "top" => 390,
            "colorLinkGroup" => $colorGroup,
            "fill" => "#fff",
            "showInColorSelection" => true
        ];
    }

}
