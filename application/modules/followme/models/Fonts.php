<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fonts extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->config->load('user/config');
    }

    public function get_all(){
        $query = [];
        $result = array();
        $this->db->select(array("fontName", "font", "relsize"))->where($query);              
        $container = 'followme.fonts';
        $result = $this->db->get($container)->result_array();
        return $result;
    }
}