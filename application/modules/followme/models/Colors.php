<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Colors extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->config->load('user/config');
    }

    public function get_names($query = []) {
        $result = array();
        $this->db->select(array("colorHex", "colorName", "category"))->where($query);              
        $container = 'followme.colors';
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    public function get_all($query = [], $plain = false) {
      $result = array();
      $this->db->select(array("colorHex"))->where($query);
      $container = 'followme.colors';
      $result = $this->db->get($container)->result_array();

      if ($plain) {
        $arr = array();
        foreach ($result as $color){
          $arr[] = $color['colorHex'];
        }
        return $arr;
      }

      return $result;
    }
}