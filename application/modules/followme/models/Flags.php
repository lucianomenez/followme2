<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Flags extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->config->load('user/config');
    }

    public function get_all(){
        $query = [];
        $result = array();
        $this->db->select(array("flagName", "flag", "show"))->where($query);              
        $container = 'followme.flags';
        $result = $this->db->get($container)->result_array();
        return $result;
    }
}