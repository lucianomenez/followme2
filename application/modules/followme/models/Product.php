<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->config->load('user/config');

        $this->load->model('Element', 'modelElement');
    }

    public function add($data) {

        $product = $this->save($data);

        return $product;
    }

    public function get($query = []) {

        $product = $this->db->get_where('products', $query)->result(false)[0];

        $product = $this->set_elements($product);

        $product['thumbnail'] = base_url().$product['thumbnail'];

        return $product;
    }

    function set_elements($product){
        foreach ($product['elements'] as $key => &$element) {
            $element = $this->modelElement->get($element);
        }

        return $product;
    }

    public function get_all($query = []) {

        $return = [];

        $products = $this->db->get_where('products', $query)->result(false);

        foreach ($products as $key => &$product) {

            $product['thumbnail'] = base_url().$product['thumbnail'];

            # order by category
            if (isset($return[$product['category']])) {
                # code...
            }

            $product = $this->set_elements($product);
        }

        return $return;
    }

    public function getAll($category = '') {

        $query = [
            'category' => $category
        ];
        $products = $this->db->get_where('products', $query)->result();

        return $products;
    }

}
