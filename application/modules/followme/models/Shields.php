<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shields extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->config->load('user/config');
    }

    public function get_all(){
        $query = [];
        $result = array();
        $this->db->select(array("shieldName", "shield", "show"))->where($query);              
        $container = 'followme.shields';
        $result = $this->db->get($container)->result_array();
        return $result;
    }
}