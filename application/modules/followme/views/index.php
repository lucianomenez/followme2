<!DOCTYPE HTML>
  <html lang="es_AR">

    <head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-170190430-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-170190430-1');
    </script>
      <meta name="title" content="Diseñá tu campera o buzo de egresados ideal en Follow Me"/>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="format-detection" content="telephone=no">
      <meta name="description" content="Entrá a nuestro Design Room y probá los diferentes diseños de buzos y camperas de egresados que tenemos en Follow Me para vos y tus compañeros!"/>
      <meta name="keywords" content="followme, followme indumentaria, indumentaria egresados, ropa estudiantes, empresa de indumentaria egresados, vestimenta de egresados, equipos para egresados"/>
      <meta name="author" content="Follow Me">
      <meta name="robots" content="index, follow">
      <meta property="og:site_name" content="Followme"/>
      <meta property="og:title" content="{title}"/>
      <meta property="og:description" content="{meta_description}"/>
      <meta property="og:url" content="{base_url}followme/demo"/>
      <meta property="og:image" itemprop="image" content="{featured_image}"/>
      <meta property="og:image:width" content="1200"/>
      <meta property="og:image:height" content="627"/>
      <meta property="og:image:alt" content="{title}"/>
      <meta property="og:type" content="website" />
      <meta name="format-detection" content="telephone=no">
      <meta property="og:locale" content="es_AR" />
      <meta name="twitter:title" content="{title}">
      <meta name="twitter:description" content="{meta_description}">
      <meta name="twitter:image" content="{featured_image}">
      <meta name="google" content="notranslate">
      <link rel="canonical" href="https://designroom.followme.com.ar/followme/demo">
      <link rel="apple-touch-icon" sizes="180x180" href="https://followme.com.ar/img/favicon/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="https://followme.com.ar/img/favicon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="https://followme.com.ar/img/favicon/favicon-16x16.png">

			<title>Followme.com.ar | Indumentaria y accesorios para egresados</title>
      <script type="application/ld+json">
      {
        "@context": "https://schema.org",
        "@type": "Organization",
        "name": "Follow Me",
        "description" : "Entrá a nuestro Design Room y probá los diferentes diseños de buzos y camperas de egresados que tenemos en Follow Me para vos y tus compañeros!",
        "url": "https://designroom.followme.com.ar",
        "logo": "https://designroom.followme.com.ar/followme/assets/img/logo10_anios.png",
        "sameAs": [
          "https://www.facebook.com/followmegrads",
          "https://www.instagram.com/followmegresados/"
        ],
        "contactPoint": {
          "@type": "ContactPoint",
          "telephone": "+54-911-5835-2669",
          "contactType": "Atención al Cliente",
          "areaServed": "AR",
          "availableLanguage": "Spanish"
        }
      }
      </script>
      <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700,800&display=swap" rel="stylesheet">
      <link href="{base_url}website/assets/css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <script>
        var base_url = "{base_url}"
      </script>

    <link rel="stylesheet" href="{module_url}/assets/css/bootstrap/css/bootstrap.min.css">
     <link rel="stylesheet" href="{module_url}/assets/css/estilos.css">
     <link rel="stylesheet" href="{module_url}/assets/css/animate.min.css">

          <!-- FUENTES GOOGLE FONTS -->
          <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Saira:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css2?family=Saira+Extra+Condensed:wght@100;200;300;400;500;600;700;800&family=Saira:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">


      <script src="{module_url}/assets/js/jquery.min.js" type="text/javascript"></script>
      <script src="{module_url}/assets/js/jquery-ui.min.js" type="text/javascript"></script>
      <script src="{base_url}jscript/bootstrap4/js/bootstrap.bundle.min.js"></script>
      <link rel="stylesheet" href="{base_url}website/assets/slick/slick.css">
      <link rel="stylesheet" href="{base_url}website/assets/slick/slick-theme.css">
      <link rel="stylesheet" type="text/css" href="{base_url}website/assets/css/main.css">
      <link rel="stylesheet" type="text/css" href="{base_url}website/assets/css/breakpoints.css">
  	<!--


     <link rel="stylesheet" type="text/css" href="{base_url}website/assets/css/agency.css?{rand}">
		The CSS for the plugin itself - required 
        <link rel="stylesheet" href="{base_url}website/assets/css/ap-fullscreen-modal.css?{rand}">
  -->
			<link rel="stylesheet" type="text/css" href="{module_url}/assets/css/FancyProductDesigner-all.min.css" />
			<!-- Style sheets -->


       <script src="https://www.google.com/recaptcha/api.js" async defer></script>
			<!-- Include js files -->

        <script src="{base_url}website/assets/slick/slick.min.js"></script>
			<!-- HTML5 canvas library - required -->
			<script src="{module_url}/assets/js/fabric.min.js" type="text/javascript"></script>
			<!-- The plugin itself - required -->
			<script src="{module_url}/assets/js/plugins.min.js" type="text/javascript"></script>
			<script src="{module_url}/assets/js/FancyProductDesigner.js" type="text/javascript"></script>

			<script src="{module_url}/assets/js/FancyProductDesignerPlus.js" type="text/javascript"></script>

			<script src="{fm_js}" type="text/javascript"></script>

      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-59145010-4"></script>
      {ignore}
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-59145010-4');
      </script>
     <style type="text/css">

      @media (min-width: 576px){
        .col-sm-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
      }
      @media (min-width: 992px){
        .col-lg-2 {
            -ms-flex: 0 0 16.666667%;
            flex: 0 0 16.666667%;
             max-width: 16.666667%;
        }
      }

      @media (max-width: 576px){
        .col-xs-6 {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
        }
      }
        #mainNav{
          position:absolute!important;
        }


        .slider {
            width: 96%;
            margin: 0 auto;
        }
        .slick-slide {
          margin: 0px 20px;
        }
        .slick-slide img {
          width: 100%;
        }
        .slick-prev:before,
        .slick-next:before {
          color: black;
        }
        .slick-slide {
          transition: all ease-in-out .3s;
          opacity: .2;
        }
        .slick-active {
          opacity: .5;
        }
        .slick-current {
          opacity: 1;
        }

        .imgRedonda {
            width:300px;
            height:300px;
            border-radius:150px;
        }
        .fpd-container .fpd-loader-wrapper {

          z-index: 50!important;
        }
      </style>
      {/ignore}
    </head>

    <body>

    <header>
        <nav class="fixed-top">
          <div class="container contenedor-nav">
            <a href="https://followme.com.ar/">
              <img src="{module_url}/assets/img/logo10_anios.png" alt="Logo FollowMe"/>
            </a>
            <ul id="ul-menu">
            <!--<li><a class="item-menu" href="https://followme.com.ar/empresas">Empresas</a></li>
            <li><a class="item-menu" href="https://followme.com.ar/weareback">Destacados</a></li>
              <li><a class="item-menu" href="https://followme.com.ar/productos">Productos</a></li>
              <li><a class="item-menu" href="https://followme.com.ar/inspirate">Inspirate</a></li>
              <li><a class="item-menu" href="https://followme.com.ar/experienciafm">Experiencia Follow Me</a></li>
              <li><a class="item-menu" href="https://followme.com.ar/contacto">Contacto</a></li>-->

              {if {login} == 1}
                <li><a class="item-menu" href="{base_url}user/logout">Salir</a></li>
              {else}
                <li><a class="item-menu" href="{base_url}user/login">Ingresar</a></li>
              {/if}
            </ul>
            <span id="icon-burger" class="icon-burger"></span>
          </div>
        </nav>
 </header>

      <h1 style="text-align:center;padding-top:10rem;">Animate y ¡diseña tu buzo o campera de egresados ahora!</h1>
 	    <input type="hidden" id="base_url" name="base_url" value="{base_url}">
      {menu}

			<div id="fpd" class="fpd-container fpd-views-inside-right fpd-no-shadow fpd-top-actions-centered fpd-bottom-actions-centered fpd-device-desktop fpd-sidebar fpd-tabs-side fpd-tabs fpd-grid-columns-1 mt-8" style="margin-top: 2rem!important;">
			</div>



      {if {login} == 1}

			<!-- <button id="generate">Generar ficha</button> -->
		  <img id="pdf-background" style="display:none;" src="{module_url}/assets/img/ficha-baseV4.png" />
      <img id="pdf-background2" style="display:none;" src="{module_url}/assets/img/chombaV3.png" />
      <img id="pdf-background3" style="display:none;" src="{module_url}/assets/img/remeraV3.png" />

      <img id="pdf-mda" style="display:none;" src="{module_url}/assets/img/mda.png" />

<div class="container-fluid marine p-2">
  <div class="container  my-4">
    <form role="form" class=" cols-12 col-sm-12 col-md-12" style="margin: 0 auto;">
      <div class="form-row row">
        <div class="col-12 col-sm-4 mb-2">
          <input type="text" autocomplete="off" id="nro_de_orden" required class="form-control input-room" placeholder="N° de orden">
        </div>
        <div class="col-12 col-sm-4 mb-2">
          <input type="text" autocomplete="off" id="fecha_de_entrega" required class="form-control input-room" placeholder="Fecha de entrega">
        </div>
        <div class="col-12 col-sm-4 mb-2">
          <input type="text" autocomplete="off" id="prototipo" required class="form-control input-room" placeholder="Prototipo">
        </div>
      </div>
      <div class="form-row row">
        <div class="col-12 col-sm-3 mb-2">
          <input type="text" autocomplete="off" id="colegio_form" required class="form-control input-room" placeholder="Colegio">
        </div>
        <div class="col-12 col-sm-2 mb-2">
          <input type="text" autocomplete="off"   id="curso" required class="form-control input-room" placeholder="Curso">
        </div>
        <div class="col-12 col-sm-4 mb-2">
          <input type="text" autocomplete="off"  id="responsable" required class="form-control input-room" placeholder="Nombre y Apellido">
        </div>
        <div class="col-12 col-sm-3 mb-2">
          <input type="email" autocomplete="off"   id="email_responsable" required class="form-control input-room" placeholder="Email Responsable">
        </div>
      </div>
      <div class="form-group">
        <textarea class="form-control input-room" rows="3" id="observaciones"  placeholder="Observaciones"></textarea>
      </div>
      <div class="form-group">
        <div class="btn pull-right a-room mt-2" id="agregar_elemento"><i class="fas fa-plus"></i> Agregar elemento</div>
      </div>
      <div id="append"></div>
      <div class="form-group">
      <input id="generate" class="btn pull-right btn-room mt-4" type="submit" name="submit" value="Generar Orden">
      </div>
    </form>
  </div>
</div>


    {/if}

    <!-- <section style="padding-bottom: 0;padding-top: 0; margin-top: 4rem;">
      <div class="container-fluid pt-2 pb-2" style="border-top: 1px solid #ebedee; border-bottom: 1px solid #ebedee">
      <div class="container">
        <div class="variable slider p-0" style="white-space:nowrap;">
          {categorias_paleta}
          <span class="categoria-paletas option_categoria_paletas" data-name="{text}" value="{value}" style="outline: none; ">{text} </span>
           {/categorias_paleta}
        </div>
      </div>
    </div>
    </section>
    <section style="padding: 0;">
      <div class="container mt-4">
        <div class="row pt-4" id="data_paletas">
            {paletas}
        </div>
      </div>
    </section> -->

    <div style="padding: 2rem 2rem 0 2rem;">
        <h2 style="margin-bottom: 2rem;">¡Diseñá tu buzo de egresados con Follow Me!</h2>
        <p>Somos Follow Me, la empresa líder en camperas y buzos de egresados en Argentina, con más de 10 años en el sector, especializándonos en ropa para egresados de la más alta calidad. Todos los años creamos nuevas campañas para que puedas inspirarte y encontrar la campera de egresados que tanto ansiabas tener.</p>
        <p>En nuestro Design Room podrás elegir el diseño de campera de egresados que más te guste y modificar cada parte de la prenda, con el estilo y los colores que más te gusten, como también agregar tu nombre, colegio, frases y demás, para visualizar tu buzo de egresados soñado.</p>
        <p>También podrás diseñar tu remera, chomba o cardigan para armar el conjunto de egresados perfecto para tu promoción.</p>
        <p>Con Follow Me te aseguras una campera de egresados de la mejor calidad y con toda la onda, como estabas soñando. ¡Diseñá tu buzo de egresados ahora y compartíselo a todos tus compañeros para encargarlo cuanto antes!</p>
      </div>

    <section id="section__seguinos">
        <div class="container">
          <div class="section__seguinos--contenedor-flex">
            <h2 class="section__contacto--titulo ">SEGUINOS</h2>
            <img class="flechas-seguinos-home animate__animated animate__slideInLeft animate__infinite" width="80px" src="https://followme.com.ar/img/flechas-seguinos-home.png">
            <div>
              <a id="icon-face" class="section__seguinos--iconredes" target="_blank" href="https://www.facebook.com/followmegrads"></a>
              <a id="icon-insta" class="section__seguinos--iconredes" target="_blank" href="https://www.instagram.com/followmegresados/"></a>
              <a id="icon-pinte" class="section__seguinos--iconredes" target="_blank" href="https://ar.pinterest.com/followmebuzosycamperas/"></a>
              <a id="icon-tiktok" class="section__seguinos--iconredes" target="_blank" href="https://www.tiktok.com/@followmeegresados?"></a>
            </div>
            <img class="icon-huella" width='40px' src="https://followme.com.ar/img/icon-huella.svg">
          </div>
        </div>
        
      </section>
      <div class="icon-whatsapp">
        <img class="flechas-wp" src="https://followme.com.ar/img/flechas-acompanan-whatsapp.svg">
        <a href="https://api.whatsapp.com/send?phone=5491158352669">
        <img class="logo-wp" src="https://followme.com.ar/img/icon-whatsapp.svg">
      </a>
    </div>
      <footer>
        <p>&copy; 2024 FOLLOW ME. Todos los derechos reservados.</p>
      </footer>
<script>
  var base_url = '{base_url}';
</script>

{ignore}
<style>
@media only screen and (min-width: 480px) {
  div.slick-track{
    transform: unset!important;
  }
}
</style>
<script type="text/javascript">
       $(document).ready(function(){
         $(".slider").slick({
          dots: false,
          infinite: true,
          rtl: false,
          centerMode: false,
          variableWidth: true,
          focusOnSelect: true,
          slidesToShow: 3,
          slidesToScroll: 3,
           adaptiveHeight: true,
          responsive: [
            {
              breakpoint: 480,
              settings: {
                arrows: false,
                centerMode: true,
                slidesToShow: 1
              }
            }]
       });


        $('body').on('click', '.option_categoria_paletas',function(e) {
            let categoria=$(this).attr('value');
          let name_categoria=$(this).attr('data-name');
          let url=$('#base_url').val() + 'website/get_paletas';
          $.ajax({
                url: url,
                type: "POST",
                data: {
                  categoria : categoria,
                  name:name_categoria,
                  post_type : 'paletas'
                }
              }).done(function(response) {

                 $("#data_paletas").html('');
                $('#data_paletas').hide().html(response).fadeIn('slow');
              })
        });
       })
</script>
<script src="{module_url}/assets/js/wow.min.js"></script>
      <script>
        new WOW().init();
      </script>
<script src="{module_url}/assets/js/Main.js"></script>
<script src="{base_url}website/assets/js/enviar.js?10"></script>
{/ignore}
<!--<script src="{module_url}/assets/js/addons.js" type="text/javascript"></script>-->
</body>
</html>
