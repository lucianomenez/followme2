<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * dna2
 *
 * Description of the class dna2
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Sucursales extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->module('elements');
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');
        $this->load->model('Model_colegios');
        $this->load->helper('file');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->module('api');
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->titulo = "Follow Me - Diseños Personalizados";
        }



    function Index() {
          $user = $this->user->get_user($this->idu);
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          $data['name'] = $user->name;
          $data['lastname'] = $user->lastname;
          $data['email'] = $user->email;
          $data['head'] = $this->elements->ui_head();
          $data['header'] = $this->elements->ui_header();
          $data['navbar'] = $this->elements->ui_navbar();
          $data['footer'] = $this->elements->ui_footer();
          $data['navbar_mobile'] = $this->elements->ui_navbar_mobile();

        return $this->parser->parse("sucursales", $data);
    }

    function navbar(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        return $this->parser->parse("navbar", $data);
    }

     function ui_header(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['title'] = $this->titulo;
        return $this->parser->parse("header", $data);
    }

     function ui_footer(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['title'] = $this->titulo;
        return $this->parser->parse("footer", $data);
    }

    function navbar_mobile(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        return $this->parser->parse("navbar_mobile", $data);
    }

    function reload_tabla(){
      $data = $this->input->post();
      if ($data["ciudad"] != "Ciudad"){
        $query['city_id'] = $data["ciudad"];
      };
      if ($data["sucursal"] != "Sucursal"){
        $query['subsidiary_id'] = $data["sucursal"];
      };
      if ($data["provincia"] != "Provincia"){
        $query['province_id'] = $data["provincia"];
      };
      if ($data["partido"] != "Partido"){
        $query['department_id'] = $data["partido"];
      };

      $data['colegios'] = $this->Model_colegios->get_colegios_x_query($query);

      foreach ($data["colegios"] as &$colegio){
        $ciudad = $this->Model_colegios->get_ciudades($colegio["city_id"]);
        $colegio["ciudad"] = $ciudad[0]['name'];

        $departamento = $this->Model_colegios->get_departamentos($ciudad["departament_id"]);
        $colegio["departamento"] = $departamento[0]['name'];

        $provincia = $this->Model_colegios->get_provincias($departamento["province_id"]);
        $colegio["provincia"] = $provincia[0]['name'];

        $sucursal = $this->Model_colegios->get_sucursal($colegio["subsidiary_id"]);
        $colegio["sucursal"] = $sucursal[0]['name'];
      }


        echo $this->parser->parse("reloaded_table", $data);

    }


}

/* End of file dna2 */
/* Location: ./system/application/controllers/welcome.php */
