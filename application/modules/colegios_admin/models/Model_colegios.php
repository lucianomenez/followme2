<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_colegios extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */

    function get_ciudades($id = null){
        $result = array();
        $container = 'cities';
        if ($id != null){
            $query['id'] = $id;
        }
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function get_sucursal($id = null){
        $result = array();
        $container = 'subsidiaries';
        if ($id != null){
            $query['id'] = $id;
        }
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function get_colegios($id = null){
        $result = array();
        $container = 'schools';
        if ($id != null){
            $query['id'] = $id;
        }
        $this->db->where($query);
        $this->db->limit(100);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function get_colegios_x_query($query){
        $result = array();
        $container = 'schools';
        $this->db->where($query);
        $this->db->limit(100);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function get_departamentos($id = null){
        $result = array();
        $container = 'departments';
        if ($id != null){
            $query['id'] = $id;
        }
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function get_provincias($id = null){
        $result = array();
        $container = 'provinces';
        if ($id != null){
            $query['id'] = $id;
        }
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }



    function get_users_(){

        $query = array ("idu" => 1);


        $result = array(
        'aggregate' => 'users',
        'pipeline' =>
           array(
               array(
                   '$match' => $query
               ),
               array(
                   '$project' => array(
                        'name' => '$name',
                        'lastname'=>'$lastname',
                        'idnumber'=>'$idnumber'
                    )
               ),

           )
        );
    $rs =  $this->db->command($result);
    return $rs['result'];
    }


}
