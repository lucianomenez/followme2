$(document).ready(function(){

  var base_url = $("#base_url").val();

  $('#filtrar').on('click', function(e){
      e.preventDefault();
      provincia = $("#select_provincia").val();
      partido = $("#select_partido").val();
      ciudad = $("#select_ciudad").val();
      sucursal = $("#select_sucursal").val();

      $.ajax({
        url : base_url + 'colegios/reload_tabla',
        type: 'POST',
        data: {
          provincia : provincia,
          partido : partido,
          ciudad : ciudad,
          sucursal : sucursal

        },
        success: function(data){
              $("#myTable").replaceWith(data);
  //          refresh();
  //          init_edicion();
  //          cargarCalendario();
        },
         error: function(data){
                alert ("Error en el filtrado")
    //          cargarCalendario();
    //          init_edicion();
         }
      });
  });

  $('.input_nomina').on('change', function(e){
    e.preventDefault();
    codigo = $('#codigo').val();
    name = $(this).attr("name");
    value = $(this).val();
    var razon = "";
    $.ajax({
      url : base_url + '/asistencia/api/process_nomina',
      type: 'POST',
      data: {
        codigo : codigo,
        name : name,
        value : value
      },
      success: function(data){
          toastr["success"]("Cambio guardado");
      },
       error: function(data){
            toastr["error"]("Los cambios no fueron guardados");
       }
    });


  });



});
