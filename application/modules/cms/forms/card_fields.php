<?php
return array(
  array(
    'type'  => 'section_title',
    'html_tag' => 'h3',
    'text' => 'Información para la card',
    'class'  => '',
    'position' => 'sidebar'
  ),
  array(
    'type'  => 'image_upload',
    'name'  => 'card_image',
    'element'  => 'card_image',
    'label' => 'Imagen Card <br><span style="font-size: 1rem;">(png 300px por 300px)</span>',
    'class'  => 'form-control',
    'position' => 'sidebar'
  )
);
