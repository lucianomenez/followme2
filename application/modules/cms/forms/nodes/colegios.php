<?php
return array(

    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'value' => 'colegios',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),
    array(
                'type'  => 'input',
                'name'  => 'title',
                'label' => 'Nombre del colegio',
                'class' => 'form-control'
            ),
        array(
                'type'  => 'input',
                'name'  => 'city',
                'label' => 'Lugar',
                'class' => 'form-control'
            ),
    array(
    'type'  => 'dropdown',
        'name'  => 'ano',
        'label' => 'Año',
        'options' => array(
            '2019'  => '2019',
            '2020'  => '2020',
            '2021'  => '2021',
            '2022'  => '2022',
            '2023'  => '2023',
            '2024'  => '2024',
            '2025'  => '2025',
            '2026'  => '2026'
        ),
        'class'  => 'form-control '
    ),
    array(
        'type'  => 'dropdown',
        'name'  => 'type',
        'label' => 'Estado del colegio',
        'options' => array(
            'activo'  => 'Activo',
            'no_activo'  => 'No Activo',
        ),
        'class'  => 'form-control'
    ),

    array(
                'type'  => 'textarea',
                'name'  => 'content',
                'label' => 'Descripción',
                'class'  => 'form-control load-js redactor',
                'data-load' => 'redactor'
            ),  
    array(
    'type'  => 'section_title',
    'html_tag' => 'h3',
    'text' => 'Información para la card',
    'class'  => '',
    'position' => 'sidebar'
  ),
          array(
            'type'  => 'image_upload',
            'name'  => 'card_image',
            'element'  => 'card_image',
            'label' => 'Imagen Card',
            'class'  => 'form-control',
            'position' => 'sidebar'
          ), array(
    'type'  => 'section_title',
    'html_tag' => 'h3',
    'text' => 'Galería. Se puede arrastrar y soltar',
    'class'  => ''
  ),
            array(
                'type'  => 'section_title',
                'html_tag' => 'div',
                'text' => '',
                'class'  => 'myDropzone'
            ),
    array(
      'type'  => 'section_title',
      'html_tag' => 'h3',
      'text' => 'Metadatos',
      'class'  => '',
      'position' => 'sidebar'
    ),
    array(
        'type'  => 'image_upload',
        'name'  => 'featured_image',
        'label' => 'Imagen destacada<br><small>El tamaño de la imagen a mostrar debe ser inferior a 300 KB</small>',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'input',
        'name'  => 'meta_keywords',
        'label' => 'Meta Keywords',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'tags',
        'name'  => 'tags',
        'label' => 'Tags',
        'class'  => '',
        'position' => 'sidebar'
    ),
);
