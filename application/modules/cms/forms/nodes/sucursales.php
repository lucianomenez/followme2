<?php
return array(

    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'value' => 'sucursales',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),    array(
                'type'  => 'input',
                'name'  => 'title',
                'label' => 'Nombre sucursal',
                'class' => 'form-control'
            ),
        array(
        'type'  => 'dropdown',
        'name'  => 'type',
        'label' => 'Estado sucursal',
        'options' => array(
            'activo'  => 'Activo',
            'no_activo'  => 'No Activo',
        ),
        'class'  => 'form-control'
    ),
      array(
                'type'  => 'input',
                'name'  => 'address',
                'label' => 'Dirección',
                'class' => 'form-control'
            ),
       array(
                'type'  => 'input',
                'name'  => 'phone',
                'label' => 'Teléfono',
                'class' => 'form-control'
            ),
      array(
        'type'  => 'input',
        'name'  => 'txt_alternativo',
        'label' => 'Texto alternativo',
        'class' => 'form-control'
        ),
        array(
                'type'  => 'input',
                'name'  => 'place',
                'label' => 'Cómo llegar',
                'class' => 'form-control'
            ),

        array(
                'type'  => 'input',
                'name'  => 'whatsapp',
                'label' => 'WhatsApp número',
                'class' => 'form-control'
            )

);
