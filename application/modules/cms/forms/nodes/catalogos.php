<?php
return array(
        array(
        'type'  => 'input',
        'name'  => 'title',
        'label' => 'Título',
        'class'  => 'title form-control',
    ),
    array(
        'type'  => 'input',
        'name'  => 'slug',
        'label' => 'Enlace permanente',
        'class'  => 'form-control',
    ),
    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'value' => 'catalogos',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),
     array(
    'type'  => 'dropdown',
        'name'  => 'privacidad',
        'label' => 'Privacidad',
        'options' => array(
            'publico'  => 'Público',
            'privado'  => 'Privado'
        ),
        'class'  => 'form-control inline'
    ),
    array(
    'type'  => 'section_title',
    'html_tag' => 'h3',
    'text' => 'Archivo ',
    'class'  => ''
  ),              
    array(
                'type'  => 'section_title',
                'html_tag' => 'div',
                'text' => '',
                'class'  => 'myDropzone inline' 
            ),

    array(
    'type'  => 'section_title',
    'html_tag' => 'h3',
    'text' => 'Información para las redes',
    'class'  => '',
    'position' => 'sidebar'
  ),
          array(
            'type'  => 'image_upload',
            'name'  => 'card_image',
            'element'  => 'card_image',
            'label' => 'Imagen Destacada',
            'class'  => 'form-control',
            'position' => 'sidebar'
          ),
);
