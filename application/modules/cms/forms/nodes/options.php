<?php
return array(
    array(
        'type'  => 'input',
        'name'  => 'title',
        'label' => 'Título',
        'class'  => 'title form-control'
    ),
    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'value' => 'options',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),
    array(
    'type'  => 'dropdown',
        'name'  => 'type',
        'label' => 'Elemento',
        'options' => array(
            ''=>'',
            'categorias'  => 'Categorías'
        ),
        'class'  => 'form-control '
    ),
    array(
            'type'  => 'repeater',
            'name'  => 'items',
            'label' => 'Opciones',
            'class'  => 'opciones',
            'data-load' => '',
            'subfields' => array(
              array(
                'type'  => 'input',
                'name'  => 'text',
                'label' => 'Texto a mostrar',
                'class'  => 'form-control text_option',
                'data-load' => ''
              ),
              array(
                'type'  => 'input',
                'name'  => 'value',
                'label' => 'Valor Interno',
                'class'  => 'form-control text_value',
                'data-load' => '',
                'readonly'=>'readonly'
              ),
            )
          )

);
