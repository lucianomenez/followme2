<?php
return array(
        array(
        'type'  => 'input',
        'name'  => 'title',
        'label' => 'Título',
        'class'  => 'title form-control',
    ),
    array(
        'type'  => 'input',
        'name'  => 'slug',
        'label' => 'Enlace permanente',
        'class'  => 'form-control',
    ),
    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'value' => 'bases_condiciones',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),    array(
    'type'  => 'dropdown',
        'name'  => 'header_footer',
        'label' => 'Incluir header y footer',
        'options' => array(
            'si'  => 'Si',
            'no'  => 'No'
        ),
        'class'  => 'form-control '
    ),

);
