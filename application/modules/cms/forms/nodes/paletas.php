<?php
return array(

    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'value' => 'paletas',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),
    array(
                'type'  => 'input',
                'name'  => 'title',
                'label' => 'Nombre',
                'class' => 'form-control'
            ),
    array(
        'type'  => 'dropdown',
        'name'  => 'type',
        'label' => 'Estado',
        'options' => array(
            'activo'  => 'Activo',
            'no_activo'  => 'No Activo',
        ),
        'class'  => 'form-control'
    ),

    array(
        'type'  => 'dropdown',
        'id'  => 'categoria_paleta',
        'name'  => 'categoria',
        'label' => 'Categoría',
        'options' => array(

        ),
        'class'  => 'form-control'
    ),
            array(
                'type'  => 'input',
                'name'  => 'categoria_value',
                'id'  => 'categoria_paleta_value',
                'label' => '',
                'class'  => 'form-control',
                'data-load' => '',
                'readonly'=>'readonly',
                'style'=>'margin-top:-20px;'
              ),
    array(
        'type'  => 'dropdown',
        'id'  => 'categoria2_paleta',
        'name'  => 'categoria2',
        'label' => 'Otra Categoría',
        'options' => array(

        ),
        'class'  => 'form-control'
    ),            array(
                'type'  => 'input',
                'name'  => 'categoria_value2',
                'id'  => 'categoria_paleta_value2',
                'label' => '',
                'class'  => 'form-control',
                'data-load' => '',
                'readonly'=>'readonly',
                'style'=>'margin-top:-20px;'
              ),

            array(
                'type'  => 'textarea',
                'name'  => 'content',
                'label' => 'Descripción',
                'class'  => 'form-control load-js redactor',
                'data-load' => 'redactor'
            ),
        array(
                'type'  => 'input',
                'name'  => 'hex_color',
                'label' => 'Hex Color (#FFFFFF)',
                'class' => 'form-control'
            ),
          array(
            'type'  => 'image_upload',
            'name'  => 'image',
            'label' => 'Imagen<br><span style="font-size: 1rem;">(png 300px por 300px)</span>',
            'class'  => 'form-control'
          )
);
