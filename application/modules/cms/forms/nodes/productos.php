<?php
return array(

    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'value' => 'productos',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),
    array(
                'type'  => 'input',
                'name'  => 'title',
                'label' => 'Nombre de producto',
                'class' => 'form-control'
            ),
    array(
        'type'  => 'dropdown',
        'name'  => 'type',
        'label' => 'Estado producto',
        'options' => array(
            'activo'  => 'Activo',
            'no_activo'  => 'No Activo',
        ),
        'class'  => 'form-control'
    ),

    array(
        'type'  => 'dropdown',
        'id'  => 'categoria',
        'name'  => 'categoria',
        'label' => 'Categoría',
        'options' => array(

        ),
        'class'  => 'form-control'
    ),
            array(
                'type'  => 'input',
                'name'  => 'categoria_value',
                'id'  => 'categoria_value',
                'label' => '',
                'class'  => 'form-control',
                'data-load' => '',
                'readonly'=>'readonly',
                'style'=>'margin-top:-20px;'
              ),
    array(
        'type'  => 'dropdown',
        'id'  => 'categoria2',
        'name'  => 'categoria2',
        'label' => 'Otra Categoría',
        'options' => array(

        ),
        'class'  => 'form-control'
    ),            array(
                'type'  => 'input',
                'name'  => 'categoria_value2',
                'id'  => 'categoria_value2',
                'label' => '',
                'class'  => 'form-control',
                'data-load' => '',
                'readonly'=>'readonly',
                'style'=>'margin-top:-20px;'
              ),

        array(
                'type'  => 'textarea',
                'name'  => 'content',
                'label' => 'Descripción',
                'class'  => 'form-control load-js redactor',
                'data-load' => 'redactor'
            ),
          array(
            'type'  => 'image_upload',
            'name'  => 'image',
            'label' => 'Imagen Frente<br><span style="font-size: 1rem;">(png 300px por 300px)</span>',
            'class'  => 'form-control'
          ),
          array(
            'type'  => 'image_upload',
            'name'  => 'image_back',
            'label' => 'Imagen Espalda<br><span style="font-size: 1rem;">(png 300px por 300px)</span>',
            'class'  => 'form-control'
          ),
          array(
            'type'  => 'image_upload',
            'name'  => 'image_right',
            'label' => 'Imagen Derecha<br><span style="font-size: 1rem;">(png 300px por 300px)</span>',
            'class'  => 'form-control'
          ),
          array(
            'type'  => 'image_upload',
            'name'  => 'image_left',
            'label' => 'Imagen Izquierda<br><span style="font-size: 1rem;">(png 300px por 300px)</span>',
            'class'  => 'form-control'
          ),
           array(
      'type'  => 'section_title',
      'html_tag' => 'h3',
      'text' => 'Metadatos',
      'class'  => '',
      'position' => 'sidebar'
    ),
    array(
        'type'  => 'image_upload',
        'name'  => 'featured_image',
        'label' => 'Imagen destacada<br><small>El tamaño de la imagen a mostrar debe ser inferior a 300 KB</small>',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'input',
        'name'  => 'meta_keywords',
        'label' => 'Meta Keywords',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'tags',
        'name'  => 'tags',
        'label' => 'Tags',
        'class'  => '',
        'position' => 'sidebar'
    )
);
