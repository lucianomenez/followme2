<?php
return array(
    array(
        'type'  => 'input',
        'name'  => 'title',
        'label' => 'Título',
        'class'  => 'title form-control',
    ),
    array(
        'type'  => 'input',
        'name'  => 'slug',
        'label' => 'Enlace permanente',
        'class'  => 'form-control',
    ),
    array(
        'type'  => 'hidden',
        'name'  => 'class'
    ),
    array(
        'type'  => 'input',
        'id'  => 'tipo_file',
        'value' => 'web',
        'class'=>'hidden',
        'style'=>'display:none;'
    ),
    array(
      'type'  => 'section_title',
      'html_tag' => 'h3',
      'text' => 'Información general',
      'class'  => '',
      'position' => 'sidebar'
    ),

    array(
        'type'  => 'datetime',
        'name'  => 'post_date',
        'label' => 'Fecha',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'hidden',
        'name'  => 'header_class',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'dropdown',
        'name'  => 'logo_color',
        'label' => 'Color del logo en el header transparente',
        'options' => array(
            'blanco'  => 'Blanco',
            'color'  => 'A color',
        ),
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'image_upload',
        'name'  => 'featured_image',
        'label' => 'Imagen destacada<br><small>El tamaño de la imagen a mostrar debe ser inferior a 300 KB</small>',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
      'type'  => 'section_title',
      'html_tag' => 'h3',
      'text' => 'Metadatos',
      'class'  => '',
      'position' => 'sidebar'
    ),
    array(
        'type'  => 'textarea',
        'name'  => 'meta_description',
        'label' => 'Meta Description<br><small>Debe contener entre 120 y 320 caracteres</small>',
        'class'  => 'form-control',
        'rows'  => 2,
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'input',
        'name'  => 'meta_keywords',
        'label' => 'Meta Keywords',
        'class'  => 'form-control',
        'position' => 'sidebar'
    ),
    array(
        'type'  => 'tags',
        'name'  => 'tags',
        'label' => 'Tags',
        'class'  => '',
        'position' => 'sidebar'
    ),
);
