<?php
return array(
    'name' => 'herobasescondiciones',
    'type' => 'herobasescondiciones',
    'label' => 'Banner',
    'class' => '',
    'icon' => '',
    'form_css' => '',
    'form_js' => '',
    'custom_css' => '',
    'custom_js' => '',
    'node' => 'bases_condiciones',
    'fields' =>
        array(
                       array(
                'type'  => 'section_title',
                'html_tag' => 'p',
                'text' => 'Primera línea',
                'class'  => ''
            ),
                array(
                'type'  => 'input',
                'name'  => 'color',
                'label' => 'Color de texto HEXA (#AAAA) ',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'title1',
                'label' => 'Título 1 <span style="font-size: 1rem;">(Primer línea)</span>',
                'class' => 'form-control'
            ),
            
            array(
                'type'  => 'input',
                'name'  => 'gradient_from',
                'label' => 'Color fondo desde HEXA (#AAAA) ',
                'class' => 'form-control'
            ),            
            array(
                'type'  => 'input',
                'name'  => 'gradient_to',
                'label' => 'Color fondo hasta HEXA (#AAAA)  ',
                'class' => 'form-control'
            ),
                                   array(
                'type'  => 'section_title',
                'html_tag' => 'p',
                'text' => 'Segunda línea',
                'class'  => ''
            ),
            array(
                'type'  => 'input',
                'name'  => 'title2',
                'label' => 'Título 2 <span style="font-size: 1rem;">(Segunda línea)</span>',
                'class' => 'form-control'
            )
        )
);
