<?php
return array(
    'name' => 'home',
    'type' => 'home',
    'label' => 'Home',
    'class' => '',
    'icon' => '',
    'form_css' => '',
    'form_js' => '',
    'custom_css' => '',
    'custom_js' => '',
    'node' => 'page',
    'fields' =>
        array(

            array(
                'type'  => 'section_title',
                'html_tag' => 'h3',
                'text' => 'HERO',
                'class'  => ''
            ),
            array(
                'type'  => 'input',
                'name'  => 'gradient_from_hero',
                'label' => 'Desde color desde rgba / fondo de hero',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'gradient_to_hero',
                'label' => 'Hasta color hasta rgba / fondo de hero',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'title_xx',
                'label' => 'Título de fondo',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'image_upload',
                'name'  => 'image',
                'label' => 'Imagen Hero Desktop',
                'class'  => 'form-control'
            ),
            array(
                'type'  => 'image_upload',
                'name'  => 'image_mobile',
                'label' => 'Imagen Hero Mobile',
                'class'  => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'title1',
                'label' => 'Título 1 <span style="font-size: 1rem;">(Primer línea)</span>',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'title2',
                'label' => 'Título 2 <span style="font-size: 1rem;">(Segunda línea)</span>',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'btn_title_top',
                'label' => 'Primer linea de boton principal',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'btn_title_bottom',
                'label' => 'Segunda linea de boton principal',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'section_title',
                'html_tag' => 'h3',
                'text' => 'EXPERIENCIA FOLLOW ME',
                'class'  => 'mt-0'
            ),
            array(
                'type'  => 'section_title',
                'html_tag' => 'h4',
                'text' => 'Estadísticas',
                'class'  => 'mt-0'
            ),
            array(
                'type'  => 'input',
                'name'  => '1_bajada',
                'label' => '1° Referencia',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => '1_dato',
                'label' => '1° Dato',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => '2_bajada',
                'label' => '2° Referencia',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => '2_dato',
                'label' => '2° Dato',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => '3_bajada',
                'label' => '3° Referencia',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => '3_dato',
                'label' => '3° Dato',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => '4_bajada',
                'label' => '4° Referencia',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => '4_dato',
                'label' => '4° Dato',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'title_institucional',
                'label' => 'Titulo institucional',
                'class' => 'form-control'
            ),
            array(
              'type'  => 'textarea',
              'name'  => 'txt_institucional',
              'label' => 'Texto institucional',
              'class'  => 'form-control load-js redactor',
              'data-load' => 'redactor'
            ),
            array(
                'type'  => 'input',
                'name'  => '1_subtitle_institucional',
                'label' => '1° Destacado institucional',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => '2_subtitle_institucional',
                'label' => '2° Destacado institucional',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'image_upload',
                'name'  => 'figura_insti',
                'label' => 'Imagen para institucional',
                'class'  => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'gradient_from_insti',
                'label' => 'Desde color desde rgba / fondo de imagen',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'gradient_to_insti',
                'label' => 'Hasta color hasta rgba / fondo de imagen',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'section_title',
                'html_tag' => 'h3',
                'text' => 'VIDEO INSTITUCIONAL',
                'class'  => 'mt-0'
            ),
            array(
                'type'  => 'video_upload',
                'name'  => 'video_institucional',
                'label' => 'Video institucional',
                'class'  => 'form-control'
            ),
            array(
                'type'  => 'image_upload',
                'name'  => 'prev_video',
                'label' => 'Preview imagen de video',
                'class'  => 'form-control'
            ),
            array(
                'type'  => 'image_upload',
                'name'  => 'prev_video_mobile',
                'label' => 'Preview imagen de video Mobile',
                'class'  => 'form-control'
            ),
            array(
            'type'  => 'dropdown',
            'name'  => 'color_txt',
            'label' => 'Color del texto',
            'options' => array(
              'blanco'  => 'Blanco',
              'azul'  => 'Azul'
                ),
            'class'  => 'form-control '
            ),
            array(
                'type'  => 'section_title',
                'html_tag' => 'h3',
                'text' => 'PUNTOS DE VENTA',
                'class'  => ''
            ),
            array(
                'type'  => 'input',
                'name'  => 'title_puntos',
                'label' => 'Título puntos de venta',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'txt_puntos',
                'label' => 'Breve bajada puntos de venta',
                'class' => 'form-control'
            ),
            array(
                    'type'  => 'repeater',
                    'name'  => 'items',
                    'label' => 'Punto de venta',
                    'class'  => 'opciones',
                    'data-load' => '',
                    'subfields' => array(
                      array(
                        'type'  => 'input',
                        'name'  => 'name_punto',
                        'label' => 'Nombre',
                        'class'  => 'form-control text_option',
                        'data-load' => ''
                      ),
                    )
                  ),
            array(
                'type'  => 'section_title',
                'html_tag' => 'h3',
                'text' => 'FOLLOW ME REWARDS',
                'class'  => ''
            ),
            array(
                'type'  => 'video_upload',
                'name'  => 'video_rewards',
                'label' => 'Video Rewards',
                'class'  => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'title_rewards1',
                'label' => 'Titulo rewards (1° linea)',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'title_rewards2',
                'label' => 'Titulo rewards (2° linea)',
                'class' => 'form-control'
            ),
            array(
              'type'  => 'textarea',
              'name'  => 'txt_rewards',
              'label' => 'Texto rewards',
              'class'  => 'form-control load-js redactor',
              'data-load' => 'redactor'
            ),
            array(
                'type'  => 'section_title',
                'html_tag' => 'h3',
                'text' => 'NUESTRO COMPROMISO',
                'class'  => ''
            ),
            array(
                'type'  => 'image_upload',
                'name'  => 'img_compromiso',
                'label' => 'Imagen compromiso',
                'class'  => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'title_compromiso',
                'label' => 'Titulo compromiso',
                'class' => 'form-control'
            ),
            array(
              'type'  => 'textarea',
              'name'  => 'txt_compromiso',
              'label' => 'Texto compromiso',
              'class'  => 'form-control load-js redactor',
              'data-load' => 'redactor'
            ),
            array(
               'type'  => 'section_title',
               'html_tag' => 'h3',
               'text' => 'Logos Caminos',
               'class'  => ''
             ),
             array(
                 'type'  => 'input',
                 'name'  => 'title_caminos',
                 'label' => 'Titulo caminos',
                 'class' => 'form-control'
             ),
             array(
                 'type'  => 'section_title',
                  'label' => 'Logos en png sin fondo / arrastrar y soltar',
                 'html_tag' => 'div',
                 'text' => '',
                 'class'  => 'myDropzoneCaminos'
             ),
             array(
                'type'  => 'section_title',
                'html_tag' => 'h3',
                'text' => 'Logos Alianzas',
                'class'  => ''
              ),
              array(
                  'type'  => 'input',
                  'name'  => 'title_alianzas',
                  'label' => 'Titulo caminos',
                  'class' => 'form-control'
              ),
            array(
                'type'  => 'section_title',
                'label' => 'Logos en png sin fondo / arrastrar y soltar',
                'html_tag' => 'div',
                'text' => '',
                'class'  => 'myDropzoneAlianzas'
            ),


        )
);
