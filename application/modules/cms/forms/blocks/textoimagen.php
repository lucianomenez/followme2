<?php
return array(
    'name' => 'textoimagen',
    'type' => 'textoimagen',
    'label' => 'Texto Imagen',
    'class' => '',
    'icon' => '',
    'form_css' => '',
    'form_js' => '',
    'custom_css' => '',
    'custom_js' => '',
    'node' => 'bases_condiciones',
    'fields' =>
        array(
                    array(
              'type'  => 'image_upload',
              'name'  => 'image',
              'label' => 'Imagen',
              'class'  => 'form-control'
          ),
            array(
                'type'  => 'input',
                'name'  => 'figcaption',
                'label' => 'Epígrafe',
                'class' => 'form-control'
            ),
            array(
                'type'  => 'textarea',
                'name'  => 'content',
                'label' => 'Contenido',
                'class'  => 'form-control load-js redactor',
                'data-load' => 'redactor'
            ) 
        )
);
