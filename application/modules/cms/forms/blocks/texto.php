<?php
return array(
  'name' => 'texto',
  'type' => 'texto',
  'label' => 'Texto',
  'class' => '',
  'icon' => '',
  'form_css' => '',
  'form_js' => '',
  'custom_css' => '',
  'custom_js' => '',
  'node' => 'bases_condiciones',
  'fields' =>
      array(
          array(
            'type'  => 'textarea',
            'name'  => 'content',
            'label' => 'Contenido',
            'class'  => 'form-control load-js redactor',
            'data-load' => 'redactor'
          ),
      )
);
