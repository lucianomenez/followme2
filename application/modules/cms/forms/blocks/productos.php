<?php
return array(
    'name' => 'productos',
    'type' => 'productos',
    'label' => 'Productos',
    'class' => '',
    'icon' => '',
    'form_css' => '',
    'form_js' => '',
    'custom_css' => '',
    'custom_js' => '',
    'node' => 'page',
    'fields' =>
        array(
            array(
                'type'  => 'dropdown',
                'id'  => 'categoria',
                'name'  => 'categoria_inicio',
                'label' => 'Categoría de inicio',
                'options' => array(

                ),
                'class'  => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'categoria_value',
                'id'  => 'categoria_value',
                'label' => '',
                'class'  => 'form-control',
                'data-load' => '',
                'readonly'=>'readonly'
              ),
        )
);
