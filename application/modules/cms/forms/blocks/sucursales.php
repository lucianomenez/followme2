<?php
return array(
    'name' => 'sucursales',
    'type' => 'sucursales',
    'label' => 'Sucursales',
    'class' => '',
    'icon' => '',
    'form_css' => '',
    'form_js' => '',
    'custom_css' => '',
    'custom_js' => '',
    'node' => 'page',
    'fields' =>
        array(
          array(
              'type'  => 'image_upload',
              'name'  => 'image',
              'label' => 'Imagen<br><span style="font-size: 1rem;">(png 300px por 300px)</span>',
              'class'  => 'form-control'
          ),
          array(
                  'type'  => 'input',
                  'id'  => 'tipo_file',
                  'value' => 'hero',
                  'class'=>'hidden',
                  'style'=>'display:none;'
          ),
          array(
              'type'  => 'input',
              'name'  => 'title1',
              'label' => 'Título 1 <span style="font-size: 1rem;">(Primer línea)</span>',
              'class' => 'form-control'
          ),
          array(
              'type'  => 'input',
              'name'  => 'title2',
              'label' => 'Título 2 <span style="font-size: 1rem;">(Segunda línea)</span>',
              'class' => 'form-control'
          ),
          array(
              'type'  => 'input',
              'name'  => 'title_back',
              'label' => 'Título de fondo',
              'class' => 'form-control'
          ),
          array(
              'type'  => 'section_title',
              'html_tag' => 'p',
              'text' => 'Gradiente',
              'class'  => ''
          ),
          array(
              'type'  => 'input',
              'name'  => 'gradient_from',
              'label' => 'Color desde rgba ',
              'class' => 'form-control'
          ),
          array(
              'type'  => 'input',
              'name'  => 'gradient_to',
              'label' => 'Color hasta rgba ',
              'class' => 'form-control'
          ),
            array(
                'type'  => 'dropdown',
                'id'  => 'categoria',
                'name'  => 'categoria_inicio',
                'label' => 'Categoría de inicio',
                'options' => array(

                ),
                'class'  => 'form-control'
            ),
            array(
                'type'  => 'input',
                'name'  => 'categoria_value',
                'id'  => 'categoria_value',
                'label' => '',
                'class'  => 'form-control',
                'data-load' => '',
                'readonly'=>'readonly'
              ),
        )
);
