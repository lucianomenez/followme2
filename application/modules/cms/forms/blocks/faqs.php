<?php
return array(
  'name' => 'faqs',
  'type' => 'faqs',
  'label' => 'FAQs',
  'class' => '',
  'icon' => '',
  'form_css' => '',
  'form_js' => '',
  'custom_css' => '',
  'custom_js' => '',
  'node' => 'bases_condiciones',
  'fields' =>
      array(
          array(
            'type'  => 'input',
            'name'  => 'title',
            'label' => 'Título',
            'class'  => 'form-control',
            'data-load' => ''
          ),
          array(
            'type'  => 'repeater',
            'name'  => 'q_a',
            'label' => 'Preguntas y respuestas',
            'class'  => '',
            'data-load' => '',
            'subfields' => array(
              array(
                'type'  => 'input',
                'name'  => 'question',
                'label' => 'Pregunta',
                'class'  => 'form-control',
                'data-load' => ''
              ),
              array(
                'type'  => 'input',
                'name'  => 'answer',
                'label' => 'Respuesta',
                'class'  => 'form-control',
                'data-load' => ''
              ),
            )
          )
      )
);
