    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>
    <!-- /.control-sidebar -->

    <!-- Modal -->
    <div class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary">Save changes</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.modal -->

    <!-- Media uploader modal -->
    <div class="modal media-uploader__modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary">Asignar imagen</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- /.media-uploader__modal -->

  </div>
  <!-- ./wrapper -->

  <!-- JS Global -->
  <script>
    var globals = {global_js};
  </script>

  {footer}




  <!-- jQuery -->
  <script src="{base_url}jscript/adminlte3/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="{base_url}jscript/adminlte3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="{base_url}cms/assets/jscript/jquery-ui/jquery-ui.min.js"></script>
  <script src="{base_url}cms/assets/jscript/jquery-ui/datepicker-es.js"></script>
  <script src="{base_url}cms/assets/jscript/jquery-ui/jquery-ui-timepicker-addon.js"></script>



  <!-- Plugins -->
  <script src="{base_url}cms/assets/tinymce/tinymce.min.js"></script>
  <script src="{base_url}cms/assets/jscript/redactor.min.js"></script>
    <script src="{base_url}cms/assets/jscript/dropzone.js"></script>
  <script src="{base_url}jscript/adminlte3/plugins/datatables/datatables.min.js"></script>
  <script src="{base_url}jscript/adminlte3/plugins/selectize/selectize.min.js"></script>

  <!-- AdminLTE App -->
  <script src="{base_url}jscript/adminlte3/dist/js/adminlte.min.js"></script>
  <script src="{base_url}cms/assets/jscript/app.js"></script>



  <!-- CMS js -->
  <script src="{base_url}cms/assets/jscript/cms.js?5"></script>
  <script src="{base_url}cms/assets/jscript/archivos.js"></script>






  <!-- JS custom -->
  {js}

  <!-- JS inline -->
  <script>
  {ignore}
  $(document).ready(function(){
  	{inlineJS}
  });
  {/ignore}
  </script>

</body>
</html>
