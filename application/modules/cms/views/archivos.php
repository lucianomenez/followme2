<?php
/*
 *  Header : CSS Load & some body
 *
 */
$this->load->view('_lte3_header.php')
?>

<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <a href="{base_url}{edit_url}" class="btn btn-primary mb-4">Subir archivo</a>
        <table id="posts" class="datatables table table-bordered table-hover" style="background-color: #fff;">
            <thead>
              <tr>
                <th></th>
                <th>nombre</th>
                <th>Operaciones</th>
              </tr>
            </thead>
            <tbody>
              {archivos}
              <tr class="file">
                <td><img src="https://archivos.fnartes.gob.ar/cms/{Key}" style="width:30px"></td>
                <td><a href="https://archivos.fnartes.gob.ar/cms/{Key}">{Key}</a></td>
                <td><a href="#" data-key="{Key}" class="asset-delete">Eliminar</a> | <a href="https://archivos.fnartes.gob.ar/cms/{Key}">Ver</a></td>
              </tr>
              {/archivos}
            </tbody>
        </table>

      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<?php
/*
 *  FOOTER
 *
 */

$this->load->view('_lte3_footer.php')

?>
