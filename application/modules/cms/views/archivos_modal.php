<ul>
  {archivos}
  <li class="media">
    <div class="media__preview">
      <div class="media__thumbnail">
        <div class="media__thumbnail--centered">
          <img src="https://archivos.fnartes.gob.ar/cms/{Key}" alt="">
        </div>
      </div>
    </div>
  </li>
  {/archivos}
</ul>
