<?php
/*
 *  Header : CSS Load & some body
 *
 */
$this->load->view('_lte3_header.php')
?>

<!-- Main content -->
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <a href="{base_url}{edit_url}" class="btn btn-primary mb-4">Crear nuevo</a>
        <table id="posts" class="datatables table table-bordered table-hover" style="background-color: #fff;">
            <thead>
              <tr>
                <th>Titulo</th>
                <th>Operaciones</th>
              </tr>
            </thead>
            <tbody>
              {nodes}
              <tr class="node">
                <td><a href="{base_url}{edit_url}{_id}">{title}</a></td>
                <td><a href="{base_url}{edit_url}{_id}">Editar</a> | <a href="{base_url}{slug}">Ver</a> | <a href="{base_url}cms/node/delete/{_id}" class="node-delete">Eliminar</a></td>
              </tr>
              {/nodes}
            </tbody>
        </table>

      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

{js}
<?php
/*
 *  FOOTER
 *
 */
$this->load->view('_lte3_footer.php')

?>
