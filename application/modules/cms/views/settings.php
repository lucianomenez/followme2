<?php $this->load->view('_header.php') ?>

<div class="wrapper row-offcanvas row-offcanvas-left wrapper-settings">

    <!-- ======== MENU LEFT ======== -->
    <aside class="left-side sidebar-offcanvas">
        <section class="sidebar">
            {menu}
        </section>
    </aside>

    <!-- ====== CONTENT AREA ======= -->
    <aside class="right-side">

        <section class="content-header"><h1>Opciones Generales</h1></section>


            <!-- Header -->
            <section class="content">
                <form action="" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="_id" value="<?=$settings['_id']?>" />

                    <fieldset>
                        <legend>Header</legend>

                        <div class="meta_image image_upload form-group">
                            <label>Logo Header</label><img src="<?=$settings['header']['logo']?>" class="image-preview">
                            <input type="file" name="header_logo_upload" label="Meta Image" class="form-control ajax_upload">
                            <input type="hidden" name="header_logo" value="">
                        </div>

                        <h4>Menu</h4>

                        <a href="javascript:;" onclick="addItem(this, 'header_menu')" class="form-anchor">Agregar item</a>

                        <table>

                            <thead>
                                <tr>
                                    <th>Texto</th>
                                    <th>Enlace</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                            <?php foreach ($settings['header']['menu'] as $item): ?>
                            <tr>
                                <td>
                                    <div class="slug input form-group">
                                        <input type="text" value="<?= $item['label']?>" name="header_menu_label[]" class="form-control"/>
                                    </div>
                                </td>
                                <td>
                                    <div class="slug input form-group">
                                        <input type="text" value="<?= $item['link'] ?>" name="header_menu_link[]" class="form-control" />
                                    </div>
                                </td>
                                <td>
                                    <div class="slug input form-group">
                                        <a href="#">Borrar</a>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                    </fieldset>

                    <!-- Footer -->
                    <fieldset>
                        <legend>Footer</legend>

                        <div class="meta_image image_upload form-group">
                            <label>Logo Footer 1</label><img src="<?=$settings['footer']['logos'][0]?>" class="image-preview">
                            <input type="file" name="footer_logo1_upload" label="Meta Image" class="form-control ajax_upload">
                            <input type="hidden" name="footer_logo1" value="">
                        </div>

                        <div class="meta_image image_upload form-group">
                            <label>Logo Footer 2</label><img src="<?=$settings['footer']['logos'][1]?>" class="image-preview">
                            <input type="file" name="footer_logo2_upload" label="Meta Image" class="form-control ajax_upload">
                            <input type="hidden" name="footer_logo2" value="">
                        </div>

                        <?php foreach ($settings['footer']['menus'] as $key => $footerMenu):?>


                            <h4>Menu <?= $footerMenu['title'] ?></h4>

                            <a href="javascript:;" onclick="addItem(this, 'footer_<?=$key?>')" class="form-anchor">Agregar item</a>

                            <table>
                                <thead>
                                    <tr>
                                        <th>Texto</th>
                                        <th>Enlace</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($footerMenu['items'] as $item): ?>
                                        <tr>
                                            <td>
                                                <div class="slug input form-group">
                                                    <input type="text" value="<?= $item['label']?>" name="footer_<?=$key?>_label[]" class="form-control" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="slug input form-group">
                                                    <input type="text" value="<?= $item['link'] ?>" name="footer_<?=$key?>_link[]" class="form-control" />
                                                </div>
                                            </td>
                                            <td>
                                                <div class="slug input form-group">
                                                    <a href="javascript:;" onclick="removeItem(this)">Borrar</a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        <?php endforeach ?>
                    </fieldset>

                    <!-- MetaTags -->
                    <fieldset>
                        <legend>Metatags &amp; SEO</legend>
                        <div class="meta_title input form-group">
                            <label>Meta Title</label>
                            <input type="input" name="meta_title" value="<?=$settings['metatags']['title']?>" label="Meta Title" class="form-control">
                        </div>
                        <div class="meta_description textarea form-group">
                            <label>Meta Description</label>
                            <textarea name="meta_description" cols="40" rows="10" type="textarea" label="Meta Description" class="form-control"><?=$settings['metatags']['description']?></textarea>
                        </div>
                        <div class="meta_image image_upload form-group">
                            <label>Meta Image</label><img src="<?=$settings['metatags']['image']?>" class="image-preview">
                            <input type="file" name="meta_image_upload" label="Meta Image" class="form-control ajax_upload">
                            <input type="hidden" name="meta_image" value="">
                        </div>
                    </fieldset>

                    <input type="submit" value="Guardar" class="btn btn-info">
                    <button type="button" class="btn" onclick="window.history.go(-1); return false;">Cancelar</button>

        </form>
        </section>

    </aside>
</div>

<?php $this->load->view('_footer.php') ?>


<script type="text/javascript">

    function addItem(elm, menuName){
        var $tr = `<tr>
            <td><div class="slug input form-group"><input name="${menuName}_label[]" class="form-control"/></div></td>
            <td><div class="slug input form-group"><input name="${menuName}_link[]" class="form-control"/></div></td>
            <td><div class="slug input form-group"><a href="javascript:;" onclick="removeItem(this)">Borrar</a></div></td>
        </tr>`;

        $(elm).next().find('tbody').eq(0).append($tr);

    }

    function removeItem(elm) {
        $(elm).closest('tr').remove();
    }

</script>
