<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include 'Node.php';

class Catalogos extends Node
{
    protected $node_type = 'catalogos';
    protected $card = true;
}
