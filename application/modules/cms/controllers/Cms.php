
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Website
 *
 * Description of the class
 *
 * @author Felipe Hernández <hernandezfeli@gmail.com>
 */
class Cms extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('cms/config');

        $this->load->library('parser');
        $this->load->library('dashboard/ui');

        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');
      $this->load->model('cms/Model_node');
        $this->load->helper('form');
        $this->load->helper('cms/dynamic_builder');
        $this->load->helper('cms/block');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
    }

    function index(){
        $cms = ($this->session->userdata('json')) ? $this->session->userdata('json'):null;

        if($cms<>''){
            $this->CMS($cms);
        } else {
            $this->CMS($this->config->item('default_cms'));
        }
    }

    function Show($file, $debug = false) {
        //---only admins can debug
        $debug = ($this->user->isAdmin()) ? $debug : false;
        if (!is_file(APPPATH . "modules/cms/views/json/$file.json")) {
            // Whoops, we don't have a page for that!
            return null;
        } else {
            $myconfig = json_decode($this->load->view("cms/json/$file.json", '', true), true);
            if (isset($myconfig['private']) && $myconfig['private'] == true) {
                return;
            }
            $this->CMS("cms/json/$file.json", $debug);
        }
    }

    function CMS($json = 'cms/json/cms.json',$debug = false,$extraData=null) {
        /* eval Group hooks first */
        $this->session->set_userdata('json', $json);
        $user = $this->user->get_user((int) $this->idu);
        $myconfig = $this->parse_config($json, $debug);

        $layout = ($myconfig['view'] <> '') ? $myconfig['view'] : 'layout';
        $customData = $myconfig;
        $customData['lang'] = $this->lang->language;
        $customData['alerts']=Modules::run('dashboard/alerts/get_my_alerts');
        $customData['brand'] = $this->config->item('brand');
        $customData['menu'] = $this->menu();
        $customData['avatar'] = Modules::run('user/profile/get_avatar'); //Avatar URL
        $customData['base_url'] = $this->base_url;
        $customData['module_url'] = $this->module_url;
        $customData['inbox_count'] = $this->msg->count_msgs($this->idu, 'inbox');
        $customData['config_panel'] =$this->parser->parse('_config_panel',  $customData['lang'], true, true);

        $customData['name'] = $user->name . ' ' . $user->lastname;
        $customData['email'] = $user->email;

        // Global JS
        $customData['global_js'] = array(
            'base_url' => $this->base_url,
            'module_url' => $this->module_url,
            'myidu' => $this->idu,
            'lang'=>$this->config->item('language')
        );


        // Toolbar
        $customData['toolbar_inbox'] = Modules::run('inbox/inbox/toolbar');

        $customData['new_toolbar_inbox'] = Modules::run('inbox/inbox/new_toolbar');

        if($extraData){
            $customData=$extraData+$customData;
        }
        $this->ui->compose($layout, $customData);
    }

    function loadDashboardVars($json = 'cms/json/cms.json',$debug = false,$extraData=null) {

        /* eval Group hooks first */
        $this->session->set_userdata('json', $json);
        $user = $this->user->get_user((int) $this->idu);
        $myconfig = $this->parse_config($json, $debug);

        $layout = ($myconfig['view'] <> '') ? $myconfig['view'] : 'layout';

        $customData = $myconfig;
        //$customData['lang'] = $this->lang->language;
        //$customData['alerts']=Modules::run('dashboard/alerts/get_my_alerts');
        $customData['brand'] = $this->config->item('brand');
        $customData['menu'] = $this->menu();
        $customData['avatar'] = Modules::run('user/profile/get_avatar'); //Avatar URL
        $customData['base_url'] = $this->base_url;
        $customData['module_url'] = $this->module_url;
           $customData['rand'] = rand();
        //$customData['inbox_count'] = $this->msg->count_msgs($this->idu, 'inbox');
        //$customData['config_panel'] =$this->parser->parse('_config_panel',  $customData['lang'], true, true);

        $customData['name'] = $user->name . ' ' . $user->lastname;
        $customData['email'] = $user->email;

        // Global JS
        $customData['global_js'] = array(
            'base_url' => $this->base_url,
            'module_url' => $this->module_url,
            'myidu' => $this->idu,
            'lang'=>$this->config->item('language')
        );


        // Toolbar
        //$customData['toolbar_inbox'] = Modules::run('inbox/inbox/toolbar');

        $customData['new_toolbar_inbox'] = Modules::run('inbox/inbox/new_toolbar');

        if($extraData){
            $customData=$extraData+$customData;
        }
        if($extraData){
            $customData=$extraData+$customData;
        }

        return $this->ui->compose_lte3($layout, $customData, true);
    }

    /**
     * Realiza los recortes (presets) definidos en cms/config.php sobre la imagen subida en $_FILES['file']
     * Una vez realizados aplicados los recortes, los sube a s3
     */

    function upload_file(){

        // Make sure file is not cached (as it happens for example on iOS devices)
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");


        /*
         // Support CORS
        header("Access-Control-Allow-Origin: *");
        // other CORS headers if any...
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
        exit; // finish preflight CORS requests here
        }
        */

        // 5 minutes execution time
        @set_time_limit(10 * 60);

        // Uncomment this one to fake upload time
        // usleep(5000);

        // Settings
        $folder = $this->input->post('tipo_file');
        $targetDir  = 'application/modules/website/assets/images/'.$folder;
        $cleanupTargetDir = true; // Remove old files
        $maxFileAge = 10 * 3600; // Temp file age in seconds

        // Create target dir
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }else{
           array_map('unlink', glob($targetDir."/".$_FILES['file']['name']));
        }

        $file2delete = $targetDir . DIRECTORY_SEPARATOR . $_FILES['file']['name'];
        $milisegundos=round(microtime(true)*1000);

        $info = new SplFileInfo($_FILES['file']['name']);

        $filePath = $targetDir . DIRECTORY_SEPARATOR .$milisegundos.'_'.$folder.'.'.$info->getExtension();
        $fileUrl = $this->base_url.'website/assets/images/'.$folder.'/'.$milisegundos.'_'.$folder.'.'.$info->getExtension();
        $fileName=$milisegundos.'_'.$folder.'.'.$info->getExtension();
        // Chunking might be enabled
        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

        // Remove old temp files
        if ($cleanupTargetDir) {
            if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                // If temp file is current file proceed to the next
                if ($tmpfilePath == "{$filePath}.part") {
                    continue;
                }

                // Remove temp file if it is older than the max age and is not the current file
                if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                    @unlink($tmpfilePath);
                }
            }
            closedir($dir);
        }


        // Open temp file
        if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!empty($_FILES)) {
            if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
            }

            // Read binary input stream and append it to temp file
            if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        } else {
            if (!$in = @fopen("php://input", "rb")) {
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);

        }

        @fclose($out);
        @fclose($in);

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off

            rename("{$filePath}.part", $filePath);
            //@unlink($file2delete);// file with same idu and other extension exists
        }

        // Return Success JSON-RPC response

        die('{"jsonrpc" : "2.0", "success" : true, "url" : "'.$fileUrl.'", "name":"'.$fileName.'"}');

    }


    function upload_file_new(){
          $data_post = $this->input->post();
          $data_file = $_FILES['file'];
          $data_image= getimagesize ($data_file['tmp_name']);

          $file_extension = pathinfo($data_file['name'], PATHINFO_EXTENSION);

          // Make dir
           $folder = $this->input->post('tipo_file');
            $milisegundos=round(microtime(true)*1000);


            $folder = $this->input->post('tipo_file');
          if (!file_exists(FCPATH.'application/modules/website/assets/images/'.$folder)){
               @mkdir(FCPATH.'application/modules/website/assets/images/'.$folder.'/',0777,true);
          }

          // File configuration
          $config['file_name']            =$milisegundos.'_'.$folder.'.'.$file_extension ;
          $config['upload_path']          =  FCPATH.'application/modules/website/assets/images/'.$folder.'/';
          $config['allowed_types']        = "gif|jpg|png|doc|DOC|docx|DOCX|jpeg|txt|TXT|JPG|PNG|JPEG|pdf|PDF|mp3|MP3|mp4|MP4|";
          $config['max_size']            = 100000;
          $config['overwrite']           = false;

          // Upload
          $this->load->library('upload', $config);

          // Set response data
          $response['uploadUrl'] =  $this->base_url.'website/assets/images/'.$folder.'/'. $config['file_name'];
          $response['relativeUrl'] = 'website/assets/images/'.$folder.'/'. $config['file_name'];
          $response['extension']=$file_extension;
          $response['file_extension']=$data_file['type'];
          $response['width']=$data_image[0];
          $response['height']=$data_image[1];
          $response['size']=$data_file['size'];
          $response['original_name']=$data_file['name'];
           $response['name']=  $config['file_name'];

          if ( ! $this->upload->do_upload('file'))
          {
              $error = array('error' => $this->upload->display_errors());
              var_dump($error, $config['upload_path']);
          }
          else
          {
              $data = array('upload_data' => $this->upload->data());
              $data['upload_path'] = $config['upload_path'];
              echo json_encode($response);
          }
        }


    function load_block($block_name, $block_count = 0, $block_data = ''){
        $block_pos = $block_count;
        $block_response = block_build_form($block_name, $block_data, $block_pos);
        header('Content-Type: application/json');
        echo json_encode($block_response);
    }

    function menu() {
        $customData['base_url'] = $this->base_url;
        $customData['module_url'] = $this->module_url;
        $customData['lang'] = $this->lang->language;
        $customData['is_admin']=$this->user->isAdmin();
        $customData['current_page'] = $this->base_url.$this->uri->segment(1).$this->uri->segment(2);

        $customData['menu_cms'] = array(
          array(
            'url' => $this->base_url.'cms/pages',
            'text' => 'Páginas',
            'icon' => 'file-o'
          ),
          array(
            'url' => $this->base_url.'cms/productos',
            'text' => 'Productos',
            'icon' => 'bullseye'
          ),
          array(
            'url' => $this->base_url.'cms/colegios',
            'text' => 'Colegios',
            'icon' => 'graduation-cap'
          ),
          array(
            'url' => $this->base_url.'cms/sucursales',
            'text' => 'Sucursales',
            'icon' => 'map-marker'
          ),
          array(
            'url' => $this->base_url.'cms/options',
            'text' => 'Opciones',
            'icon' => 'filter'
          ),
          array(
            'url' => $this->base_url.'cms/catalogos',
            'text' => 'Catálogos',
            'icon' => 'file-pdf-o'
          ),
          array(
            'url' => $this->base_url.'cms/bases_condiciones',
            'text' => 'Bases y condiciones',
            'icon' => 'check-square'
          )
        );

        return $this->parser->parse('cms/menu', $customData, true, true);
    }

    // ============ Parse JSON config
    function parse_config($file, $debug = false) {
        $myconfig = json_decode($this->load->view($file, '', true), true);
        $minwidth=2;

        //Root config

        foreach ($myconfig as $key => $value) {
            if ($key != 'zones')
                $return[$key] = $value;
        }


        $return['js']=array();
        $return['css']=array();
        $return['inlineJS']="";
        // CSS
        if(isset($myconfig['css'])){
            foreach($myconfig['css'] as $item){
                foreach($item as $k=>$v){
                    if(is_numeric($k))
                        $return['css'][]=$v;
                    else{
                        $return['css'][$k]=$v;
                    }
                }
            }
        }
        // JS
        if(isset($myconfig['js'])){
            foreach($myconfig['js'] as $item){
                foreach($item as $k=>$v){
                    if(is_numeric($k))
                        $return['js'][]=$v;
                    else{
                        $return['js'][$k]=$v;
                    }
                }
            }
        }
        return $return;
    }

    function get_categorias(){
          $args2['post_status'] = 'published';
          $args2['post_type'] = 'options';
          $args2['type'] = 'categorias';

          $block['categorias']= $this->Model_node->get($args2, 1)[0]['items'];

          if (!$block['categorias']) {
            $block='null';
          }
            echo json_encode($block);

    }

        function get_categorias_paleta(){
          $args2['post_status'] = 'published';
          $args2['post_type'] = 'options';
          $args2['type'] = 'categorias_paleta';

          $block['categorias']= $this->Model_node->get($args2, 1)[0]['items'];

          if (!$block['categorias']) {
            $block='null';
          }
            echo json_encode($block);

    }
    function get_colegio(){
          $args2['post_type'] = 'colegios';
          $args2['slug'] = $this->input->post('slug');
          $block['colegio']= $this->Model_node->get($args2, 1)[0]['image'];

          if (!$block['colegio']) {
            $block='null';
          }
            echo json_encode($block);

    }
    function get_catalogo(){
          $args2['post_type'] = 'catalogos';
          $args2['slug'] = $this->input->post('slug');
          $catalogo=array();
          $catalogo['catalogo']= $this->Model_node->get($args2, 1)[0]['catalogo'];
              $catalogo['data_catalogo']= $this->Model_node->get($args2, 1)[0]['data_catalogo'];
          if (!$catalogo) {
            $catalogo='null';
          }
            echo json_encode($catalogo);

    }

    function get_page(){
          $args2['post_type'] = 'page';
          $args2['slug'] = $this->input->post('slug');
          $file=$this->input->post('file');
          $block[$file]= $this->Model_node->get($args2, 1)[0][$file];

          if (!$block[$file]) {
            $block='null';
          }
            echo json_encode($block);

    }
}
