<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Posts helpers
 *
 * Description of the class
 *
 * @author Felipe Hernández <hernandezfeli@gmail.com>
 * @date    Dec 26, 2017
 */
class Helpers extends MX_Controller
{

  function __construct() {
      parent::__construct();
      $this->user->authorize();
      $this->load->model('user');
      $this->load->model('cms/Model_novedades');
      $this->load->model('cms/Model_page');

      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
  }

  function Index() {
      echo "helpers";
  }

  function check_slug() {
    $title = $this->input->post('title');

    if ($title) {
      $slug = url_title($title, '-', true);

      if (!$this->Model_page->get_pages(array('slug' => $slug))) {

        $return['slug'] = $slug;
        echo json_encode($return);

      } else {
        return false;
      }

    }

  }

}
