<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include 'Node.php';

class Productos extends Node
{
    protected $node_type = 'productos';
    protected $card = true;
}
