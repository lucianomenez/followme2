<?php
if ( ! defined('BASEPATH')) exit('Access denied');

include_once 'Model_node.php';

class Model_colegios extends Model_node {

    function get($params = array(), $limit = 999, $offset = 0){
        $params['post_type'] = 'colegios';
       	$result= parent::get($params, $limit);
	    if (!$result) {
	    	$result=array();
	    }
        return $result;
    }

    function save($data){
        $data['post_type'] = 'colegios';
        return parent::save($data);
    }

}
