<?php
if ( ! defined('BASEPATH')) exit('Access denied');

include_once 'Model_node.php';

class Model_page extends Model_node{

	public function get($params = array(), $limit = 999 ,$offset = 0){
      $params['post_type'] = 'page';
       	$result= parent::get($params, $limit,0);
	    if (!$result) {
	    	$result=array();
	    }

        return $result;
	}

	function save($data){
	    $data['post_type'] = 'page';
	    return parent::save($data);
    }

}
