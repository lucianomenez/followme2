<?php
if ( ! defined('BASEPATH')) exit('Access denied');

class Model_settings extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->library('cimongo/Cimongo', '', 'db');
        $this->config->load('cimongo');
    }

    function save_settings($post){


        # Header Structure
        $data['header']['logo'] = $post['header_logo'];
        foreach ($post['header_menu_label'] as $index => $label){
            $data['header']['menu'][] = ['label' => $label, 'link' => $post['header_menu_link'][$index]];
        }

        # Footer Structure
        $footer_menu_0_items = [];
        $footer_menu_1_items = [];
        foreach ($post['footer_0_label'] as $index => $label){
            $footer_menu_0_items[] = ['label' => $label, 'link' => $post['footer_0_link'][$index]];
        }
        foreach ($post['footer_1_label'] as $index => $label){
            $footer_menu_1_items[] = ['label' => $label, 'link' => $post['footer_1_link'][$index]];
        }

        $data['footer']['logos'][] = $post['footer_logo1'];
        $data['footer']['logos'][] = $post['footer_logo2'];

        $data['footer']['menus'][] = [
            'title' => 'Institucional',
            'items' => $footer_menu_0_items
        ];

        $data['footer']['menus'][] = [
            'title' => 'Oportunidades y Servicios',
            'items' => $footer_menu_1_items
        ];

        $data['metatags'] = [
            'title' => $post['meta_title'],
            'description' => $post['meta_description'],
            'image' => $post['meta_image'],
        ];


        $this->db->switch_db($this->config->item('cmsdb'));
        $query = array('_id' => new MongoId($post['_id']));
        $this->db->where($query);
        return $this->db->update('container.settings', $data, array('upsert'=> true));
    }

    function get_settings(){
        $this->db->switch_db($this->config->item('cmsdb'));
        $result = $this->db->get('container.settings')->result_array();
        return empty($result)
            ? [
                'header' => [ 'logo' => null, 'menu' => [] ],
                'footer' => [ 'logos' => [], 'menus' => [] ],
                'metatags' => [ 'title', 'description', 'image' ]
            ]
            : array_pop($result);
    }
}
