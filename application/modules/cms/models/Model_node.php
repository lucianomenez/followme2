<?php
if ( ! defined('BASEPATH')) exit('Access denied');

class Model_node extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->library('cimongo/Cimongo', '', 'db');
        $this->config->load('cimongo');
    }

    function get_one($conditions){
        $this->db->switch_db($this->config->item('cmsdb'));
        $node = $this->db->get_where('container.pages', $conditions)->row_array();
        $this->db->switch_db($this->config->item('db'));

        return empty($node) ? false : $node;
    }

    public function get($params = array(), $limit = 40, $offset = 0) {
    
        $this->db->switch_db($this->config->item('cmsdb'));
        $nodes = $this->db
                    ->order_by(array('post_date' => 'DESC'))
                    ->get_where('container.pages', $params, $limit, $offset)
                    ->result_array();
        $this->db->switch_db($this->config->item('db'));
        return empty($nodes) ? false : $nodes;
    }

    public function get_paleta($params = array(), $limit = 40, $offset = 0) {
        $this->db->switch_db($this->config->item('db'));
        $nodes = $this->db
                    ->order_by(array('colorName' => 'ASC'))
                    ->get_where('followme.colors', $params, $limit, $offset)
                    ->result_array();

        return empty($nodes) ? false : $nodes;
    }

    public function get_productos($params = array(),$params2 ,$limit = 40, $offset = 0) {
      
        $this->db->switch_db($this->config->item('cmsdb'));
        $nodes = $this->db
                    ->order_by(array('post_date' => 'DESC'))
                    ->where($params)
                    ->or_like('categoria', $params2)
                    ->or_like('categoria2', $params2)
                    ->get('container.pages',$limit, $offset)
                    ->result_array();
        $this->db->switch_db($this->config->item('db'));


        return empty($nodes) ? false : $nodes;
    }

    public function search($params = array(), $limit = 40, $offset = 0) {
    
        $this->db->switch_db($this->config->item('cmsdb'));

        $search=$params['search'];
        unset($params['search']);

            $nodes =$this->db
                    ->where($params)
                    ->or_like('title', $search)
                    ->or_like('ano', $search)
                    ->or_like('city', $search)
                    ->order_by(array('post_date' => 'DESC'))
                    ->get('container.pages',$limit, $offset)
                    ->result_array();

        $this->db->switch_db($this->config->item('db'));
        return empty($nodes) ? false : $nodes;
    }

    function get_by_tag($params = array(), $limit = 25, $offset = 0) {
        $this->db->switch_db($this->config->item('cmsdb'));

        $regex = new MongoRegex("/".$params['tag']."/i");
        $query = array(
                'aggregate' => 'container.pages',
                'pipeline' =>
                  array(
                    array(
                      '$match' => array (
                          '$or' => array(
                            array('name' => $regex),
                          ),
                        ),
                      ),
                    )
                );
        $nodes =  $this->db->command($query);

        $this->db->switch_db($this->config->item('db'));

        return empty($nodes) ? false : $nodes;
    }

    function get_relacionadas($tag){
        $this->db->switch_db($this->config->item('cmsdb'));
        $container = 'container.pages';
        $query = array(
            'post_status' => 'published',
            'tags'=> array( '$elemMatch' => array('key' => $tag)),
        );
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function save($data){
        $this->db->switch_db($this->config->item('cmsdb'));
        $container = 'container.pages';

        $query = array('_id' => new MongoId($data['_id']));
        unset($data['_id']);
        unset($data['meta_image_upload']);
        unset($data['img_upload']);

        # post processing
        foreach ($data as $key => &$value){
            $method_name = 'field_'. $key . '_alter';
            if(method_exists($this, $method_name)){
                $this->{$method_name}($value);
            }
        }

        # layout processing
        if (is_array($data['layout'])) {
          foreach ($data['layout'] as $key => &$value) {
            $method_name = 'field_'. $key . '_alter';
            if(method_exists($this, $method_name)){
                $this->{$method_name}($value);
            }
          }
        }

        # blocks processing
        // reorder blocks
        $data['blocks'] = array_values($data['blocks']);

        // alter fields
        foreach ($data['blocks'] as &$block){
          foreach ($block['data'] as $key => &$value) {
            $method_name = 'field_'. $key . '_alter';
            if(method_exists($this, $method_name)){
                $this->{$method_name}($value);
            }
          }
        }

        $this->db->where($query);

        return $this->db->update($container, $data, array('upsert'=> true));

    }

    function delete($_id){
      $this->db->switch_db($this->config->item('cmsdb'));

      $_id = new MongoId($_id);
      $query = array('_id' => $_id);

      if ($this->db->where($query)->delete('container.pages')) {
        $this->db->switch_db($this->config->item('db'));
        return true;
      } else {
        $this->db->switch_db($this->config->item('db'));
        return false;
      }

    }

    function save_revision($node){
      $this->db->switch_db($this->config->item('cmsdb'));
      $container = 'container.revisions';
      unset($node['_id']);
      $node['revision_date'] = new MongoDate();

      return $this->db->save($container, $node);
    }

    function get_tags(){
        $this->db->switch_db($this->config->item('cmsdb'));
        $results = $this->db->select(['tags'])
                            ->where(['tags' => ['$ne' => null]])
                            ->get('container.pages')
                            ->result();

        foreach ($results as $result){
            foreach ($result->tags as $tag){
                if ($tag['value']!='') {
                  $tags[] = $tag['value'];
                }
            }


        }
        $unique = array_keys(array_flip($tags));

        return $unique;

    }

    function field_date_alter(&$value){
        $date = date_create_from_format('d/m/Y H:i', $value);
        $value = new MongoDate(strtotime($date->format("Y-m-d H:i")));
    }

    function field_post_date_alter(&$value){
        $date = date_create_from_format('d/m/Y H:i', $value);
        $value = new MongoDate(strtotime($date->format("Y-m-d H:i")));
    }

    function field_open_alter(&$value){
      if ($value == 1) {
        $value = true;
      } else {
        $value = false;
      }
    }

    function field_tags_alter(&$value){

      $data['tags'] = explode(',', $value);

      foreach($data['tags'] as &$tag) {
          $tag = array('key' => strtolower(url_title($tag)), 'value' => $tag);
      }

      $value = $data['tags'];
    }

}
