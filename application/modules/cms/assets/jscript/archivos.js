$(document).on('click', '.asset-delete', function(e) {
  e.preventDefault()
  let key = $(this).data('key')
  let $tr = $(this).parents('.file')
  if (window.confirm('Borrar archivo ' + key)) {

    $.ajax({
        url: globals['base_url'] + 'cms/archivos/delete',
        data: {key: key},
        type: "POST",
        dataType: 'json',
    }).done(function(data) {

        if (data.success) {
          $tr.remove()
        }
    })

  }
})

$(document).on('click', '.select-file', function(e) {
  e.preventDefault()

  $.ajax({
        url: globals['base_url'] + 'cms/archivos/get_modal',
        type: "POST",
        dataType: 'json',
    }).done(function(data) {
      console.log(data);
      $('.modal .modal-body').html()
      $('.modal').modal('show')
    })



})
