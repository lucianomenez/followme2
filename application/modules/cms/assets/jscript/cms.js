$(document).ready(function(){

    selectBackground();
    //$.fn.dataTable.moment( 'Y-m-d h:i:s');
    initDataTables();
    initTags();
    initSlug();
    initAjaxUpload();
    initBlocksFunctionalities();
    initBlockPlugins();
    initRepeaters();
    loadCategorias();
    initCategoriaValue();
loadDropzone();
    //initDependencies();

    $(document).on({
      click: function(e) {
        e.preventDefault();
        $('.media-uploader__modal').modal('show')
        $.ajax({
              url: globals['base_url'] + 'cms/archivos/get_modal',
              type: "POST",
          }).done(function(response) {
            console.log(response);
            $('.media-uploader__modal .modal-body').html(response)
          })
      }
    }, '.media-uploader__modal-button');

    $(document).on({
      click: function(e) {
        e.preventDefault();
        let node = $(this).parents('.node')
        let url = $(this).attr('href')

        if (window.confirm('Está seguro de borrar este post?')) {
          $.ajax({
              url: url,
              type: "POST",
            }).done(function(response) {
              response = JSON.parse(response)
              if (response.status) {
                node.remove()
              }
            })
        }

      }
    }, '.node-delete');

});



function selectBackground(){

    $(".iCheck-helper").click(function() {
        var $radioButton = $(this).prev();
        if( $radioButton.is("#background_option_image")  ) {
                $("#background-video-container").hide();
                $("#background-image-container").show();

            }else if ( $radioButton.is("#background_option_video")) {
                $("#background-image-container").hide();
                $("#background-video-container").show();

            }
        }
    );
}

function redactorLoad(e) {
  $(e).redactor({
    buttons: ['format', 'bold', 'italic', 'underline', 'ul', 'ol', 'link', 'html'],
    replaceTags:  {
      'b': 'strong',
      'i': 'em'
    },
    plugins: ['buttons']
  })
}

function tagsLoad(e) {
  $(e).selectize({
      delimiter: ',',
      persist: false,
      valueField: 'text',
      labelField: 'text',
      searchField: 'text',
      plugins: ['remove_button'],
      load: function(query, callback) {
          if (!query.length) return callback();
          $.get( globals['base_url']+'/cms/posts/tags', { term: query } , function( data ) {
              data = JSON.parse(data);
              callback(data.map(function(x) { return { text: x } }));
          });
      }
  });
}

function datepickerLoad(e) {
  $(e).datepicker( $.datepicker.regional[ "es" ] );
}

function undefinedLoad(e) {
  return false;
}

function datetimepickerLoad(e) {
    $.timepicker.regional['es'] = {
        timeText: 'tiempo',
        hourText: 'hora',
        minuteText: 'minutos',
        secondText: 'segundos',
        millisecText: 'milisegundos',
        timezoneText: 'zona horaria',
        timeFormat: 'HH:mm',
        currentText: 'hoy',
        closeText: 'cerrar',
    };
    $(e).datetimepicker( $.timepicker.regional[ "es" ] );

}

function initAjaxUpload() {
    $(document).on('change', '.ajax_upload', function(event) {
        //event.preventDefault();
        var uploadField =  event.target;
        var urlField = '';
        var imgField = '';
        var url = globals['base_url'];
        var uploadUrl = url + 'cms/upload_file';
        var loadingImg = url + 'cms/assets/img/loading-balls.gif';

        file = event.target.files[0];

        isImage = file.type.match('image');
        isVideo = file.type.match('video');
        isPdf = file.type.match('pdf');
        tipo_file=$('#tipo_file').val();

            data = new FormData();
            data.append( 'file', file );
            data.append( 'tipo_file', tipo_file );
            var postData = {
                url: uploadUrl,
                data: data,
                type: "POST",
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false,
                dataType: 'json',
            }

        if (isImage || isVideo) {
 
            if (isImage) {
                urlField = $(uploadField).siblings('input[type=hidden]').first();
                imgField = $(uploadField).siblings('img').first();
                imgField.attr('src',loadingImg);
                $.ajax(postData).done(function(data) {

                    if (data.success) {
                        urlField.attr('value', data.url);
                        imgField.attr('src', data.url);
                    }
                });
            } else {
                urlField = $(uploadField).siblings('input[type=text]').first();
                var parentContainer = $(uploadField).parent();
                parentContainer.append('<img class="loading" src="'+loadingImg+'"/>');
                $.ajax(postData).done(function(data) {
                    if (data.success) {
                        urlField.attr('value', data.url);
                        parentContainer.find('.loading').remove();
                    }
                });
            }
        }else if(isPdf){
                urlField = $(uploadField).siblings('input[type=hidden]').first();
                imgField = $(uploadField).siblings('img').first();
                imgField.attr('src',loadingImg);
                $.ajax(postData).done(function(data) {

                    if (data.success) {
                        urlField.attr('value', data.url);
                        imgField.attr('src', url + 'cms/assets/img/pdf.png');
                    }
                });
        }

    });
}


function initBlocksFunctionalities() {
    $(document).on('click', ".collapse-block", function(e) {
      e.preventDefault()
        var i = $(this).find('i');
        var block = $(this).parents('.block');
        if (block.hasClass('closed')) {
            i.removeClass('fa-expand').addClass('fa-compress');
            block.removeClass('closed');
        } else {
            i.removeClass('fa-compress').addClass('fa-expand');
            block.addClass('closed');
        }

    });

    // ==== Load blocks
    $(document).on('click', '.add-block', function(event) {
        event.preventDefault();
        var blocksCount = $('.blocks-container .block').length;
        var blockName = $(this).data('block');
        var url = globals['base_url'];
        url = url + '/cms/load_block/' +  blockName + '/' + blocksCount;

        var blockContainer = $('.blocks-container');
        $.ajax({
            url: url,
            context: url
        }).done(function(data) {
            blockContainer.append(data.fields);
            $('.blocks-container').sortable({
              handle: ".move-block",
              axis: "y"
            })
        });
    })

    $('.blocks-container').sortable({
      handle: ".move-block",
      axis: "y"
    })

    $(document).on('click', '.remove-block', function(e) {
        e.preventDefault();
        var blockToRemove = $(this).parents('.block');
        if (window.confirm('Borrar Bloque')) {
          blockToRemove.remove();
        }
    });


    $(document).on('DOMNodeInserted', function(e) {
        // console.log($(e.target).find('.load-js') );
        //if ( $(e.target).find('.load-js').length > 0 ) {
        $.each($(e.target).find('.load-js'), function (i,e) {
            var pluginToLoad = $(e).data('load');
            console.log(!$(this).hasClass('plugin-loaded'));
            if (!$(this).hasClass('plugin-loaded')) {
              if (pluginToLoad != 'undefined') {
                var loadFunctionName = pluginToLoad + 'Load';
                $(this).addClass('plugin-loaded')
                window[loadFunctionName](e)
              }
            }


        });
        $('.repeater items').sortable({
          handle: ".move-item",
          axis: "y"
        })

    });
  }



function initSlug() {
    // Provee un valor para el slug en base al titulo
    $('input[name="title"]').on('change', (e) => {

        let postType = $('input[name=post_type]').val();
        let postTitle = $('input[name="title"]').val();
         let slug_val = $('input[name="slug"]').val();

        switch(postType){
            case 'page':
              if(slug_val!='user-login'&&slug_val!='design-room'){
                 $str = slugify(postTitle); 
              }else{
                 $str =slug_val;
              }
             
            break;
            case 'catalogos': $str ='catalogos/'+slugify(postTitle); break;
            case 'bases_condiciones': $str = 'bases_condiciones/'+slugify(postTitle); break;
        }

        $('input[name="slug"]').val($str);
    });

    // Inicializa el boton para visualizar el post
    let slug = $('input[name=slug]').val();
    if(slug){
        let postType = $('input[name=post_type]').val();
        $('#node-view').attr('href', '/' + slug);
        if(postType == 'page') postType = 'página';
        $('#node-view').html('Ver ' + postType);
        $('#node-view').show();
    }
}

function initCategoriaValue() {
      $('.items').on('change', 'input.text_option',function(e) {

        let postTitle = $(this).val();

        $str = slugify(postTitle);

        $(this).parent().parent().find('input.text_value').val($str);
      });
}

const slugify = text => {
  // Use hash map for special characters
  let specialChars = {"à":'a',"ä":'a',"á":'a',"â":'a',"æ":'a',"å":'a',"ë":'e',"è":'e',"é":'e', "ê":'e',"î":'i',"ï":'i',"ì":'i',"í":'i',"ò":'o',"ó":'o',"ö":'o',"ô":'o',"ø":'o',"ù":'o',"ú":'u',"ü":'u',"û":'u',"ñ":'n',"ç":'c',"ß":'s',"ÿ":'y',"œ":'o',"ŕ":'r',"ś":'s',"ń":'n',"ṕ":'p',"ẃ":'w',"ǵ":'g',"ǹ":'n',"ḿ":'m',"ǘ":'u',"ẍ":'x',"ź":'z',"ḧ":'h',"·":'-',"/":'-',"_":'-',",":'-',":":'-',";":'-'};

    return text.toString().toLowerCase()
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/./g,(target, index, str) => specialChars[target] || target) // Replace special characters using the hash map
      .replace(/&/g, '-and-')         // Replace & with 'and'
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/-+$/, '');             // Trim - from end of text
};

function initTags() {
    $('input[name="tags"]').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'text',
        labelField: 'text',
        searchField: 'text',
        plugins: ['remove_button'],
        create: function(input, callback) {
            return {
                value: input,
                text: input
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.get( globals['base_url']+'/cms/posts/tags', { term: query } , function( data ) {
                data = JSON.parse(data);
                callback(data.map(function(x) { return { text: x } }));
            });
        }
    });

}

function initBlockPlugins() {
    $.each($('.load-js'), function (i,e) {
        var pluginToLoad = $(e).data('load');
        if (pluginToLoad !== 'undefined') {
          $(this).addClass('plugin-loaded');
          var loadFunctionName = pluginToLoad + 'Load';
          window[loadFunctionName](e)
        }

    });
}

function initDataTables() {
    $('.datatables').DataTable({
        "iDisplayLength": 25,
        "order": [[ 1, "desc" ]],
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        }
    });
}

function initRepeaters() {

  $(document).on({
    click: function(e) {
      e.preventDefault();
      let block = $(this).parents()

      let parent = $(this).parents('.repeater').find('.items')
      let item = parent.find('.item').last()
      let index = parent.find('.item').length

      if ( item[0].outerHTML.indexOf("items[") > -1) {
        var replace = /items\[[0-9]?[0-9]{1,2}\]/g
        var newReplace = "items["+index+"]"
      }else if ( item[0].outerHTML.indexOf("[items][") > -1) {
        var replace = /\[items][[0-9]?[0-9]{1,2}\]/g
        var newReplace = "[items]["+index+"]"
      }else if ( item[0].outerHTML.indexOf("[q_a][") > -1) {
        var replace = /\[q_a][[0-9]?[0-9]{1,2}\]/g
        var newReplace = "[q_a]["+index+"]"
      }

      let newItem = $(item[0].outerHTML.replace(replace, newReplace))

      newItem.find('input').val('')

      $(parent).append(newItem);
    }
  }, '.repeater .add-item');

  $(document).on({
    click: function(e) {
      e.preventDefault();
      let parent = $(this).parent().parent().remove()
    }
  }, '.repeater .remove-item');

  $('.repeater .items').sortable({
    handle: ".move-item",
    axis: "y"
  })

}

function loadCategorias(){

    let url=globals['base_url'] + 'cms/get_categorias';
        $.ajax({
              url: url,
              type: "POST",
            }).done(function(response) {
              response = JSON.parse(response)
              if (response!='null') {
                if ($.isArray(response.categorias)) {
                  var options='';
                  var options2='';
                  options+='<option value="">Sin definir</option>';
                  options2+='<option value="">Sin definir</option>';
                   $.each(response.categorias, function( index, value ) {
                    if (value.value==$('#categoria_value').val()) {
                      options+='<option value="'+value.value+'" selected="selected">'+value.text+'</option>';
                    }else{
                      options+='<option value="'+value.value+'">'+value.text+'</option>';
                    }
                    if (value.value==$('#categoria_value2').val()) {
                      options2+='<option value="'+value.value+'" selected="selected">'+value.text+'</option>';
                    }else{
                      options2+='<option value="'+value.value+'">'+value.text+'</option>';
                    }
                  });
                  $('#categoria').html(options);
                   $('#categoria2').html(options2);
                }
              }

            })

      $('form').on('change', '#categoria',function(e) {

        let val = $(this).val();
        $('#categoria_value').val(val);
      });

      $('form').on('change', '#categoria2',function(e) {

        let val2 = $(this).val();
        $('#categoria_value2').val(val2);
      });

    let url_paleta=globals['base_url'] + 'cms/get_categorias_paleta';
        $.ajax({
              url: url_paleta,
              type: "POST",
            }).done(function(response) {
              response = JSON.parse(response)
              if (response!='null') {
                if ($.isArray(response.categorias)) {
                  options='';
                  options2='';
                  options+='<option value="">Sin definir</option>';
                  options2+='<option value="">Sin definir</option>';
                   $.each(response.categorias, function( index, value ) {
                    if (value.value==$('#categoria_paleta_value').val()) {
                      options+='<option value="'+value.value+'" selected="selected">'+value.text+'</option>';
                    }else{
                      options+='<option value="'+value.value+'">'+value.text+'</option>';
                    }
                    if (value.value==$('#categoria_paleta_value2').val()) {
                      options2+='<option value="'+value.value+'" selected="selected">'+value.text+'</option>';
                    }else{
                      options2+='<option value="'+value.value+'">'+value.text+'</option>';
                    }
                  });
                  $('#categoria_paleta').html(options);
                   $('#categoria2_paleta').html(options2);
                }
              }

            })

      $('form').on('change', '#categoria_paleta',function(e) {

        let val = $(this).val();
        $('#categoria_paleta_value').val(val);
      });

      $('form').on('change', '#categoria2_paleta',function(e) {

        let val2 = $(this).val();
        $('#categoria_paleta_value2').val(val2);
      });


}

function loadDropzone(){
  if ($('#tipo_file').val()=='catalogos') {

      $('div.myDropzone').html('<div class="dropzone"></div>');

        Dropzone.autoDiscover = false;
        var url = globals['base_url'];
        var uploadUrl = url + 'cms/upload_file_new';
        var slug=$('input[name="slug"]').val();
        var myDropzone = new Dropzone("div.myDropzone div.dropzone", {
          url: uploadUrl,
          dictDefaultMessage: '<i class="fas fa-cloud-download-alt"></i><br>Click para subir PDF<br>Se puede arrastrar y soltar',
          dictFileTooBig: 'El archivo pesa {{filesize}}MB, el máximo permitido es de {{maxFilesize}}MB.',
          maxFilesize: 30, //MB
          maxFiles: 1,
          timeout: 120000,
          acceptedFiles: ".pdf,.PDF",
          addRemoveLinks: true,
          dictCancelUpload: "Cancelar",
          dictRemoveFile: "Eliminar archivo",
          parallelUploads: 1,

                init: function() {
                  if ($('[name="slug"]')&&$('[name="slug"]').val()!='') {
                    $.ajax({
                      type: "POST",
                      data: {
                        slug : slug
                      },
                      url: globals['base_url'] + 'cms/get_catalogo',
                      success: function(result) {
                        response = JSON.parse(result);
                         if (response.catalogo&&response.catalogo!='null'&&response.catalogo!='') {
                                      var xhr = new XMLHttpRequest();
                                      xhr.open("HEAD", response.catalogo, true);
                                      xhr.onreadystatechange = function() {
                                          if (this.readyState == this.DONE) {
                                            if (response.data_catalogo) {
                                              var fileImage = {
                                                name: response.data_catalogo.original_name,
                                                 size: parseInt(response.data_catalogo.size),
                                                 dataURL: response.catalogo
                                               };
                                            }else{
                                              var fileImage = {
                                                name: 'Archivo PDF',
                                                 dataURL: response.catalogo
                                               };
                                            }

                                             myDropzone.emit('addedfile', fileImage);
                                             myDropzone.emit('processing', fileImage);
                                             myDropzone.emit('complete', fileImage);
                                            
                                             myDropzone.files.push(fileImage);
                                             
                                           }
                                       };
                                      xhr.send();
                                
                                $('div.myDropzone').append('<a class="a-catalogo mt-2" href="'+response.catalogo+'" target="_blank">Ver archivo cargado</a>');
                                  $('div.myDropzone').append('<input type="hidden" name="catalogo" value="'+response.catalogo+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[url]" value="'+response.data_catalogo.url+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[extension]" value="'+response.data_catalogo.extension+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[file_extension]" value="'+response.data_catalogo.file_extension+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[size]" value="'+response.data_catalogo.size+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[original_name]" value="'+response.data_catalogo.original_name+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[name]" value="'+response.data_catalogo.name+'"/>');
                          }
                      }
                    })
                  }
                },
                sending: function(file, xhr, formData) {
                   formData.append("tipo_file",'catalogos');
                },
                success: function(file, response) {
                  response = JSON.parse(response);
                  $('input[name^="catalogo"').remove();
                  $('input[name^="data_catalogo"').remove();
                   $('.a-catalogo').remove();
                  $('div.myDropzone').append(  '<a class="a-catalogo mt-2" href="'+response.uploadUrl+'" target="_blank">Ver archivo cargado</a>');
                  $('div.myDropzone').append('<input type="hidden" name="catalogo" value="'+response.uploadUrl+'"/>');
                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[url]" value="'+response.relativeUrl+'"/>');
                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[extension]" value="'+response.extension+'"/>');
                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[file_extension]" value="'+response.file_extension+'"/>');
                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[size]" value="'+response.size+'"/>');
                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[original_name]" value="'+response.original_name+'"/>');
                  $('div.myDropzone').append('<input type="hidden" name="data_catalogo[name]" value="'+response.name+'"/>');
                },
                removedfile: function(file) {
                    file.previewElement.remove();
                     $('.a-catalogo').remove();
                    $('input[name="catalogo2"]').val('');
                }

        });
  }

  if ($('#tipo_file').val()=='colegios') {

      $('div.myDropzone').html('<div class="dropzone"></div>');

        Dropzone.autoDiscover = false;
        var url = globals['base_url'];
        var uploadUrl = url + 'cms/upload_file';
        var imageIndex=0;
        var slug='colegios/'+$('[name="_id"]').val();

        var myDropzone = new Dropzone("div.myDropzone div.dropzone", {
          url: uploadUrl,
          dictDefaultMessage: '<i class="fas fa-cloud-download-alt"></i><br>Click para subir fotos (Máximo 20)<br>(formato JPG, JPEG o PNG)',
          dictFileTooBig: 'El archivo pesa {{filesize}}MB, el máximo permitido es de {{maxFilesize}}MB.',
          maxFilesize: 10, //MB
          maxFiles: 20,
          acceptedFiles: ".jpeg,.jpg,.png,.JPG,.JPEG,.PNG",
          addRemoveLinks: true,
          dictCancelUpload: "Cancelar",
          dictRemoveFile: "Eliminar archivo",
          parallelUploads: 1,
                init: function() {
                  if ($('[name="_id"]')&&$('[name="_id"]').val()!='') {
                    imageIndex=0;

                    $.ajax({
                      type: "POST",
                      data: {
                        slug : slug
                      },
                      url: globals['base_url'] + 'cms/get_colegio',
                      success: function(result) {
                        response = JSON.parse(result);
                        
                         if (response!='null') {
                            isObject=typeof response.colegio === 'object' && response.colegio !== null;

                            if ($.isArray(response.colegio)||isObject) {
                               $.each(response.colegio, function( index, value_image ) {
                                if (value_image.url!='') {
                                      var xhr = new XMLHttpRequest();
                                      xhr.open("HEAD", value_image.url, true);
                                      xhr.onreadystatechange = function() {
                                          if (this.readyState == this.DONE) {
                                            var fileImage = {
                                               name: value_image.name,
                                               size: parseInt(value_image.size),
                                               dataURL: value_image.url
                                             };
                                             myDropzone.emit('addedfile', fileImage);
                                             myDropzone.emit('processing', fileImage);
                                             myDropzone.emit('complete', fileImage);

                                             myDropzone.createThumbnailFromUrl(fileImage,
                                              myDropzone.options.thumbnailWidth, 
                                              myDropzone.options.thumbnailHeight,
                                              myDropzone.options.thumbnailMethod, true, function (thumbnail) 
                                                  {
                                                      myDropzone.emit('thumbnail', fileImage, thumbnail);
                                                  });
                                            
                                             myDropzone.files.push(fileImage);
                                             
                                           }
                                       };
                                      xhr.send();
                                
                                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][url]" value="'+value_image.url+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][name]" value="'+value_image.name+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][type]" value="'+value_image.type+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][size]" value="'+value_image.size+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][width]" value="'+value_image.width+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][height]" value="'+value_image.height+'"/>');
                                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][index]" value="'+imageIndex+'"/>');
                                 imageIndex++;
                                }
                              });
                            }
                          }
                      }
                    })
                  }
                },
                sending: function(file, xhr, formData) {
                   formData.append("tipo_file",'colegios');
                },
                success: function(file, response) {
                  response = JSON.parse(response);
                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][url]" value="'+response.url+'"/>');
                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][name]" value="'+response.name+'"/>');
                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][type]" value="'+file.type+'"/>');
                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][size]" value="'+file.size+'"/>');
                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][width]" value="'+file.width+'"/>');
                  $('div.myDropzone').append('<input type="hidden" index="'+imageIndex+'" name="image['+imageIndex+'][height]" value="'+file.height+'"/>');
                  $('div.myDropzone').append('<input type="hidden" name="image['+imageIndex+'][index]" value="'+imageIndex+'"/>');
                  imageIndex++;
                },
                removedfile: function(file) {
                    file.previewElement.remove();
                    //var obj = JSON.parse( file.xhr.response);
                    indexDelete= $('input[value="'+file.name+'"]').attr('index');
                    $('input[index="'+indexDelete+'"]').remove();
                    $('button[name="post_status"][value="draft"]').trigger('click')
                }

        });
  }

if ($('#tipo_file').val()=='web'&&$('[name="slug"]').val()=='') {

      $('div.myDropzoneAlianzas').html('<div class="dropzone"></div>');

        Dropzone.autoDiscover = false;
        var url = globals['base_url'];
        var uploadUrl = url + 'cms/upload_file';
        var imageIndex=0;
        var slug='';

        var myDropzoneAlianzas = new Dropzone("div.myDropzoneAlianzas div.dropzone", {
          url: uploadUrl,
          dictDefaultMessage: '<i class="fas fa-cloud-download-alt"></i><br>Click para subir fotos (Máximo 20)<br>(2MB máximo por foto, formato JPG, JPEG o PNG)',
          dictFileTooBig: 'El archivo pesa {{filesize}}MB, el máximo permitido es de {{maxFilesize}}MB.',
          maxFilesize: 2, //MB
          maxFiles: 20,
          acceptedFiles: ".jpeg,.jpg,.png,.JPG,.JPEG,.PNG",
          addRemoveLinks: true,
          dictCancelUpload: "Cancelar",
          dictRemoveFile: "Eliminar archivo",
          parallelUploads: 1,
                init: function() {
                  if ($('[name="_id"]')&&$('[name="_id"]').val()!='') {
                    imageAlianzasIndex=0;

                    $.ajax({
                      type: "POST",
                      data: {
                        slug : slug,
                         file : 'alianzas'
                      },
                      url: globals['base_url'] + 'cms/get_page',
                      success: function(result) {
                        response = JSON.parse(result);
                        
                         if (response!='null') {
                            isObject=typeof response.alianzas === 'object' && response.alianzas !== null;

                            if ($.isArray(response.alianzas)||isObject) {
                               $.each(response.alianzas, function( index, value_image ) {
                                if (value_image.url!='') {
                                      var xhr = new XMLHttpRequest();
                                      xhr.open("HEAD", value_image.url, true);
                                      xhr.onreadystatechange = function() {
                                          if (this.readyState == this.DONE) {
                                            var fileImage = {
                                               name: value_image.name,
                                               size: parseInt(value_image.size),
                                               dataURL: value_image.url
                                             };
                                             myDropzoneAlianzas.emit('addedfile', fileImage);
                                             myDropzoneAlianzas.emit('processing', fileImage);
                                             myDropzoneAlianzas.emit('complete', fileImage);

                                             myDropzoneAlianzas.createThumbnailFromUrl(fileImage,
                                              myDropzoneAlianzas.options.thumbnailWidth, 
                                              myDropzoneAlianzas.options.thumbnailHeight,
                                              myDropzoneAlianzas.options.thumbnailMethod, true, function (thumbnail) 
                                                  {
                                                      myDropzoneAlianzas.emit('thumbnail', fileImage, thumbnail);
                                                  });
                                            
                                             myDropzoneAlianzas.files.push(fileImage);
                                             
                                           }
                                       };
                                      xhr.send();
                                
                                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][url]" value="'+value_image.url+'"/>');
                                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][name]" value="'+value_image.name+'"/>');
                                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][type]" value="'+value_image.type+'"/>');
                                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][size]" value="'+value_image.size+'"/>');
                                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][width]" value="'+value_image.width+'"/>');
                                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][height]" value="'+value_image.height+'"/>');
                                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][index]" value="'+imageAlianzasIndex+'"/>');
                                 imageAlianzasIndex++;
                                }
                              });
                            }
                          }
                      }
                    })
                  }
                },
                sending: function(file, xhr, formData) {
                   formData.append("tipo_file",'web');
                },
                success: function(file, response) {
                  response = JSON.parse(response);
                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][url]" value="'+response.url+'"/>');
                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][name]" value="'+response.name+'"/>');
                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][type]" value="'+file.type+'"/>');
                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][size]" value="'+file.size+'"/>');
                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][width]" value="'+file.width+'"/>');
                  $('div.myDropzoneAlianzas').append('<input type="hidden" index="'+imageAlianzasIndex+'" name="alianzas['+imageAlianzasIndex+'][height]" value="'+file.height+'"/>');
                  $('div.myDropzoneAlianzas').append('<input type="hidden" name="alianzas['+imageAlianzasIndex+'][index]" value="'+imageAlianzasIndex+'"/>');
                  imageAlianzasIndex++;
                },
                removedfile: function(file) {
                    file.previewElement.remove();
                    //var obj = JSON.parse( file.xhr.response);
                    indexDelete= $('input[value="'+file.name+'"]').attr('index');
                    $('input[index="'+indexDelete+'"]').remove();
                    $('button[name="post_status"][value="draft"]').trigger('click')
                }

        });

      $('div.myDropzoneCaminos').html('<div class="dropzone"></div>');

        Dropzone.autoDiscover = false;
        var url = globals['base_url'];
        var uploadUrl = url + 'cms/upload_file';
        var imageIndex=0;
        var slug='';

        var myDropzoneCaminos = new Dropzone("div.myDropzoneCaminos div.dropzone", {
          url: uploadUrl,
          dictDefaultMessage: '<i class="fas fa-cloud-download-alt"></i><br>Click para subir fotos (Máximo 20)<br>(2MB máximo por foto, formato JPG, JPEG o PNG)',
          dictFileTooBig: 'El archivo pesa {{filesize}}MB, el máximo permitido es de {{maxFilesize}}MB.',
          maxFilesize: 2, //MB
          maxFiles: 20,
          acceptedFiles: ".jpeg,.jpg,.png,.JPG,.JPEG,.PNG",
          addRemoveLinks: true,
          dictCancelUpload: "Cancelar",
          dictRemoveFile: "Eliminar archivo",
          parallelUploads: 1,
                init: function() {
                  if ($('[name="_id"]')&&$('[name="_id"]').val()!='') {
                    imageCaminosIndex=0;

                    $.ajax({
                      type: "POST",
                      data: {
                        slug : slug,
                         file : 'caminos'
                      },
                      url: globals['base_url'] + 'cms/get_page',
                      success: function(result) {
                        response = JSON.parse(result);
             
                         if (response!='null') {
                            isObject=typeof response.caminos === 'object' && response.caminos !== null;

                            if ($.isArray(response.caminos)||isObject) {
                               $.each(response.caminos, function( index, value_image ) {
                                if (value_image.url!='') {
                                      var xhr = new XMLHttpRequest();
                                      xhr.open("HEAD", value_image.url, true);
                                      xhr.onreadystatechange = function() {
                                          if (this.readyState == this.DONE) {
                                            var fileImage = {
                                               name: value_image.name,
                                               size: parseInt(value_image.size),
                                               dataURL: value_image.url
                                             };
                                             myDropzoneCaminos.emit('addedfile', fileImage);
                                             myDropzoneCaminos.emit('processing', fileImage);
                                             myDropzoneCaminos.emit('complete', fileImage);

                                             myDropzoneCaminos.createThumbnailFromUrl(fileImage,
                                              myDropzoneCaminos.options.thumbnailWidth, 
                                              myDropzoneCaminos.options.thumbnailHeight,
                                              myDropzoneCaminos.options.thumbnailMethod, true, function (thumbnail) 
                                                  {
                                                      myDropzoneCaminos.emit('thumbnail', fileImage, thumbnail);
                                                  });
                                            
                                             myDropzoneCaminos.files.push(fileImage);
                                             
                                           }
                                       };
                                      xhr.send();
                                
                                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][url]" value="'+value_image.url+'"/>');
                                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][name]" value="'+value_image.name+'"/>');
                                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][type]" value="'+value_image.type+'"/>');
                                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][size]" value="'+value_image.size+'"/>');
                                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][width]" value="'+value_image.width+'"/>');
                                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][height]" value="'+value_image.height+'"/>');
                                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][index]" value="'+imageCaminosIndex+'"/>');
                                 imageCaminosIndex++;
                                }
                              });
                            }
                          }
                      }
                    })
                  }
                },
                sending: function(file, xhr, formData) {
                   formData.append("tipo_file",'web');
                },
                success: function(file, response) {
                  response = JSON.parse(response);
                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][url]" value="'+response.url+'"/>');
                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][name]" value="'+response.name+'"/>');
                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][type]" value="'+file.type+'"/>');
                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][size]" value="'+file.size+'"/>');
                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][width]" value="'+file.width+'"/>');
                  $('div.myDropzoneCaminos').append('<input type="hidden" index="'+imageCaminosIndex+'" name="caminos['+imageCaminosIndex+'][height]" value="'+file.height+'"/>');
                  $('div.myDropzoneCaminos').append('<input type="hidden" name="caminos['+imageCaminosIndex+'][index]" value="'+imageCaminosIndex+'"/>');
                  imageCaminosIndex++;
                },
                removedfile: function(file) {
                    file.previewElement.remove();
                    //var obj = JSON.parse( file.xhr.response);
                    indexDelete= $('input[value="'+file.name+'"]').attr('index');
                    $('input[index="'+indexDelete+'"]').remove();
                    $('button[name="post_status"][value="draft"]').trigger('click')
                }

        });
  }
} 