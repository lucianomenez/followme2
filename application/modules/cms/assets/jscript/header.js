/**
 * Header JS
 * Author: Felipe Hernández
 * 2017
 **/

 $(document).ready(function() {
 $('.ui.menu .ui.dropdown').dropdown({
   on: 'hover'
 });
 $('.ui.menu a.item')
   .on('click', function() {
     $(this)
       .addClass('active')
       .siblings()
       .removeClass('active')
     ;
   });
});

// Help modal
$('#help').on('click', function(e) {
    e.preventDefault();
    $('.modal.help').modal({inverted: true}).modal('show');
});



/*$('#showMobileMenu').on('click', function(e) {
  $('.ui.sidebar')
    .sidebar('toggle')
  ;
});*/
