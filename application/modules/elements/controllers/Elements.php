<?php

class Elements extends MX_Controller {

  function __construct() {
      parent::__construct();
      $this->base_url = base_url();
      $this->module_url = base_url() . $this->router->fetch_module() . '/';
//      $this->user->authorize();
      $this->load->helper('file');
      $this->load->library('parser');
      $this->load->helper('url');
      $this->load->model('app');
      $this->load->model('user/user');
      $this->idu = $this->user->idu;
      $this->usuario= $this->user->get_user($this->idu);

  }

    //==== MAIN usuario

    function ui_navbar() {
      $data["module_url"] = $this->module_url;
      $data["base_url"] =    $this->base_url;
      $data["idu"] = $this->idu;
      $data["name"] = $this->usuario->name;
      $data["lastname"] = $this->usuario->lastname;
      return $this->parser->parse("elements/navbar", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function ui_navbar_mobile() {
      $data["module_url"] = $this->module_url;
      $data["base_url"] =  base_url();
      $data["idu"] = $this->idu;
      $data["name"] = $this->usuario->name;
      $data["lastname"] = $this->usuario->lastname;
      return $this->parser->parse("elements/navbar", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function ui_head(){
      $data["module_url"] = $this->module_url;
      $data["base_url"] = base_url();
      $data["idu"] = $this->idu;
      return $this->parser->parse("elements/head", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function ui_header() {
      $data["module_url"] = $this->module_url;
      $data["base_url"] =  base_url();
      $data["idu"] = $this->idu;
      return $this->parser->parse("elements/header", $data, true, true);
     	//$customData['usercan_create']=true;
    }

    function ui_footer() {
      $data["module_url"] = $this->module_url;
      $data["base_url"] =  base_url();
      $data["idu"] = $this->idu;
      return $this->parser->parse("elements/footer", $data, true, true);
     	//$customData['usercan_create']=true;
    }




} //

?>
