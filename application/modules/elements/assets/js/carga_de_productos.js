

$(document).ready(function(){

$('.zmdi-search').on('click', function(e){
    
    var producto_id = $(this).parent().parent().find(".producto_id").val();
    var base_url = $("#base_url").val();
    
        $.ajax({
          url : 'get_data',
          type: 'POST',
          dataType: 'JSON',
              data: {
                producto_id : producto_id  
              },
            success: function(response){
                var len = response.length;
                for(var i=0; i<len; i++){
                    var id = response[i].id;
                    var status = response[i].status;
                    var name = response[i].name;
                    var description = response[i].description;
                    var price = response[i].price;
                    var category = response[i].category;
                    var preview_img = response[i].preview_img;
                    var base_img = response[i].base_img;
                    var shadow_img = response[i].shadow_img;
                    var highlights_img = response[i].highlights_img;
                    
                    $("#DetalleTitulo").text(name);
                    $("#DetalleDescription").text(description);
                    $("#category").text(category);
                    $("#status").text(status);
                    $("#price").text(price);
                    
                    $('#target_preview').attr("src", base_url + preview_img);
                    $('#shadow_preview').attr("src", base_url + shadow_img);
                    $('#highlights_preview').attr("src", base_url + highlights_img);
                    $('#base_preview').attr("src", base_url + base_img);
                }
    
            },
          error: function(response){
          }
        })    
    
    
    
    e.preventDefault()
    $('#detailModal')
    .modal({
      inverted: true,
      onApprove: function () {

        $.ajax({
          url : '../../asistencia/agregar_dia',
          type: 'POST',
              data: {
              },
          success: function(response){
            toastr["success"]("Cambio guardado");
            $('#consultar').trigger("click");
          },
          error: function(response){
            toastr["error"]("Error.");
          }
        })
      }
    }).modal('show');

});

$('.zmdi-delete').on('click', function(e){
    e.preventDefault()
    $('#deleteModal')
    .modal({
      inverted: true,
      onApprove: function () {

        $.ajax({
          url : '../../asistencia/agregar_dia',
          type: 'POST',
              data: {

              },
          success: function(response){
            toastr["success"]("Cambio guardado");
            $('#consultar').trigger("click");
          },
          error: function(response){
            toastr["error"]("Error.");
          }
        })
      }
    }).modal('show');

});

$('.zmdi-edit').on('click', function(e){
    e.preventDefault()
    $('#editModal')
    .modal({
      inverted: true,
      onApprove: function () {

        $.ajax({
          url : '../../asistencia/agregar_dia',
          type: 'POST',
              data: {

              },
          success: function(response){
            toastr["success"]("Cambio guardado");
            $('#consultar').trigger("click");
          },
          error: function(response){
            toastr["error"]("Error.");
          }
        })
      }
    }).modal('show');

});



});
