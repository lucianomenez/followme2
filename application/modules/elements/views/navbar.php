<aside class="menu-sidebar d-none d-lg-block">
  <div class="logo">
      <a href="#">
          <img src="{module_url}assets/images/logo.png"  width="175" height="50" class="img-thumbnail" alt="Follow Me">
      </a>
  </div>
  <div class="menu-sidebar__content js-scrollbar1">

      <nav class="navbar-sidebar">
          <ul class="list-unstyled navbar__list">
              <li>
                  <a href="{base_url}admin/carga_de_fuentes">
                      <i class="fas fa-font"></i>Administrar Fuentes</a>
              </li>
              <li>
                  <a href="{base_url}admin/carga_de_colores">
                      <i class="fas fa-pencil-square"></i>Administrar Colores</a>
              </li>
              <li>
                  <a href="{base_url}admin/carga_de_escudos">
                      <i class="fas fa-shield-alt"></i>Administrar Escudos</a>
              </li>
              <li>
                  <a href="{base_url}admin/carga_de_banderas">
                      <i class="fas fa-flag"></i>Administrar Banderas</a>
              </li>
              <!-- <li>
                  <a href="{base_url}admin/productos">
                      <i class="far fa-check-square"></i>Modelos Diseñados</a>
              </li> -->
              <li>
                  <a href="{base_url}followme/demo">
                      <i class="fas fa-magic"></i>Design Room</a>
              </li>
              <li>
                  <a href="{base_url}usuarios">
                      <i class="fas fa-user"></i>Administrar Usuarios</a>
              </li>
              <!-- <li>
                  <a href="{base_url}Colegios">
                  <i class="fas fa-graduation-cap"></i>Colegios</a>
              </li>
              <li>
                  <a href="{base_url}sucursales">
                      <i class="fas fa-building"></i>Sucursales</a>
              </li>
              <li>
                  <a href="{base_url}ventas">
                  <i class="fas fa-dollar-sign"></i>Ventas</a>
              </li> -->

          </ul>
      </nav>
  </div>

</aside>

<input id="module_url" name="module_url" type="hidden" value="{module_url}">
<input id="base_url" name="base_url" type="hidden" value="{base_url}">
