<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Beneficios
 */
$route['productos'] = 'website/index';
$route['productos/(:any)'] = 'website/index';
$route['colegios'] = 'website/index';
$route['colegios/(:any)'] = 'website/index';
$route['homeprueba'] = 'website/index';
$route['puntos-de-venta'] = 'website/index';
$route['puntos-de-venta'] = 'website/index';
$route['catalogos/(:any)'] = 'website/index';
$route['bases_condiciones/(:any)'] = 'website/index';
$route['basesycondiciones'] = 'website/index';
$route['presentacion'] = 'website/index';
$route['presentacion_interior'] = 'website/index';

$route['design-room'] = 'website/internas';
$route['user-login'] = 'website/internas';
