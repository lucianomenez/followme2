
function menuScroll(){

  const nav = $('.navbar')

  $(window).on("scroll load resize", function(){
    if(this.scrollY <= 50){
      nav.removeClass('scrolled'); 
    }else{
      nav.addClass('scrolled');      
    }
  });

}


 $(document).ready(function(){

  menuScroll();


  $('#modal-content-menu').apFullscreenModal({
    backgroundColor: '#fafafa',
    openSelector: '#nav-icon',
    closeSelector: '.close-modal',
    animationDuration: 1000
  });

  $('#modal-content-menu-m').apFullscreenModal({
    backgroundColor: '#fafafa',
    openSelector: '#nav-icon-m',
    closeSelector: '.close-modal',
    animationDuration: 1000
  });

  $('#modal-content-mensaje').apFullscreenModal({
    backgroundColor: '#fafafa',
    openSelector: '#mensaje-modal',
    closeSelector: '.close-modal',
    animationDuration: 1000
  });

  $('#modal-content-mensaje-m').apFullscreenModal({
    backgroundColor: '#fafafa',
    openSelector: '#mensaje-modal-m',
    closeSelector: '.close-modal',
    animationDuration: 1000
  });


 });
