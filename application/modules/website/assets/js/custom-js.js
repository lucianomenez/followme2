
var imagenesCargadas = [$('#image').val(), $('#seccion1').val(), $('#seccion2').val()];

for (var i=0; i < imagenesCargadas.length; i++) {
  $('<img>').attr('src',imagenesCargadas[i]);
}


  function cargarInfiniteScrollEscuelas(){

    var $grid = $('.contenedor_colegios').masonry({
        itemSelector: '.colegios-item',
        horizontalOrder: true,

    });


    // get Masonry instance
    var msnry = $grid.data('masonry');

    // init Infinite Scroll
    $grid.infiniteScroll({
      path: function() {
        var offset = ( this.loadCount + 1 ) * 12;
        return base_url + 'website/get-posts/colegios/12/' + offset;
      },
      append: '.colegios-item',
      outlayer: msnry,
      history: false,
    });

 }


  function buscar_colegios(query){

          $.ajax({
            url : $("#base_url").val() + 'website/get_colegios',
            type: 'POST',
            data: {
              query:query,
              limit:6,
            },
            success: function(data){

              $('#contenedor_colegios').html('').hide().html(data).show('slow');
            },
            error: function(data){

            }
          });
  }


function urlToPromise(url) {
return new Promise(function(resolve, reject) {
    JSZipUtils.getBinaryContent(url, function (err, data) {
        if(err) {
            reject(err);
        } else {
            resolve(data);
        }
    });
});
}



 $(document).ready(function(){

      $('body').on('click', '.btn-video.play',function(e) {
        if ($(this).hasClass('mobile')) {
           $('#video_institucionalMobile').trigger('play');
        }else{
          $('#video_institucional').trigger('play');
        }

          $('.btn-video.pause').show();
            $('.btn-video.play').hide();
      });
      $('body').on('click', '.btn-video.pause',function(e) {
        if ($(this).hasClass('mobile')) {
           $('#video_institucionalMobile').trigger('pause');
        }else{
          $('#video_institucional').trigger('pause');
        }
          $('.btn-video.play').show();
          $('.btn-video.pause').hide();

      });

  if ($('#page').val()=='colegios') {

  $('#scripts').append('<script src="'+$('#base_url').val() +'website/assets/js/infinite-scroll.pkgd.js"></script>');

    cargarInfiniteScrollEscuelas();

      $('body').on('click', '.buscar_colegios',function(e) {
         query=$('#filtro_colegio').val();
          buscar_colegios(query);
      });

      $('#filtro_colegio').keypress(function (e) {
          if (e.which == '13') {
            query=$('#filtro_colegio').val();
            buscar_colegios(query);
            return false;
          }

      });

      if ($('#slug').val()!='colegios') {
        var zip = new JSZip();
        var img = zip.folder("fotos");
        $('body').on('click', '.btn_descargar',function(e) {
           $( ".image_download" ).each(function( index ) {
                inputVal=$(this);
                inputUrl=inputVal.val();
                img_name=inputVal.attr('data-name');

                  img.file(img_name, urlToPromise(inputUrl), {binary:true});

            });

            zip.generateAsync({type:"blob"}).then(function(content) {
              // save out
                saveAs(content, "galeria.zip");
            });
        })
      }




  }

  if ($('#page').val()=='home') {




   if($(window).width() > 992){
     new fullpage('#homePageComputer', {
      scrollingSpeed: 1600,
      scrollBar: true,
      autoScrolling: true,
      touchSensitivity:7,
      css3: true,
      keyboardScrolling: true,
      normalScrollElements: '#modal-content-mensaje',
     });

      $('.slider').slick({
       infinite: true,
       slidesToShow: 6,
       slidesToScroll: 1,
       speed: 300,
       centerMode: false,
       variableWidth: true,
       autoplay:true,
       autoplaySpeed: 2000,
       arrows:true,
     });
   }else{

     new fullpage('#homePageMobile', {
      scrollingSpeed: 1000,
      scrollBar: true,
      autoScrolling: true,
      touchSensitivity:7,
      css3: true,
      keyboardScrolling: true,
      normalScrollElements: '#modal-content-mensaje',
      });

      $('.slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 300,
        centerMode: false,
        variableWidth: true,
        autoplay:true,
        autoplaySpeed: 2000,
        arrows:true,
      });
    }


  }



  $('#modal-content-menu').apFullscreenModal({
    backgroundColor: '#fafafa',
    openSelector: '#nav-icon',
    closeSelector: '.close-modal',
    animationDuration: 1000
  });

  $('#modal-content-menu-m').apFullscreenModal({
    backgroundColor: '#fafafa',
    openSelector: '#nav-icon-m',
    closeSelector: '.close-modal',
    animationDuration: 1000
  });

  $('#modal-content-mensaje').apFullscreenModal({
    backgroundColor: '#fafafa',
    openSelector: '#mensaje-modal',
    closeSelector: '.close-modal',
    animationDuration: 1000
  });


 });


(function(jQuery, window, document, undefined) {
  'use strict';
  // Polyfill for requestAnimationFrame
  // via: https://gist.github.com/paulirish/1579671
  (function() {
    var lastTime = 0,
      vendors = ['ms', 'moz', 'webkit', 'o'],
      clearTimeout;
    for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
      window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
      window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame || !window.requestAnimationFrame)
      window.requestAnimationFrame = function(callback, element) {

        var currTime = new Date().getTime();
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        var id = window.setTimeout(function() {
            callback(currTime + timeToCall);
          },
          timeToCall);
        lastTime = currTime + timeToCall;
        return id;
      };
    if (!window.cancelAnimationFrame)
      window.cancelAnimationFrame = function(id) {
        clearTimeout(id);
      };
  }());

  // Parallax Constructor
  function Parallax(element, options) {
    var self = this;

    if (typeof options == 'object') {
      delete options.refresh;
      delete options.render;
      jQuery.extend(this, options);
    }

    this.$element = jQuery(element);

    if (!this.imageSrc && this.$element.is('img')) {
      this.imageSrc = this.$element.attr('src');
    }

    var positions = (this.position + '').toLowerCase().match(/\S+/g) || [];

    if (positions.length < 1) {
      positions.push('center');
    }
    if (positions.length == 1) {
      positions.push(positions[0]);
    }

    if (positions[0] == 'top' || positions[0] == 'bottom' || positions[1] == 'left' || positions[1] == 'right') {
      positions = [positions[1], positions[0]];
    }

    if (this.positionX !== undefined) positions[0] = this.positionX.toLowerCase();
    if (this.positionY !== undefined) positions[1] = this.positionY.toLowerCase();

    self.positionX = positions[0];
    self.positionY = positions[1];

    if (this.positionX != 'left' && this.positionX != 'right') {
      if (isNaN(parseInt(this.positionX))) {
        this.positionX = 'center';
      } else {
        this.positionX = parseInt(this.positionX);
      }
    }

    if (this.positionY != 'top' && this.positionY != 'bottom') {
      if (isNaN(parseInt(this.positionY))) {
        this.positionY = 'center';
      } else {
        this.positionY = parseInt(this.positionY);
      }
    }

  }
  // Parallax Instance Methods
  jQuery.extend(Parallax.prototype, {
    speed: 0.2,
    bleed: 0,
    zIndex: -100,
    iosFix: true,
    androidFix: true,
    position: 'center',
    overScrollFix: false,

    refresh: function() {
      this.boxWidth = this.$element.outerWidth();
      this.boxHeight = this.$element.outerHeight() + this.bleed * 2;
      this.boxOffsetTop = this.$element.offset().top - this.bleed;
      this.boxOffsetLeft = this.$element.offset().left;
      this.boxOffsetBottom = this.boxOffsetTop + this.boxHeight;

      var winHeight = Parallax.winHeight;
      var docHeight = Parallax.docHeight;
      var maxOffset = Math.min(this.boxOffsetTop, docHeight - winHeight);
      var minOffset = Math.max(this.boxOffsetTop + this.boxHeight - winHeight, 0);
      var imageHeightMin = this.boxHeight + (maxOffset - minOffset) * (1 - this.speed) | 0;
      var imageOffsetMin = (this.boxOffsetTop - maxOffset) * (1 - this.speed) | 0;

      if (imageHeightMin * this.aspectRatio >= this.boxWidth) {
        this.imageWidth = imageHeightMin * this.aspectRatio | 0;
        this.imageHeight = imageHeightMin;
        this.offsetBaseTop = imageOffsetMin;

        var margin = this.imageWidth - this.boxWidth;

        if (this.positionX == 'left') {
          this.offsetLeft = 0;
        } else if (this.positionX == 'right') {
          this.offsetLeft = -margin;
        } else if (!isNaN(this.positionX)) {
          this.offsetLeft = Math.max(this.positionX, -margin);
        } else {
          this.offsetLeft = -margin / 2 | 0;
        }
      } else {
        this.imageWidth = this.boxWidth;
        this.imageHeight = this.boxWidth / this.aspectRatio | 0;
        this.offsetLeft = 0;

        var marginHeight = this.imageHeight - imageHeightMin;

        if (this.positionY == 'top') {
          this.offsetBaseTop = imageOffsetMin;
        } else if (this.positionY == 'bottom') {
          this.offsetBaseTop = imageOffsetMin - marginHeight;
        } else if (!isNaN(this.positionY)) {
          this.offsetBaseTop = imageOffsetMin + Math.max(this.positionY, -marginHeight);
        } else {
          this.offsetBaseTop = imageOffsetMin - marginHeight / 2 | 0;
        }
      }
    },

    render: function() {
      var scrollTop = Parallax.scrollTop;
      var scrollLeft = Parallax.scrollLeft;
      var overScroll = this.overScrollFix ? Parallax.overScroll : 0;
      var scrollBottom = scrollTop + Parallax.winHeight;

      if (this.boxOffsetBottom > scrollTop && this.boxOffsetTop <= scrollBottom) {
        this.visibility = 'visible';
        this.mirrorTop = this.boxOffsetTop - scrollTop;
        this.mirrorLeft = this.boxOffsetLeft - scrollLeft;
        this.offsetTop = this.offsetBaseTop - this.mirrorTop * (1 - this.speed);
      } else {
        this.visibility = 'hidden';
      }

      this.$mirror.css({
        transform: 'translate3d(0px, 0px, 0px)',
        visibility: this.visibility,
        top: this.mirrorTop - overScroll,
        left: this.mirrorLeft,
        height: this.boxHeight,
        width: this.boxWidth
      });

      this.$slider.css({
        transform: 'translate3d(0px, 0px, 0px)',
        position: 'absolute',
        top: this.offsetTop,
        left: this.offsetLeft,
        height: this.imageHeight,
        width: this.imageWidth,
        maxWidth: 'none'
      });
    }
  });
  // Parallax Static Methods
  jQuery.extend(Parallax, {
    scrollTop: 0,
    scrollLeft: 0,
    winHeight: 0,
    winWidth: 0,
    docHeight: 1 << 30,
    docWidth: 1 << 30,
    sliders: [],
    isReady: false,
    isFresh: false,
    isBusy: false,

    setup: function() {
      if (this.isReady) return;

      var $doc = jQuery(document),
        $win = jQuery(window);

      var loadDimensions = function() {
        Parallax.winHeight = $win.height();
        Parallax.winWidth = $win.width();
        Parallax.docHeight = $doc.height();
        Parallax.docWidth = $doc.width();
      };

      var loadScrollPosition = function() {
        var winScrollTop = $win.scrollTop();
        var scrollTopMax = Parallax.docHeight - Parallax.winHeight;
        var scrollLeftMax = Parallax.docWidth - Parallax.winWidth;
        Parallax.scrollTop = Math.max(0, Math.min(scrollTopMax, winScrollTop));
        Parallax.scrollLeft = Math.max(0, Math.min(scrollLeftMax, $win.scrollLeft()));
        Parallax.overScroll = Math.max(winScrollTop - scrollTopMax, Math.min(winScrollTop, 0));
      };

      $win.on('resize.px.parallax load.px.parallax', function() {
          loadDimensions();
          Parallax.isFresh = false;
          Parallax.requestRender();
        })
        .on('scroll.px.parallax load.px.parallax', function() {
          loadScrollPosition();
          Parallax.requestRender();
        });

      loadDimensions();
      loadScrollPosition();

      this.isReady = true;
    },

    configure: function(options) {
      if (typeof options == 'object') {
        delete options.refresh;
        delete options.render;
        jQuery.extend(this.prototype, options);
      }
    },

    refresh: function() {
      jQuery.each(this.sliders, function() {
        this.refresh();
      });
      this.isFresh = true;
    },

    render: function() {
      this.isFresh || this.refresh();
      jQuery.each(this.sliders, function() {
        this.render();
      });
    },

    requestRender: function() {
      var self = this;

      if (!this.isBusy) {
        this.isBusy = true;
        window.requestAnimationFrame(function() {
          self.render();
          self.isBusy = false;
        });
      }
    },
    destroy: function(el) {
      var i,
        parallaxElement = jQuery(el).data('px.parallax');
      parallaxElement.$mirror.remove();
      for (i = 0; i < this.sliders.length; i += 1) {
        if (this.sliders[i] == parallaxElement) {
          this.sliders.splice(i, 1);
        }
      }
      jQuery(el).data('px.parallax', false);
      if (this.sliders.length === 0) {
        jQuery(window).off('scroll.px.parallax resize.px.parallax load.px.parallax');
        this.isReady = false;
        Parallax.isSetup = false;
      }
    }
  });
  // Parallax Plugin Definition
  function Plugin(option) {
    return this.each(function() {
      var $this = jQuery(this);
      var destroy;
      var options = typeof option == 'object' && option;

      if (this == window || this == document || $this.is('body')) {
        Parallax.configure(options);
      } else if (!$this.data('px.parallax')) {
        options = jQuery.extend({}, $this.data(), options);
        $this.data('px.parallax', new Parallax(this, options));
      } else if (typeof option == 'object') {
        jQuery.extend($this.data('px.parallax'), options);
      }
      if (typeof option == 'string') {
        if (option == destroy) {
          Parallax.destroy(this);
        } else {
          Parallax[option]();
        }
      }
    });
  }
  var old = jQuery.fn.parallax;
  jQuery.fn.parallax = Plugin;
  jQuery.fn.parallax.Constructor = Parallax;
  // Parallax No Conflict
  jQuery.fn.parallax.noConflict = function() {
    jQuery.fn.parallax = old;
    return this;
  };
  // Parallax Data-API
  jQuery(document).on('ready.px.parallax.data-api', function() {
    jQuery('[data-parallax="scroll"]').parallax();
  });

  jQuery('.parallax-window').parallax();

  jQuery(window).scroll(function() {
    var wtop = jQuery(window).scrollTop();
    var timer;

    if (timer) {
      window.clearTimeout(timer);
    }

    timer = window.setTimeout(function() {

      jQuery('.overlay-window-vertical').each(function() {
        var offset = jQuery(this).offset();

        var val = (wtop *2.5) / jQuery(this).data('scroll-speed');

     if (val > 0) {
          jQuery(this).css("transform", "translate3d(0px" + ", -" + val + "px" + ", 0)");
        }

      });
    jQuery('.overlay-window-horizontal').each(function() {
        var offset = jQuery(this).offset();

        var valHorizontal = (wtop *60) / jQuery(this).data('scroll-speed');
        var valVertical= (wtop *2) / jQuery(this).data('scroll-speed');
     if (valHorizontal > 0) {
          jQuery(this).css("transform", "translate3d(-"+valHorizontal+"px, "+valVertical+"px, 0px)");
        }

      });
      jQuery('.overlay-window-horizontal-inverted').each(function() {
        var offset = jQuery(this).offset();

        var valHorizontal = (wtop *60) / jQuery(this).data('scroll-speed');
        var valVertical= (wtop *2) / jQuery(this).data('scroll-speed');
     if (valHorizontal > 0) {
          jQuery(this).css("transform", "translate3d("+valHorizontal+"px, "+valVertical+"px, 0px)");
        }

      });

    }, 100);

  });

})(jQuery, window, document);

//jQuery(window).trigger('resize').trigger('scroll');
    wow = new WOW(
      {
        animateClass: 'animated',
        offset:       100,
        callback:     function(box) {
          console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
        }
      }
    );
    wow.init();

      $('body').on('mouseover', 'img.producto , img.producto-relacionado , div.colegio.link',function(e) {

          if ($(this).parent().find('img.back').attr('src')!='') {
            $(this).parent().find('img.front').hide();
            $(this).parent().find('img.back').show();
          }
          if ($(this).hasClass("colegio")) {
            $(this).addClass("productoOver");
          }else{
            $(this).parent().addClass("productoOver");
          }

      });


      $('body').on('mouseout', 'img.producto , img.producto-relacionado , div.colegio.link',function(e) {

      $(this).parent().find('img.front').show();
      $(this).parent().find('img.back').hide();

      if ($(this).hasClass("colegio")) {
            $(this).removeClass("productoOver");
          }else{
            $(this).parent().removeClass("productoOver");
          }
      });


    $('body').on('click', 'img.producto.link ,img.producto-relacionado-mobile.link , img.producto-relacionado.link , div.colegio.link , paleta-link',function(e) {

          window.location.href = $(this).attr("slug");
      });



    $('.image_click').on('click',function(e){
      src= $(this).attr("src");

      $(this).parent().parent().parent().find('.image_detalle img.detalle').attr("src",src)
       $(this).parent().parent().parent().parent().parent().find('.image_detalle').find('.detalle-mobile').attr("src",src)
  })

if ($('#page').val()=='colegios') {

    $('.gallery-item').magnificPopup({
      type: 'image',
      gallery:{
        enabled:true
      },

            zoom: {
        enabled: true, // By default it's false, so don't forget to enable it

        duration: 300, // duration of the effect, in milliseconds
        easing: 'ease-in-out', // CSS transition easing function

        // The "opener" function should return the element from which popup will be zoomed in
        // and to which popup will be scaled down
        // By defailt it looks for an image tag:
        opener: function(openerElement) {
          // openerElement is the element on which popup was initialized, in this case its <a> tag
          // you don't need to add "opener" option if this code matches your needs, it's defailt one.
          return openerElement.is('img') ? openerElement : openerElement.find('img');
        }
      }
    });
}
