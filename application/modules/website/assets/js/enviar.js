
    var elementos_custom = 0;


    $('#form_enviar').on('submit', function(e) {
        e.preventDefault();
        recaptcha=$('#form_enviar [name="g-recaptcha-response"]').val();
        if (recaptcha!='') {
        	var form_enviar = $('#form_enviar').serializeArray();
             $.ajax({
              url : $('#base_url').val() + 'website/send_email',
              type: 'POST',
              data: form_enviar,
              success: function(resultado){

              	$('#form_enviar').hide();
    				    $('#submit_computer_succes').show();
              }
        	});
        }else{
          $('#recaptcha_msg').show();
        }
      });


        $(document).on("click", "#borrar" , function() {
            $(this).parent().parent().parent().remove();
        });



      $('#agregar_elemento').on('click', function(e){
          elementos_custom++;
          $("#append").append("<br/><div> <div class='form-row row-custom-element row' data-row='"+elementos_custom+"'> <div class='col-xs-12 col-sm-3 mb-2'> <input type='text' autocomplete='off' name='texto[]' class='form-control input-room custom_element_texto' placeholder='Texto / Aplique / Elemento'> </div><div class='col-xs-12 col-sm-1 mb-2'> <select name='select' autocomplete='off'name='fuente[]' class='form-control input-room custom_element_fuente'> <option value='-'>-</option> <option value='A1'>A1</option> <option value='A2'>A2</option> <option value='A3'>A3</option> <option value='A4'>A4</option> <option value='A5'>A5</option> <option value='A6'>A6</option> <option value='A7'>A7</option> <option value='A8'>A8</option> <option value='A9'>A9</option> <option value='B1'>B1</option> <option value='B2'>B2</option> <option value='B3'>B3</option> <option value='B4'>B4</option> <option value='B5'>B5</option> <option value='B6'>B6</option> <option value='B7'>B7</option> <option value='C1'>C1</option> <option value='C2'>C2</option> <option value='C3'>C3</option> <option value='C4'>C4</option> <option value='C5'>C5</option> <option value='C6'>C6</option> <option value='C7'>C7</option> <option value='C8'>C8</option> <option value='C9'>C9</option> </select> </div><div class='col-xs-12 col-sm-1 mb-2'> <input type='text' autocomplete='off' name='cm[]' class='form-control input-room custom_element_cm' placeholder='CM'> </div><div class='col-xs-12 col-sm-3 mb-2'> <select autocomplete='off' name='estilo[]' class='form-control input-room custom_element_estilo'> <option value='AS'>Simple</option> <option value='B'>Bordado</option> <option value='AF'>Aplique Festón</option> <option value='ADR'>Doble Regular</option> <option value='ADI'>Doble Irregular</option> <option value='C'>Contorno</option> <option value='E'>Estampado</option> <option value='S'>Sublimado</option> </select> </div><div class='col-xs-12 col-sm-2 mb-1'> <select id='selectColores' autocomplete='off' name='color[]' class='form-control input-room custom_element_color selectColores' placeholder='Color'></select> </div><div class='col-xs-12 col-sm-1 mb-1'> <select autocomplete='off' name='curva[]' class='form-control input-room custom_element_curva'> <option value='C1'>C1</option> <option value='C2'>C2</option> <option value='C3'>C3</option> <option value='C4'>C4</option> <option value='-'>-</option> </select> </div><div class='col-xs-12 col-sm-1 mb-1'> <input type='button' class='btn-room' value='Borrar' id='borrar'/> </div></div><div class='form-row'> <div class='col-xs-12 col-sm-12 mb-2'> <input type='text' autocomplete='off' name='aclaracion[]' class='form-control input-room custom_element_aclaracion' placeholder='Aclaración del elemento agregado'> </div></div></div>");
          $.getJSON(`colores/nombres/Frisa`)
          .done(function(data){
            const colores = Object.values(data);
            colores.forEach((valor) => {
              const optionElement = document.createElement('option');
              optionElement.textContent = valor;
              $(".selectColores")[elementos_custom-1].append(optionElement);
            });
          })
        });
