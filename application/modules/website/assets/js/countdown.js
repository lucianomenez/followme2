
import { CountUp } from './dist/countUp.js';

$(document).ready(function() {
var terminado=false;
    $(window).scroll(function() {
     
      if ($(window).scrollTop() > 500) {
          if (!terminado) {
            $( ".banner-number-input" ).each(function( index ) {

               loadCount($(this).attr('id')+'count',$(this).val());
              loadCount($(this).attr('id')+'countMobile',$(this).val());
            });
            terminado=true;
          }
      } 
        });


})


function loadCount(id,to){
  const options = {
  duration: 1.5,
  separator: '.',
  };
  var demo = new CountUp(id, to,options);
  demo.start();
}