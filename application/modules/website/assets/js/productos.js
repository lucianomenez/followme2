    var skip =0;


	parameter=getURLParameter("categoria")
	if (parameter) {
		categoria_inicio=parameter; 
		$('#categoria').val(categoria_inicio)
	}else{
		categoria_inicio=$('#categoria').val();   
	}
    
    changeUrlParam("categoria",categoria_inicio);
    index_item=parseInt($('.option_categoria[value="'+categoria_inicio+'"]').attr('index_item'))

	  $('.slider').slick({
	    infinite: true,
	    speed: 300,
	    slidesToShow: 1,
	    centerMode: false,
	    variableWidth: true,	
		initialSlide:index_item,
	    focusOnSelect: true
	  }); 

		getProductos(categoria_inicio)
		load_infinitescroll(categoria_inicio)


      $('body').on('click', '.option_categoria',function(e) {
        let categoria=$(this).attr('value');
        categoria_inicio=categoria;
		changeUrlParam("categoria",categoria);
		$('.contenedor_productos').infiniteScroll('destroy')
		getProductos(categoria)
		skip=0;
		load_infinitescroll(categoria)
		$('.contenedor_productos').infiniteScroll('resetpage')
      });

      function load_infinitescroll(categoria){

			    // init Infinite Scroll
			    $('.contenedor_productos').infiniteScroll({
			      path: function() {
			        var productos ='null';

			       // var offset = ( this.loadCount + 1 ) * 12;

			        	var offset = ( skip + 1 ) * 12;
			        	if (this.loadCount>skip) {
 							skip++;
						}

				        return $("#base_url").val() +'website/get-posts/productos/12/' + offset+'/'+categoria;

			      },
			      append: '.productos-item',
			      status: '.page-load-status',
			      history: false,
			      prefill: true
			    });

			    $('.contenedor_productos').on( 'last.infiniteScroll', function( event, response, path ) {
						console.log('last')
				});


				$('.contenedor_productos').on( 'load.infiniteScroll', function() {

				});

      }

function getProductos(categoria){
	    let url=$('#base_url').val() + 'website/get_productos';
        $.ajax({
              url: url,
              type: "POST",
              data: {
                categoria : categoria,
                count : 12,
                offset:0
              }
            }).done(function(response) {
              $('#categoria').val(categoria);
               $(".contenedor_productos").html('');
              $('.contenedor_productos').hide().html(response).fadeIn('slow');
				load_infinitescroll(categoria)
            })

}


function getURLParameter(name) { 
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null; 
} 

function changeUrlParam (param, value) { 
        var currentURL = window.location.href+'&'; 
        var change = new RegExp('('+param+')=(.*)&', 'g'); 
        var newURL = currentURL.replace(change, '$1='+value+'&'); 
 
        if (getURLParameter(param) !== null){ 
                try { 
                        window.history.replaceState('', '', newURL.slice(0, - 1) ); 
                } catch (e) { 
                        console.log(e); 
                } 
        } else { 
                var currURL = window.location.href; 
                if (currURL.indexOf("?") !== -1){ 
                        window.history.replaceState('', '', currentURL.slice(0, - 1) + '&' + param + '=' + value); 
                } else { 
                        window.history.replaceState('', '', currentURL.slice(0, - 1) + '?' + param + '=' + value); 
                } 
        } 
}

