         {data}
          <div class="col-xs-6 col-sm-6 col-lg-2 p-4">
              <div class="container text-center paleta-link p-2" slug="{base_url}{slug}">

                <div class="circulo" style="display:inline-block; background:{colorHex} url('{image}');background-position: center; -webkit-box-shadow: 0px 0px 34px -19px rgba(0,0,0,0.75);-moz-box-shadow: 0px 0px 34px -19px rgba(0,0,0,0.75);box-shadow: 0px 0px 34px -19px rgba(0,0,0,0.75);"></div>
                <div class="categoria-paletas-cu mt-3">{categoria_paleta_name}</div>
                <div class="nombre-paletas">{colorName}</div>
              </div>
          </div>
           {/data}