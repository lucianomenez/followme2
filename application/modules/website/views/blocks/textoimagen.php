
  <div class="container mt-4" style="">
	<div class="row  align-items-center">
	  	<div class="col-xs-12 col-sm-6 col-lg-6">
		  <img src="{image}" width="100%">
		  <div style="font-size: 0.875rem;font-weight: 300;color: rgba(0, 0, 0, 0.7); text-align: center;margin-top: .5rem;width: 100%;">{figcaption}</div>
		</div>
	       <div class="col-xs-12 col-sm-6 col-lg-6" style="font-size: 1rem;"> {content}</div>
	</div>
  </div>
