<div class="computer">

<section class="page-section p-0" style="margin-top:1rem;margin-bottom:3rem;">
  <div class="container-fluid">
<!-- ACA EL SLICK -->
    <section class="variable slider p-0" style="white-space:nowrap;">
      {items}
        <!-- nueva o clasicos -->
          <span class="title-pestana option_categoria" value="{value}" index_item={index_item}>{text} / </span>
       {/items}
    </section>
  </div>
</section>
</div>

<div class="mobile">

<section class="page-section p-0" style="margin-top:1rem; margin-bottom:3rem;">
  <div class="container-fluid">
<!-- ACA EL SLICK -->
    <section class="variable slider p-0" style="white-space:nowrap;">
      {items}
        <!-- nueva o clasicos -->
          <span class="title-pestana-mobile option_categoria" value="{value}" index_item={index_item}>{text} / </span>
       {/items}
    </section>
  </div>
</section>
</div>
