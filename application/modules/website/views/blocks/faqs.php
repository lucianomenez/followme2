  <div class="container mt-4">
        <div style="font-size: 1.4rem;font-weight: bold;">{title}</div>

        <div class="listado collapse" id="faqs__listado-{block_index}">
          {q_a}
            <p>
              <strong>{question}</strong><br>
              {answer}
            </p>
          {/q_a}
        </div>

  </div>
