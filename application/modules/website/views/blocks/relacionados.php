  <section class="page-section p-0 computer" style="margin-bottom: 2rem;">

    <div class="container">
    <div class="title-relac">Productos Relacionados</div>

        <div class="row pt-4 justify-content-center" style="">
          {productos}
            <div class=" col-3 p-4 align-self-center">
                <div class="producto marco-relacionado neutral d-flex">
                    <img data-pin-nopin="false" src="{image}" class="producto-relacionado front link" slug="{base_url}{slug}">
                    <img data-pin-nopin="false" src="{image_back}" class="producto-relacionado back link" slug="{base_url}{slug}" style="display: none;">
                </div>
                 <div class="title-prod pl-1 pr-1 mt-2 ">{title}</div>

            </div>
          {/productos}
        </div>

    </div>
  </section>

  <section class="page-section mobile p-0" style="margin-bottom: 2rem;">

    <div class="container text-center">
    <div class="title-relac-mobile">Productos Relacionados</div>
        <div class="row pt-5 justify-content-center" style="">
          {productos}
            <div class="col-6 align-self-center mt-2">
                <div class="producto marco-relacionado-mobile neutral d-flex">
                    <img data-pin-nopin="false" src="{image}" class="producto-relacionado-mobile front link" slug="{base_url}{slug}">
                    <img data-pin-nopin="false" src="{image_back}" class="producto-relacionado-mobile back link" slug="{base_url}{slug}" style="display: none;">
                </div>
                                  <div class="title-prod-mobile pl-1 pr-1 mt-2 ">{title}</div>
            </div>
          {/productos}
        </div>

    </div>
  </section>
