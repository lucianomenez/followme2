
<script>
  var base_url = '{base_url}';
</script>

<div id="scripts">
  <!-- Bootstrap core JavaScript -->


  <!-- Plugin JavaScript -->
  <script src="{base_url}website/assets/fullPage/dist/fullpage.min.js"></script>

  <script src="{base_url}website/assets/popup-master/dist/jquery.magnific-popup.min.js"></script>

  <script src="{base_url}jscript/js/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="{base_url}jscript/js/agency.min.js"></script>
  <script src="{base_url}jscript/WOW-master/dist/wow.js"></script>

  <script src="{base_url}website/assets/js/ap-fullscreen-modal.min.js"></script>
  <script src="{base_url}website/assets/js/menu.js"></script>
  <script src="{base_url}website/assets/js/custom-js.js"></script>
    <script src="{base_url}website/assets/js/lazysizes.min.js" async></script>

    <script src="{base_url}website/assets/js/jquery-serializeForm.js"></script>

  </div>



 <div style="display: none;">
   <div id="modal-content-menu" class="justify-content-center">
    <div class="container text-center" style="margin-top:15vh;">
        <div>
          <a class="link-modal" href="{base_url}productos">Productos</a>
        </div>
        <div>
          <a class="link-modal" href="{base_url}followme/demo">Design room</a>
        </div>
        <div>
          <a class="link-modal" href="{base_url}colegios">Colegios</a>
        </div>
        <!-- <div>
          <a class="link-modal" href="{base_url}puntos-de-venta">Puntos de venta</a>
        </div> -->
    </div>
  </div>

    <div id="modal-content-menu-m" class="justify-content-center">
      <div class="container text-center" style="padding-top:30%">
        {if {login} == 1}
          <div class="p-3">
            <a class="link-modal-m" href="{base_url}user/logout">Cerrar sesión</a>
          </div>
        {else}
        <div class="p-3">
          <a class="link-modal-m" href="{base_url}user/login">Ingresar</a>
        </div>
        {/if}
        <div class="p-3">
          <a class="link-modal-m" href="{base_url}productos">Productos</a>
        </div>
        <div class="p-3">
          <a class="link-modal-m" href="{base_url}followme/demo">Design room</a>
        </div>
        <div class="p-3">
          <a class="link-modal-m" href="{base_url}colegios">Colegios</a>
        </div>
        <!-- <div class="p-3">
          <a class="link-modal-m" href="{base_url}puntos-de-venta">Puntos de venta</a>
        </div> -->
      </div>
  </div>

<div id="modal-content-mensaje" class="modal-content">
  <div class="container align-items-center" style="padding-top:5vh;">
    <div class="title-modal">
      Contactate<br />con nosotros
    </div>
<section class="section main-content clientes align-items-center pt-5">
  <div class="container align-items-center">
    <div class="row align-items-center" style="position: relative;">
      <span id="submit_mobile_succes" class="" style="    font-weight: bold;
    font-size: 3vh;display: none;color:#2DF19C;position: absolute;top:50px;">Mensaje Eviando<br>Nos comunicaremos a la brevedad</span>
      <form role="form" id="form_enviar_mobile" action="" class="form-contact col-xs-12 col-sm-7 col-md-8" style="margin: 0 auto;">
        <div class="row">
          <div class="col-xs-12 col-sm-12">
            <div class="form-group">
              <input type="text" class="form-control input-modal-mobile" placeholder="Nombre de Contacto *" name="nombre" required="">
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <input type="text" class="form-control input-modal-mobile" placeholder="Colegio *" name="colegio" required="">
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <input type="text" class="form-control input-modal-mobile" placeholder="Localidad *" name="localidad" required="">
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <input type="text" class="form-control input-modal-mobile" placeholder="Teléfono *" name="telefono" required="">
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <input type="text" class="form-control input-modal-mobile" placeholder="Email*" name="email" required="">
                      </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <select class="form-control input-modal-mobile" name="nivel" required="">
                <option value="" selected="" disabled="">Nivel escolar *</option>
                <option>Primaria</option>
                <option>Secundaria</option>
              </select>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group ">
              <select class="form-control input-modal-mobile" name="promocion"  required="">
                <option value="" selected="" disabled="">Promoción *</option>
                <option>2020</option>
                <option>2021</option>
              </select>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12">
            <div class="form-group">
              <textarea class="form-control input-modal-mobile" name="consulta" placeholder="Consulta*" rows="2" required=""></textarea>
            </div>
             <div class="g-recaptcha" data-sitekey="6Le7YuQUAAAAAMIjimpp-eb60ZbkoF7jhFaQoQ0f"></div>
            <div class="form-group mt-2" style="position: relative;">
              <span id="recaptcha_msg_mobile" class="" style=" font-weight: bold;display: none;color:red;position: absolute;top: 0;right: 10px;">Marcá no soy un robot</span>
              <input id="submit" type="submit" name="submit" value="Enviar" class="btn pull-right btn-modal-mobile">
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
</div>
</div>

</div>

  <script src="{base_url}website/assets/js/enviar_mobile.js"></script>
