<main>
  <section class="page__title">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h1>{title}</h1>
          <p>{meta_description}</p>
        </div>
      </div>
    </div>
  </section>

  {blocks}

</main>
