<input type="hidden" id="page" value="colegios">
<input type="hidden" id="id" value="{_id}">


<style type="text/css">
  .hover-effect-colegio img {
    -webkit-transition: all 500ms ease 0s;
    -ms-transition: all 500ms ease 0s;
    -o-transition: all 500ms ease 0s;
    -moz-transition: all 500ms ease 0s;
    transition: all 500ms ease 0s;
}

.hover-effect-colegio {
width: 100%;

}
.hover-effect-colegio img:hover{

  transform: scale(1.2);
  position: relative;
  z-index: 99;

}
</style>

<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v6.0"></script>
      <script type="text/javascript" src="//assets.pinterest.com/js/pinit.js" ></script>

<section class="page-section pt-0 " style="margin-top: 6rem; margin-bottom:1.5rem;">



  <div class="container ">



      <a href="{base_url}colegios" class="btn-volver" style="position: relative;z-index: 99;">Volver</a>
      <input type="hidden" id="title_download" value="{title}">
        <div class="info-colegio bold mt-2">
                    {title} <small>{ano}</small>
        </div>
        <div class="info-colegio-s">
                    {city}
         </div>
         <div style="text-align: right;"> 
                
                <div class="fb-share-button" data-href="{base_url}{slug}" data-layout="button" data-size="small"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={base_url}{slug}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Compartir</a></div>
                <a href="//pinterest.com/pin/create/button/" data-pin-do="buttonBookmark"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png"></a>

                <div class="btn_descargar"><big><i class="far fa-file-archive"></i> </big><b>Descargar galería</b></div>
         </div>
    <div class="my-gallery row mt-3" style="">

              <!-- nueva o clasicos -->
          {image}
                        <input type="hidden" class="image_download" value="{url}" data-name="{name}">

                          <a href="{url}" style="display: inline-block;display: flex;justify-content: center;   align-items: center;padding: 15px;" class="grid-item col-xs-12 col-sm-6 col-lg-4 gallery-item">
                            <div class="hover-effect-colegio" style="vertical-align: middle;"> 
                              <img src="{url}" alt="{title}" class="colegioimg" width= "100%"/>
                            </div>
                          </a>

          {/image}

    </div>
    <a id="lnk" href=""></a>
  </div>

</section>
<script type="text/javascript" src="{base_url}website/assets/jszip/jszip-utils.js"></script>
<script type="text/javascript" src="{base_url}website/assets/jszip/jszip.js"></script>
<script type="text/javascript" src="{base_url}website/assets/filesaver/FileSaver.js"></script>