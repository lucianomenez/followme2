
<div class="computer">
<section class="marine" style="padding-top:6rem; padding-bottom:3rem;">
  <div class="container align-items-center">
    <div class="row align-items-end" >
      <div class="col-5">
        <div class="container">
          <div class="footer-title-2" style="color:#2DF19C;">
            envíos <b>gratis</b> a<br>donde <b>vos</b> estés
          </div>
        </div>
        <div class="container mb-4">
          <div class="footer-title mb-3" style="color:#fff;">
            contactanos
          </div>

          <div class="footer-txt"><i class="fab fa-whatsapp fa-1x mr-3 fff"></i>+54 9 11 5835-2669</div>

          <div class="footer-txt mt-4"><i class="far fa-envelope fa-1x mr-3 fff"></i> info@followme.com.ar</div>
        </div>
        <div class="container">
          <div class="footer-title mr-4 mb-3" style="color:#fff;">
            seguinos
          </div>
          <a class="footer-link p-0" href="https://www.facebook.com/followmegrads" target="_blank">
            <img data-pin-nopin="false"  class="icon-redes mr-4 filtro" src="{base_url}images/web/facebook.png">
          </a>
          <a class="footer-link p-0" href="https://www.instagram.com/followmegresados/" target="_blank">
            <img data-pin-nopin="false" class="icon-redes mr-4 filtro" src="{base_url}images/web/instagram.png">
          </a>
          <a class="footer-link p-0" href="https://ar.pinterest.com/followmebuzosycamperas/" target="_blank">
            <img class="icon-redes mr-4 filtro" src="{base_url}images/web/pinterest.png"></a>
          <a class="footer-link p-0" href="https://www.tiktok.com/@followmeegresados" target="_blank">
            <img class="icon-redes mr-4 filtro" src="{base_url}images/web/tiktok.png"></a>
        </div>
      </div>

      <div class="col-7" style="position:relative;">
         <span id="submit_computer_succes" class="" style="display: none;color:#2DF19C;position: absolute;top:-260px;">Mensaje Eviando<br>Nos comunicaremos a la brevedad</span>

        <form role="form" id="form_enviar"  action="" class="form-contact col-xs-12 col-sm-12 col-md-12" style="margin: 0 auto;">
          <div class="row">
            <div class="col-xs-12 col-sm-12">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="nombre" placeholder="Nombre de Contacto *" name="nombre" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="colegio" placeholder="Colegio *" name="colegio" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="localidad" placeholder="Localidad *" name="localidad" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="telefono" placeholder="Teléfono *" name="telefono" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <input type="text" class="form-control input-modal" id="email" placeholder="Email*" name="email" required="">
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <select class="form-control input-modal custom-select" name="nivel" id="nivel" required="">
                  <option value="" selected="" disabled="">Nivel escolar *</option>
                  <option>Primaria</option>
                  <option>Secundaria</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group ">
                <select class="form-control input-modal custom-select" name="promocion" id="promocion" required="">
                  <option value="" selected="" disabled="">Promoción *</option>
                  <option>2021</option>
                  <option>2022</option>
                </select>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12" >
              <div class="form-group">
                <textarea class="form-control input-modal" id="consulta" name="consulta" placeholder="Consulta*" rows="4" required=""></textarea>
              </div>

                <div class="g-recaptcha" data-sitekey="6Le7YuQUAAAAAMIjimpp-eb60ZbkoF7jhFaQoQ0f"></div>

                <div class="form-group" style="position: relative;">
                  <span id="recaptcha_msg" class="" style="display: none;color:red;position: absolute;top: 0;">Marcá no soy un robot</span>
                  <input id="submit_computer" type="submit" name="submit" value="Enviar" class="btn pull-right btn-modal mt-4">
                </div>

            </div>
          </div>
        </form>
      </div>
      </div>
    </div>
</section>
</div>

<div class="mobile">
  <section class="marine section fp-auto-height" style="padding-top:2rem; padding-bottom:3rem;">
      <div class="container align-items-center justify-content-center text-center">
            <div class="btn-footer mt-3" id="mensaje-modal"></i>ENVIANOS TUS COMENTARIOS</div>
            <div class="footer-txt-m mt-4" style="color:#fff;"><i class="fab fa-whatsapp fa-1x mr-2 fff"></i>+54 9 11 5835-2669</div>
            <div class="footer-txt-m mt-4" style="color:#fff;"><i class="far fa-envelope fa-1x mr-2 fff"></i>info@followme.com.ar</div>
            <div class="footer-title-m mt-4" style="color:#fff;">
              seguinos
            </div>
            <div class="mt-2">
              <a class="footer-link p-0" href="https://www.facebook.com/followmegrads" target="_blank">
                <img data-pin-nopin="false" class="icon-redes filtro" src="{base_url}images/web/facebook.png">
              </a>
              <a class="footer-link p-0" href="https://www.instagram.com/followmegresados/" target="_blank">
                <img data-pin-nopin="false" class="icon-redes ml-2 filtro" src="{base_url}images/web/instagram.png">
              </a>
              <a class="footer-link p-0" href="https://ar.pinterest.com/followmebuzosycamperas/" target="_blank">
                <img data-pin-nopin="false" class="icon-redes ml-2 filtro" src="{base_url}images/web/pinterest.png">
              </a>
              <a class="footer-link p-0" href="https://www.tiktok.com/@followmeegresados" target="_blank">
                <img data-pin-nopin="false" class="icon-redes ml-2 filtro" src="{base_url}images/web/tiktok.png">
              </a>
            </div>
        </div>
  </section>
</div>
  <script src="{base_url}website/assets/js/enviar.js"></script>
