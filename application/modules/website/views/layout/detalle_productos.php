
<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
<section class="page-section pt-0 pb-2" style="margin-bottom:1.5rem;">
    <div class="container " style="margin-top: 6vh;">
    <div style="text-align: right;">

    </div>
    </div>
  <div class="container computer" style="margin-top: 6vh;position: relative;">

    <div style="top:0;position: relative;margin-bottom: 6rem; margin-left:2rem;">
      <a href="{base_url}productos"class="btn-volver">Volver</a>
    </div>
    <div class="row justify-items-center " style="position: relative;">
      <div class="col-1 col-preview">

        <div class="row mb-4" style="position: relative;  height: 6rem; width: 6rem;">
          <img src="{image}" class="preview image_click">
        </div>
        {if {image_back}!=''}
        <div class="row mb-4" style="position: relative; height: 6rem; width: 6rem;">
          <img src="{image_back}" class="preview image_click">
        </div>
        {/if}

        {if {image_right}!=''}
        <div class="row mb-4" style="position: relative; height: 6rem; width: 6rem;">
          <img src="{image_right}" class="preview image_click">
        </div>
        {/if}
        {if {image_left}!=''}
        <div class="row mb-4" style="position: relative; height: 6rem; width: 6rem;">
          <img src="{image_left}" class="preview image_click">
        </div>
        {/if}
      </div>
      <div class="col-8 image_detalle">
          <div class="detalle marco neutral d-flex">
              <!-- <button type="button" class="btn nuevatempo compartir align-self-end ml-auto m-4"><i class="fas fa-plus"></i> -->
          </div>
          <img src="{image}" class="detalle ">
      </div>
      <div class="col-3 col-txt" style="max-width: 25%;">
        <div class="col-title">
          {title}
        </div>
        <div class="col-data">
          {content}
        </div>
        <div class="row" style="margin-top:1rem; margin-left: .7rem;">
          <a target="_blank" href="https://api.whatsapp.com/send?phone=5491158352669&text=Hola%2C%20te%20consulto%20por%20el%20producto%3A%20{title}.%20Link%20del%20producto%3A%20{base_url}{slug}%0A%0AGracias%21"><span class="col-btn mr-3" >Consultar</span></<a>
          <a target="_blank" href="http://www.facebook.com/sharer.php?u={base_url}{slug}" class="fb-xfbml-parse-ignore"><span class="redes-btn"><i class="fab fa-facebook-f mr-3"></i></span></a>
          <a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url={base_url}{slug}" data-pin-custom="true" class="redes-btn" style="position: relative;"><img data-pin-nopin="false" class="mr-3" src="{base_url}website/assets/svg/pinterest-p-brands.png" height="25px" style="cursor:pointer;position:relative;top:-3px;"></a>

        </div>
      </div>
    </div>
  </div>

  <div class="container mobile" style="">
    <div style="margin-top: -2rem;">
      <a href="{base_url}productos"class="btn-volver-mobile">Volver</a>
    </div>
    <div class="container image_detalle pl-2 pr-2" style="position: relative;margin-top: 10vh;">
        <div class="detalle marco-mobile neutral">
        </div>
        <img src="{image}" class="detalle-mobile">
    </div>
    <div class="container mt-3">
      <div class="row col-preview-mobile text-center align-items-center">
        <div class="col-3 p-0">
          <img src="{image}" class="preview-mobile image_click" onclick='return false;'>
        </div>
        {if {image_back}!=''}
        <div class="col-3 p-0">
          <img src="{image_back}" class="preview-mobile image_click" onclick='return false;'>
        </div>
        {/if}
        {if {image_right}!=''}
        <div class="col-3 p-0">
          <img src="{image_right}" class="preview-mobile image_click" onclick='return false;'>
        </div>
        {/if}
        {if {image_left}!=''}
        <div class="col-3 p-0">
          <img src="{image_left}" class="preview-mobile image_click" onclick='return false;'>
        </div>
        {/if}
      </div>
    </div>
      <div class="container  mt-4">
        <div class="col-title-mobile p-0">
          {title}
        </div>
        <div class="col-data-mobile p-0">
          {content}
        </div>
        <div>
          <a href="https://api.whatsapp.com/send?phone=5491158352669&text=Hola%2C%20te%20consulto%20por%20el%20producto%3A%20{title}.%20Link%20del%20producto%3A%20{base_url}{slug}%0A%0AGracias%21"><span class="col-btn-mobile mr-3">Consultar</span></a>
          <a target="_blank" href="http://www.facebook.com/sharer.php?u={base_url}{slug}" class="fb-xfbml-parse-ignore"><span class="redes-btn"><i class="fab fa-facebook-f mr-3"></i></span></a>
          <a data-pin-do="buttonPin" href="https://www.pinterest.com/pin/create/button/?url={base_url}{slug}" data-pin-custom="true" class="redes-btn" style="position: relative;"><img data-pin-nopin="false" class="mr-3" src="{base_url}website/assets/svg/pinterest-p-brands.png" height="25px" style="cursor:pointer;position:relative;top:-3px;"></a>
<!--           <a href=""><span class="redes-btn"><i class="fab fa-instagram mr-3"></i></span></a>
 -->        </div>
      </div>
      <hr class="mt-4">
  </div>
</section>
