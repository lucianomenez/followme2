<!DOCTYPE html>
<html>

<head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <link rel="icon" type="image/png" href="{base_url}images/web/favicon.png">
      <title>Buzos de egresados - Follow Me</title>
      {robots}
      <meta property="og:site_name" content="Followme"/>
      <meta property="og:title" content="{title}"/>
      <meta property="og:description" content="Catálogo"/>
      <meta property="og:url" content="{base_url}{slug}"/>
      <meta property="og:image" content="{card_image}"/>
      <meta property="og:image:width" content="1200"/>
      <meta property="og:image:height" content="627"/>
      <meta property="og:image:alt" content="{title}"/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>

<link rel="stylesheet" type="text/css" href="{base_url}jscript/flipBook/css/flipbook.style.css">
<link rel="stylesheet" type="text/css" href="{base_url}jscript/flipBook/css/font-awesome.css">



{ignore}
<script type="text/javascript">

    $(document).ready(function () {
        $("#container").flipBook({
            pdfUrl:"{catalogo}",
        });

    })
</script>
<script>
    $(document).on('contextmenu', function() {
        return false;
    });
</script>
{/ignore}

</head>

<body>
<input type="hidden" id="base_url" value="{base_url}">
<div id="container">
</div>

<script src="{base_url}jscript/flipBook/js/flipbook.min.js?5"></script>
</body>

</html>
