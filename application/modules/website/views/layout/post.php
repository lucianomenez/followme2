<main>
  <section class="" style="margin-top: 5rem;">
    <div class="container">
      <div class="row">
        <div class="col-md-8 offset-md-2">
          <h1>{title}</h1>
          <p>{extract}</p>
        </div>
      </div>
    </div>
  </section>

  {blocks}

  <h1>OGETE</h1>


</main>

    <script>
        $("#share").jsSocials({
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        });
    </script>

<div id="share"></div>
