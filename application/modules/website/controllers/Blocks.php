<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Blocks extends MX_Controller {

  function __construct(){
    parent::__construct();
    $this->load->model('user');
    $this->load->model('app');
    $this->load->config('config');
    $this->load->library('parser');
    $this->load->model('cms/Model_node');

  }


    function colegios($block = array()){

    $args['post_status'] = 'published';
    $args['post_type'] = 'colegios';
    $args['type'] = 'activo';


    $block['data']['colegios'] = $this->Model_node->get($args,12);

    if (! $block['data']['colegios'] ) {
      $block['data']['colegios'] =array();
    }


    return $block;
  }

  function sucursales($block = array()){

    $args['post_status'] = 'published';
    $args['post_type'] = 'sucursales';
    $args['type'] = 'activo';

    $block['data']['sucursales'] = $this->Model_node->get($args, 40);

    if (! $block['data']['sucursales'] ) {
      $block['data']['sucursales'] =array();
    }else if(is_array($block['data']['sucursales'] )){
      foreach ($block['data']['sucursales']  as $key => &$value) {
        $value['como_llegar']='';
        $value['sucursal_whatsapp']='';
        if ($value['address']!=''||$value['phone']!='') {
          if ($value['address']!='') {
            $value['address']='<div><i class="fas fa-map-marker-alt fa-lg"></i><div class="txt-sucu bold mt-2">'.$value['address'].'</div></div>';
          }
          if ($value['place']!='') {
            $value['como_llegar']='<div class="col-6 pl-1 pr-1" style="margin:0 auto;"><a href="'.$value['place'].'" class="btn btn-sucu">CÓMO LLEGAR</a></div>';
          }

          if ($value['phone']!='') {
            $value['phone']='<div><i class="fas fa-mobile-alt fa-lg mt-4"></i><div class="txt-sucu mt-2">'.$value['phone'].'</div> </div>';
          }
          
          if ($value['whatsapp']!='') {

            $value['sucursal_whatsapp']='<div class="col-6 pl-1 pr-1"><a href="https://api.whatsapp.com/send?phone='.$value['whatsapp'].'" class="btn btn-sucu">WHATSAPPEANOS</a></div>';
          }
         $value['txt_alternativo']='';
        }else{
           $value['txt_alternativo']= '<div><div class="txt-sucu mt-2">'. $value['txt_alternativo'].'</div> </div>';
        }
      }
    }

    return $block;
  }


  function navbar($block = array()){

      $args2['post_status'] = 'published';
      $args2['post_type'] = 'options';
      $args2['type'] = 'categorias';

      $block['data']['items'] = $this->Model_node->get($args2, 1)[0]['items'];

      if (! $block['data']['items'] ) {
        $block['data']['items'] =array();
      }else{
        if (is_array($block['data']['items'])) {
          $index_item=0;
          foreach ($block['data']['items']as $key => &$value) {
            $value['index_item']=$index_item;
            $index_item++;
          }
        }
      }

    return $block;
  }


  function hero($block = array()){

        $block['base_url'] = $this->base_url;
        $block['module_url'] = $this->module_url;
    return $block;
  }

  function galeria_imagenes($block = array()){

    switch ($block['data']['columns']) {
      case '1':
        $block['data']['col'] = "col-md-12";
        break;
      case '2':
        $block['data']['col'] = "col-md-6";
        break;

      case '3':
        $block['data']['col'] = "col-md-4";
        break;

      case '4':
        $block['data']['col'] = "col-md-3";
        break;
      default:
          $block['data']['col'] = "col-md-6";
        break;
    }

    return $block;
  }

  function libros_slider($block = array()){

    $args['post_type'] = 'libro';
    if ($block['data']['genero']!='') {
      $args['genero'] = $block['data']['genero'];
    }
    if ($block['data']['duracion']!='') {


      switch ($block['data']['duracion']) {
          case 'corta':
              $args['duracion'] = $block['data']['duracion'];
              break;
          case 'media':
              $args['duracion'] = $block['data']['duracion'];
              break;
          case 'larga':
              $args['duracion'] = $block['data']['duracion'];
              break;
      }
    }    
    $args['post_status'] = 'published';

    $block['data']['libros'] = $this->Model_node->get($args);

    return $block;
  }

  function publicaciones_slider($block = array()){

    $args['post_type'] = 'publicacion';
    if ($block['data']['categoria']!='') {
      $args['categoria'] = $block['data']['categoria'];
    }
    
    $args['post_status'] = 'published';

    $block['data']['publicaciones'] = $this->Model_node->get($args);

    return $block;
  }

  function posts_lista($block = array()) {

    $args['post_type'] = $block['data']['post_type'];
    $args['post_status'] = 'published';
    if ($block['data']['limit'] == '') {
      $block['data']['limit'] = 4;
    }

    $block['data']['posts'] = $this->Model_node->get($args, $block['data']['limit']);

    return $block;
  }

  function posts_slider($block = array()){

    $args['post_type'] = $block['data']['post_type'];
    $args['post_status'] = 'published';
    if ($block['data']['limit'] == '') {
      $block['data']['limit'] = 4;
    }

    // la búsqueda por tags está funcionando solo con el primero
    if (is_array($block['data']['tags'])) {
      $key = $block['data']['tags'][0]['key'];
      $args['tags'] = array(
        '$elemMatch' => array('key' => new MongoRegex("/$key/i")),
      );


    }

    $block['data']['posts'] = $this->Model_node->get($args, $block['data']['limit']);

    if (!isset($block['data']['view_all_text'])) {
      $block['data']['view_all_text'] = '';
    }


    return $block;
  }


}
