<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Website
 *
 * Description of the class
 *
 * @author Héctor Gabriel Alegre <gabriel.h.alegre@gmail.com>
 */
class Website extends MX_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('user');

			$this->load->config('config');
      $this->load->library('parser');
      $this->load->model('cms/Model_node');
      $this->load->module('website/blocks');
      $this->load->module('website/layouts');
			//---base variables
			$this->base_url = base_url();
			$this->module_url = base_url() . $this->router->fetch_module() . '/';
			$this->lang->load('user/profile', $this->config->item('language'));
			$this->idu = (double) $this->session->userdata('iduser');
		  $this->user_data = $this->user->get_user($this->idu);

      error_reporting(E_ERROR | E_PARSE);

		}

    function index(){

      $params = array('slug' => $this->uri->uri_string);
      if ($page = $this->Model_node->get_one($params)) {

        $page['base_url'] = $this->base_url;
        $page['module_url'] = $this->module_url;
        $page['isloggedin'] = $this->isloggedin();
        $page['name'] = $this->user_data->name;
        $page['lastname'] = $this->user_data->lastname;
        $page['email'] = $this->user_data->email;
        $page['rand']=rand();
        $page['div_separator']='';
        $page['robots']='';
        
         if( $page['post_type']=='catalogos'){
              if ($page['privacidad']=='privado') {

                  $this->user->authorize();
              }
              $page['robots']='<meta name="robots" content="noindex, nofollow">';
               $this->parser->parse('layout/detalle_catalogos', $page);
                  
         }else{
           $this->header($page);
           if ($page['header_footer']!='no') {
            if ($page['post_type']=='bases_condiciones') {
              $page['div_separator']='<div class="container" style="height:15vh; background:#fff;"></div>';
              $page['content']='Bases y condiciones';
              $page['image']='https://followme.com.ar/images/web/follow-me-logo-blue.png';
            }
                $this->navbar($page);
            }
         }


        // si tiene layout
        if (isset($page['layout']) && $page['layout']!='') {

          if ( method_exists('layouts', $page['layout']['name']) ) {

            // manipulo la data de la página
            $page = Modules::run( 'layouts/'.$page['layout']['name'], $page );

          }

          $this->blocks($page, true);

          // renderizo el layout

          if (!isset($page['layout']['name'])){

            $this->parser->parse('layout/'.$page['layout'], $page);

          }else{
          $this->parser->parse('layout/'.$page['layout']['name'], $page);

          }

        } else {

          $this->blocks($page);

        }

         if (isset($page['post_type']) && $page['post_type']=='productos') {
            $this->detalle_productos($page);
            $this->footer($page);
            $this->scripts($page);

         }else if (isset($page['post_type']) && $page['post_type']=='colegios') {
            $this->detalle_colegios($page);

            $this->footer($page);
            $this->scripts($page);
         }else if ($page['post_type']=='page' || $page['post_type']=='bases_condiciones') {
            if ($page['slug']!=''&&$page['header_footer']!='no') {
             $this->footer($page);
            }
            if ($page['header_footer']!='no') {
              $this->scripts($page);
            }

         }
            $this->end($page);


      } else {

        // muestro 404
        set_status_header(404);
        $page['div_separator']='';
        $page['base_url'] = $this->base_url;
        $page['module_url'] = $this->module_url;
        $page['isloggedin'] = $this->isloggedin();
        $page['name'] = $this->user_data->name;
        $page['lastname'] = $this->user_data->lastname;
        $page['email'] = $this->user_data->email;
        $page['title'] = 'Página no encontrada';

        $this->header($page);
        $this->navbar($page);

        $this->parser->parse('layout/404', $page);

        $this->footer($page);

      }
    }

    function internas(){

      $params = array('slug' => $this->uri->uri_string);
      if ($page = $this->Model_node->get_one($params)) {
        switch ($page['slug']) {
          case 'design-room':
              header("Location: ".   $this->base_url ."followme/demo");
              exit();
            break;
          case 'user-login':
              header("Location: ".   $this->base_url ."user/login");
              exit();
            break;
        }


      }
    }


    private function detalle_productos($page)
    {
      $block['base_url'] = $this->base_url;
      $block['module_url'] = $this->module_url;
      $args['post_status'] = 'published';
      $args['post_type'] = 'options';
      $args['type'] = 'categorias';

      $block['items'] = $this->Model_node->get($args, 1)[0]['items'];

      if (! $block['items'] ) {
        $block['items'] =array();
      }
      $args_hero['post_type']='page';
      $args_hero['post_status'] = 'published';
      $args_hero['slug'] = 'productos';
      $hero = $this->Model_node->get($args_hero, 1)[0]['blocks'];
      $data_hero='';
      if (is_array($hero)) {
       foreach ($hero as $key_hero => $value_hero) {
         if ($value_hero['type']=='hero') {
          $data_hero=$value_hero['data'];
         }
       }
      }

      $this->parser->parse('blocks/hero', $data_hero);


      $this->parser->parse('layout/detalle_productos', $page);

      $args2['post_type']='productos';
      $args2['post_status'] = 'published';
      $args2['categoria'] = $page['categoria'];
      $relacionados['base_url'] = $this->base_url;
      $relacionados['module_url'] = $this->module_url;
      $relacionados['productos'] = $this->Model_node->get($args2, 5);

      $key_delete ='null';
      if (is_array($relacionados['productos'])) {

        foreach ($relacionados['productos'] as $key_producto => &$value_producto) {
          if ($value_producto['_id']==$page['_id']) {
            $key_delete=$key_producto;
          }
        }

        if (  $key_delete !='null') {
            if ($key_delete == 0) {

                unset($relacionados['productos'][0]);
            } elseif ($key_delete) {
                array_splice($relacionados['productos'], $key_delete, 1);
            }
         }else{
           array_splice($relacionados['productos'], 4, 1);
         }
        $relacionados['productos']=array_values($relacionados['productos']);

      }

      $this->parser->parse('blocks/relacionados', $relacionados);
    }

    function get_colegios(){

      $args['post_status'] = 'published';
      $args['post_type'] = 'colegios';
      $args['type'] = 'activo';
      $args['search']=$this->input->post('query');
      $limit=(int)$this->input->post('limit');

      $result['colegios'] = $this->Model_node->search($args, 12);
      $result['base_url'] = $this->base_url;

      if (!$result['colegios']) {
         echo '';
      }else{
          $this->parser->parse('items-colegios-desktop', $result);
      }


    }

    private function detalle_colegios($page)
    {
      $block['base_url'] = $this->base_url;
      $block['module_url'] = $this->module_url;

      if (! $block['items'] ) {
        $block['items'] =array();
      }
      $args_hero['post_type']='page';
      $args_hero['post_status'] = 'published';
      $args_hero['slug'] = 'colegios';
      $hero = $this->Model_node->get($args_hero, 1)[0]['blocks'];
      $data_hero='';

      if (is_array($hero)) {
       foreach ($hero as $key_hero => $value_hero) {
         if ($value_hero['type']=='hero') {
          $data_hero=$value_hero['data'];
         }
       }
      }

      $this->parser->parse('blocks/hero', $data_hero);


      $this->parser->parse('layout/detalle_colegios', $page);


    }


    private function header($vars)
    {
        $vars['body_classes'] = join(' ', array_filter(
          array(
            $vars['class'],$vars['post_type'],$vars['beneficio']
          )
        ));

       if ($vars['post_type']=='productos') {
         $vars['meta_description']=strip_tags ( $vars['content'] ) ;

         $vars['robots']='<link rel="canonical" href="'.$vars['base_url'].'productos" />';
       }

       if ($vars['post_type']=='colegios') {
         $vars['meta_description']='Colegios: '.$vars['title'].' '.$vars['ano'].' '.$vars['city'];
         $vars['robots']='<link rel="canonical" href="'.$vars['base_url'].'colegios" />';
       }

      return $this->parser->parse('layout/header', $vars);
    }


    private function footer($vars)
    {

      return $this->parser->parse('layout/footer', $vars, false, false);
    }

        private function end($vars)
    {

      return $this->parser->parse('layout/end', $vars, false, false);
    }

        private function scripts($vars)
    {
      $vars['login'] = $this->isloggedin();
      return $this->parser->parse('layout/scripts', $vars, false, false);
    }


    private function navbar($vars)
    {
      $vars['login'] = $this->isloggedin();
      $vars['base_url'] = $this->base_url;
      $vars['module_url'] = $this->module_url;
      return $this->parser->parse('layout/menu-loggedin', $vars);
    }


    private function blocks(&$page, $echo = false){
      if (!empty($page['blocks'])) {

        $blocks = '';

        foreach ($page['blocks'] as $key => &$block) {
        if ($block['type']=='home') {
            $block['data']['rand']=rand();
          $block['data']['alianzas']=$page['alianzas'];
          $block['data']['caminos']=$page['caminos'];
          if (is_array($block['data']['items'])) {
            $last_item=end(array_keys($block['data']['items'] ));
            foreach ($block['data']['items'] as $key_item => &$value_item) {
              $value_item['guion']=' - ';
              if ($last_item==$key_item) {
                $value_item['guion']='';
              }
            }
          }

        }
            $block['data']['slug']=$page['slug'];
          $block['data']['block_index'] = $key;

          if ($block['enabled'] == true) {

            $block['data']['base_url'] = $this->base_url;
            $block['data']['module_url'] = $this->module_url;

            //hookeo los bloques con el controller blocks
            if ( method_exists('blocks', $block['type']) ) {

              // manipulo la data del bloque
              $block = Modules::run( 'blocks/'.$block['type'], $block );

            }

            $block['data']['isloggedin'] = $this->isloggedin();
            if (file_exists(APPPATH.'modules/website/views/blocks/'.$block['type'].'.php')) {
              $blocks .= $this->parser->parse('blocks/'.$block['type'], $block['data'], $echo);
            }

          }
        }

        $page['blocks'] = $blocks;
      }

      return $page;
    }


    private function isloggedin()
    {
        if (!$this->session->userdata('loggedin')) {
            $this->session->userdata('loggedin',false);
            return false;
        } else {
            return true;
        }
    }



    public function error_404()
    {
      $vars['base_url'] = $this->base_url;
      $vars['module_url'] = $this->module_url;
      //$vars['menu'] = $this->menuPrincipal(($vars['color_logo'])? $vars['color_logo'] : 'blanco');
      $vars['title'] = $page['title'];
      //$vars['newsletter'] = $this->load->view('layout/newsletter', NULL, TRUE);
      $this->load->view('layout/header', $vars);
      $this->load->view('layout/404', $vars);
      $this->load->view('layout/footer', $vars);
    }

    public function get_posts($post_type = 'colegios', $count = 12, $offset = 0,$categoria=''){

      $args['post_type'] = $post_type;
      $args['type'] = 'activo';
      $args['post_status'] = 'published';

      $data['count'] = $count;
      $data['offset'] = $offset;
      $data['base_url'] = $this->base_url;

      if ($post_type=='productos') {
        if ($categoria!='') {
         $args2 = $categoria;
         $data['productos'] = $this->Model_node->get_productos($args,$args2, $count, $offset);

        }else{
          $data['productos'] = $this->Model_node->get_node($args, $count, $offset);
        }


        if (!$data['productos']) {
          return false;
        }

        foreach ($data['productos'] as &$producto) {
          $producto['post_date'] = date('d/m/Y', $producto['post_date']->sec );
        }

        $this->parser->parse('data_productos_html', $data);
      }
      if ($post_type=='colegios') {

        $data['colegios'] = $this->Model_node->get($args, $count, $offset);

        if (!$data['colegios']) {
          return false;
        }

        foreach ($data['colegios'] as &$colegio) {
          $colegio['post_date'] = date('d/m/Y', $colegio['post_date']->sec );
        }

        $this->parser->parse('items-colegios-desktop', $data);
      }

    }


  function get_productos($data_categoria='null',$data_count=12,$data_offset=0){

    $categoria=$this->input->post('categoria');
    if (!$categoria) {
     $categoria=$data_categoria;
    }
    $count=$this->input->post('count');
    if (!$count) {
     $count=(int)$data_count;
    }
    $offset=$this->input->post('offset');
    if (!$offset) {
     $offset=(int)$data_offset;
    }
    $args['type'] = 'activo';
    $args['post_type'] = 'productos';
    $args['post_status'] = 'published';
    $args2 = $categoria;
    $data['productos'] = $this->Model_node->get_productos($args,$args2, $count, $offset);
       $data['base_url'] = $this->base_url;
    if (!$data['productos']) {
       echo '';
    }else{
        $this->parser->parse('data_productos_html', $data);
    }

  }

  function get_paletas(){

    $categoria=$this->input->post('categoria');
    //$name=$this->input->post('name');
    // $post_type=$this->input->post('post_type');
    // $args['post_type'] = $post_type;
    // $args['post_status'] = 'published';
    // $args2 = $categoria;
    // $paletas['data'] = $this->Model_node->get_productos($args,$args2, 999, 0);
        $data['categorias_paleta']  =array();
        $data['categorias_paleta'][0]=array(
            'text'=>'Camperas y buzos',
            'value'=>'camperas_buzos',
            'items'=>array('Frisa','Jersey')
        );

        $data['categorias_paleta'][1]=array(
            'text'=>'Remeras y chombas',
            'value'=>'remeras_chombas',
            'items'=>array('Pique')
        );

        $data['categorias_paleta'][2]=array(
            'text'=>'Pelito y peluche',
            'value'=>'pelito_peluche'
        );

        $paletas['data']=array();
        $array_paletas_exists=array();
        $array_paletas=array();


        if($categoria=='pelito_peluche'){
        $disenos = file_get_contents( $this->base_url."followme/assets/json/examples/disenos.json");

        $disenos = json_decode($disenos, true);

        if(is_array($disenos)){
            foreach ($disenos as $key_disenos => $value_disenos) {
                if(is_array($value_disenos['designs'])){
                    foreach ($value_disenos['designs'] as $key_designs => $value_designs) {
                        $is_peluche=strpos($value_designs['title'], 'eluche') !==false;
                        $is_corderito=strpos($value_designs['title'], 'orderito') !==false;
                        if($is_peluche||$is_corderito){
                            $new_item=array();
                            $new_item['image']= $this->base_url."followme/".$value_designs['thumbnail'];
                            $new_item['colorName']=$value_designs['title'];
                            $new_item['categoria_paleta_name']='Pelito y peluche';
                            $array_paletas[]= $new_item;
                        }
                        
                    }
                }
            }
        }
        $paletas['data'] = $array_paletas;

        }else{
          if(is_array( $data['categorias_paleta'])){
            foreach ( $data['categorias_paleta'] as $key_paleta => $value_paleta) {
              if($value_paleta['value']==$categoria){
                if(is_array( $value_paleta['items'])){
                    foreach ( $value_paleta['items'] as $key_item => $value_item) {
                        $args=array();
                        $args['category']=$value_item;
                        $result= $this->Model_node->get_paleta($args, 999, 0);
                        if(is_array($result)){
                            foreach ($result as $key_result => $value_result) {
                                $name = explode(" - ", $value_result['colorName']);
                                $value_result['colorName']=$name[1];
                                if(!in_array($value_result['colorName'],$array_paletas_exists)){
                                    $array_paletas_exists[]=$value_result['colorName'];
                                    $value_result['categoria_paleta_name']= $value_paleta['text'];
                                    $array_paletas[]= $value_result;

                                }
                            }
                        }
                    }
                    $paletas['data'] = $array_paletas;
                }
                break;
              }
            }
        }


        }


    if (! $paletas['data']) {
       echo '';
    }else{
       //$paletas['categoria_name']=$name;
        echo $this->parser->parse('data_paletas_html', $paletas,true,true);
    }

  }


  function test(){
    $this->load->model('msg');
    $msg=array(
              'subject'=>'Consulta',
              'body'=>"<h1>Testing</h1>",
              'name'=> "Luciano",
              'from'=>"lucianomenez1212@gmail.com",
               'debug'=>true,
                            );


    $user = new stdClass();
    $user->email = "info@followme.com.ar";

    $this->msg->send_mail_old($msg,$user);

  }


  function send_email(){
      $this->load->model('msg');
      $data=$this->input->post();

        $user_email=new stdClass();
        $user_email->email='info@followme.com.ar';
        $body= "<h3>Colegio:</h3> ".$data['colegios'].'<br>'.
                "<h4>Nivel:</h4> ".$data['nivel'].'<br><br>'.
                "<b>Promoción:</b> ".$data['promocion'].'<br><br>'.
                "<b>Localidad:</b> ".$data['localidad'].'<br>'.
                "<b>Teléfono:</b> ".$data['telefono'].'<br>'.
                "<b>Email:</b> ".$data['email'].'<br><br><br>'.
                "<b><u>CONSULTA:</u></b> ".$data['consulta'].'<br><br>'.
                 "<i>" .nl2br($data['consulta'])."</i>";
        $msg=array(
                                'subject'=>'Consulta ('.$data['colegios'].')',
                                'body'=>$body,
                                'name'=>$data['nombre'],
                                'from'=>$data['email'],
                                'debug'=>true,
                                );


       $this->msg->send_mail_old($msg,$user_email,'true');
  }
}
