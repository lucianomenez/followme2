$(document).ready(function(){

    // Función para obtener el nombre del rol a partir del grupo
    function obtenerRolPorGrupo(grupo) {
        // Buscar el rol con el grupo dado en el array de roles
        var rolEncontrado = $.grep(roles, function(rol) {
            return rol.grupo === parseInt(grupo);
        });

        // Si se encontró un rol con el grupo dado, retornar su nombre
        if (rolEncontrado.length > 0) {
            return rolEncontrado[0].nombre;
        } else {
            // Si no se encontró ningún rol con el grupo dado, retornar null o un mensaje indicando que no se encontró
            return null; // O podrías retornar un mensaje como "Rol no encontrado"
        }
    }

    var roles = [
        { nombre: "Empleado", grupo: 1000 },
        { nombre: "Administrador", grupo: 500 }
    ];

    // Selecciona el elemento select por su ID
    var select = $('#input_add_rol');

    // Recorre el array de roles y añade cada uno como opción al select
    $.each(roles, function(index, value) {
        select.append($('<option>', {
            value: value.grupo,
            text: value.nombre
        }));
    });

    var base_url = $("#base_url").val();

    $('#btn_agregar_usuario').on('click', function(e){
        e.preventDefault();
        $('#addModalUser').modal({inverted: true}).modal('show');
    });
      
    $("#agregar_usuario").on("click", function(e){
        var usuario = $("#input_add_nickName").val();
        var pass = $("#input_add_pass").val();
        var repass = $("#input_add_repass").val();
        var nombre = $("#input_add_name").val();
        var apellido = $("#input_add_surname").val();
        var grupo = $("#input_add_rol").val();
        var rol = obtenerRolPorGrupo($("#input_add_rol").val());
        var email = $("#input_add_email").val();

        if(pass !== repass){
            alert("Las contraseñas no coinciden");
            return;
        }
      
        $.ajax({
            url :  base_url + 'usuarios/add_user',
            type: 'POST',
                data: {
                    nick: usuario,
                    passw: pass,
                    name: nombre,
                    lastname: apellido,
                    rol: rol,
                    group: grupo,
                    email: email
                },
            success: function(response){
                alert ("Usuario agregado!");
                $("#input_add_nickName").val("");
                $("#input_add_pass").val("");
                $("#input_add_repass").val("");
                $("#input_add_name").val("");
                $("#input_add_surname").val("");
                $("#input_add_rol").val("");
                $("#input_add_email").val("");
                location.reload();
            },
            error: function(response){
              alert ("Ups. Error en el agregado")
            }
        })
    });

    $(".zmdi-eye").on('click', function(event){
        $('#modal-vista').modal('show');
    });

    $(".zmdi-edit").on('click', function(e){
        e.preventDefault();
        var id = $(this).attr("data-id");
        $.ajax({
            url : base_url + 'usuarios/get_user',
            type: 'POST',
            data: {
                id : id
            },
            success: function(data){

                response = JSON.parse(data);

                // Valor seleccionado que has traído desde algún lugar
                var valorSeleccionado = parseInt(response.idgroup);

                // Selecciona el elemento select por su ID
                var select = $('#input_rol');

                // Recorre el array de roles y añade cada uno como opción al select
                $.each(roles, function(index, value) {
                    // Crea una opción para cada rol
                    var option = $('<option>', {
                        value: value.grupo,
                        text: value.nombre
                    });

                    // Si el valor coincide con el valor seleccionado, marca la opción como seleccionada
                    if (value.grupo === valorSeleccionado) {
                        option.prop('selected', true);
                    }

                    // Agrega la opción al select
                    select.append(option);
                });

                console.log(response);
                $("#input_id").val(response._id.$id);
                $("#input_nickName").val(response.nick);
                $("#input_name").val(response.name);
                $("#input_surname").val(response.lastname);
                $("#input_email").val(response.email);
            }
        });
        $('#editModalUser').modal({inverted: true}).modal('show');
    });

    $("#editar_usuario").on("click", function(e){
        var id = $("#input_id").val();
        var usuario =  $("#input_nickName").val();
        var pass = $("#input_pass").val();
        var repass = $("#input_repass").val();
        var nombre = $("#input_name").val();
        var apellido =  $("#input_surname").val();
        var grupo = $("#input_rol").val();
        var rol = obtenerRolPorGrupo($("#input_rol").val());
        var email =  $("#input_email").val();

        if(pass !== repass){
            alert("Las contraseñas no coinciden");
            return;
        }

        $.ajax({
            url :  base_url + 'usuarios/update_user',
            type: 'POST',
                data: {
                    id: id,
                    nick: usuario,
                    passw: pass,
                    name: nombre,
                    lastname: apellido,
                    rol: rol,
                    group: grupo,
                    email: email
                },
            success: function(response){
                console.log(response);
                alert ("Cambio Guardado!");
                location.reload();
            },
            error: function(response){
                alert ("Ups. Error en el guardado")
            }
        })
    });

    $(".zmdi-delete").on('click', function(e){
        e.preventDefault();
        var id = $(this).attr("data-id");

        $('#deleteModalUser').modal({inverted: true}).modal('show');

        $('#borrar_usuario').on('click', function(e){
            $.ajax({
                url :  base_url + 'usuarios/delete_user',
                type: 'POST',
                    data: {
                    id : id
                    },
                success: function(response){
                    alert ("Usuario Eliminado con éxito!")
                    location.reload();
                },
                error: function(response){
                    alert ("Ups. Error en el borrado.")
                }
            })
        })
    });

}); 
