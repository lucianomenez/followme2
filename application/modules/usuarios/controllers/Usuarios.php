<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * dna2
 *
 * Description of the class dna2
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Usuarios extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');
        $this->load->model('Model_usuarios');
        $this->load->helper('file');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->module('elements');
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->titulo = "Follow Me - Diseños Personalizados";
        }



    function Index() {

      $user = $this->user->get_user($this->idu);
      $data['base_url'] = $this->base_url;
      $data['module_url'] = $this->module_url;
      $data['name'] = $user->name;
      $data['lastname'] = $user->lastname;
      $data['email'] = $user->email;
      $data['head'] = $this->elements->ui_head();
      $data['header'] = $this->elements->ui_header();
      $data['navbar'] = $this->elements->ui_navbar();
      $data['footer'] = $this->elements->ui_footer();
      $data['navbar_mobile'] = $this->elements->ui_navbar_mobile();
      $data['usuarios'] = $this->Model_usuarios->get_usuarios();
      return $this->parser->parse("usuarios", $data);
    }

    function navbar(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        return $this->parser->parse("navbar", $data);
    }

     function ui_header(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['title'] = $this->titulo;
        return $this->parser->parse("header", $data);
    }

     function ui_footer(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['title'] = $this->titulo;
        return $this->parser->parse("footer", $data);
    }

    function navbar_mobile(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        return $this->parser->parse("navbar_mobile", $data);
    }

    function reload_tabla(){
      $data = $this->input->post();
      if ($data["ciudad"] != "Ciudad"){
        $query['city_id'] = $data["ciudad"];
      };
      if ($data["sucursal"] != "Sucursal"){
        $query['subsidiary_id'] = $data["sucursal"];
      };
      if ($data["provincia"] != "Provincia"){
        $query['province_id'] = $data["provincia"];
      };
      if ($data["partido"] != "Partido"){
        $query['department_id'] = $data["partido"]; 
      };

      $data['usuarios'] = $this->Model_usuarios->get_usuarios_x_query($query);

      foreach ($data["usuarios"] as &$colegio){
        $ciudad = $this->Model_usuarios->get_ciudades($colegio["city_id"]);
        $colegio["ciudad"] = $ciudad[0]['name'];

        $departamento = $this->Model_usuarios->get_departamentos($ciudad["departament_id"]);
        $colegio["departamento"] = $departamento[0]['name'];

        $provincia = $this->Model_usuarios->get_provincias($departamento["province_id"]);
        $colegio["provincia"] = $provincia[0]['name'];

        $sucursal = $this->Model_usuarios->get_sucursal($colegio["subsidiary_id"]);
        $colegio["sucursal"] = $sucursal[0]['name'];
      }


        echo $this->parser->parse("reloaded_table", $data);

    }

    function add_user(){
      $user_data = $this->input->post();
        //$data = $this->input->post();
        //$salt = bin2hex(random_bytes(16));
        //$hashed_password = password_hash($data['pass'] . $salt, PASSWORD_BCRYPT);
        //$hashed_password = password_hash($data['pass'], PASSWORD_DEFAULT);
        //$data['passw'] = $hashed_password;
        //$this->Model_usuarios->insert_usuarios($data);
        //return;
        $user_data['idgroup'] = $user_data['group'];
        $user_data['group'] = array($user_data['group']);
        $user_data['perm'] = array('ADM');
        // se envia el rol en $user_data['rol']
        //if (!is_array($user_data['group']))
            //$user_data['group'] = array_map('intval', explode(',', $user_data['group']));
            //$user_data['group'] = array(500);
        $user_data['idu'] = isset($user_data['idu']) ? $user_data['idu'] : null;
        if ($user_data['idu']) {
            //---set proper typo 4 id
            $user_data['idu'] = (int) $user_data['idu'];
            //---if found then update data
            $user = (array) $this->getbyid($user_data['idu']);
            //---add previous data not submited _id & iduser
            $user_data+=$user;
            //---Preserves password if not set, else make a hash
            $user_data['passw'] = ($user_data['passw'] == '') ? $user['passw'] : md5($user_data['passw']);

            $result = $this->Model_usuarios->insert_usuarios($user_data);
            //var_dump($result);
        } else {
            $user_data['idu'] = $this->genid();
            //---hash that password down
            $user_data['passw'] = $this->hash($user_data['passw']);
            $result = $this->Model_usuarios->insert_usuarios($user_data);
        }

        $user = $user_data;
        return $user;

    }

    function hash($str) {
      return password_hash($str, PASSWORD_DEFAULT);
    }

    function genid() {
      $insert = array();
      $trys = 10;
      $i = 0;
      $id = mt_rand();
      $container = 'users';
      //---if passed specific id
      if (func_num_args() > 0) {
          $id = (int) func_get_arg(0);
          $passed = true;
          //echo "passed: $id<br>";
      }
      $hasone = false;

      while (!$hasone and $i <= $trys) {//---search until found or $trys iterations
          //while (!$hasone) {//---search until found or 1000 iterations
          $query = array('idu' => $id);
          $result = $this->db->where($query)->count_all_results($container);
          $i++;
          if ($result) {
              if ($passed) {
                  show_error("id:$id already Exists in $container");
                  break;
              }
              $hasone = false;
              $id = mt_rand();
          } else {
              $hasone = true;
          }
      }
      if (!$hasone) {//-----cant allocate free id
          show_error("Can't allocate an id in $container after $trys attempts");
      }
      return $id;
  }

    function get_user(){
      $data = $this->input->post();
      $user = $this->Model_usuarios->get_usuarios($data['id']);
      echo json_encode($user[0]);
    }

    function update_user() {
        $data = $this->input->post();
        if(isset($data['id'])) {
            $data['id'] = $data['id'];
        }
        $data['passw'] = $this->hash($data['passw']);
        $data['idgroup'] = $data['group'];
        $data['group'] = array($data['group']);
        $data['perm'] = array('ADM');
        $this->Model_usuarios->update_usuarios($data);
        return;
    }

    function delete_user(){
        $data = $this->input->post();
        if(isset($data['id'])){
            $data['id'] = $data['id'];
        }
        $result = $this->Model_usuarios->delete_usuarios($data['id']);
        echo json_encode($result);
    }


}

/* End of file dna2 */
/* Location: ./system/application/controllers/welcome.php */
