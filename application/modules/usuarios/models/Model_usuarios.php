<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_usuarios extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
    */

    function insert_usuarios($data){
        $container = 'users';
        $this->db->insert($container, $data);
        return;
    }

    function get_usuarios($id = null){
        $result = array();
        $container = 'users';
        if ($id != null){
            $id = new \MongoDB\BSON\ObjectId($id);
            $query['_id'] = $id;
        }
        $this->db->where($query);
        $this->db->limit(100); 
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function update_usuarios($data){
        $result = array();
        $container = 'users';
        $id = new \MongoDB\BSON\ObjectId($data['id']);
        $query = array('_id' => ($id));
        $this->db->where($query);
        $result = $this->db->update($container, $data);
        var_dump($result);
        die();
        return $result;
      }

    function delete_usuarios($id){
        $result = array();
        $container = 'users';
        $id = new \MongoDB\BSON\ObjectId($id);
        $query = array('_id' => ($id));
        $this->db->where($query);
        $result = $this->db->delete($container);
        return $result;
    }
}
