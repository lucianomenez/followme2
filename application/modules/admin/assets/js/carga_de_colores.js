
var base_url = $("#base_url").val();

$('.zmdi-delete').on('click', function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");

    $('#deleteModal').modal({inverted: true}).modal('show');

    $('#borrar_color').on('click', function(e){
      $.ajax({
        url :  base_url + 'admin/carga_de_colores/borrar_color',
        type: 'POST',
            data: {
              id : id
            },
        success: function(response){
          alert ("Fuente Borrada con éxito!")
          location.reload();
        },
        error: function(response){
          alert ("Ups. Error en el borrado.")
        }
      })
    })
});


$('.zmdi-edit').on('click', function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");
    $.ajax({
      url : base_url + 'admin/carga_de_colores/get_color',
      type: 'POST',
      data: {
        id : id
      },
      success: function(data){
        response = JSON.parse(data);
        console.log(response);
        $("#input_colorName").val(response.colorName);
        $("#input_colorHex").val(response.colorHex);
        $("#input_number").val(response.number);
        $("#category").val(response.category);
      }
    });

    $('#editModal').modal({inverted: true}).modal('show');
    
  });


$("#editar_color").on("click", function(e){
    var colorHex =  $("#input_colorHex").val();
    var colorName = $("#input_colorName").val();
    var number =  $("#input_number").val();
    var category =  $("#category").val();

      $.ajax({
        url :  base_url + 'admin/carga_de_colores/update_color',
        type: 'POST',
            data: {
              colorHex : colorHex,
              colorName: colorName,
              number: number,
              category: category
            },
        success: function(response){
          alert ("Cambio Guardado!");
          location.reload();
        },
        error: function(response){
          alert ("Ups. Error en el guardado")
        }
      })
});

$('.btn-agregar').on('click', function(e){
  e.preventDefault();
  $('#addModal').modal({inverted: true}).modal('show');
});

$("#agregar_color").on("click", function(e){
  var colorName = $("#input_add_colorName").val();
  var number =  $("#input_add_number").val();
  var category =  $("#category_add").val();
  //var colorHexOriginal;
  var colorHex;
  /* var colorHexADecimal;
  var colorHexADecimalRestado; */

  colorHex =  $("#input_add_colorHex").val();

  /* switch(category){
    case "Frisa": // Color Hex original
      colorHex =  $("#input_add_colorHex").val();
    break;
    case "Jersey": // Color Hex original - 1
      colorHexOriginal = $("#input_add_colorHex").val();
      if(colorHexOriginal[0] === "#"){
        colorHex = colorHexOriginal.substring(1);
      }
      else{
        colorHex = colorHexOriginal;
      }
      console.log("Número hexadecimal original:", colorHex);
      colorHexADecimal = parseInt(colorHex, 16);
      colorHexADecimalRestado = colorHexADecimal - 1;
      colorHex = '#' + colorHexADecimalRestado.toString(16).toUpperCase();
      console.log("Resultado hexadecimal después de restar 1:", colorHex);
    break;
    case "Pique": // Color Hex original - 2
      colorHexOriginal = $("#input_add_colorHex").val();
      if(colorHexOriginal[0] === "#"){
        colorHex = colorHexOriginal.substring(1);
      }
      else{
        colorHex = colorHexOriginal;
      }
      console.log("Número hexadecimal original:", colorHex);
      colorHexADecimal = parseInt(colorHex, 16);
      colorHexADecimalRestado = colorHexADecimal - 2;
      colorHex = '#' + colorHexADecimalRestado.toString(16).toUpperCase();
      console.log("Resultado hexadecimal después de restar 2:", colorHex);
    break;
  } */

    $.ajax({
      url :  base_url + 'admin/carga_de_colores/add_color',
      type: 'POST',
          data: {
            colorHex : colorHex,
            colorName: colorName,
            number: number,
            category: category
          },
      success: function(response){
        alert ("Color agregado!");
        $("#input_add_colorHex").val("");
        $("#input_add_colorName").val("");
        $("#input_add_number").val("");
        $("#category_add").val("");
        location.reload();
      },
      error: function(response){
        alert ("Ups. Error en el agregado")
      }
    })
});