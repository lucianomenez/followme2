
var base_url = $("#base_url").val();

$('.btn-agregar').on('click', function(e){
    e.preventDefault();
    $('#addModal').modal({inverted: true}).modal('show');
});

$("#agregar_escudo").on("click", function(e){
    e.preventDefault(); // Evita el comportamiento predeterminado del formulario
    var formData = new FormData(); // Crea un objeto FormData
    formData.append('add_shield_name', $("#add_shield_name").val());
    formData.append('add_shield_file', $("#add_shield_file")[0].files[0]); // Agrega el archivo
    if($("#add_shield_show").prop('checked')){
        formData.append('add_shield_show', 'true');
    } else{
        formData.append('add_shield_show', 'false');
    }

    $.ajax({
        url : base_url + 'admin/carga_de_escudos/add_escudo',
        type: 'POST',
        data: formData,
        processData: false, // Evita el procesamiento automático de datos
        contentType: false, // Evita el encabezado Content-Type
        success: function(response){
            console.log(response)
            alert("Escudo agregado!");
            $("#add_shield_name").val("");
            $("#add_shield_show").val("");
            $("#add_shield_file").val(""); // Limpia el campo de archivo
            location.reload();
        },
        error: function(response){
            alert ("Ups. Error al agregar el escudo");
        }
    });
});

$('.zmdi-edit').on('click', function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");
    $.ajax({
        url :  base_url + 'admin/carga_de_escudos/get_escudo',
        type: 'POST',
        data: {
            id: id
        },
        success: function(data){
            response = JSON.parse(data);
            console.log(response);
            $("#edit_shield_name").val(response.shieldName);
            //$("#edit_shield_file").val(response.shield);
            $("#edit_shield_show").val(response.show);
            $("#edit_shield_id").val(response._id.$id);
        },
        error: function(response){
            console.log(response);
        }
    });

    $('#editModal').modal({inverted: true}).modal('show');
});

$("#editar_escudo").on("click", function(e){
    e.preventDefault(); // Evita el comportamiento predeterminado del formulario
    const formData = new FormData(); // Crea un objeto FormData
    formData.append('edit_shield_id', $("#edit_shield_id").val());
    formData.append('edit_shield_name', $("#edit_shield_name").val());
    formData.append('edit_shield_file', $("#edit_shield_file")[0].files[0]); // Agrega el archivo
    if($("#edit_shield_show").prop('checked')){
        formData.append('edit_shield_show', 'true');
    } else{
        formData.append('edit_shield_show', 'false');
    }

    $.ajax({
        url :  base_url + 'admin/carga_de_escudos/update_escudo',
        type: 'POST',
        data: formData,
        processData: false, // Evita el procesamiento automático de datos
        contentType: false, // Evita el encabezado Content-Type
        success: function(response){
            console.log(response)
            alert ("Cambio Guardado!");
            location.reload();
        },
        error: function(response){
            console.log(response)
            alert ("Ups. Error en el guardado")
        }
    })
});

$('.zmdi-delete').on('click', function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");

    $('#deleteModal').modal({inverted: true}).modal('show');

    $('#borrar_escudo').on('click', function(e){
        $.ajax({
            url :  base_url + 'admin/carga_de_escudos/borrar_escudo',
            type: 'POST',
                data: {
                id : id
            },
            success: function(response){
                alert ("Escudo Borrado con éxito!")
                location.reload();
            },
            error: function(response){
                console.log(response)
                alert ("Ups. Error en el borrado.")
            }
        })
    })
});