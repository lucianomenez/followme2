
var base_url = $("#base_url").val();

$('.btn-agregar').on('click', function(e){
    e.preventDefault();
    $('#addModal').modal({inverted: true}).modal('show');
});

$("#agregar_bandera").on("click", function(e){
    e.preventDefault(); // Evita el comportamiento predeterminado del formulario
    var formData = new FormData(); // Crea un objeto FormData
    formData.append('add_flag_name', $("#add_flag_name").val());
    formData.append('add_flag_file', $("#add_flag_file")[0].files[0]); // Agrega el archivo
    if($("#add_flag_show").prop('checked')){
        formData.append('add_flag_show', 'true');
    } else{
        formData.append('add_flag_show', 'false');
    }

    $.ajax({
        url : base_url + 'admin/carga_de_banderas/add_bandera',
        type: 'POST',
        data: formData,
        processData: false, // Evita el procesamiento automático de datos
        contentType: false, // Evita el encabezado Content-Type
        success: function(response){
            console.log(response)
            alert("Bandera agregada!");
            $("#add_flag_name").val("");
            $("#add_flag_show").val("");
            $("#add_flag_file").val(""); // Limpia el campo de archivo
            location.reload();
        },
        error: function(response){
            alert ("Ups. Error al agregar la bandera");
        }
    });
});

$('.zmdi-edit').on('click', function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");
    $.ajax({
        url :  base_url + 'admin/carga_de_banderas/get_bandera',
        type: 'POST',
        data: {
            id: id
        },
        success: function(data){
            response = JSON.parse(data);
            console.log(response);
            $("#edit_flag_name").val(response.flagName);
            //$("#edit_flag_file").val(response.flag);
            $("#edit_flag_show").val(response.show);
            $("#edit_flag_id").val(response._id.$id);
        },
        error: function(response){
            console.log(response);
        }
    });

    $('#editModal').modal({inverted: true}).modal('show');
});

$("#editar_bandera").on("click", function(e){
    e.preventDefault(); // Evita el comportamiento predeterminado del formulario
    const formData = new FormData(); // Crea un objeto FormData
    formData.append('edit_flag_id', $("#edit_flag_id").val());
    formData.append('edit_flag_name', $("#edit_flag_name").val());
    formData.append('edit_flag_file', $("#edit_flag_file")[0].files[0]); // Agrega el archivo
    if($("#edit_flag_show").prop('checked')){
        formData.append('edit_flag_show', 'true');
    } else{
        formData.append('edit_flag_show', 'false');
    }

    $.ajax({
        url :  base_url + 'admin/carga_de_banderas/update_bandera',
        type: 'POST',
        data: formData,
        processData: false, // Evita el procesamiento automático de datos
        contentType: false, // Evita el encabezado Content-Type
        success: function(response){
            console.log(response)
            alert ("Cambio Guardado!");
            location.reload();
        },
        error: function(response){
            console.log(response)
            alert ("Ups. Error en el guardado")
        }
    })
});

$('.zmdi-delete').on('click', function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");

    $('#deleteModal').modal({inverted: true}).modal('show');

    $('#borrar_bandera').on('click', function(e){
        $.ajax({
            url :  base_url + 'admin/carga_de_banderas/borrar_bandera',
            type: 'POST',
                data: {
                id : id
            },
            success: function(response){
                alert ("Bandera Borrada con éxito!")
                location.reload();
            },
            error: function(response){
                console.log(response)
                alert ("Ups. Error en el borrado.")
            }
        })
    })
});