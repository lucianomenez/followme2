
var base_url = $("#base_url").val();

$('.zmdi-delete').on('click', function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");

    $('#deleteModal').modal({inverted: true}).modal('show');

    $('#borrar_fuente').on('click', function(e){
      $.ajax({
        url :  base_url + 'admin/carga_de_fuentes/borrar_fuente',
        type: 'POST',
            data: {
              id : id
            },
        success: function(response){
          alert ("Fuente Borrada con éxito!")
          location.reload();
        },
        error: function(response){
          console.log(response)
          alert ("Ups. Error en el borrado.")
        }
      })
    })
});

$('.zmdi-edit').on('click', function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");
    $.ajax({
      url :  base_url + 'admin/carga_de_fuentes/get_font',
      type: 'POST',
      data: {
        id: id
      },
      success: function(data){
        response = JSON.parse(data);
        console.log(response);
        $("#edit_font_name").val(response.fontName);
        //$("#edit_font_file").val(response.font);
        $("#edit_font_relsize").val(response.relsize);
        $("#edit_font_id").val(response._id.$id);
      },
      error: function(response){
        console.log(response);
      }
    });

    $('#editModal').modal({inverted: true}).modal('show');
});

$("#editar_fuente").on("click", function(e){
  e.preventDefault(); // Evita el comportamiento predeterminado del formulario
  const formData = new FormData(); // Crea un objeto FormData
  formData.append('edit_font_id', $("#edit_font_id").val());
  formData.append('edit_font_name', $("#edit_font_name").val());
  formData.append('edit_font_file', $("#edit_font_file")[0].files[0]); // Agrega el archivo
  formData.append('edit_font_relsize', $("#edit_font_relsize").val());

  $.ajax({
    url :  base_url + 'admin/carga_de_fuentes/update_font',
    type: 'POST',
    data: formData,
    processData: false, // Evita el procesamiento automático de datos
    contentType: false, // Evita el encabezado Content-Type
    success: function(response){
      console.log(response)
      alert ("Cambio Guardado!");
      location.reload();
    },
    error: function(response){
      console.log(response)
      alert ("Ups. Error en el guardado")
    }
  })
});

$('.btn-agregar').on('click', function(e){
  e.preventDefault();
  $('#addModal').modal({inverted: true}).modal('show');
});

$("#agregar_fuente").on("click", function(e){
  e.preventDefault(); // Evita el comportamiento predeterminado del formulario
  var formData = new FormData(); // Crea un objeto FormData
  formData.append('add_font_name', $("#add_font_name").val());
  formData.append('add_font_file', $("#add_font_file")[0].files[0]); // Agrega el archivo
  formData.append('add_font_relsize', $("#add_font_relsize").val());

  $.ajax({
    url : base_url + 'admin/carga_de_fuentes/add_font',
    type: 'POST',
    data: formData,
    processData: false, // Evita el procesamiento automático de datos
    contentType: false, // Evita el encabezado Content-Type
    success: function(response){
      console.log(response)
        alert("Fuente agregada!");
        $("#add_font_name").val("");
        $("#add_font_relsize").val("");
        $("#add_font_file").val(""); // Limpia el campo de archivo
        location.reload();
    },
    error: function(response){
        alert ("Ups. Error al agregar la fuente");
    }
  });
});