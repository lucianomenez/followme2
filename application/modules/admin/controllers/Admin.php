<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * dna2
 *
 * Description of the class dna2
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Admin extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');
        $this->load->model('Model_colores');
        $this->load->helper('file');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->module('api');
        $this->load->module('elements');
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->titulo = "Follow Me - Diseños Personalizados";
        }



        /* function Index() {
          $data['base_url'] = $this->base_url;
          $data['module_url'] = $this->module_url;
          $data['navbar'] = $this->navbar();
          $data['footer'] = $this->ui_footer();
          $data['header'] = $this->ui_header();
          $data['navbar_mobile'] = $this->navbar_mobile();
          $data['productos'] = $this->Model_productos->get_productos();
          $this->parser->parse("carga_de_productos", $data);
        } */

        function Index() {
            $user = $this->user->get_user($this->idu);
            $data['base_url'] = $this->base_url;
            $data['module_url'] = $this->module_url;
            $data['name'] = $user->name;
            $data['lastname'] = $user->lastname;
            $data['email'] = $user->email;
            $data['head'] = $this->elements->ui_head();
            $data['header'] = $this->elements->ui_header();
            $data['navbar'] = $this->elements->ui_navbar();
            $data['footer'] = $this->elements->ui_footer();
            $data['navbar_mobile'] = $this->elements->ui_navbar_mobile();
            $data['colores'] = $this->Model_colores->get_colores();
            return $this->parser->parse("carga_de_colores", $data);
        }

    function Index_2() {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar;
        $this->parser->parse("index2", $data);
    }

    function Index_3() {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar;
        $this->parser->parse("index3", $data);
    }

    function usuarios(){
      redirect($this->base_url. 'usuarios');
    }

    function charts(){
        $user = $this->user->get_user($this->idu);
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['header'] = $this->ui_header();
        $data['name'] = $user->name;
        $data['lastname'] = $user->lastname;
        $data['email'] = $user->email;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['navbar_mobile'] = $this->navbar_mobile();
  //      $data['js'] =     '<script src='.$this->module_url.'assets/js/main_custom.js"></script>';

        $provincias = $this->data_set_provincias();
        $anos = $this->anos;





        //ORDENO PROVINCIAS COMO EL DATA SET DE LOS CHARTS












        $this->parser->parse("charts", $data);
    }

    function switchs(){
        $user = $this->user->get_user($this->idu);
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['name'] = $user->name;
        $data['lastname'] = $user->lastname;
        $data['email'] = $user->email;
        $data['header'] = $this->ui_header();
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['navbar_mobile'] = $this->navbar_mobile();
        $this->parser->parse("switchs", $data);
    }

    function badges(){
        $user = $this->user->get_user($this->idu);
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['name'] = $user->name;
        $data['lastname'] = $user->lastname;
        $data['email'] = $user->email;
        $data['header'] = $this->ui_header();
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['navbar_mobile'] = $this->navbar_mobile();
        $this->parser->parse("badges", $data);
    }

    function Index_4() {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar;
        $this->parser->parse("index4", $data);
    }

    function Login() {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $this->parser->parse("login", $data);
    }

    function Registrarse() {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $this->parser->parse("register", $data);
    }

    function Recuperar_contrasena() {
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $this->parser->parse("forget-pass", $data);
    }

    function navbar(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        return $this->parser->parse("navbar", $data);
    }

     function ui_header(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['title'] = $this->titulo;
        return $this->parser->parse("header", $data);
    }

     function ui_footer(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['title'] = $this->titulo;
        return $this->parser->parse("footer", $data);
    }

    function navbar_mobile(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        return $this->parser->parse("navbar_mobile", $data);
    }

    function cards(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        $this->parser->parse("cards", $data);
    }

    function fontawesome(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        $this->parser->parse("fontawesome", $data);
    }

    function grids(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        $this->parser->parse("grids", $data);
    }

    function alerts(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        $this->parser->parse("alerts", $data);
    }

    function modals(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        $this->parser->parse("modals", $data);
    }

    function maps(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        $this->parser->parse("maps", $data);
    }

    function carga_de_productos(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        $data['productos'] = $this->Model_productos->get_productos();
        $this->parser->parse("carga_de_productos", $data);
    }

    function inbox(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $this->parser->parse("inbox", $data);
    }

    function tables(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        return $this->parser->parse("tables", $data);
    }

    function typo(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        return $this->parser->parse("typo", $data);
    }

    function buttons(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        return $this->parser->parse("buttons", $data);
    }

    function tabs(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['header'] = $this->ui_header();
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['navbar_mobile'] = $this->navbar_mobile();
        return $this->parser->parse("tabs", $data);
    }

    function progress_bars(){
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['navbar'] = $this->navbar();
        $data['footer'] = $this->ui_footer();
        $data['header'] = $this->ui_header();
        $data['navbar_mobile'] = $this->navbar_mobile();
        return $this->parser->parse("progress-bar", $data);
    }

    public function data_set_provincias(){
        $provincias = $this->app->get_option(39);
        $data['provincias'] = $provincias['data'];
        $text = array_column($data['provincias'], 'text');
        array_multisort($text, SORT_ASC, $data['provincias']);
        return $data['provincias'];
    }

    function get_data($id = "1") {
        $data = $this->input->post();
        if (empty($data)){
            $data['producto_id'] = $id;
        }
        $producto = $this->Model_productos->get_productos($data['producto_id']);
        echo json_encode($producto);

    }

    function becas_x_genero($ano = 2019) {
        $generos = ["male", "female"];
        $data = array();
        $query['ano'] = $ano;
        $query['idwf'] = "beca_creacion";
        foreach ($generos as $gender){
            $query['gender'] = $gender;
            $aux = $this->Model_graficos->count_inscripciones($query);
            array_push($data, $aux);
        };
        output_json($data);
    }

    function becas_x_area() {
        $areas = ["GBA", "INTERIOR"];
        $data = array();
        $query['ano'] = 2019;
        $query['idwf'] = "beca_creacion";
        foreach ($areas as $area){
            $query['area'] = $area;
            $aux = $this->Model_graficos->count_inscripciones($query);
            array_push($data, $aux);
        };
        output_json($data);
    }

    function becas_x_region() {
        $regiones = ["C.A.B.A", "Patagonia", "Cuyo", "Centro", "Noroeste", "Noreste"];
        $data = array();
        $query['ano'] = 2019;
        $query['idwf'] = "beca_creacion";
        foreach ($regiones as $region){
                var_dump($region);
            $query['region'] = $region;
            $aux = $this->Model_graficos->count_inscripciones($query);
            array_push($data, $aux);
        };


        var_dump($data);
        exit;

        output_json($data);
    }

     function view_graficos4(){
            $data = array();
            $data['title'] = "Disciplina Principal - Usuarios Registrados";
            $data['json_url'] = $this->base_url . 'graficos/partidos';
            $data['class'] = "pie_chart";
            $data['id'] = __FUNCTION__;
            $data['class-tour'] = "tour-coordinadores paso-cinco";
            return $this->parser->parse('view_graficos', $data, true, true);
    }


}

/* End of file dna2 */
/* Location: ./system/application/controllers/welcome.php */
