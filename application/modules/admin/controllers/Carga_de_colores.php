<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * dna2
 *
 * Description of the class dna2
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Carga_de_colores extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');
        $this->load->model('Model_colores');
        $this->load->helper('file');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->module('elements');
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->titulo = "Follow Me - Diseños Personalizados";
        }

    function Index() {
        $user = $this->user->get_user($this->idu);
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['name'] = $user->name;
        $data['lastname'] = $user->lastname;
        $data['email'] = $user->email;
        $data['head'] = $this->elements->ui_head();
        $data['header'] = $this->elements->ui_header();
        $data['navbar'] = $this->elements->ui_navbar();
        $data['footer'] = $this->elements->ui_footer();
        $data['navbar_mobile'] = $this->elements->ui_navbar_mobile();
        $data['colores'] = $this->Model_colores->get_colores();
        return $this->parser->parse("carga_de_colores", $data);
    }

    function add_color() {
        $data = $this->input->post();
        if(isset($data['colorHex'])) {
            $data['colorHex'] = strtoupper($data['colorHex']);
        }
        $this->Model_colores->insert_color($data);
        return;
    }

    function get_color() {
        $data = $this->input->post();
        $color = $this->Model_colores->get_colores($data['id']);
        echo json_encode($color[0]);
        //$customData['usercan_create']=true;
    }

    function update_color() {
        $data = $this->input->post();
        if(isset($data['colorHex'])) {
            $data['colorHex'] = strtoupper($data['colorHex']);
        }
        $this->Model_colores->update_color($data);
        return;
        //$customData['usercan_create']=true;
    }

    function borrar_color(){
        $data = $this->input->post();
        $result = $this->Model_colores->delete_color($data['id']); //$data
        echo json_encode($result);
    }
}
