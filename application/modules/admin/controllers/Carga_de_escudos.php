<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Carga_de_escudos extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');
        $this->load->model('Model_escudos');
        $this->load->helper('file');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->module('elements');
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->titulo = "Follow Me - Diseños Personalizados";
    }

    function Index() {
        $user = $this->user->get_user($this->idu);
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['name'] = $user->name;
        $data['lastname'] = $user->lastname;
        $data['email'] = $user->email;
        $data['head'] = $this->elements->ui_head();
        $data['header'] = $this->elements->ui_header();
        $data['navbar'] = $this->elements->ui_navbar();
        $data['footer'] = $this->elements->ui_footer();
        $data['navbar_mobile'] = $this->elements->ui_navbar_mobile();
        $data['escudos'] = $this->Model_escudos->get_escudos();
        return $this->parser->parse("carga_de_escudos", $data);
    }

    function add_escudo(){
        $shieldName = $this->input->post('add_shield_name');
        //$shieldShow = $this->input->post('add_shield_show');
        ///$config['upload_path'] = './uploads/'; // Ruta de almacenamiento
        $config['upload_path'] = './application/modules/followme/assets/img/escudos/';
        //$config['allowed_types'] =['ttf','otf']; // Tipos de archivo permitidos
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);

        if($this->upload->do_upload('add_shield_file')){
            $uploadData = $this->upload->data();
            $file_path = 'assets/img/escudos/' . $uploadData['file_name'];

            $data = array(
                'shieldName' => $shieldName,
                'shield' => $file_path,
                //'show' => $shieldShow,
            );

            $this->Model_escudos->insert_escudo($data);
 
        }
        else{
            $error = $this->upload->display_errors();
            echo "Error de carga de archivo: " . $error;
        }
    }

    function get_escudo(){
        $data = $this->input->post();
        $escudo = $this->Model_escudos->get_escudos($data['id']);
        echo json_encode($escudo[0]);
    }

    function update_escudo(){
        $id = $this->input->post('edit_shield_id');
        $shieldName = $this->input->post('edit_shield_name');
        //$shieldShow = $this->input->post('edit_shield_show');
        $config['upload_path'] = './application/modules/followme/assets/img/escudos/'; // Ruta de almacenamiento
        //$config['allowed_types'] =['ttf','otf']; // Tipos de archivo permitidos
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);

        if($this->upload->do_upload('edit_shield_file')){
            $uploadData = $this->upload->data();
            $file_path = 'assets/img/escudos/' . $uploadData['file_name'];

            $data = array(
                'id' => $id,
                'shieldName' => $shieldName,
                'shield' => $file_path,
                //'show' => $shieldShow,
            );

            $result = $this->Model_escudos->update_escudo($data);
        }
        else{
            $error = $this->upload->display_errors();
            echo "Error de carga de archivo: " . $error;
        }

        return $result;
    }

    function borrar_escudo(){
        $data = $this->input->post();
        $result = $this->Model_escudos->delete_escudo($data['id']);
        echo json_encode($result);
    }

}
