<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Carga_de_banderas extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');
        $this->load->model('Model_banderas');
        $this->load->helper('file');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->module('elements');
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->titulo = "Follow Me - Diseños Personalizados";
        }

    function Index() {
        $user = $this->user->get_user($this->idu);
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['name'] = $user->name;
        $data['lastname'] = $user->lastname;
        $data['email'] = $user->email;
        $data['head'] = $this->elements->ui_head();
        $data['header'] = $this->elements->ui_header();
        $data['navbar'] = $this->elements->ui_navbar();
        $data['footer'] = $this->elements->ui_footer();
        $data['navbar_mobile'] = $this->elements->ui_navbar_mobile();
        $data['banderas'] = $this->Model_banderas->get_banderas(); 
        return $this->parser->parse("carga_de_banderas", $data);
    }

    function add_bandera(){
        $flagName = $this->input->post('add_flag_name');
        //$flagShow = $this->input->post('add_flag_show');
        ///$config['upload_path'] = './uploads/'; // Ruta de almacenamiento
        $config['upload_path'] = './application/modules/followme/assets/img/banderas/';
        //$config['allowed_types'] =['ttf','otf']; // Tipos de archivo permitidos
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);

        if($this->upload->do_upload('add_flag_file')){
            $uploadData = $this->upload->data();
            $file_path = 'assets/img/banderas/' . $uploadData['file_name'];

            $data = array(
                'flagName' => $flagName,
                'flag' => $file_path,
                //'show' => $flagShow,
            );

            $this->Model_banderas->insert_bandera($data);
 
        }
        else{
            $error = $this->upload->display_errors();
            echo "Error de carga de archivo: " . $error;
        }
    }

    function get_bandera(){
        $data = $this->input->post();
        $bandera = $this->Model_banderas->get_banderas($data['id']);
        echo json_encode($bandera[0]);
    }

    function update_bandera(){
        $id = $this->input->post('edit_flag_id');
        $flagName = $this->input->post('edit_flag_name');
        //$flagShow = $this->input->post('edit_flag_show');
        $config['upload_path'] = './application/modules/followme/assets/img/banderas/'; // Ruta de almacenamiento
        //$config['allowed_types'] =['ttf','otf']; // Tipos de archivo permitidos
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);

        if($this->upload->do_upload('edit_flag_file')){
            $uploadData = $this->upload->data();
            $file_path = 'assets/img/banderas/' . $uploadData['file_name'];

            $data = array(
                'id' => $id,
                'flagName' => $flagName,
                'flag' => $file_path,
                //'show' => $flagShow,
            );

            $result = $this->Model_banderas->update_bandera($data);
        }
        else{
            $error = $this->upload->display_errors();
            echo "Error de carga de archivo: " . $error;
        }

        return $result;
    }

    function borrar_bandera(){
        $data = $this->input->post();
        $result = $this->Model_banderas->delete_bandera($data['id']);
        echo json_encode($result);
    }

}
