<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Productos extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('user/user');
        $this->load->model('Model_productos');
        $this->load->helper('file');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->module('elements');
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->titulo = "Follow Me - Diseños Personalizados";
        }



    function Index() {
        $user = $this->user->get_user($this->idu);
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['name'] = $user->name;
        $data['lastname'] = $user->lastname;
        $data['email'] = $user->email;
        $data['head'] = $this->elements->ui_head();
        $data['header'] = $this->elements->ui_header();
        $data['navbar'] = $this->elements->ui_navbar();
        $data['footer'] = $this->elements->ui_footer();
        $data['navbar_mobile'] = $this->elements->ui_navbar_mobile();
        $data['productos'] = $this->Model_productos->get_productos();

        foreach ($data["productos"] as &$producto){
          $usuario = $this->user->get_user($producto['created_by']);
          $producto['created_by'] = $usuario->name." ".$usuario->lastname;
          $producto["png_url"] = substr($producto["png_url"], 43);
        }

        return $this->parser->parse("carga_de_productos", $data);
    }

}
