<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Carga_de_fuentes extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->config('dashboard/config');
        $this->load->library('parser');
        $this->load->model('app');
        $this->load->model('bpm/bpm');
        $this->load->model('msg');
        $this->load->model('Model_fuentes');
        $this->load->helper('file');
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->module('elements');
        $this->user->authorize();
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->idu = $this->user->idu;
        $this->titulo = "Follow Me - Diseños Personalizados";
        }

    function Index() {
        $user = $this->user->get_user($this->idu);
        $data['base_url'] = $this->base_url;
        $data['module_url'] = $this->module_url;
        $data['name'] = $user->name;
        $data['lastname'] = $user->lastname;
        $data['email'] = $user->email;
        $data['head'] = $this->elements->ui_head();
        $data['header'] = $this->elements->ui_header();
        $data['navbar'] = $this->elements->ui_navbar();
        $data['footer'] = $this->elements->ui_footer();
        $data['navbar_mobile'] = $this->elements->ui_navbar_mobile();
        $data['fuentes'] = $this->Model_fuentes->get_fuentes();
        return $this->parser->parse("carga_de_fuentes", $data);
    }

    function add_font(){
        $fontName = $this->input->post('add_font_name');
        $relsize = $this->input->post('add_font_relsize');
        ///$config['upload_path'] = './uploads/'; // Ruta de almacenamiento
        $config['upload_path'] = './application/modules/followme/assets/fonts/';
        //$config['allowed_types'] =['ttf','otf']; // Tipos de archivo permitidos
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);

        if($this->upload->do_upload('add_font_file')){
            $uploadData = $this->upload->data();
            $file_path = '/followme/assets/fonts/' . $uploadData['file_name'];

            $data = array(
                'fontName' => $fontName,
                'font' => $file_path,
                'relsize' => $relsize,
            );

            $this->Model_fuentes->insert_font($data);
 
        }
        else{
            $error = $this->upload->display_errors();
            echo "Error de carga de archivo: " . $error;
        }
    }

    function get_font(){
        $data = $this->input->post();
        $font = $this->Model_fuentes->get_fuentes($data['id']);
        echo json_encode($font[0]);
    }

    function update_font(){
        $id = $this->input->post('edit_font_id');
        $fontName = $this->input->post('edit_font_name');
        $relsize = $this->input->post('edit_font_relsize');
        $config['upload_path'] = './application/modules/followme/assets/fonts/'; // Ruta de almacenamiento
        //$config['allowed_types'] =['ttf','otf']; // Tipos de archivo permitidos
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);

        if($this->upload->do_upload('edit_font_file')){
            $uploadData = $this->upload->data();
            $file_path = '/followme/assets/fonts/' . $uploadData['file_name'];

            $data = array(
                'id' => $id,
                'fontName' => $fontName,
                'font' => $file_path,
                'relsize' => $relsize,
            );

            $result = $this->Model_fuentes->update_font($data);
        }
        else{
            $error = $this->upload->display_errors();
            echo "Error de carga de archivo: " . $error;
        }

        return $result;
    }

    function borrar_fuente(){
        $data = $this->input->post();
        $result = $this->Model_fuentes->delete_font($data['id']);
        echo json_encode($result);
    }

}
