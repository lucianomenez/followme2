<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_productos extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */

    function get_productos($id = null){
        $result = array();
        $container = 'followme.productos';
        if ($id != null){
            $query['id'] = $id;
        }
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function insert_producto($data){
      $container = 'followme.productos';
      $this->db->insert($container, $data);
      return;
    }


}
