<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_banderas extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */

    function get_banderas($id = null){
        $result = array();
        $container = 'followme.flags';
        if ($id != null){
            $query['_id'] = new MongoDB\BSON\ObjectId($id);
        }
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function delete_bandera($id){
        $result = array();
        $container = 'followme.flags';
        $query['_id'] = new MongoDB\BSON\ObjectId($id);
        $this->db->where($query);
        $result = $this->db->delete($container);
        return $result;
    }

    function update_bandera($data){
        $result = array();
        $container = 'followme.flags';
        $query['_id'] = new MongoDB\BSON\ObjectId($data['id']);
        $this->db->where($query);
        unset($data['id']); // Excluye el campo _id de los datos a actualizar
        $result = $this->db->update($container, $data);
        return $result;
    }

    function insert_bandera($data){
        $container = 'followme.flags';
        $this->db->insert($container, $data);
        return;
    }
}