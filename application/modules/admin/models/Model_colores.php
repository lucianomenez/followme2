<?php

/**
 * Funciones para el manejo de datos del container inscripciones
 * @author Luciano Menez <lucianomenez1212@gmail.com>
 * @date 3/05/2016
 *
 */

class Model_colores extends CI_Model {

    public function __construct() {
        // Call the Model constructor
        parent::__construct();
        $this->load->library('cimongo/cimongo', '', 'db');
    }
    /**
     * Lista todas las inscripciones cargadas
     * @return array $result
     */

    function get_colores($id = null){
        $result = array();
        $container = 'followme.colors';
        if ($id != null){
            $query['number'] = $id;
        }
        $this->db->where($query);
        $result = $this->db->get($container)->result_array();
        return $result;
    }

    function delete_color($number){
        $result = array();
        $container = 'followme.colors';
        $query = array('number' => ($number));
        $this->db->where($query);
        $result = $this->db->delete($container);
        return $result;
    }

    function update_color($data){
      $result = array();
      $container = 'followme.colors';
      $query= array('number' => ($data['number']));
      $this->db->where($query);
      $result = $this->db->update($container, $data);
      return $result;
    }

    function insert_color($data){
      $container = 'followme.colors';
      $this->db->insert($container, $data);
      return;
    }
}