{head}
<body class="animsition">
  <input type="hidden" id="base_url" name="base_url" value="{base_url}">

    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        {navbar_mobile}
        <!-- END HEADER MOBILE-->
        <!-- MENU SIDEBAR-->
        {navbar}
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            {header}
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Diseños Creados</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                            <i class="zmdi zmdi-plus"></i>Agregar</button>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>N° Orden</th>
                                                <th>Creado por</th>
                                                <th>Responsable</th>
                                                <th>Colegio/Curso</th>
                                                <th>Ficha</th>
                                                <th>Fecha</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {productos}
                                            <tr class="tr-shadow">
                                                <td>{nro_orden}</td>
                                                <td>
                                                    <span class="block-email">{created_by}</span>
                                                </td>
                                                <td>{colegio} / {curso}</td>
                                                <td>{responsable} / {responsable_email}</td>
                                                <td><a href="{base_url}admin/assets/fichas/{nro_orden}.pdf">Ver Ficha</a></td>
                                                <td>{date}</td>
                                            </tr>
                                            <tr class="spacer"></tr>
                                            {/productos}
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Eliminar Diseño</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>
                  ¿Está seguro que desea eliminar éste diseño?
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="borrar_fuente" class="btn btn-success">Si</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
  				<div class="modal-dialog modal-lg" role="document">
  					<div class="modal-content">
  						<div class="modal-header">
  							<h5 class="modal-title" id="largeModalLabel">Editar</h5>
  							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  								<span aria-hidden="true">&times;</span>
  							</button>
  						</div>
  						<div class="modal-body">
                <div class="card-body card-block">
                  <form action="" method="post" class="form-horizontal">
                  <div class="row form-group">
                    <div class="col col-md-3">
                    <label for="hf-email" class=" form-control-label">Creado por</label>
                    </div>
                    <div class="col-12 col-md-9">
                    <input type="text" id="font_name" name="font_name" class="form-control">
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col col-md-3">
                    <label for="hf-email" class=" form-control-label">Colegio/Curso</label>
                    </div>
                    <div class="col-12 col-md-9">
                    <input type="text" id="font_name" name="font_name" class="form-control">
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col col-md-3">
                    <label for="hf-email" class=" form-control-label">Imagen</label>
                    </div>
                    <div class="col-12 col-md-9">
                    <input type="text" id="font_name" name="font_name" class="form-control">
                    </div>
                  </div>
                  </form>
                </div>

  						</div>
  						<div class="modal-footer">
  							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  							<button type="button" class="btn btn-primary">Confirmar</button>
  						</div>
  					</div>
  				</div>
  			</div>



    </div>
    <!-- Jquery JS-->
    {footer}
    <script src="{module_url}assets/js/carga_de_fuentes.js"></script>

</body>

</html>
<!-- end document-->
