{head}
<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        {navbar_mobile}
        <!-- END HEADER MOBILE-->
        <!-- MENU SIDEBAR-->
        {navbar}
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            {header}
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Colores</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small btn-agregar">
                                            <i class="zmdi zmdi-plus"></i>Agregar</button>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>ID/Número</th>
                                                <th>Muestra</th>
                                                <th>Codigo Hexa</th>
                                                <th>Nombre</th>
                                                <th>Número</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {colores}
                                            <tr class="tr-shadow">
                                                <td>{number}</td>
                                                <td style="background-color: {colorHex};"></td>
                                                <td>
                                                    <span class="block-email">{colorHex}</span>
                                                </td>
                                                <td class="desc">{colorName}</td>
                                                <td class="desc">{category}</td>
                                                <td>
                                                    <div class="table-data-feature">
                                                        <button class="item" data-toggle="tooltip"  data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit" data-id="{number}"></i>
                                                        </button>
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete" data-id="{number}"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="spacer"></tr>
                                            {/colores}
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="largeModalLabel">Eliminar Color</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>
                          ¿Está seguro que desea eliminar éste color?
                        </p>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="button" id="borrar_color" class="btn btn-success">Si</button>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
          				<div class="modal-dialog modal-lg" role="document">
          					<div class="modal-content">
          						<div class="modal-header">
          							<h5 class="modal-title" id="largeModalLabel">Editar</h5>
          							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          								<span aria-hidden="true">&times;</span>
          							</button>
          						</div>
          						<div class="modal-body">
                        <div class="card-body card-block">
                          <form action="" method="post" class="form-horizontal">
                          <div class="row form-group">
                            <div class="col col-md-3">
                            <label for="hf-email" class=" form-control-label">Nombre</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <input type="text" id="input_colorName" name="colorName" class="form-control">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                            <label for="hf-email" class=" form-control-label">Código Hexadecimal</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <input type="text" id="input_colorHex" name="colorHex" class="form-control">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                            <label for="hf-email" class=" form-control-label">Número/ID</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <input type="text" id="input_number" name="number" class="form-control">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                            <label for="hf-email" class=" form-control-label">Categoría</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <select id="category" name="category" class="form-control">
                              <option value="Frisa">Frisa</option>
                              <option value="Jersey">Jersey</option>
                              <option value="Pique">Piqué</option>
                            </select>
                            </div>
                          </div>
                          </form>
                        </div>

          						</div>
          						<div class="modal-footer">
          							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          							<button type="button" id="editar_color" class="btn btn-primary">Confirmar</button>
          						</div>
          					</div>
          				</div>
          			</div>

                <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
          				<div class="modal-dialog modal-lg" role="document">
          					<div class="modal-content">
          						<div class="modal-header">
          							<h5 class="modal-title" id="largeModalLabel">Agregar</h5>
          							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          								<span aria-hidden="true">&times;</span>
          							</button>
          						</div>
          						<div class="modal-body">
                        <div class="card-body card-block">
                          <form action="" method="post" class="form-horizontal">
                          <div class="row form-group">
                            <div class="col col-md-3">
                            <label for="hf-email" class=" form-control-label">Nombre</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <input type="text" id="input_add_colorName" name="colorName" class="form-control">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                            <label for="hf-email" class=" form-control-label">Código Hexadecimal</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <input type="text" id="input_add_colorHex" name="colorHex" class="form-control">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                            <label for="hf-email" class=" form-control-label">Número/ID</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <input type="text" id="input_add_number" name="number" class="form-control">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col col-md-3">
                            <label for="hf-email" class=" form-control-label">Categoría</label>
                            </div>
                            <div class="col-12 col-md-9">
                            <select id="category_add" name="category" class="form-control">
                              <option value="Frisa">Frisa</option>
                              <option value="Jersey">Jersey</option>
                              <option value="Pique">Piqué</option>
                            </select>
                            </div>
                          </div>
                          </form>
                        </div>

          						</div>
          						<div class="modal-footer">
          							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
          							<button type="button" id="agregar_color" class="btn btn-primary">Confirmar</button>
          						</div>
          					</div>
          				</div>
          			</div>

    </div>
    <!-- Jquery JS-->
    {footer}
    <script src="{module_url}assets/js/carga_de_colores.js"></script>

</body>

</html>
<!-- end document-->
