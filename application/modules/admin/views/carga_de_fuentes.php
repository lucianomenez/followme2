{head}
<body class="animsition">
  <input type="hidden" id="base_url" name="base_url" value="{base_url}">

    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        {navbar_mobile}
        <!-- END HEADER MOBILE-->
        <!-- MENU SIDEBAR-->
        {navbar}
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            {header}
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Fuentes</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small btn-agregar">
                                            <i class="zmdi zmdi-plus"></i>Agregar</button>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Archivo</th>
                                                <th>relSize</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {fuentes}
                                            <tr class="tr-shadow">
                                                <td>
                                                    <span class="block-email">{fontName}</span>
                                                </td>
                                                <td class="desc">{font}</td>
                                                <td>{relsize}</td>
                                                <td>
                                                    <div class="table-data-feature">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit" data-id="{_id}"></i>
                                                        </button>
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete" data-id="{_id}"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="spacer"></tr>
                                            {/fuentes}
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
  				<div class="modal-dialog modal-lg" role="document">
  					<div class="modal-content">
  						<div class="modal-header">
  							<h5 class="modal-title" id="largeModalLabel">Agregar</h5>
  							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  								<span aria-hidden="true">&times;</span>
  							</button>
  						</div>
  						<div class="modal-body">
                <div class="card-body card-block">
                  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Nombre</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="text" id="add_font_name" name="add_font_name" class="form-control">
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Archivo</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="file" id="add_font_file" name="add_font_file" class="form-control">
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Relsize</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="text" id="add_font_relsize" name="add_font_relsize" class="form-control">
                      </div>
                    </div>
                  </form>
                </div>

  						</div>
  						<div class="modal-footer">
  							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  							<button type="button" id="agregar_fuente" class="btn btn-primary">Confirmar</button>
  						</div>
  					</div>
  				</div>
  			</div>

        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Eliminar Fuente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>
                  ¿Está seguro que desea eliminar esta fuente?
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="borrar_fuente" class="btn btn-success">Si</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
  				<div class="modal-dialog modal-lg" role="document">
  					<div class="modal-content">
  						<div class="modal-header">
  							<h5 class="modal-title" id="largeModalLabel">Editar</h5>
  							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  								<span aria-hidden="true">&times;</span>
  							</button>
  						</div>
  						<div class="modal-body">
                <div class="card-body card-block">
                  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Nombre</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="text" id="edit_font_name" name="edit_font_name" class="form-control">
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Archivo</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="file" id="edit_font_file" name="edit_font_file" class="form-control">
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Relsize</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="text" id="edit_font_relsize" name="edit_font_relsize" class="form-control">
                      </div>
                    </div>
                    <input type="hidden" id="edit_font_id" name="edit_font_id">
                  </form>
                </div>

  						</div>
  						<div class="modal-footer">
  							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  							<button type="button" id="editar_fuente" class="btn btn-primary">Confirmar</button>
  						</div>
  					</div>
  				</div>
  			</div>



    </div>
    <!-- Jquery JS-->
    {footer}
    <script src="{module_url}assets/js/carga_de_fuentes.js"></script>

</body>

</html>
<!-- end document-->
