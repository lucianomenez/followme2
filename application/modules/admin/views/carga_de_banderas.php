{head}
<body class="animsition">
  <input type="hidden" id="base_url" name="base_url" value="{base_url}">

    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        {navbar_mobile}
        <!-- END HEADER MOBILE-->
        <!-- MENU SIDEBAR-->
        {navbar}
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            {header}
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Banderas</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small btn-agregar">
                                            <i class="zmdi zmdi-plus"></i>Agregar</button>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Archivo</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {banderas}
                                            <tr class="tr-shadow">
                                                <td>
                                                    <span class="block-email">{flagName}</span>
                                                </td>
                                                <!--<td class="desc">{flag}</td>-->
                                                <td>
                                                  <img src="../followme/{flag}" width="50px" alt="">
                                                </td>
                                                <td>
                                                    <div class="table-data-feature">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit" data-id="{_id}"></i>
                                                        </button>
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete" data-id="{_id}"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="spacer"></tr>
                                            {/banderas}
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
  				<div class="modal-dialog modal-lg" role="document">
  					<div class="modal-content">
  						<div class="modal-header">
  							<h5 class="modal-title" id="largeModalLabel">Agregar</h5>
  							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  								<span aria-hidden="true">&times;</span>
  							</button>
  						</div>
  						<div class="modal-body">
                <div class="card-body card-block">
                  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Nombre</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="text" id="add_flag_name" name="add_flag_name" class="form-control">
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Archivo</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="file" id="add_flag_file" name="add_flag_file" class="form-control">
                      </div>
                    </div>
                    <!--<div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Mostrar</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="checkbox" id="add_flag_show" name="add_flag_show" class="form-control" value="true">
                      </div>
                    </div>-->
                  </form>
                </div>

  						</div>
  						<div class="modal-footer">
  							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  							<button type="button" id="agregar_bandera" class="btn btn-primary">Confirmar</button>
  						</div>
  					</div>
  				</div>
  			</div>

        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Eliminar Bandera</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>
                  ¿Está seguro que desea eliminar esta bandera?
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="borrar_bandera" class="btn btn-success">Si</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
  				<div class="modal-dialog modal-lg" role="document">
  					<div class="modal-content">
  						<div class="modal-header">
  							<h5 class="modal-title" id="largeModalLabel">Editar</h5>
  							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  								<span aria-hidden="true">&times;</span>
  							</button>
  						</div>
  						<div class="modal-body">
                <div class="card-body card-block">
                  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Nombre</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="text" id="edit_flag_name" name="edit_flag_name" class="form-control">
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Archivo</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="file" id="edit_flag_file" name="edit_flag_file" class="form-control">
                      </div>
                    </div>
                    <!--<div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Mostrar</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="checkbox" id="edit_flag_show" name="edit_flag_show" class="form-control" value="true">
                      </div>
                    </div>-->
                    <input type="hidden" id="edit_flag_id" name="edit_flag_id">
                  </form>
                </div>

  						</div>
  						<div class="modal-footer">
  							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  							<button type="button" id="editar_bandera" class="btn btn-primary">Confirmar</button>
  						</div>
  					</div>
  				</div>
  			</div>



    </div>
    <!-- Jquery JS-->
    {footer}
    <script src="{module_url}assets/js/carga_de_banderas.js"></script>

</body>

</html>
<!-- end document-->
