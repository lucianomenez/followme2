{head}
<body class="animsition">
  <input type="hidden" id="base_url" name="base_url" value="{base_url}">

    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        {navbar_mobile}
        <!-- END HEADER MOBILE-->
        <!-- MENU SIDEBAR-->
        {navbar}
        <!-- END MENU SIDEBAR-->

        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            {header}
            <!-- END HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">Escudos</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-right">
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small btn-agregar">
                                            <i class="zmdi zmdi-plus"></i>Agregar</button>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Archivo</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {escudos}
                                            <tr class="tr-shadow">
                                                <td>
                                                    <span class="block-email">{shieldName}</span>
                                                </td>
                                                <!--<td class="desc">{shield}</td>-->
                                                <td>
                                                  <img src="../followme/{shield}" width="50px" alt="">
                                                </td>
                                                <td>
                                                    <div class="table-data-feature">
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit" data-id="{_id}"></i>
                                                        </button>
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                            <i class="zmdi zmdi-delete" data-id="{_id}"></i>
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="spacer"></tr>
                                            {/escudos}
                                        </tbody>
                                    </table>
                                </div>
                                <!-- END DATA TABLE -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
  				<div class="modal-dialog modal-lg" role="document">
  					<div class="modal-content">
  						<div class="modal-header">
  							<h5 class="modal-title" id="largeModalLabel">Agregar</h5>
  							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  								<span aria-hidden="true">&times;</span>
  							</button>
  						</div>
  						<div class="modal-body">
                <div class="card-body card-block">
                  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Nombre</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="text" id="add_shield_name" name="add_shield_name" class="form-control">
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Archivo</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="file" id="add_shield_file" name="add_shield_file" class="form-control">
                      </div>
                    </div>
                    <!--<div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Mostrar</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="checkbox" id="add_shield_show" name="add_shield_show" class="form-control" value="true">
                      </div>
                    </div>-->
                  </form>
                </div>

  						</div>
  						<div class="modal-footer">
  							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  							<button type="button" id="agregar_escudo" class="btn btn-primary">Confirmar</button>
  						</div>
  					</div>
  				</div>
  			</div>

        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="largeModalLabel">Eliminar Escudo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>
                  ¿Está seguro que desea eliminar este escudo?
                </p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="borrar_escudo" class="btn btn-success">Si</button>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
  				<div class="modal-dialog modal-lg" role="document">
  					<div class="modal-content">
  						<div class="modal-header">
  							<h5 class="modal-title" id="largeModalLabel">Editar</h5>
  							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  								<span aria-hidden="true">&times;</span>
  							</button>
  						</div>
  						<div class="modal-body">
                <div class="card-body card-block">
                  <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Nombre</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="text" id="edit_shield_name" name="edit_shield_name" class="form-control">
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Archivo</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="file" id="edit_shield_file" name="edit_shield_file" class="form-control">
                      </div>
                    </div>
                    <!--<div class="row form-group">
                      <div class="col col-md-3">
                      <label for="hf-email" class=" form-control-label">Mostrar</label>
                      </div>
                      <div class="col-12 col-md-9">
                      <input type="checkbox" id="edit_shield_show" name="edit_shield_show" class="form-control" value="true">
                      </div>
                    </div>-->
                    <input type="hidden" id="edit_shield_id" name="edit_shield_id">
                  </form>
                </div>

  						</div>
  						<div class="modal-footer">
  							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
  							<button type="button" id="editar_escudo" class="btn btn-primary">Confirmar</button>
  						</div>
  					</div>
  				</div>
  			</div>



    </div>
    <!-- Jquery JS-->
    {footer}
    <script src="{module_url}assets/js/carga_de_escudos.js"></script>

</body>

</html>
<!-- end document-->
