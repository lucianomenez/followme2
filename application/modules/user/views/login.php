<!DOCTYPE HTML>
  <html lang="es_AR">

    <head>

			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="format-detection" content="telephone=no">
      <meta name="description" content="{meta_description}" />
      <meta name="author" content="Follow Me">
      <meta property="og:site_name" content="Followme"/>
      <meta property="og:title" content="{title}"/>
      <meta property="og:description" content="{meta_description}"/>
      <meta property="og:url" content="{base_url}followme/demo"/>
      <meta property="og:image" itemprop="image" content="{featured_image}"/>
      <meta property="og:image:width" content="1200"/>
      <meta property="og:image:height" content="627"/>
      <meta property="og:image:alt" content="{title}"/>
      <meta property="og:type" content="website" />
      <meta name="format-detection" content="telephone=no">
      <meta property="og:locale" content="es_AR" />
      <meta name="twitter:title" content="{title}">
      <meta name="twitter:description" content=”{meta_description}”>
      <meta name="twitter:image" content="{featured_image}">
      <meta name="google" content="notranslate">
      <link rel="apple-touch-icon" sizes="180x180" href="https://followme.com.ar/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="https://followme.com.ar/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="https://followme.com.ar/img/favicon/favicon-16x16.png">

			<title>Follow Me - Diseños Personalizados</title>
      <link href="https://fonts.googleapis.com/css?family=Lato:400,700&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700,800&display=swap" rel="stylesheet">
      <link href="{base_url}website/assets/css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
      <script>
        var base_url = "{base_url}"
      </script>

    <link rel="stylesheet" href="{base_url}followme/assets/css/bootstrap/css/bootstrap.min.css">
     <link rel="stylesheet" href="{base_url}followme/assets/css/estilos.css">
     <link rel="stylesheet" href="{base_url}followme/assets/css/animate.min.css">
	 <link rel="stylesheet" href="{base_url}website/assets/css/main.css">

          <!-- FUENTES GOOGLE FONTS -->
          <link rel="preconnect" href="https://fonts.gstatic.com">
     <link href="https://fonts.googleapis.com/css2?family=Saira:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css2?family=Saira+Extra+Condensed:wght@100;200;300;400;500;600;700;800&family=Saira:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">


      <script src="{base_url}followme/assets/js/jquery.min.js" type="text/javascript"></script>
      <script src="{base_url}followme/assets/js/jquery-ui.min.js" type="text/javascript"></script>
      <script src="{base_url}jscript/bootstrap4/js/bootstrap.bundle.min.js"></script>
      <link rel="stylesheet" type="text/css" href="{base_url}website/assets/css/breakpoints.css">

			<style type="text/css">

			.mainNav{
				background: linear-gradient(
90deg
, #06192E, #172770);
			}

			</style>
      {/ignore}
    </head>

    <body>

    <header>
        <nav class="fixed-top">
          <div class="container contenedor-nav">
            <a href="https://followme.com.ar/">
				<img src="{base_url}followme/assets/img/logo10_anios.png" alt="Logo FollowMe"/>
            </a>
            <!--<ul id="ul-menu">
              <li><a class="item-menu" href="https://followme.com.ar/productos.html">Productos</a></li>
              <li><a class="item-menu" href="https://followme.com.ar/inspirate.html">Inspirate</a></li>
              <li><a class="item-menu" href="https://followme.com.ar/experienciafm.php">Experiencia Follow Me</a></li>
              <li><a class="item-menu" href="https://followme.com.ar/contacto.php">Contacto</a></li>
            </ul>-->
            <span id="icon-burger" class="icon-burger"></span>
          </div>
        </nav>
 </header>




<section class="p-0 computer mainNav fixed-top" style="height:100vh;">

<div class="container">

	<div class="row justify-content-center" style="margin-top: 13.2vh;">
		<div class="col-4 " style="margin-top: 10vh;">
			<div class="title-log text-center mb-4">
				{lang loginMsg}
			</div>
			<div class="form-box" id="login-box">
					<!-- <div class="header bg-navy txt-blanco">{lang loginMsg}</div> -->
					<form id="formAuth" action="{authUrl}" method="post">
							<div class="body bg-gray">
							<!--  MSG -->
						{if {show_warn}}
									<div class="form-group alert alert-warning">
						<strong>{msgcode}</strong>
									</div>
				{/if}
			 <!--  NAME -->
									<div class="form-group txt-blanco sinborde my-3">
											<input type="text" name="username" class="form-control input-log " placeholder="{lang username}"/>
									</div>
							 <!--  PASS -->
									<div class="form-group sinborde my-3">
											<input type="password" name="password" class="form-control input-log" placeholder="{lang password}"   onkeyup="
	var start = this.selectionStart;
	var end = this.selectionEnd;
	this.value = this.value.toLowerCase();
	this.setSelectionRange(start, end);
"/>
									</div>
							 <!--  REMEMBERME -->
									<div class="form-group txt-blanco">
										 <input class="input-log" type="checkbox" value="remember-me" > {lang rememberButton}
									</div>
							</div>
							<div class="footer my-3">
									<button type="submit" class="btn btn-log btn-block mb-2" style="width:100%;"><span  style="font-weight: bold;">{lang loginButton}</span></button>

									<p > <a class="txt-blanco" href="{module_url}recover" >
												{lang forgotPassword}
										</a></p>

							</div>
					</form>
				</div>
		</div>
	</div>

  </div>
</section>

<section class="p-0 mobile mainNav fixed-top" style="height:100vh;">


<div class="container">

	<div class="row justify-content-center" style="margin-top: 8vh;">
		<div class="col " style="margin-top: 15vh;">
			<div class="title-log text-center mb-4">
				{lang loginMsg}
			</div>
			<div class="form-box" id="login-box">
					<!-- <div class="header bg-navy txt-blanco">{lang loginMsg}</div> -->
					<form id="formAuth" action="{authUrl}" method="post">
							<div class="body bg-gray">
							<!--  MSG -->
						{if {show_warn}}
									<div class="form-group alert alert-warning ">
						<strong>{msgcode}</strong>
									</div>
				{/if}
			 <!--  NAME -->
									<div class="form-group txt-blanco sinborde  my-3">
											<input type="text" name="username" class="form-control input-log " placeholder="{lang username}"/>
									</div>
							 <!--  PASS -->
									<div class="form-group sinborde  my-3">
											<input type="password" name="password" class="form-control input-log" placeholder="{lang password}"   onkeyup="
	var start = this.selectionStart;
	var end = this.selectionEnd;
	this.value = this.value.toLowerCase();
	this.setSelectionRange(start, end);
"/>
									</div>
							 <!--  REMEMBERME -->
									<div class="form-group txt-blanco">
										 <input class="input-log" type="checkbox" value="remember-me" > {lang rememberButton}
									</div>
							</div>
							<div class="footer p-0  my-3">
							<button type="submit" class="btn btn-log btn-block mb-2" style="width:100%;"><span  style="font-weight: bold;">{lang loginButton}</span></button>

									<p > <a class="txt-blanco" href="{module_url}recover" >
												{lang forgotPassword}
										</a></p>

							</div>
					</form>
				</div>
		</div>
	</div>

  </div>
  
</section>

  <footer class="fixed-bottom">
	<p>&copy; 2024 FOLLOW ME. Todos los derechos reservados.</p>
  </footer>

<script>
  var base_url = '{base_url}';
</script>

{ignore}

<script src="{base_url}followme/assets/js/wow.min.js"></script>
      <script>
        new WOW().init();
      </script>
<script src="{base_url}followme/assets/js/Main.js"></script>
{/ignore}

</body>
</html>

