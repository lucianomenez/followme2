<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.js"></script>

<link rel="stylesheet" type="text/css" href="{module_url}assets/css/flipbook.style.css">
<link rel="stylesheet" type="text/css" href="{module_url}assets/css/font-awesome.css">

<script src="{module_url}assets/js/flipbook.min.js"></script>

{ignore}
<script type="text/javascript">

    $(document).ready(function () {
        $("#container").flipBook({
            pdfUrl:"catalogo_escudos/assets/pdf/muestrario_escudos-online.pdf",
        });

    })
</script>
<script>
    $(document).on('contextmenu', function() {
        return false;
    });
</script>
{/ignore}

</head>

<body>
<div id="container">
</div>

</body>

</html>
