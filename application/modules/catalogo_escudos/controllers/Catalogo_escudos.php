<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * dna2
 *
 * Description of the class dna2
 *
 * @author Juan Ignacio Borda <juanignacioborda@gmail.com>
 * @date   Mar 23, 2013
 */
class Catalogo_escudos extends MX_Controller {

    function __construct() {
        parent::__construct();
        //---base variables
        $this->base_url = base_url();
        $this->module_url = base_url() . $this->router->fetch_module() . '/';
        $this->load->module('api');
        $this->load->library('parser');
        //---update session ttl
        $this->session->sess_update();
        //----LOAD LANGUAGE
        $this->lang->load('library', $this->config->item('language'));
        $this->titulo = "Follow Me - Diseños Personalizados";
        }



    function Index() {
        $data['module_url'] = $this->module_url;
        echo $this->parser->parse("index.php", $data);
    }



}

/* End of file dna2 */
/* Location: ./system/application/controllers/welcome.php */
